﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Net.Mail;
using System.Configuration;
using daoProject.Model;
using daoProject.Service;

namespace daoProject.Utilities
{
    public class MailHelper
    {
        private SmtpClient smtpClient;
        private string from;
        private AlternateView avTo;
        public void GotImageLogo(AlternateView avFrom)
        {
            avTo = avFrom;
        }
        public MailHelper()
        {
            try
            {
                #region locally email fromgmail
                //string emailOutgoingServer = ConfigurationManager.AppSettings["EmailOutgoingServer"];

                //string smtpDomain = GetSmtpDomain(emailOutgoingServer);
                //string fromAddress = GetFromAddress(emailOutgoingServer);
                //string username = GetUsername(emailOutgoingServer);
                //string password = GetPassword(emailOutgoingServer);
                //int port = GetPort(emailOutgoingServer);
                //bool sslEnabled = GetSslEnabled(emailOutgoingServer);

                //smtpClient = new SmtpClient();
                //smtpClient.Host = smtpDomain;
                //if (!string.IsNullOrEmpty(username))
                //{
                //    smtpClient.Credentials = new System.Net.NetworkCredential(username, password);
                //}
                //if (port != 0)
                //{
                //    smtpClient.Port = port;
                //}
                //smtpClient.EnableSsl = sslEnabled;
                //from = fromAddress;
                #endregion
                #region server mail
                smtpClient = new SmtpClient("localhost");

                List<EmailAddress> emailAddressList = new EmailAddressServiceImpl().GetAll();
                if (emailAddressList.Count > 0)
                {
                    EmailAddress emailAddressObj = emailAddressList[0];
                    if (!string.IsNullOrEmpty(emailAddressObj.MailAddress))
                    {
                        from = emailAddressObj.MailAddress;
                    }
                    else
                    {
                        from = "whrc@ucsfmedctr.org";
                    }
                }
                else
                {
                    from = "whrc@ucsfmedctr.org";
                }
                #endregion
            }
            catch (Exception ex)
            {
                smtpClient = null;
            }
        }

        private string GetSmtpDomain(string emailOutgoingServer)
        {
            string smtpDomain = "";

            try
            {
                Match matchSmtpDomain = Regex.Match(emailOutgoingServer, "SmtpDomain=((?!;).)*;", RegexOptions.IgnoreCase);
                smtpDomain = matchSmtpDomain.Value.Split('=', ';')[1].Trim();
            }
            catch (Exception ex)
            {
                smtpDomain = "";
            }

            return smtpDomain;
        }

        private string GetFromAddress(string emailOutgoingServer)
        {
            string fromAddress = "";

            try
            {
                Match matchFromAddress = Regex.Match(emailOutgoingServer, "FromAddress=((?!;).)*;", RegexOptions.IgnoreCase);
                fromAddress = matchFromAddress.Value.Split('=', ';')[1].Trim();
            }
            catch (Exception ex)
            {
                fromAddress = "";
            }

            return fromAddress;
        }

        private string GetUsername(string emailOutgoingServer)
        {
            string username = "";

            try
            {
                Match matchUsername = Regex.Match(emailOutgoingServer, "Username=((?!;).)*;", RegexOptions.IgnoreCase);
                username = matchUsername.Value.Split('=', ';')[1].Trim();
            }
            catch (Exception ex)
            {
                username = "";
            }

            return username;
        }

        private string GetPassword(string emailOutgoingServer)
        {
            string password = "";

            try
            {
                Match matchPassword = Regex.Match(emailOutgoingServer, "Password=((?!;).)*;", RegexOptions.IgnoreCase);
                password = matchPassword.Value.Split('=', ';')[1].Trim();
            }
            catch (Exception ex)
            {
                password = "";
            }

            return password;
        }

        private int GetPort(string emailOutgoingServer)
        {
            int port = 0;

            try
            {
                Match matchPort = Regex.Match(emailOutgoingServer, "Port=((?!;).)*;", RegexOptions.IgnoreCase);
                string portString = matchPort.Value.Split('=', ';')[1].Trim();

                if (!string.IsNullOrEmpty(portString))
                {
                    port = Convert.ToInt32(portString);
                }
            }
            catch (Exception ex)
            {
                port = 0;
            }

            return port;
        }

        private bool GetSslEnabled(string emailOutgoingServer)
        {
            bool sslEnabled = false;

            try
            {
                Match matchSslEnabled = Regex.Match(emailOutgoingServer, "SslEnabled=((?!;).)*;", RegexOptions.IgnoreCase);
                string sslEnabledString = matchSslEnabled.Value.Split('=', ';')[1].Trim();

                if (!string.IsNullOrEmpty(sslEnabledString))
                {
                    sslEnabled = sslEnabledString.ToLower().Equals("true");
                }
            }
            catch (Exception ex)
            {
                sslEnabled = false;
            }

            return sslEnabled;
        }

        public bool Send(string to, string subject, string body)
        {
            bool valid = false;

            try
            {
                MailMessage mailMessage = new MailMessage();

                mailMessage.From = new MailAddress(from);
                mailMessage.To.Add(to);
                mailMessage.Subject = subject;
                mailMessage.Body = body;
                mailMessage.AlternateViews.Add(avTo);
                mailMessage.IsBodyHtml = true;
                smtpClient.Send(mailMessage);
                valid = true;
            }
            catch (Exception ex)
            {
                valid = false;
            }

            return valid;
        }

        public bool Send(string from, string to, string subject, string body)
        {
            bool valid = false;

            try
            {
                MailMessage mailMessage = new MailMessage();

                mailMessage.From = new MailAddress(from);
                mailMessage.To.Add(to);
                mailMessage.Subject = subject;
                mailMessage.Body = body;
               mailMessage.AlternateViews.Add(avTo);
                mailMessage.IsBodyHtml = true;
                smtpClient.Send(mailMessage);
                valid = true;
            }
            catch (Exception ex)
            {
                valid = false;
            }

            return valid;
        }

        public string GetFromAddress()
        {
            return from;
        }
    }
}
