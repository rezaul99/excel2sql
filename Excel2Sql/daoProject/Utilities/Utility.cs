﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Security.Cryptography;
using System.Collections;
using System.IO;
using System.Runtime.InteropServices;

namespace daoProject.Utilities
{
    public class Utility
    {
        //  Call this function to remove the key from memory after use for security
        [System.Runtime.InteropServices.DllImport("KERNEL32.DLL", EntryPoint = "RtlZeroMemory")]
        public static extern bool ZeroMemory(IntPtr Destination, int Length);
        /// <summary>
        /// This class Convert the username plane text to encrypted format
        /// The encryption format is Hashing ASCII format
        /// </summary>
        /// <param name="thisPassword"></param>
        /// <returns></returns>
        public byte[] Hash(string thisPassword)
        {

            // This is a built in class MD5 of Cryptography namespace
            MD5CryptoServiceProvider md5 = new MD5CryptoServiceProvider();

            //byte array
            byte[] tmpSource;
            byte[] tmpHash;

            //Convert the ASCII byte format    
            tmpSource = ASCIIEncoding.ASCII.GetBytes(thisPassword); // Turn password into byte array

            //Convert the byte into MD5 hashing byte 
            tmpHash = md5.ComputeHash(tmpSource);   // return into hash byte

            // return the plan text to Hash format
            return tmpHash;
        }

        /// <summary>
        /// Convert byte to Base64
        /// </summary>
        /// <param name="encodebyte"> byte array</param>
        /// <returns> Base64 string</returns>
        public string Base64Encode(byte[] encodebyte)
        {
            // Convert byte array to Base64 string
            string Base64string = Convert.ToBase64String(encodebyte);// return into Base64 string

            //retern Base64 string
            return Base64string;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="encodedData"> String format</param>
        /// <returns> Base64 byte array</returns>
        public byte[] Base64Decode(string encodedData)
        {
            // Convert Base64 string to byte array
            byte[] encodedDataAsBytes
            = System.Convert.FromBase64String(encodedData);// return byte array

            // return base64 byte array
            return encodedDataAsBytes;

            //   string returnValue =
            //   Convert.ToString(encodedDataAsBytes);
            //   System.Text.ASCIIEncoding.ASCII.GetString(encodedDataAsBytes);
        }

        /// <summary>
        /// Function to Generate a 64 bits Key.
        /// </summary>
        /// <returns></returns>
        static string GenerateKey()
        {
            // Create an instance of Symetric Algorithm. Key and IV is generated automatically.
            DESCryptoServiceProvider desCrypto = (DESCryptoServiceProvider)DESCryptoServiceProvider.Create();

            // Use the Automatically generated key for Encryption. 
            return ASCIIEncoding.ASCII.GetString(desCrypto.Key);
        }

        /// <summary>
        /// Encrypt the information
        /// </summary>
        /// <param name="thisPassword">Clear Information </param>
        /// <returns>Encrypt string Array</returns>
        public string[] SecurityInformaionEncrypt(string information)
        {
            string[] returnvalue = new string[2];
            // Must be 64 bits, 8 bytes.
            // Distribute this key to the user who will decrypt this file.
            // Get the Key for the file to Encrypt.
            string sKey = GenerateKey();

            //// For additional security Pin the key.
            GCHandle gch = GCHandle.Alloc(sKey, GCHandleType.Pinned);

            //Convert the string into ASCII byte.
            byte[] clearData = ASCIIEncoding.ASCII.GetBytes(information);

            //Store in the memory
            MemoryStream ms = new MemoryStream();

            // Set DES algorithm
            DESCryptoServiceProvider alg = new DESCryptoServiceProvider();

            //A 64 bit key and IV is required for this provider.
            //Set secret key For DES algorithm.
            alg.Key = ASCIIEncoding.ASCII.GetBytes(sKey);

            //Set initialization vector.
            alg.IV = ASCIIEncoding.ASCII.GetBytes(sKey);

            //Create crypto stream set to read and do a 
            //DES decryption transform on incoming bytes.
            CryptoStream cs = new CryptoStream(ms,
                       alg.CreateEncryptor(), CryptoStreamMode.Write);

            //Print the contents of the decrypted file.
            cs.Write(clearData, 0, clearData.Length);

            //Close The file
            cs.Close();

            //Set data into Byte Array
            byte[] encryptedData = ms.ToArray();

            //Convert into Base64 string
            string str = Convert.ToBase64String(encryptedData);

            returnvalue[0] = str.ToString();
            returnvalue[1] = sKey.ToString();
            // Remove the Key from memory. 
            //ZeroMemory(gch.AddrOfPinnedObject(), sKey.Length * 2);


            //return Encrypt Information
            return returnvalue;

        }

        /// <summary>
        /// Decript the secure information
        /// </summary>
        /// <param name="information"> Encrypt Information</param>
        /// <param name="skey">Security Key</param>
        /// <returns>Clear Text</returns>
        public string SecurityInformaionDecrypt(string information, string sKey)
        {

            //// For additional security Pin the key.
            GCHandle gch = GCHandle.Alloc(sKey, GCHandleType.Pinned);

            //Convert string to Base64string
            byte[] cipherData = System.Convert.FromBase64String(information);

            //Store in the memory
            MemoryStream ms = new MemoryStream();

            //Set DES algorith
            DESCryptoServiceProvider alg = new DESCryptoServiceProvider();

            //A 64 bit key and IV is required for this provider.
            //Set secret key For DES algorithm.
            alg.Key = ASCIIEncoding.ASCII.GetBytes(sKey);

            //Set initialization vector.
            alg.IV = ASCIIEncoding.ASCII.GetBytes(sKey);

            //Create a DES decryptor from the DES instance.
            ICryptoTransform desdecrypt = alg.CreateDecryptor();

            //Create crypto stream set to read and do a 
            //DES decryption transform on incoming bytes.
            CryptoStream cs = new CryptoStream(ms, desdecrypt,CryptoStreamMode.Write);

            //Print the contents of the decrypted file.
            cs.Write(cipherData, 0, cipherData.Length);

            // Close the file read
            cs.Close();
            byte[] decryptedData = ms.ToArray();

            //string str = Convert.ToString(decryptedData);
            string str = System.Text.ASCIIEncoding.ASCII.GetString(decryptedData);

            //// Remove the Key from memory. 
            //ZeroMemory(gch.AddrOfPinnedObject(), sKey.Length * 2);

            //Retunr the actual string
            return str;
        }

        public string CreateRandomCode(int codeCount)
        {
            string allChar = "0,1,2,3,4,5,6,7,8,9,A,B,C,D,E,F,G,H,I,J,K,L,M,N,O,P,Q,R,S,T,U,V,W,X,Y,Z";
            string[] allCharArray = allChar.Split(',');
            string randomCode = "";
            int temp = -1;

            Random rand = new Random();
            for (int i = 0; i < codeCount; i++)
            {
                if (temp != -1)
                {
                    rand = new Random(i * temp * ((int)DateTime.Now.Ticks));
                }
                int t = rand.Next(36);
                if (temp != -1 && temp == t)
                {
                    return CreateRandomCode(codeCount);
                }
                temp = t;
                randomCode += allCharArray[t];
            }
            return randomCode;
        }
    }
}

