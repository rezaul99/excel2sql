﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using daoProject.Model;
using daoProject.Utilities;
using daoProject.Mapper;
using System.Data;
using System.Xml;
using daoProject.Infrastructure;

namespace daoProject.Service
{
    public class MembershipDescriptionServiceImpl
    {
        public List<MembershipDescriptionWHRC> GetAllMembershipDescription(XmlNode xmlNode)
        {
            List<MembershipDescriptionWHRC> membershipDescriptionWHRCList = new List<MembershipDescriptionWHRC>();
            MembershipDescriptionWHRC membershipDescriptionWHRC = null;

            if (xmlNode != null)
            {
                XmlNodeList datas = xmlNode.ChildNodes;
                for (int i = 0; i < datas.Count; i++)
                {
                    List<XmlNode> nodeList = new List<XmlNode>(datas[i].ChildNodes.Cast<XmlNode>());
                    XmlNode recordIDNode = (from item in nodeList
                                            where item.Name.Replace(" ", "") == "RecordId"
                                            select item).FirstOrDefault();
                    XmlNode membershipDescriptionNode = (from item in nodeList
                                                        where item.Name.Replace(" ", "") == "MembershipDetails"
                                                         select item).FirstOrDefault();


                    membershipDescriptionWHRC = new MembershipDescriptionWHRC();
                    membershipDescriptionWHRC.RecordID = Convert.ToInt64(recordIDNode.InnerText);
                    if (membershipDescriptionNode.InnerText != "")
                    {
                        membershipDescriptionWHRC.MembershipDescription = membershipDescriptionNode.InnerText;
                    }

                    membershipDescriptionWHRCList.Add(membershipDescriptionWHRC);
                }
            }
            return membershipDescriptionWHRCList;
        }

        public XmlDocument InsertManageMembershipDescription(MembershipDescriptionWHRC membershipDescriptionWHRC)
        {
            DataTable dt = new DataTable();
            DataColumn dc1 = new DataColumn("Name");
            DataColumn dc2 = new DataColumn("Value");

            dt.Columns.Add(dc1);
            dt.Columns.Add(dc2);

            AddDataRow("MembershipDetails", Convert.ToString(membershipDescriptionWHRC.MembershipDescription), dt);

            XmlDocument xmlDoc = new XmlDocument();
            DataSet dataSet = new DataSet();
            dataSet.Tables.Add(dt);
            string strXml = dataSet.GetXml();
            xmlDoc.LoadXml(strXml);

            return xmlDoc;
        }

        private void AddDataRow(string name, string value, DataTable dt)
        {
            DataRow dr = dt.NewRow();
            dr["Name"] = name;
            dr["Value"] = value;
            dt.Rows.Add(dr);
        }
    }
}
