﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using daoProject.Model;
using daoProject.Utilities;
using daoProject.Mapper;

namespace daoProject.Service
{
    public class ClientDepositServiceImpl
    {
        private PackageDataMapper dataMapper;

        public ClientDepositServiceImpl()
        {
            dataMapper = new PackageDataMapper();
        }

        public void Add(ClientDeposit item)
        {
            dataMapper.Create(item);
        }

        public void Update(ClientDeposit item)
        {
            dataMapper.Update(item);
        }

        public bool Delete(long id)
        {
            return dataMapper.Delete(id);
        }

        public List<ClientDeposit> GetAll()
        {
            return dataMapper.GetAll();
        }

        public ClientDeposit GetById(long id)
        {
            return dataMapper.GetById(id);
        }
    }
}
