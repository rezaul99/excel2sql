﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using daoProject.Mapper;
using daoProject.Model;

namespace daoProject.Service
{
   public  class PurchaseDisclaimerServiceImpl
    {
        private PurchaseDisclaimerDataMapper dataMapper;

        public PurchaseDisclaimerServiceImpl()
        {
            dataMapper = new PurchaseDisclaimerDataMapper();
        }
        public List<PurchaseDisclaimer> GetAll()
        {
            return dataMapper.GetAll();
        }
        public PurchaseDisclaimer GetAllById(long Id)
        {
            return dataMapper.GetAllById(Id);
        }
        public void Add(PurchaseDisclaimer obj)
        {
            dataMapper.Add(obj);
        }
        public void Update(PurchaseDisclaimer obj)
        {
            dataMapper.Update(obj);
        }
    }
}
