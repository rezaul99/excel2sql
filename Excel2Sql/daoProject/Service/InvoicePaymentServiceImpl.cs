﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using daoProject.Model;
using daoProject.Utilities;
using daoProject.Mapper;
using System.Data;

namespace daoProject.Service
{
    public class InvoicePaymentServiceImpl
    {
        private InvoicePaymentDataMapper dataMapper;

        public InvoicePaymentServiceImpl()
        {
            dataMapper = new InvoicePaymentDataMapper();
        }

        public void Add(InvoicePaymentDetail item)
        {
            dataMapper.Create(item);
        }

        public void Add(InvoicePaymentDetail item, System.Data.Odbc.OdbcConnection conn, System.Data.Odbc.OdbcTransaction tx)
        {
            dataMapper.Create(item, conn, tx);
        }

        public void Update(InvoicePaymentDetail item)
        {
            dataMapper.Update(item);
        }

        public void UpdateAmount(InvoicePaymentDetail invoicePaymentDetail)
        {
            dataMapper.UpdateAmount(invoicePaymentDetail);
        }

        public void updateTotalAmount(decimal totalPriceAmount,long invoiceId)
        {
            dataMapper.updateTotalAmount(totalPriceAmount, invoiceId);
        }

        public void UpdatePaymentStatus(int statusNumber, long invoiceId)
        {
            dataMapper.UpdatePaymentStatus(statusNumber, invoiceId);
        }

        public void UpdateTotalAmountForMediCalMethod(decimal updateAmountInvoicePayment,long invoicePaymentId)
        {
            dataMapper.UpdateTotalAmountForMediCalMethod(updateAmountInvoicePayment,invoicePaymentId);
        }

        public bool Delete(long id)
        {
            return dataMapper.Delete(id);
        }

        public List<InvoicePaymentDetail> GetAll()
        {
            return dataMapper.GetAll();
        }


        public InvoicePaymentDetail GetById(long id)
        {
            return dataMapper.GetById(id);
        }

        public List<InvoicePaymentDetail> GetListByInvoiceId(long invoiceId)
        {
            return dataMapper.GetListByInvoiceId(invoiceId);
        }

        public bool DeleteByInvoiceId(long invoiceId)
        {
            return dataMapper.DeleteByInvoiceId(invoiceId);
        }

        public decimal GetTotalPaymentAmountByInvoiceID(long invoiceID)
        {
            return dataMapper.GetTotalPaymentAmountByInvoiceID(invoiceID);
        }
        public decimal GetTotalPaymentAmountOnlyByInvoiceID(long invoiceID)
        {
            return dataMapper.GetTotalPaymentAmountOnlyByInvoiceID(invoiceID);
        }
        public bool IsPatientReceiptExist(string patientReceiptNumber)
        {
            return dataMapper.IsPatientReceiptExist(patientReceiptNumber);
        }
        public void UpdateCheckNumber(long invoiceID, string checkNumber)
        {
            dataMapper.UpdateCheckNumber(invoiceID, checkNumber);
        }
  
    }
}
