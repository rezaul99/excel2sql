﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using daoProject.Model;
using daoProject.Utilities;
using daoProject.Mapper;
using System.Data;
using System.Xml;
using daoProject.Infrastructure;
namespace daoProject.Service
{
    public class CreditCardPaymentSetupServiceImpl
    {
        public List<CreditCardPaymentSetup> GetAllCreditCardPaymentSetup()
        {
            CreaditCardPaymentSetupDataMapper creaditCardPaymentSetupDataMapper = new CreaditCardPaymentSetupDataMapper();
            List<CreditCardPaymentSetup> creditCardPaymentSetupList = creaditCardPaymentSetupDataMapper.GetAll();
            return creditCardPaymentSetupList;
        }

        public void InsertCreditCardPaymentSetup(CreditCardPaymentSetup creditCardPaymentSetup)
        {
            CreaditCardPaymentSetupDataMapper creaditCardPaymentSetupDataMapper = new CreaditCardPaymentSetupDataMapper();
            creaditCardPaymentSetupDataMapper.Create(creditCardPaymentSetup);
        }

        public void UpdateCreditCardPaymentSetup(CreditCardPaymentSetup creditCardPaymentSetup)
        {
            new CreaditCardPaymentSetupDataMapper().Update(creditCardPaymentSetup);
        }
        public void UpdateActiveStatusforOthers(CreditCardPaymentSetup creditCardPaymentSetup)
        {
            new CreaditCardPaymentSetupDataMapper().UpdateActiveStatusforOthers(creditCardPaymentSetup);
        }

        public CreditCardPaymentSetup GetById(long Id)
        {
            return new CreaditCardPaymentSetupDataMapper().GetById(Id);
        }

      
        public void DeleteById(long id)
        {
            new CreaditCardPaymentSetupDataMapper().Delete(id);
        }
    }
}
