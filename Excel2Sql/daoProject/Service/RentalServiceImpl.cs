﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using daoProject.Model;
using daoProject.Utilities;
using daoProject.Mapper;
using System.Data;
using System.Xml;
using daoProject.Infrastructure;


namespace daoProject.Service
{
    public class RentalServiceImpl
    {
        #region Rental
        public List<RentalEquipmentWHRC> GetAllRentalEquipment(XmlNode xmlNode)
        {
            List<RentalEquipmentWHRC> rentalEquipmentList = new List<RentalEquipmentWHRC>();

            RentalEquipmentWHRC rentalEquipmentWHRC = null;

            if (xmlNode != null)
            {
                XmlNodeList datas = xmlNode.ChildNodes;
                for (int i = 0; i < datas.Count; i++)
                {
                    List<XmlNode> nodeList = new List<XmlNode>(datas[i].ChildNodes.Cast<XmlNode>());
                    XmlNode recordIDNode = (from item in nodeList
                                            where item.Name.Replace(" ", "") == "RecordId"
                                            select item).FirstOrDefault();
                    XmlNode serialNode = (from item in nodeList
                                         where item.Name.Replace(" ", "") == "Serial"
                                         select item).FirstOrDefault();

                    XmlNode vendorNameNode = (from item in nodeList
                                        where item.Name.Replace(" ", "") == "VendorName"
                                        select item).FirstOrDefault();

                    XmlNode modelNameNode = (from item in nodeList
                                       where item.Name.Replace(" ", "") == "ModelName"
                                       select item).FirstOrDefault();

                    XmlNode statusNode = (from item in nodeList
                                                     where item.Name.Replace(" ", "") == "Status"
                                       select item).FirstOrDefault();

                    XmlNode depositAmountNode = (from item in nodeList
                                                     where item.Name.Replace(" ", "") == "DepositAmount"
                                                     select item).FirstOrDefault();
                    XmlNode DatereceivedfromvendorNode = (from item in nodeList
                                                 where item.Name.Replace(" ", "") == "DateReceivedfromVendor"
                                                 select item).FirstOrDefault();
                    XmlNode DatereturnedtovendorNode = (from item in nodeList
                                                 where item.Name.Replace(" ", "") == "DateReturnedtoVendor"
                                                 select item).FirstOrDefault();
                    XmlNode notesNode = (from item in nodeList
                                                 where item.Name.Replace(" ", "") == "Notes"
                                                 select item).FirstOrDefault();

                    rentalEquipmentWHRC = new RentalEquipmentWHRC();
                    rentalEquipmentWHRC.RecordID = Convert.ToInt64(recordIDNode.InnerText);
                    rentalEquipmentWHRC.Serial = serialNode.InnerText;
                    rentalEquipmentWHRC.VendorName = vendorNameNode.InnerText;
                    rentalEquipmentWHRC.ModelName = modelNameNode.InnerText;
                    rentalEquipmentWHRC.Status = statusNode.InnerText;
                    if (depositAmountNode.InnerText != "")
                    {
                        rentalEquipmentWHRC.DepositAmount =Convert.ToDecimal(depositAmountNode.InnerText);
                    }
                    if (DatereceivedfromvendorNode != null)
                    {
                        if (DatereceivedfromvendorNode.InnerText != "")
                        {
                        rentalEquipmentWHRC.DateReceivedFromVendor = Convert.ToDateTime(DatereceivedfromvendorNode.InnerText);
                        }
                    }
                    if (DatereturnedtovendorNode != null)
                    {
                        if (DatereturnedtovendorNode.InnerText != "")
                        {
                            rentalEquipmentWHRC.DateReturnedToVendr = Convert.ToDateTime(DatereturnedtovendorNode.InnerText);
                        }
                    }
                    if (notesNode != null)
                    {
                        if (notesNode.InnerText != "")
                        {
                            rentalEquipmentWHRC.Note = notesNode.InnerText;
                        }
                    }

                    rentalEquipmentList.Add(rentalEquipmentWHRC);
                }
            }

            return rentalEquipmentList;
        }

        public XmlDocument InsertRentalEquipment(RentalEquipmentWHRC rentalEquipmentWHRC)
        {
            DataTable dt = new DataTable();
            DataColumn dc1 = new DataColumn("Name");
            DataColumn dc2 = new DataColumn("Value");

            dt.Columns.Add(dc1);
            dt.Columns.Add(dc2);

            AddDataRow("Serial", rentalEquipmentWHRC.Serial, dt);
            AddDataRow("VendorName", rentalEquipmentWHRC.VendorName, dt);
            AddDataRow("ModelName", rentalEquipmentWHRC.ModelName, dt);
            AddDataRow("Status", rentalEquipmentWHRC.Status, dt);
            AddDataRow("DepositAmount", rentalEquipmentWHRC.DepositAmount.ToString(), dt);
            AddDataRow("DateReceivedfromVendor", rentalEquipmentWHRC.DateReceivedFromVendor.ToString("MM/dd/yyy"), dt);
            AddDataRow("DateReturnedtoVendor", rentalEquipmentWHRC.DateReturnedToVendr.ToString("MM/dd/yyy"), dt);
            AddDataRow("Notes", rentalEquipmentWHRC.Note.ToString(), dt);

            XmlDocument xmlDoc = new XmlDocument();
            DataSet dataSet = new DataSet();
            dataSet.Tables.Add(dt);
            string strXml = dataSet.GetXml();
            xmlDoc.LoadXml(strXml);

            return xmlDoc;
        }

        public XmlDocument UpdateClass(ClassWHRC classWHRC)
        {
            XmlDocument xmlDocument = null;

            return xmlDocument;
        }

        public void DeleteClass(long recordID)
        {

        }
        #endregion

        #region Rental Period
        public List<RentalPeriodWHRC> GetAllRentalPeriod(XmlNode xmlNode)
        {
            List<RentalPeriodWHRC> rentalPeriodList = new List<RentalPeriodWHRC>();
            RentalPeriodWHRC rentalPeriodWHRC = null;

            if (xmlNode != null)
            {
                XmlNodeList datas = xmlNode.ChildNodes;
                long availableCount = 0;
                for (int i = 0; i < datas.Count; i++)
                {
                    List<XmlNode> nodeList = new List<XmlNode>(datas[i].ChildNodes.Cast<XmlNode>());
                    XmlNode recordIDNode = (from item in nodeList
                                            where item.Name.Replace(" ", "") == "RecordId"
                                            select item).FirstOrDefault();
                    XmlNode lengthofRentalinDaysNode = (from item in nodeList
                                               where item.Name.Replace(" ", "") == "LengthofRentalinDays"
                                               select item).FirstOrDefault();

                    XmlNode rentalFeesNode = (from item in nodeList
                                             where item.Name.Replace(" ", "") == "RentalFees"
                                             select item).FirstOrDefault();

                    XmlNode modelNameNode = (from item in nodeList
                                                   where item.Name.Replace(" ", "") == "ModelName"
                                                   select item).FirstOrDefault();


                    rentalPeriodWHRC = new RentalPeriodWHRC();
                    rentalPeriodWHRC.RecordID = Convert.ToInt64(recordIDNode.InnerText);
                    rentalPeriodWHRC.LengthofRentalinDays =Convert.ToInt32(lengthofRentalinDaysNode.InnerText);
                    rentalPeriodWHRC.Fee =Convert.ToDecimal(rentalFeesNode.InnerText);
                    rentalPeriodWHRC.ModelName = modelNameNode.InnerText;
                    rentalPeriodWHRC.TotalDescriptionForDDL = rentalPeriodWHRC.LengthofRentalinDays.ToString() + " -$" + rentalPeriodWHRC.Fee.ToString("0.00");
                    

                    rentalPeriodList.Add(rentalPeriodWHRC);
                }
            }

            return rentalPeriodList;
        }

        public XmlDocument InsertRentalPeriod(RentalPeriodWHRC rentalPeriodWHRC)
        {
            DataTable dt = new DataTable();
            DataColumn dc1 = new DataColumn("Name");
            DataColumn dc2 = new DataColumn("Value");

            dt.Columns.Add(dc1);
            dt.Columns.Add(dc2);

            AddDataRow("LengthofRentalinDays", rentalPeriodWHRC.LengthofRentalinDays.ToString(), dt);
            AddDataRow("RentalFees", rentalPeriodWHRC.Fee.ToString(), dt);
            AddDataRow("ModelName", rentalPeriodWHRC.ModelName, dt);
          

            XmlDocument xmlDoc = new XmlDocument();
            DataSet dataSet = new DataSet();
            dataSet.Tables.Add(dt);
            string strXml = dataSet.GetXml();
            xmlDoc.LoadXml(strXml);

            return xmlDoc;
        }

        public XmlDocument UpdateSection(Section section)
        {
            XmlDocument xmlDocument = null;

            return xmlDocument;
        }

        public void DeleteSection(long recordID)
        {

        }
        #endregion

        private void AddDataRow(string name, string value, DataTable dt)
        {
            DataRow dr = dt.NewRow();
            dr["Name"] = name;
            dr["Value"] = value;
            dt.Rows.Add(dr);
        }

    }
}