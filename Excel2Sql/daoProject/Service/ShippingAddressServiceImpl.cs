﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using daoProject.Model;
using daoProject.Utilities;
using daoProject.Mapper;

namespace daoProject.Service
{
    public class ShippingAddressServiceImpl
    {
        private ShippingAddressDataMapper dataMapper;

        public ShippingAddressServiceImpl()
        {
            dataMapper = new ShippingAddressDataMapper();
        }

        public void Add(ShippingAddress item)
        {
            dataMapper.Create(item);
        }

        public void Update(ShippingAddress item)
        {
            dataMapper.Update(item);
        }
        public void UpdateDefaultShippingAddressByClientId(ShippingAddress item)
        {
            dataMapper.UpdateDefaultShippingAddressByClientId(item);
        }
        public void UpdateNotDefaultShippingAddressRestAllByClientId(ShippingAddress item)
        {
            dataMapper.UpdateNotDefaultShippingAddressRestAllByClientId(item);
        }
        public bool Delete(long id)
        {
            return dataMapper.Delete(id);
        }

        public List<ShippingAddress> GetAll()
        {
            return dataMapper.GetAll();
        }

        public ShippingAddress GetById(long id)
        {
            return dataMapper.GetById(id);
        }
    }
}
