﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using daoProject.Model;
using daoProject.Utilities;
using daoProject.Mapper;
using System.Data;
using System.Xml;
using daoProject.Infrastructure;


namespace daoProject.Service
{
    public class VendorMemberTaxServiceImpl
    {
        #region Vendor
        public List<VendorWHRC> GetAllVendor(XmlNode xmlNode)
        {
            List<VendorWHRC> vendorList = new List<VendorWHRC>();
            VendorWHRC vendorWHRC = null;

            if (xmlNode != null)
            {
                XmlNodeList datas = xmlNode.ChildNodes;
                for (int i = 0; i < datas.Count; i++)
                {
                    List<XmlNode> nodeList = new List<XmlNode>(datas[i].ChildNodes.Cast<XmlNode>());
                    XmlNode recordIDNode = (from item in nodeList
                                            where item.Name.Replace(" ", "") == "RecordId"
                                            select item).FirstOrDefault();
                    XmlNode vendorNameNode = (from item in nodeList
                                               where item.Name.Replace(" ", "") == "VendorName"
                                         select item).FirstOrDefault();


                    vendorWHRC = new VendorWHRC();
                    vendorWHRC.RecordID = Convert.ToInt64(recordIDNode.InnerText);
                    vendorWHRC.VendorName = vendorNameNode.InnerText;

                    vendorList.Add(vendorWHRC);
                }
            }
            return vendorList;
        }

        public XmlDocument InsertVendor(VendorWHRC vendorWHRC)
        {
            DataTable dt = new DataTable();
            DataColumn dc1 = new DataColumn("Name");
            DataColumn dc2 = new DataColumn("Value");

            dt.Columns.Add(dc1);
            dt.Columns.Add(dc2);

            AddDataRow("VendorName", vendorWHRC.VendorName, dt);

            XmlDocument xmlDoc = new XmlDocument();
            DataSet dataSet = new DataSet();
            dataSet.Tables.Add(dt);
            string strXml = dataSet.GetXml();
            xmlDoc.LoadXml(strXml);

            return xmlDoc;
        }

        public XmlDocument UpdateVendor(VendorWHRC vendorWHRC)
        {
            XmlDocument xmlDocument = null;

            return xmlDocument;
        }

        public void DeleteVendor(long recordID)
        {

        }
        #endregion

        #region Membership
        public List<MembershipWHRC> GetAllMembership(XmlNode xmlNode)
        {
            List<MembershipWHRC> membershipWHRCList = new List<MembershipWHRC>();
            MembershipWHRC membershipWHRC = null;

            if (xmlNode != null)
            {
                XmlNodeList datas = xmlNode.ChildNodes;
                for (int i = 0; i < datas.Count; i++)
                {
                    List<XmlNode> nodeList = new List<XmlNode>(datas[i].ChildNodes.Cast<XmlNode>());
                    XmlNode recordIDNode = (from item in nodeList
                                            where item.Name.Replace(" ", "") == "RecordId"
                                            select item).FirstOrDefault();
                    XmlNode nameofMembershipNode = (from item in nodeList
                                               where item.Name.Replace(" ", "") == "NameofMembership"
                                               select item).FirstOrDefault();

                    XmlNode durationPeriodNode = (from item in nodeList
                                              where item.Name.Replace(" ", "") == "DurationPeriod"
                                                  select item).FirstOrDefault();


                    XmlNode membershipFeeNode = (from item in nodeList
                                              where item.Name.Replace(" ", "") == "MembershipFee"
                                              select item).FirstOrDefault();


                    membershipWHRC = new MembershipWHRC();
                    membershipWHRC.RecordID = Convert.ToInt64(recordIDNode.InnerText);
                    membershipWHRC.NameofMembership = nameofMembershipNode.InnerText;
                    membershipWHRC.DurationPeriod = durationPeriodNode.InnerText;
                    if (membershipFeeNode.InnerText != "")
                    {
                        membershipWHRC.MembershipFee = Convert.ToDecimal(membershipFeeNode.InnerText);
                    }

                    membershipWHRCList.Add(membershipWHRC);
                }
            }
            return membershipWHRCList;
        }

        public XmlDocument InsertMembership(MembershipWHRC membershipWHRC)
        {
            DataTable dt = new DataTable();
            DataColumn dc1 = new DataColumn("Name");
            DataColumn dc2 = new DataColumn("Value");

            dt.Columns.Add(dc1);
            dt.Columns.Add(dc2);

            AddDataRow("NameofMembership", membershipWHRC.NameofMembership, dt);
            AddDataRow("DurationPeriod", membershipWHRC.DurationPeriod, dt);
            AddDataRow("MembershipFee", membershipWHRC.MembershipFee.ToString(), dt);

            XmlDocument xmlDoc = new XmlDocument();
            DataSet dataSet = new DataSet();
            dataSet.Tables.Add(dt);
            string strXml = dataSet.GetXml();
            xmlDoc.LoadXml(strXml);

            return xmlDoc;
        }

        public XmlDocument UpdateMembership(MembershipWHRC membershipWHRC)
        {
            XmlDocument xmlDocument = null;

            return xmlDocument;
        }

        public void DeleteMembership(long recordID)
        {

        }
        #endregion

        #region SalesTax
        public List<ManageSalesTaxRateWHRC> GetAllManageSalesTaxRate(XmlNode xmlNode)
        {
            List<ManageSalesTaxRateWHRC> manageSalesTaxRateList = new List<ManageSalesTaxRateWHRC>();
            ManageSalesTaxRateWHRC manageSalesTaxRateWHRC = null;

            if (xmlNode != null)
            {
                XmlNodeList datas = xmlNode.ChildNodes;
                for (int i = 0; i < datas.Count; i++)
                {
                    List<XmlNode> nodeList = new List<XmlNode>(datas[i].ChildNodes.Cast<XmlNode>());
                    XmlNode recordIDNode = (from item in nodeList
                                            where item.Name.Replace(" ", "") == "RecordId"
                                            select item).FirstOrDefault();
                    XmlNode salesTaxRateNode = (from item in nodeList
                                               where item.Name.Replace(" ", "") == "SalesTaxRate"
                                               select item).FirstOrDefault();


                    manageSalesTaxRateWHRC = new ManageSalesTaxRateWHRC();
                    manageSalesTaxRateWHRC.RecordID = Convert.ToInt64(recordIDNode.InnerText);
                    if (salesTaxRateNode.InnerText != "")
                    {
                        manageSalesTaxRateWHRC.SalesTaxRate =Convert.ToDecimal(salesTaxRateNode.InnerText);
                    }

                    manageSalesTaxRateList.Add(manageSalesTaxRateWHRC);
                }
            }
            return manageSalesTaxRateList;
        }

        public XmlDocument InsertManageSalesTaxRate(ManageSalesTaxRateWHRC manageSalesTaxRateWHRC)
        {
            DataTable dt = new DataTable();
            DataColumn dc1 = new DataColumn("Name");
            DataColumn dc2 = new DataColumn("Value");

            dt.Columns.Add(dc1);
            dt.Columns.Add(dc2);

            AddDataRow("SalesTaxRate", manageSalesTaxRateWHRC.SalesTaxRate.ToString(), dt);

            XmlDocument xmlDoc = new XmlDocument();
            DataSet dataSet = new DataSet();
            dataSet.Tables.Add(dt);
            string strXml = dataSet.GetXml();
            xmlDoc.LoadXml(strXml);

            return xmlDoc;
        }

        public XmlDocument UpdateManageSalesTaxRate(ManageSalesTaxRateWHRC manageSalesTaxRateWHRC)
        {
            XmlDocument xmlDocument = null;

            return xmlDocument;
        }

        public void DeleteManageSalesTaxRate(long recordID)
        {

        }
        #endregion

        #region RentalEquipmentDelayFee
        public List<RentalEquipmentDelayFeeWHRC> GetAllRentalEquipmentDelayFee(XmlNode xmlNode)
        {
            List<RentalEquipmentDelayFeeWHRC> rentalEquipmentDelayFeeList = new List<RentalEquipmentDelayFeeWHRC>();
            RentalEquipmentDelayFeeWHRC rentalEquipmentDelayFeeWHRC = null;

            if (xmlNode != null)
            {
                XmlNodeList datas = xmlNode.ChildNodes;
                for (int i = 0; i < datas.Count; i++)
                {
                    List<XmlNode> nodeList = new List<XmlNode>(datas[i].ChildNodes.Cast<XmlNode>());
                    XmlNode recordIDNode = (from item in nodeList
                                            where item.Name.Replace(" ", "") == "RecordId"
                                            select item).FirstOrDefault();
                    XmlNode delayFeeNode = (from item in nodeList
                                                where item.Name.Replace(" ", "") == "DelayFee"
                                                select item).FirstOrDefault();


                    rentalEquipmentDelayFeeWHRC = new RentalEquipmentDelayFeeWHRC();
                    rentalEquipmentDelayFeeWHRC.RecordID = Convert.ToInt64(recordIDNode.InnerText);
                    if (delayFeeNode.InnerText != "")
                    {
                        rentalEquipmentDelayFeeWHRC.DelayFee = Convert.ToDecimal(delayFeeNode.InnerText);
                    }

                    rentalEquipmentDelayFeeList.Add(rentalEquipmentDelayFeeWHRC);
                }
            }
            return rentalEquipmentDelayFeeList;
        }

        public XmlDocument InsertRentalEquipmentDelayFee(RentalEquipmentDelayFeeWHRC rentalEquipmentDelayFeeWHRC)
        {
            DataTable dt = new DataTable();
            DataColumn dc1 = new DataColumn("Name");
            DataColumn dc2 = new DataColumn("Value");

            dt.Columns.Add(dc1);
            dt.Columns.Add(dc2);

            AddDataRow("DelayFee", rentalEquipmentDelayFeeWHRC.DelayFee.ToString(), dt);

            XmlDocument xmlDoc = new XmlDocument();
            DataSet dataSet = new DataSet();
            dataSet.Tables.Add(dt);
            string strXml = dataSet.GetXml();
            xmlDoc.LoadXml(strXml);

            return xmlDoc;
        }

        public XmlDocument UpdateRentalEquipmentDelayFee(RentalEquipmentDelayFeeWHRC rentalEquipmentDelayFeeWHRC)
        {
            XmlDocument xmlDocument = null;

            return xmlDocument;
        }

        public void DeleteRentalEquipmentDelayFee(long recordID)
        {

        }
        #endregion

        #region StudentFlatRateWHRC
        public List<StudentFlatRateWHRC> GetAllStudentFlatRate(XmlNode xmlNode)
        {
            List<StudentFlatRateWHRC> studentFlatRateList = new List<StudentFlatRateWHRC>();
            StudentFlatRateWHRC studentFlatRateWHRC = null;

            if (xmlNode != null)
            {
                XmlNodeList datas = xmlNode.ChildNodes;
                for (int i = 0; i < datas.Count; i++)
                {
                    List<XmlNode> nodeList = new List<XmlNode>(datas[i].ChildNodes.Cast<XmlNode>());
                    XmlNode recordIDNode = (from item in nodeList
                                            where item.Name.Replace(" ", "") == "RecordId"
                                            select item).FirstOrDefault();
                    XmlNode discountRateperClassNode = (from item in nodeList
                                            where item.Name.Replace(" ", "") == "DiscountRateperClass"
                                            select item).FirstOrDefault();


                    studentFlatRateWHRC = new StudentFlatRateWHRC();
                    studentFlatRateWHRC.RecordID = Convert.ToInt64(recordIDNode.InnerText);
                    if (discountRateperClassNode.InnerText != "")
                    {
                        studentFlatRateWHRC.DiscountRatePerClass = Convert.ToDecimal(discountRateperClassNode.InnerText);
                    }

                    studentFlatRateList.Add(studentFlatRateWHRC);
                }
            }
            return studentFlatRateList;
        }

        public XmlDocument InsertStudentFlatRate(StudentFlatRateWHRC studentFlatRateWHRC)
        {
            DataTable dt = new DataTable();
            DataColumn dc1 = new DataColumn("Name");
            DataColumn dc2 = new DataColumn("Value");

            dt.Columns.Add(dc1);
            dt.Columns.Add(dc2);

            AddDataRow("DiscountRateperClass", studentFlatRateWHRC.DiscountRatePerClass.ToString(), dt);

            XmlDocument xmlDoc = new XmlDocument();
            DataSet dataSet = new DataSet();
            dataSet.Tables.Add(dt);
            string strXml = dataSet.GetXml();
            xmlDoc.LoadXml(strXml);

            return xmlDoc;
        }

        public XmlDocument UpdateStudentFlatRate(StudentFlatRateWHRC studentFlatRateWHRC)
        {
            XmlDocument xmlDocument = null;

            return xmlDocument;
        }

        public void DeleteStudentFlatRate(long recordID)
        {

        }
        #endregion

        private void AddDataRow(string name, string value, DataTable dt)
        {
            DataRow dr = dt.NewRow();
            dr["Name"] = name;
            dr["Value"] = value;
            dt.Rows.Add(dr);
        }

    }
}