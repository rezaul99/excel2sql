﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using daoProject.Service;
using daoProject.WS;
using System.Xml;
using daoProject.Mapper;
namespace daoProject.Model
{
    public class ReportDataGenerator
    {

        public DataTable test(long ReportId)
        {
            DataTable Result = null;
            List<ReportClassObjects> TableList = new ReportGenerationServiceImpl().GetTableListByDataBaseID(ReportId);

            if (TableList.Count > 1)
            {
                List<ReportObjectRelation> relationList = GetRelationShipList(TableList);

                if (relationList != null)
                {
                    if (relationList.Count > 0)
                    {
                        Result = new DataTable();
                        

                        DataTable tempTable = null;
                      //  int countLastTable=0;
                        foreach (ReportObjectRelation relation in relationList)
                        {
                            //countLastTable++;
                            //if (countLastTable == relationList.Count)
                            //{
                            //    ReportClassObjects checkTable1 = TableList.FirstOrDefault(x => (x.Name == relation.Table1Id));
                            //    ReportClassObjects checkTable2 = TableList.FirstOrDefault(x => (x.Name == relation.Table2Id));
                            //    if (checkTable1 == null && checkTable2 == null)
                            //    {
                            //        break;
                            //    }
                            //}
                            DataTable Table1 = GetData(relation.Table1Id);
                            Table1.TableName = relation.Table1Id;

                            DataTable Table2 = GetData(relation.Table2Id);
                            Table2.TableName = relation.Table2Id;

                            bool isResultTable = false;

                            tempTable = GetTempTable(Table1, Table2, relation, isResultTable);

                            if (relationList.Count == 1)
                            {
                                Result = tempTable.Copy();
                                Result.TableName = "Result";
                            }
                            else
                            {
                                if (Result.Columns.Count > 0)
                                {
                                    isResultTable = true;
                                    Result = GetTempTable(tempTable, Result, relation, isResultTable);
                                    Result.TableName = "Result";
                                }
                                else
                                {
                                    Result = tempTable.Copy();
                                    Result.TableName = "Result";
                                }
                            }
                        }

                    }

                }
            }
            else
            {
                Result = GetData(TableList[0].Name);
                foreach (DataColumn column in Result.Columns)
                {
                    Result.Columns[column.ColumnName].ColumnName = TableList[0].Name + "_" + column.ColumnName;
                }
            }
            return Result;
        }

        public DataTable GetTempTable(DataTable Table1, DataTable Table2, ReportObjectRelation relation, bool isResultTable)
        {
            DataTable tempTable = new DataTable();
            tempTable.TableName = "tempTable";

            DataTable Result = new DataTable();
            Result.TableName = "Result";

            string col1 = string.Empty;
            string col2 = string.Empty;

            if (isResultTable)
            {
               // relation.ColumnName1 = "Whrc_Users_Id";
               // relation.ColumnName2 = "Whrc_Invoice_ClientId";
                if (Table2.Columns.Contains(relation.ColumnName2))
                {
                    col1 = relation.ColumnName1;
                    col2 = relation.ColumnName2;
                }
                else
                {
                    col1 = relation.ColumnName2;
                    col2= relation.ColumnName1;
                }
                var Query = (from table1 in Table1.AsEnumerable()
                             join table2 in Table2.AsEnumerable()
                             on table1[col1].ToString() equals table2[col2].ToString()
                             select new { table1, table2 });
                

               foreach (DataColumn column in Table2.Columns)
               {
                   Result.Columns.Add(column.ColumnName);
               }

               foreach (DataColumn column in Table1.Columns)
               {
                   if (!Table2.Columns.Contains(column.ToString()))
                   {
                       Result.Columns.Add(column.ColumnName);
                   }
               }

               foreach (var DRow in Query)
               {
                   DataRow Row = null;
                   Row = Result.NewRow();

                   int i;

                    for (int a = 0; a < Result.Columns.Count; a++)
                    {
                        for (i = 0; i < Table1.Columns.Count; i++)
                        {
                            if (Result.Columns[a].ColumnName == Table1.Columns[i].ColumnName)
                            {
                                Row[a] = DRow.table1.ItemArray[i];
                            }
                        }
                    }

                    for (int a = 0; a < Result.Columns.Count; a++)
                    {
                        for (i = 0; i < Table2.Columns.Count; i++)
                        {
                            if (Result.Columns[a].ColumnName == Table2.Columns[i].ColumnName)
                            {
                                Row[a] = DRow.table2.ItemArray[i];
                            }
                        }
                    }

                    Result.Rows.Add(Row);
               }
               // someTable.Select(r => new { r.attribute1_name, r.attribute2_name }).Distinct();
               List<string> columnList = new List<string>();
               foreach (DataColumn column in Result.Columns)
               {
                   columnList.Add(column.ToString());
               }
               string[] columns = columnList.ToArray();
               return Result.DefaultView.ToTable(Result.TableName,true, columns);
            }
            else
            {
               
               var Query = from table1 in Table1.AsEnumerable()
                           join table2 in Table2.AsEnumerable()
                           on table1[relation.ColumnPK].ToString() equals table2[relation.ColumnFK].ToString()
                           select new { table1, table2 };

               foreach (DataColumn column in Table1.Columns)
               {
                   tempTable.Columns.Add(Table1.TableName + "_" + column);
               }

               foreach (DataColumn column in Table2.Columns)
               {
                   tempTable.Columns.Add(Table2.TableName + "_" + column);
               }

               foreach (var DRow in Query)
               {
                   DataRow Row = null;
                   Row = tempTable.NewRow();
                   int i;

                    for (i = 0; i < Table1.Columns.Count; i++)
                    {
                        Row[i] = DRow.table1.ItemArray[i];
                    }
                    for (int k = 0; k < Table2.Columns.Count; k++)
                    {
                        Row[i] = DRow.table2.ItemArray[k];
                        i++;
                    }
                   
                    tempTable.Rows.Add(Row);
               }

               List<string> columnList = new List<string>();
               foreach (DataColumn column in tempTable.Columns)
               {
                   columnList.Add(column.ToString());
               }
               string[] columns = columnList.ToArray();
              // return Result.DefaultView.ToTable(Result.TableName, true, columns);
               return tempTable.DefaultView.ToTable(tempTable.TableName, true, columns); ;
            }
        }

        public List<ReportObjectRelation> GetRelationShipList(List<ReportClassObjects> TableList)
        {
            List<ReportObjectRelation> relationList = new List<ReportObjectRelation>();
            List<ReportObjectRelation> tempRelationList = new List<ReportObjectRelation>();
            List<ReportObjectRelation> removeRelationList = new List<ReportObjectRelation>();

            for (int i = 0; i < TableList.Count; i++)
            {
                for (int j = 1; j < TableList.Count; j++)
                {
                    if (!(TableList[i].Name.Equals(TableList[j].Name)))
                    {
                        tempRelationList = GetRelationShipList(TableList[i].Name, TableList[j].Name);
                        if (tempRelationList.Count > 0)
                        {
                            foreach (ReportObjectRelation relation in tempRelationList)
                            {
                                ReportObjectRelation existsRelation = relationList.FirstOrDefault(x => x.Id == relation.Id);
                                if (existsRelation != null)
                                {
                                    removeRelationList.Add(relation); 
                                }
                            }
                            foreach (ReportObjectRelation removeitem in removeRelationList)
                            {
                                tempRelationList.RemoveAll(x => x.Id == removeitem.Id);
                            }
                            relationList =  relationList.Concat(tempRelationList).ToList();
                        }
                    }
                   // relationList = relationList.Concat(relationList).ToList();
                }
            }
            return relationList;
        }

        public DataTable GenerateReportData(long ReportId)
        {
            List<ReportClassObjects> TableList = new ReportGenerationServiceImpl().GetTableListByDataBaseID(ReportId);

            DataTable Table1 = GetData(TableList[0].Name);
            Table1.TableName = TableList[0].Name;

            DataTable Result = Table1.Copy();
            Result.TableName = "Result";
          
            DataTable Table2 = new DataTable();

            string PrevTabNames = string.Empty;

            for (int l = 1; l < TableList.Count; l++)
            {
               
                Table2 = GetData(TableList[l].Name);
                Table2.TableName = TableList[l].Name;
               // List<ReportObjectRelation> relationList = GetRelationShipList(PrevTabNames, Table2.TableName);

                ReportObjectRelation Reln = new ReportGenerationServiceImpl().GetReportObjectRelationByObjectsId(PrevTabNames, Table2.TableName);
              
               // if (relationList.Count > 0)
              //  {
                 //   foreach (ReportObjectRelation Reln in relationList)
                   // {
                        if (Reln != null)
                        {
                            string column1 = string.Empty;
                            string column2 = string.Empty;
                           
                                if (Table2.TableName.Equals(Reln.Table2Id))
                                {
                                    column1 = Reln.ColumnName1.ToString();
                                    column2 = Reln.ColumnName2.ToString();
                                }
                                else
                                {
                                    column1 = Reln.ColumnName2.ToString();
                                    column2 = Reln.ColumnName1.ToString();
                                }
                           
                            PrevTabNames += "~" + Table2.TableName;

                            var Query = from table1 in Table1.AsEnumerable()
                                        join table2 in Table2.AsEnumerable()
                                        on table1[column1].ToString() equals table2[column2].ToString()
                                        select new { table1, table2 };
                            // on table1.Field<long>(column1).ToString() equals table2.Field<long>(column2).ToString()
                            foreach (DataColumn dc in Table2.Columns)
                            {
                                bool flg = false;
                                foreach (DataColumn dctmp in Table1.Columns)
                                {
                                    if ((dc.ColumnName.ToString().Equals(dctmp.ColumnName.ToString())))
                                    {
                                        flg = true;
                                        break;
                                    }
                                }

                                if (flg)
                                    continue;
                                Result.Columns.Add(dc.ColumnName, dc.DataType);
                            }

                            Result.Rows.Clear();

                            foreach (var DRow in Query)
                            {
                                DataRow Row = Result.NewRow();
                                int i;
                                for (i = 0; i < Table1.Columns.Count; i++)
                                {
                                    Row[i] = DRow.table1.ItemArray[i];
                                }
                                for (int k = 0; k < Table2.Columns.Count; k++)
                                {
                                    bool flg = false;
                                    foreach (DataColumn dctmp in Table1.Columns)
                                    {
                                        if ((Table2.Columns[k].ColumnName.ToString().Equals(dctmp.ColumnName.ToString())))
                                        {
                                            flg = true;
                                            break;
                                        }

                                    }
                                    if (flg)
                                        continue;

                                    Row[i] = DRow.table2.ItemArray[k];
                                    i++;
                                }
                                Result.Rows.Add(Row);
                            }

                            Table1.Clear();
                            Table1 = Result.Copy();
                        }
                   // } 
                //}
            }
            
            return Table1;
        }

        public DataTable GetData(string tableObjName)
        {
            DataTable table = new DataTable();
            Reportobject reportObject = new Reportobject();

            Dictionary<string, string> JijotyTable = reportObject.JijotyClassObjects;
            Dictionary<string, string> WhrcDBTable = reportObject.WhrcDBClassObjects;

            List<KeyValuePair<string, string>> Jijotylist = JijotyTable.ToList();
            foreach (KeyValuePair<string, string> pair in Jijotylist)
            {
                if (tableObjName.Equals(pair.Key))
                {
                    table = GetDataFromService(tableObjName);
                }
            }

            List<KeyValuePair<string, string>> Whrclist = WhrcDBTable.ToList();
            foreach (KeyValuePair<string, string> pair in Whrclist)
            {
                if (tableObjName.Equals(pair.Key))
                {
                    table = GetDataFromDataBase(tableObjName);
                }
            }

           
            return table;
        }

        public DataTable GetDataFromDataBase(string tableObjName)
        {
            DataTable dt = new DataTable();
            dt = new SystemDataBaseDataMapper().GetDataTable(tableObjName);
            return dt;
        }

        public DataTable GetDataFromService(string tableObjName)
        {
            DataTable dt = new DataTable();
            WS.ToolService ts = new ToolService();
            XmlNode xmlNode = ts.GetApplicationData(tableObjName);
            XmlNodeList productNodeList = xmlNode.ChildNodes;

            foreach (XmlNode xmlNodeObj in productNodeList[0].ChildNodes)
            {
                dt.Columns.Add(xmlNodeObj.Name.Replace(" ", "").Trim());
            }

            foreach (XmlNode xmlNodeObj in productNodeList)
            {
                DataRow dr = dt.NewRow();
                for (int i = 0; i < dt.Columns.Count; i++)
                {
                    dr[xmlNodeObj.ChildNodes[i].Name] = xmlNodeObj.ChildNodes[i].InnerText;
                }
                dt.Rows.Add(dr);
            }
            return dt;
        }

        public List<ReportObjectRelation> GetRelationShipList( string firstTable, string secondTable)
        {
                List<ReportObjectRelation> relationObjectlist = new ReportGenerationServiceImpl().GetAllReportObjectRelationList();
                List<ReportObjectRelation> relationlist = null;
                
                if (relationObjectlist.Count > 0)
                {
                    relationlist = GetRelationOneToOne(firstTable, secondTable, relationObjectlist);

                    if (relationlist.Count == 0)
                    {
                        relationlist = GetRelationOneToMany(firstTable, secondTable, relationObjectlist);

                        if (relationlist.Count == 0)
                        {
                            List<ReportObjectRelation> temp = new List<ReportObjectRelation>();
                          //  List<ReportObjectRelation> allRelationList = GetAllFirstList(firstTable, secondTable, relationObjectlist);
                            List<ReportObjectRelation> firstList = GetAllFirstList(firstTable, relationObjectlist);
                            List<ReportObjectRelation> secondtList = GetAllSecondList( secondTable, relationObjectlist);

                            foreach (ReportObjectRelation first in firstList)
                            {
                                foreach (ReportObjectRelation second in secondtList)
                                {
                                   // if (first.Id != second.Id)
                                   // {
                                        temp = GetRelationOneToOne(first.Table2Id, second.Table2Id, relationObjectlist);
                                    
                                        relationlist = relationlist.Concat(temp).ToList();

                                   // }
                                }
                            }
                            if (relationlist.Count > 0)
                            {
                                temp = GetRelationOneToOne(firstTable, relationlist[0].Table2Id, relationObjectlist);

                                if (temp.Count == 0)
                                {
                                    temp = GetRelationOneToMany(firstTable, relationlist[0].Table2Id, relationObjectlist);
                                    if (temp.Count == 0)
                                    {
                                        temp = GetRelationOneToOne(firstTable, relationlist[0].Table1Id, relationObjectlist);
                                        if (temp.Count == 0)
                                        {
                                            temp = GetRelationOneToMany(firstTable, relationlist[0].Table1Id, relationObjectlist);
                                        }
                                    }

                                }
                                temp = RemoveFromList(temp, relationlist);
                                relationlist = relationlist.Concat(temp).ToList();


                                temp = GetRelationOneToMany(secondTable, relationlist[0].Table2Id, relationObjectlist);
                                if (temp.Count == 0)
                                {
                                    temp = GetRelationOneToOne(secondTable, relationlist[0].Table2Id, relationObjectlist);
                                    if (temp.Count == 0)
                                    {
                                        temp = GetRelationOneToMany(secondTable, relationlist[0].Table1Id, relationObjectlist);
                                        if (temp.Count == 0)
                                        {
                                            temp = GetRelationOneToOne(secondTable, relationlist[0].Table1Id, relationObjectlist);
                                        }
                                    }

                                }
                                temp = RemoveFromList(temp, relationlist);
                                relationlist = relationlist.Concat(temp).ToList();
                            }
                        }
                      
                    }
                }
              //  relationlist = null;
                return relationlist;
        }

        public List<ReportObjectRelation> RemoveFromList(List<ReportObjectRelation> temp, List<ReportObjectRelation> relationlist)
        {
            List<ReportObjectRelation> removeRelationList = new List<ReportObjectRelation>();
            foreach (ReportObjectRelation relation in temp)
            {
                ReportObjectRelation existsRelation = relationlist.FirstOrDefault(x => x.Id == relation.Id);
                if (existsRelation != null)
                {
                    removeRelationList.Add(relation);
                }
            }
            foreach (ReportObjectRelation removeitem in removeRelationList)
            {
                temp.RemoveAll(x => x.Id == removeitem.Id);
            }
            return temp;
        }
        public List<ReportObjectRelation> GetRelationOneToOne(string firstTable, string secondTable, List<ReportObjectRelation> relationObjectlist)
        {
            List<ReportObjectRelation> relationlist = new List<ReportObjectRelation>();
            ReportObjectRelation relation = relationObjectlist.FirstOrDefault(x => ((x.Table1Id == firstTable) || (x.Table2Id == firstTable)) && ((x.Table1Id == secondTable) || (x.Table2Id == secondTable)));
            if (relation != null)
            {
                relationlist.Add(relation);
            }
            return relationlist;
        }

        public List<ReportObjectRelation> GetRelationOneToMany(string firstTable, string secondTable, List<ReportObjectRelation> relationObjectlist)
        {
            List<ReportObjectRelation> relationlist = new List<ReportObjectRelation>();
            ReportObjectRelation relation = null;
            bool isRelationExists = false;
            List<ReportObjectRelation> firstRelationObjectlist = relationObjectlist.Where(x => (x.Table1Id == firstTable) || (x.Table2Id == firstTable)).ToList();
            List<ReportObjectRelation> secondRelationObjectlist = relationObjectlist.Where(x => (x.Table1Id == secondTable) || (x.Table2Id == secondTable)).ToList();
            foreach (ReportObjectRelation firstRelation in firstRelationObjectlist)
            {
                relation = secondRelationObjectlist.FirstOrDefault(x => ((x.Table1Id == firstRelation.Table1Id) || (x.Table2Id == firstRelation.Table1Id)) || ((x.Table1Id == firstRelation.Table2Id) || (x.Table2Id == firstRelation.Table2Id)));

                if (relation != null)
                {
                    relationlist.Add(relation);
                    isRelationExists = true;
                    break;
                }
            }

            if (isRelationExists)
            {
                // relationlist.Add(relation);
                relation = firstRelationObjectlist.FirstOrDefault(x => ((x.Table1Id == relation.Table1Id) || (x.Table2Id == relation.Table1Id)) || ((x.Table1Id == relation.Table2Id) || (x.Table2Id == relation.Table2Id)));
                if (relation != null)
                {
                    relationlist.Add(relation);
                }
            }
            return relationlist;
        }

        public List<ReportObjectRelation> GetAllFirstList(string firstTable,  List<ReportObjectRelation> relationObjectlist)
        {
            List<ReportObjectRelation> relationList = null;
            List<ReportObjectRelation> allRelationList = new List<ReportObjectRelation>();

            List<ReportObjectRelation> firstRelationObjectlist = relationObjectlist.Where(x => (x.Table1Id == firstTable) || (x.Table2Id == firstTable)).ToList();
          //  List<ReportObjectRelation> secondRelationObjectlist = relationObjectlist.Where(x => (x.Table1Id == secondTable) || (x.Table2Id == secondTable)).ToList();
            
            foreach (ReportObjectRelation firstRelation in firstRelationObjectlist)
            {
                relationList = relationObjectlist.Where(x => ((x.Table1Id == firstRelation.Table1Id) || (x.Table2Id == firstRelation.Table2Id)) || ((x.Table1Id == firstRelation.Table2Id) || (x.Table2Id == firstRelation.Table1Id))).ToList();
                allRelationList = allRelationList.Concat(relationList).ToList();
            }
            
            //foreach (ReportObjectRelation secondRelation in secondRelationObjectlist)
            //{
            //    relationList = relationObjectlist.Where(x => ((x.Table1Id == secondRelation.Table1Id) || (x.Table2Id == secondRelation.Table2Id)) || ((x.Table1Id == secondRelation.Table2Id) || (x.Table2Id == secondRelation.Table1Id))).ToList();
            //    allRelationList = allRelationList.Concat(relationList).ToList();
            //}

            List<ReportObjectRelation> removeRelationlist = GetRemoveList(firstTable, allRelationList);

            foreach (ReportObjectRelation remove in removeRelationlist)
            {
                allRelationList.RemoveAll(x => x.Id == remove.Id);
            }

            return allRelationList;
        }

        public List<ReportObjectRelation> GetAllSecondList(string secondTable, List<ReportObjectRelation> relationObjectlist)
        {
            List<ReportObjectRelation> relationList = null;
            List<ReportObjectRelation> allRelationList = new List<ReportObjectRelation>();

            //List<ReportObjectRelation> firstRelationObjectlist = relationObjectlist.Where(x => (x.Table1Id == firstTable) || (x.Table2Id == firstTable)).ToList();
            List<ReportObjectRelation> secondRelationObjectlist = relationObjectlist.Where(x => (x.Table1Id == secondTable) || (x.Table2Id == secondTable)).ToList();

            //foreach (ReportObjectRelation firstRelation in firstRelationObjectlist)
            //{
            //    relationList = relationObjectlist.Where(x => ((x.Table1Id == firstRelation.Table1Id) || (x.Table2Id == firstRelation.Table2Id)) || ((x.Table1Id == firstRelation.Table2Id) || (x.Table2Id == firstRelation.Table1Id))).ToList();
            //    allRelationList = allRelationList.Concat(relationList).ToList();
            //}

            foreach (ReportObjectRelation secondRelation in secondRelationObjectlist)
            {
                relationList = relationObjectlist.Where(x => ((x.Table1Id == secondRelation.Table1Id) || (x.Table2Id == secondRelation.Table2Id)) || ((x.Table1Id == secondRelation.Table2Id) || (x.Table2Id == secondRelation.Table1Id))).ToList();
                allRelationList = allRelationList.Concat(relationList).ToList();
            }

            List<ReportObjectRelation> removeRelationlist = GetRemoveList( secondTable, allRelationList);

            foreach (ReportObjectRelation remove in removeRelationlist)
            {
                allRelationList.RemoveAll(x => x.Id == remove.Id);
            }

            return allRelationList;
        }

        public List<ReportObjectRelation> GetRemoveList(string firstTable, List<ReportObjectRelation> allRelationList)
        {
            List<ReportObjectRelation> removeList = new List<ReportObjectRelation>();
            foreach (ReportObjectRelation relation in allRelationList)
            {
                removeList = allRelationList.Where(x => ((x.Table1Id == firstTable) || (x.Table2Id == firstTable))).ToList();
            }
            return removeList;
        }
       
        public void  GetRelations( string firstTable, string secondTable, List<ReportObjectRelation> firstRelationObjectlist, List<ReportObjectRelation> secondRelationObjectlist)
        {
            foreach (ReportObjectRelation firstRelation in firstRelationObjectlist)
            {
                foreach (ReportObjectRelation secondRelation in secondRelationObjectlist)
                {
                    string firstName = string.Empty;
                    string secondName = string.Empty;

                    if (firstRelation.Table1Id == firstTable)
                    {
                        firstName = firstRelation.Table2Id;
                    }
                    else
                    {
                        firstName = firstRelation.Table1Id;
                    }

                    if (secondRelation.Table1Id == secondTable)
                    {
                        secondName = secondRelation.Table2Id;
                    }
                    else
                    {
                        secondName = secondRelation.Table1Id;
                    }

                    if (firstName != secondName)
                    {
                        GetRelationShipList(firstName, secondName);
                    }
                }
            }
        }

        #region testing
        //public DataTable GenerateReportData(long ReportId)
        //{
        //    DataTable ReturnTable = new DataTable();
        //    DataSet dataset = new DataSet();

        //    List<ReportClassObjects> tableList = new ReportGenerationServiceImpl().GetTableListByDataBaseID(ReportId);

        //    if (tableList.Count > 0)
        //    {
        //        DataTable table = new DataTable();
        //        List<ReportAttributes> attributeListForOneObject = new List<ReportAttributes>();
        //        List<ReportAttributes> attributeListForAllObject = new List<ReportAttributes>();

        //        foreach (ReportClassObjects tableObj in tableList)
        //        {
        //            attributeListForOneObject = new ReportGenerationServiceImpl().GetColumnListByTableId(tableObj.Id);
        //            attributeListForAllObject = attributeListForAllObject.Concat(attributeListForOneObject).ToList();
        //            table = (DataTable)GetData(tableObj.Name);
        //            table.TableName = tableObj.Name;
        //            dataset.Tables.Add(table.Copy());
        //        }

        //        //do query and generate return datatable
        //        if (dataset.Tables.Count == 1)
        //        {
        //            ReturnTable = dataset.Tables[0];
        //        }
        //        else if (dataset.Tables.Count >= 2)
        //        {
        //            bool RelationWithTable1Column = false;
        //            bool RelationWithTable2Column = false;
        //            DataTable firstTable = new DataTable();
        //            DataTable SecondTable = new DataTable();
        //            List<ReportObjectRelation> ReportObjectRelationList = new List<ReportObjectRelation>();
        //            foreach (ReportAttributes columnField in attributeListForAllObject)
        //            {
        //                ReturnTable.Columns.Add(columnField.Name);
        //            }

        //            for (int i = 0; i < dataset.Tables.Count; i++)
        //            {
        //                for (int j = i + 1; j < dataset.Tables.Count; j++)
        //                {
        //                    ReportObjectRelation reportObjectRelation = new ReportGenerationServiceImpl().GetReportObjectRelationByObjectsId(tableList[i].Name, tableList[j].Name);
        //                    if (reportObjectRelation != null)
        //                    {
        //                        ReportObjectRelationList.Add(reportObjectRelation);
        //                    }
        //                }
        //            }



        //            #region string linq
        //            string from = "";
        //            string where = "where  ";
        //            string select = "select new {";

        //            for (int i = 0; i < (dataset.Tables.Count); i++)
        //            {
        //                from += "from table" + i + " " + "in " + (dataset.Tables[i].AsEnumerable()) + " ";
        //            }
        //            for (int i = 0; i < ReportObjectRelationList.Count; i++)
        //            {
        //                if (ReportObjectRelationList.Count == 1)
        //                {
        //                    where += "table0" + ".Columns[" + ReportObjectRelationList[i].ColumnName1 + "]" + ".ToString() " + "  ==  " +
        //                       "table1" + ".Columns[" + ReportObjectRelationList[i].ColumnName2 + "]" + ".ToString() ";
        //                }
        //                if (ReportObjectRelationList.Count > 1)
        //                {

        //                    where += ReportObjectRelationList[i].Table1Id + ".Columns[" + ReportObjectRelationList[i].ColumnName1 + "]" + " == " +
        //                        ReportObjectRelationList[i].Table2Id + ".Columns[" + ReportObjectRelationList[i].ColumnName2 + "]";
        //                    if (i == (ReportObjectRelationList.Count - 1))
        //                    {
        //                        where += "";
        //                    }
        //                    else
        //                    {
        //                        where += " and ";
        //                    }
        //                }

        //            }
        //            for (int i = 0; i < tableList.Count; i++)
        //            {
        //                select += " table" + i;
        //                if (i == (tableList.Count - 1))
        //                {
        //                    select += " ";
        //                }
        //                else
        //                {
        //                    select += " ,";
        //                }

        //            }
        //            select += " }";

        //            var datatableQuery = (from + where + select);


        //            var query = datatableQuery.Cast<IEnumerable<DataRow>>();
        //            foreach (var tableRow in datatableQuery)
        //            {
        //            }
        //            #endregion


        //            //for (int table1Column = 0; table1Column < dataset.Tables[i].Columns.Count; table1Column++)
        //            //{
        //            //    if (reportObjectRelation.ColumnName1.Equals(dataset.Tables[i].Columns[table1Column].ColumnName))
        //            //    {
        //            //        RelationWithTable1Column = true;
        //            //        break;
        //            //    }
        //            //}
        //            //for (int table2Column = 0; table2Column < dataset.Tables[j].Columns.Count; table2Column++)
        //            //{
        //            //    if (reportObjectRelation.ColumnName2.Equals(dataset.Tables[j].Columns[table2Column].ColumnName))
        //            //    {
        //            //        RelationWithTable2Column = true;
        //            //        break;
        //            //    }
        //            //}
        //            //if (RelationWithTable1Column && RelationWithTable2Column)
        //            //{
        //            //    var linqQuery = from table1 in dataset.Tables[0].AsEnumerable()
        //            //                    join table2 in dataset.Tables[1].AsEnumerable()
        //            //                    on table1[reportObjectRelation.ColumnName1].ToString() equals table2[reportObjectRelation.ColumnName2].ToString()
        //            //                    select new { table1, table2 };
        //            //    foreach (var tableRow in linqQuery)
        //            //    {
        //            //        firstTable = tableRow.table1.Table;
        //            //        SecondTable = tableRow.table2.Table;
        //            //    }

        //            //    foreach (DataRow row in firstTable.Rows)
        //            //    {
        //            //        DataRow dr = ReturnTable.NewRow();
        //            //        foreach (DataColumn columnfield in firstTable.Columns)
        //            //        {
        //            //            foreach (ReportAttributes columnField in attributeListForAllObject)
        //            //            {
        //            //                if (columnfield.Equals(columnField.Name))
        //            //                {
        //            //                    dr[columnfield] = row[columnfield];
        //            //                }
        //            //            }
        //            //        }
        //            //        ReturnTable.Rows.Add(dr);
        //            //    }
        //            //    foreach (DataRow row in SecondTable.Rows)
        //            //    {
        //            //        DataRow dr = ReturnTable.NewRow();
        //            //        foreach (DataColumn columnfield in SecondTable.Columns)
        //            //        {
        //            //            foreach (ReportAttributes columnField in attributeListForAllObject)
        //            //            {
        //            //                if (columnfield.Equals(columnField.Name))
        //            //                {
        //            //                    dr[columnfield] = row[columnfield];
        //            //                }
        //            //            }
        //            //        }
        //            //        ReturnTable.Rows.Add(dr);
        //            //    }

        //            //}
        //            //    }
        //            //}

        //        }

        //    }
        //    return ReturnTable;//do query for user defined column names matching
        //}
        #endregion
    }
}
