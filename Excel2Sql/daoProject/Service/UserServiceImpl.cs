﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using daoProject.Model;
using daoProject.Utilities;
using daoProject.Mapper;
using System.Data;
using System.Xml;
using daoProject.Infrastructure;


namespace daoProject.Service
{
    public class UserServiceImpl
    {      
        public List<User> GetAllUsers()
        {
            UserServiceImpl userServiceImpl = new UserServiceImpl();
            UserDataMapper userDataMapper = new UserDataMapper();

            List<User> userList = userDataMapper.GetAll();

            return userList;
        }

        public User ValidateUser(string email,string password)
        {  
            User userObj = null;
            List<User> userList = GetAllUsers();

            User user = null;

            Utility util = new Utility();

            //Convert the plane password text to hashing 
            byte[] passwordHash = util.Hash(password.ToLower());
            //convert the password into Base64
            string encodePassword = util.Base64Encode(passwordHash);

            if (userList.Count > 0)
            {
                userObj = (from item in userList
                           where (item.Email.ToLower() == email.ToLower()) && (item.Password == encodePassword)
                           select item).FirstOrDefault();
            }
            return userObj;
        }

        public User GetUserByEmail(string email)
        {
            User user = null;
            UserDataMapper userDataMapper = new UserDataMapper();
            user = userDataMapper.GetByEmailId(email);
            return user;
        }

        public void InsertUser(User userObj)
        {
            UserDataMapper userDataMapper = new UserDataMapper();
            userDataMapper.Create(userObj);
        }

        public bool UpdateUserFromMobile(User userObj)
        {
            return new UserDataMapper().UpdateFromMobile(userObj);
        }
        public bool UpdateUser(User userObj)
        {
            return new UserDataMapper().Update(userObj);
        }

        public User GetUserByUserRecordId(long userRecordId)
        {
            return new UserDataMapper().GetUserByUserRecordId(userRecordId);
        }

        public User GetUserById(long userId)
        {
            return new UserDataMapper().GetUserById(userId);
        }

        public List<User> GetAllAdminUsers()
        {
            List<User> adminUserList = new UserDataMapper().GetAllAdminAccounts();
            return adminUserList;
        }

        //public bool InsertNewRecords()
        //{
        //    bool result = false;

        //    UserDataMapper userDataMapper = new UserDataMapper();
        //    long recordId = userDataMapper.GetMaxRecordId();

        //    WS.ToolService ts = new ToolService();

        //    XmlDocument xmlDoc = new XmlDocument();

        //   // xmlDoc = ts.InsertApplicationData("AddClient", xmlDoc);
        //    string xmlNode = ts.GetApplicationNewRecords("WHRCaddclient", recordId);
        //   if (!string.IsNullOrEmpty(xmlNode))
        //   {
        //       xmlDoc.LoadXml(xmlNode);
        //       InsertUsers(xmlDoc);
        //   }
        //    return result;
        //}

        public bool InsertUsers(XmlDocument xmlDoc)
        {
            UserServiceImpl userServiceImpl = new UserServiceImpl();

            XmlNodeList recordList = xmlDoc.SelectNodes("/master/recordNode");
            foreach (XmlNode recordNode in recordList)
            {

                XmlNode emailAddressNode = recordNode.SelectNodes("valuePair[@name='EmailAddress']")[0];
                if (emailAddressNode != null)
                {
                    string email = emailAddressNode.Attributes["value"].Value;
                    if (!string.IsNullOrEmpty(email))
                    {
                        User user = userServiceImpl.GetUserByEmail(email);
                        if (user == null)
                        {
                            User userObj = new User();

                            userObj.Email = email;
                            userObj.UserType = UserType.Member;
                            Utility Util = new Utility();
                            byte[] PasswordArr = Util.Hash("123456");
                            string PasswordEnc = Util.Base64Encode(PasswordArr);

                            userObj.Password = PasswordEnc;

                            #region User properties
                            XmlNode recordIdNode = recordNode.SelectNodes("valuePair[@name='UserRecordId']")[0];
                            if (recordIdNode != null)
                            {
                                long recordId = Convert.ToInt64(recordIdNode.Attributes["value"].Value);
                                userObj.UserRecordId = recordId;
                            }

                            #region First Name
                            XmlNode firstNameNode = recordNode.SelectNodes("valuePair[@name='FirstName']")[0];
                            if (firstNameNode != null)
                            { 
                                string firstName = Convert.ToString(firstNameNode.Attributes["value"].Value);
                                userObj.FirstName = firstName;
                            }
                            else
                            {
                                userObj.FirstName = "";
                            }
                            #endregion


                            #region Last Name
                            XmlNode lastNameNode = recordNode.SelectNodes("valuePair[@name='LastName']")[0];
                            if (lastNameNode != null)
                            {
                                string value = Convert.ToString(lastNameNode.Attributes["value"].Value);
                                userObj.LastName = value;
                            }
                            else
                            {
                                userObj.FirstName = "";
                            }

                            #endregion



                            #region Middle Name
                            XmlNode middleNameNode = recordNode.SelectNodes("valuePair[@name='MiddleName']")[0];
                            if (middleNameNode != null)
                            {
                                string value = Convert.ToString(middleNameNode.Attributes["value"].Value);
                                userObj.MiddleName = value;
                            }
                            else
                            {
                                userObj.MiddleName = "";
                            }

                            #endregion

                            #region Address1
                            //Address1-Line1
                            string address1;
                            string ad1line1 = string.Empty;
                            string ad1line2 = string.Empty;
                            XmlNode Address1Line1Node = recordNode.SelectNodes("valuePair[@name='Address1-Line1']")[0];
                            if (Address1Line1Node != null)
                            {
                                ad1line1 = Convert.ToString(Address1Line1Node.Attributes["value"].Value);
                            }
                            XmlNode Address1Line2Node = recordNode.SelectNodes("valuePair[@name='Address1-Line2']")[0];
                            if (Address1Line2Node != null)
                            {
                                ad1line2 = Convert.ToString(Address1Line2Node.Attributes["value"].Value);
                            }
                            address1 = ad1line1 + "~" + ad1line2;
                            userObj.Address1 = address1;

                            #endregion
                            #region City1
                            //City 1
                            XmlNode city1Node = recordNode.SelectNodes("valuePair[@name='City']")[0];
                            if (city1Node != null)
                            {
                                string value = Convert.ToString(city1Node.Attributes["value"].Value);
                                userObj.Ad1City = value;
                            }
                            else
                            {
                                userObj.Ad1City = "";
                            }
                            #endregion

                            #region State1
                            //State 1
                            XmlNode state1Node = recordNode.SelectNodes("valuePair[@name='State']")[0];
                            if (state1Node != null)
                            {
                                string value = Convert.ToString(state1Node.Attributes["value"].Value);
                                userObj.Ad1State = value;
                            }
                            else
                            {
                                userObj.Ad1State = "";
                            }
                            #endregion

                            #region Zip1
                            //Zip 1
                            XmlNode zip1Node = recordNode.SelectNodes("valuePair[@name='Zip']")[0];
                            if (zip1Node != null)
                            {
                                string value = Convert.ToString(zip1Node.Attributes["value"].Value);
                                userObj.Ad1Zip = value;
                            }
                            else
                            {
                                userObj.Ad1State = "";
                            }
                            #endregion

                            #region Address2
                            //Address2
                            string address2;
                            string ad2line1 = string.Empty;
                            string ad2line2 = string.Empty;
                            XmlNode Address2Line1Node = recordNode.SelectNodes("valuePair[@name='Address2-Line1']")[0];
                            if (Address1Line1Node != null)
                            {
                                ad1line1 = Convert.ToString(Address2Line1Node.Attributes["value"].Value);
                            }
                            XmlNode Address2Line2Node = recordNode.SelectNodes("valuePair[@name='Address2-Line2']")[0];
                            if (Address1Line2Node != null)
                            {
                                ad2line2 = Convert.ToString(Address1Line2Node.Attributes["value"].Value);
                            }
                            address2 = ad1line1 + "~" + ad2line2;
                            userObj.Address2 = address2;

                            #endregion

                            #region City2
                            //City 2
                            XmlNode city2Node = recordNode.SelectNodes("valuePair[@name='City']")[1];
                            if (city2Node != null)
                            {
                                string value = Convert.ToString(city2Node.Attributes["value"].Value);
                                userObj.Ad2City = value;
                            }
                            else
                            {
                                userObj.Ad2City = "";
                            }
                            #endregion

                            #region State2
                            //State 2
                            XmlNode state2Node = recordNode.SelectNodes("valuePair[@name='State']")[1];
                            if (city2Node != null)
                            {
                                string value = Convert.ToString(city2Node.Attributes["value"].Value);
                                userObj.Ad2State = value;
                            }
                            else
                            {
                                userObj.Ad2State = "";
                            }
                            #endregion

                            #region Zip2
                            //Zip 2
                            XmlNode zip2Node = recordNode.SelectNodes("valuePair[@name='Zip']")[1];
                            if (zip2Node != null)
                            {
                                string value = Convert.ToString(zip2Node.Attributes["value"].Value);
                                userObj.Ad2Zip = value;
                            }
                            else
                            {
                                userObj.Ad2Zip = "";
                            }
                            #endregion


                            #region Date Of Birth
                            //Date of Birth
                            XmlNode dobNode = recordNode.SelectNodes("valuePair[@name='DateofBirth']")[0];
                            if (dobNode != null)
                            {
                                string value = Convert.ToString(dobNode.Attributes["value"].Value);
                                DateTime dateConvert;
                                if (DateTime.TryParse(value, out dateConvert))
                                {
                                    userObj.DateOfBirth = Convert.ToDateTime(value);
                                }
                                else
                                {
                                    userObj.DateOfBirth = Convert.ToDateTime("1/1/1900");
                                }
                            }
                            else
                            {
                                userObj.DateOfBirth = Convert.ToDateTime("1/1/1900");
                            }
                            #endregion

                            #region Due Date
                            //Due Date
                            XmlNode dueDateNode = recordNode.SelectNodes("valuePair[@name='DueDate']")[0];
                            if (dueDateNode != null)
                            {
                                string value = Convert.ToString(dueDateNode.Attributes["value"].Value);
                                DateTime dateConvert;
                                if (DateTime.TryParse(value, out dateConvert))
                                {
                                    userObj.DueDate = Convert.ToDateTime(value);
                                }
                                else
                                {
                                    userObj.DueDate = Convert.ToDateTime("1/1/1900");
                                }                                
                              
                            }
                            else
                            {
                                userObj.DueDate = Convert.ToDateTime("1/1/1900");
                            }
                            #endregion

                            #region Insurance
                            //Insurance
                            XmlNode insuranseNode = recordNode.SelectNodes("valuePair[@name='Insurance']")[0];
                            if (insuranseNode != null)
                            {
                                string value = Convert.ToString(insuranseNode.Attributes["value"].Value);
                                userObj.Insurance = value;
                            }
                            else
                            {
                                userObj.Insurance = "";
                            }
                            #endregion

                            #region Hospital
                            //Hospital
                            XmlNode hospitalNode = recordNode.SelectNodes("valuePair[@name='Hospital']")[1];
                            if (hospitalNode != null)
                            {
                                string value = Convert.ToString(hospitalNode.Attributes["value"].Value);
                                userObj.Hospital = value;
                            }
                            else
                            {
                                userObj.Hospital = "";
                            }
                            #endregion

                            #region Hospital practice
                            //Hospital practice
                            XmlNode hospitalPracticeNode = recordNode.SelectNodes("valuePair[@name='Hospital']")[1];
                            if (hospitalNode != null)
                            {
                                string value = Convert.ToString(hospitalPracticeNode.Attributes["value"].Value);
                                userObj.HospitalPractice = value;
                            }
                            else
                            {
                                userObj.HospitalPractice = "";
                            }
                            #endregion

                            #region Notes
                            //Notes
                            XmlNode notesNode = recordNode.SelectNodes("valuePair[@name='Notes']")[0];
                            if (notesNode != null)
                            {
                                string value = Convert.ToString(notesNode.Attributes["value"].Value);
                                userObj.Notes = value;
                            }
                            else
                            {
                                userObj.Notes = "";
                            }
                            #endregion

                            #region Area code
                            //Area code
                            XmlNode areaCodeNode = recordNode.SelectNodes("valuePair[@name='Notes']")[0];
                            if (areaCodeNode != null)
                            {
                                string value = Convert.ToString(areaCodeNode.Attributes["value"].Value);
                                userObj.AreaCode = value;
                            }
                            else
                            {
                                userObj.AreaCode = "";
                            }
                            #endregion

                            #region Phone
                            //Phone
                            XmlNode phoneNode = recordNode.SelectNodes("valuePair[@name='Phone']")[0];
                            if (phoneNode != null)
                            {
                                string value = Convert.ToString(phoneNode.Attributes["value"].Value);
                                userObj.phone = value;
                            }
                            else
                            {
                                userObj.phone = "";
                            }

                            #endregion
                            #region PhoneType
                            //Phone type
                            XmlNode phoneTypeNode = recordNode.SelectNodes("valuePair[@name='PhoneType']")[0];
                            if (phoneTypeNode != null)
                            {
                                string value = Convert.ToString(phoneTypeNode.Attributes["value"].Value);
                                userObj.phoneType = value;
                            }
                            else
                            {
                                userObj.phoneType = "";
                            }
                            #endregion

                            #region Phone Notes
                            //Phone notes
                            XmlNode phoneNotesNode = recordNode.SelectNodes("valuePair[@name='Notes']")[1];
                            if (phoneNotesNode != null)
                            {
                                string value = Convert.ToString(phoneNotesNode.Attributes["value"].Value);
                                userObj.PhoneNotes = value;
                            }
                            else
                            {
                                userObj.PhoneNotes = "";
                            }
                            #endregion

                            userObj.Active = 1;

                            InsertUser(userObj);
                            #endregion

                        }
                    }
                }
            }

           



            return false;
        }

        public long GetMaxRecordId()
        {
            UserDataMapper userDataMapper = new UserDataMapper();
            long recordId = userDataMapper.GetMaxRecordId();
            return recordId;
        }

        public List<User> GetUserListBySearchInput(string searchInput)
        {
            UserDataMapper userDataMapper = new UserDataMapper();
            return userDataMapper.SearchUserByInput(searchInput);
        }

        public void DeleteUserById(long id)
        {
            new UserDataMapper().DeleteUserById(id);
        }

        public bool CheckAllMandatoryFields(long userId)
        {
            bool result = false;
            User userObj = GetUserById(userId);
            if (userObj != null)
            {
                string[] address1 = new string[2];
                if (!string.IsNullOrEmpty(userObj.Address1))
                {
                    address1 = userObj.Address1.Split('~');
                    
                }
                if (!string.IsNullOrEmpty(userObj.FirstName)
                    && !string.IsNullOrEmpty(userObj.LastName)
                    && !string.IsNullOrEmpty(address1[0])
                    && !string.IsNullOrEmpty(userObj.Ad1City)
                    && !string.IsNullOrEmpty(userObj.Ad1State)
                    && !string.IsNullOrEmpty(userObj.Ad1Zip)
                    && !string.IsNullOrEmpty(userObj.Email)
                    && !string.IsNullOrEmpty(userObj.AreaCode)
                    && !string.IsNullOrEmpty(userObj.phone)
                    )
                {
                    result = true;
                }

            }
            return result;
        }

        #region Visitor
        public List<User> GetAllVisitors()
        {
            List<User> visitorList = new UserDataMapper().GetAllVisitors();
            return visitorList;
        }
        public List<User> GetWalkInListBySearchInput(string searchInput)
        {
            UserDataMapper userDataMapper = new UserDataMapper();
            return userDataMapper.SearchVisitorByInput(searchInput);
        }

        #endregion

        public void UpdateUserStatusById(long id)
        {
            User UserObj = new User();
            UserObj.Id = id;
            UserObj.UserStatus = UserStatus.Inactive;
            new UserDataMapper().UpdateUserStatus(UserObj);
        }

    }
}