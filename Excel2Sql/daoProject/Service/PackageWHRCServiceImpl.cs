﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using daoProject.Model;
using daoProject.Utilities;
using daoProject.Mapper;
using daoProject.Infrastructure;

namespace daoProject.Service
{
    public class PackageWHRCServiceImpl
    {
         private PackageWHRCDataMapper dataMapper;

         public PackageWHRCServiceImpl()
         {
             dataMapper = new PackageWHRCDataMapper();
         }

         public void Add(PackageWHRC item)
         {
             using (System.Data.Odbc.OdbcConnection conn = daoProject.Infrastructure.DBConnection.GetConnection())
             {
                 conn.ConnectionTimeout = ConstantsGlobal.ConnectionTimeoutBypass;
                 conn.Open();
                 System.Data.Odbc.OdbcTransaction tx = conn.BeginTransaction();

                 try
                 {
                     dataMapper.Create(item, conn, tx);
                     tx.Commit();
                 }
                 catch (Exception ex1)
                 {
                     try
                     {
                         tx.Rollback();

                         throw ex1;
                     }
                     catch (Exception ex2)
                     {

                     }
                 }
             }
         }

         public List<PackageWHRC> GetAll()
         {
             return dataMapper.GetAll();
         }

         public List<PackageWHRC> GetAllPackageByPackageType(PackageType packageType)
         {
             return dataMapper.GetAll(packageType);
         }

         public PackageWHRC GetPackageById(long id)
         {
             return dataMapper.GetById(id);  
         }

         public void Update(PackageWHRC item)
         {
             using (System.Data.Odbc.OdbcConnection conn = daoProject.Infrastructure.DBConnection.GetConnection())
             {
                 conn.ConnectionTimeout = ConstantsGlobal.ConnectionTimeoutBypass;
                 conn.Open();
                 System.Data.Odbc.OdbcTransaction tx = conn.BeginTransaction();

                 try
                 {
                     dataMapper.Update(item, conn, tx);
                     tx.Commit();
                 }
                 catch (Exception ex1)
                 {
                     try
                     {
                         tx.Rollback();

                         throw ex1;
                     }
                     catch (Exception ex2)
                     {

                     }
                 }
             }
         }

         public void Delete(long id)
         {
             dataMapper.Delete(id);
         }

         public List<PackageWHRC> GetPackageByPackageItemRecordId(long recordId)
         {
             List<PackageWHRC> packageWhrcList = dataMapper.GetPackageById(recordId);
             return packageWhrcList;
         }
    }
}
