﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using daoProject.Model;
using ReportLibrary.Infrastructure;
using daoProject.Mapper;
using System.Drawing;
using System.IO;
using System.Drawing.Imaging;

namespace daoProject.Service
{
    public class ReportElementGenerator
    {
        public DataTable GetTablularReportColumnList(List<string> tableNameList, List<KeyValuePair<string, string>> keyRemoveList)
        {
            DataTable dt = new DataTable();
            Reportobject reportObject = new Reportobject();
            Dictionary<string, string> item = reportObject.GetAllAttributesByClassObjectName(tableNameList);
            List<KeyValuePair<string, string>> list = item.ToList();
            if (list.Count > 0)
            {
                dt.Columns.Add("Key");
                dt.Columns.Add("Value");
                dt.Columns.Add("Sorting");
                dt.Columns.Add("AggregateFunction");
                if (keyRemoveList != null && keyRemoveList.Count > 0)
                {
                    foreach (KeyValuePair<string, string> removePair in keyRemoveList)
                    {
                        list.Remove(removePair);
                    }
                }

                foreach (KeyValuePair<string, string> pair in list)
                {
                    DataRow dr = dt.NewRow();
                    dr["Key"] = pair.Key;
                    dr["Value"] = pair.Value;
                    dr["Sorting"] = "Sorting";
                    dr["AggregateFunction"] = "";
                    dt.Rows.Add(dr);
                }
            }
            return dt;
        }

        public DataTable GetDataTableForPopulateGroupingGridView()
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("RowNumber");
            dt.Columns.Add("ddlMatriceColumnGrp");
           
            DataRow dr = dt.NewRow();
            dr["RowNumber"] = 1;
            dr["ddlMatriceColumnGrp"] = "";
          
            dt.Rows.Add(dr);

            return dt;
        }

        public DataTable GetDataTableForAggregateFuction()
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("AggregateFunction");
            string[] expr = Enum.GetNames(typeof(ReportExpressions));
            foreach (string expression in expr)
            {
                DataRow drForDropDownlist = dt.NewRow();
                drForDropDownlist["AggregateFunction"] = expression;
                dt.Rows.Add(drForDropDownlist);
            }
            return dt;
        }

        public bool CheckNameAlreadyExist(string name)
        {
            bool exists = false;
            List<ReportDetails> ReportsList = new ReportGenerationDataMapper().GetAllReports();
            if (ReportsList.Count > 0)
            {
                ReportDetails reportDetails = ReportsList.FirstOrDefault(x => x.ReportTitle.ToLower().Replace(" ", "").Trim() == name.ToLower().Replace(" ", "").Trim());
                if (reportDetails != null)
                {
                    exists = true;
                }
            }
            return exists;
        }
        public DataTable GetDataTableForPopulateTableList(bool isGridView)
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("Key");
            dt.Columns.Add("Value");
            if (isGridView)
            {
                DataRow dr = dt.NewRow();
                dr["Key"] = "";
                dr["Value"] = "";
                dt.Rows.Add(dr);
            }
            return dt;
        }
        public DataTable GetDataTableForPopulateTableListForDDL()
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("Key");
            dt.Columns.Add("Value");
           
            Reportobject reportObject = new Reportobject();
            Dictionary<string, string> JijotyTable = reportObject.JijotyClassObjects;
            Dictionary<string, string> WhrcDBTable = reportObject.WhrcDBClassObjects;
            List<KeyValuePair<string, string>> list = JijotyTable.Concat(WhrcDBTable).ToList();
            foreach (KeyValuePair<string, string> pair in list)
            {
                DataRow dr = dt.NewRow();
                dr["Key"] = pair.Key;
                dr["Value"] = pair.Value;
                dt.Rows.Add(dr);
            }
            return dt;
        }
        public string ConvertImageIntoStingType(Bitmap imageBit)
        {
            MemoryStream imageStream = new MemoryStream();
            imageBit.Save(imageStream, ImageFormat.Png);
            return Convert.ToBase64String(imageStream.ToArray());
        }

        public Dictionary<string, FieldDataType> GetReportFieldDataType(List<string> fieldList)
        {
            Dictionary<string, FieldDataType> ReportFieldDataType = new Dictionary<string, FieldDataType>();

            Reportobject reportObject = new Reportobject();
            List<KeyValuePair<string, FieldDataType>> list = reportObject.FieldFormat.ToList();

            if (list.Count > 1)
            {
                foreach (KeyValuePair<string, FieldDataType> pair in list)
                {
                    if (fieldList.Count > 0)
                    {
                        foreach (string field in fieldList)
                        {
                            if (pair.Key.Equals(field))
                            {
                                ReportFieldDataType.Add(pair.Key, pair.Value);
                            }
                        }
                    }
                    else
                    {
                        break;
                    }
                }
            }
            return ReportFieldDataType;
        }
    }
}
