﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace daoProject.Service
{
    public class AmazonPaymentsHelper : SignatureUtilities
    {
        public static string SIGNATURE_VERSION_2= "2";
        public static string HMAC_SHA256_ALGORITHM = "HmacSHA256";
        public static string SIGNATURE_VERSION_KEYNAME = "signatureVersion";
        public static string SIGNATURE_METHOD_KEYNAME = "signatureMethod";
        public static String SignParameters(IDictionary<String, String> parameters, String key, String HttpMethod, String Host, String RequestURI)
        {
            String algorithm = "HmacSHA256";
            return signParameters(parameters,  key,  HttpMethod,  Host,  RequestURI,algorithm);
        }
    }
}
