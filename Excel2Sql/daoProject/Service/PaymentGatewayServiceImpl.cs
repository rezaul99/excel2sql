﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using daoProject.Mapper;
using daoProject.Model;

namespace daoProject.Service
{
    public class PaymentGatewayServiceImpl
    {
        PaymentGatewayDataMapper paymentGatewayDataMapper = null;

        public List<PaymentGatewayDetail> GetAll()
        {
            paymentGatewayDataMapper = new PaymentGatewayDataMapper();
            return paymentGatewayDataMapper.GetAll();
        }
    }
}
