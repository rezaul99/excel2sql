﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using daoProject.Model;
using System.Data;
using System.Xml;

namespace daoProject.Service
{
    public class ProductServiceImpl
    {

        #region Product Category

        public XmlDocument GetProductCaterogyXmlFromObject(ProductCategoryWhrc productCategoryWhrc)
        {
            DataTable dt = new DataTable();
            DataColumn dc1 = new DataColumn("Name");
            DataColumn dc2 = new DataColumn("Value");

            dt.Columns.Add(dc1);
            dt.Columns.Add(dc2);

            #region product Category Properties

            AddDataRow("ProductCategoryName", productCategoryWhrc.ProductCategoryName, dt);
            #endregion
            XmlDocument xmlDoc = new XmlDocument();
            DataSet dataSet = new DataSet();
            dataSet.Tables.Add(dt);
            string strXml = dataSet.GetXml();
            xmlDoc.LoadXml(strXml);

            return xmlDoc;
        }

        public List<ProductCategoryWhrc> GetAllProductCategoryWhrc(XmlNode xmlNode)
        {
            List<ProductCategoryWhrc> productCategoryList = new List<ProductCategoryWhrc>();
            ProductCategoryWhrc productCategoryWhrc = null;

            if (xmlNode != null)
            {
                XmlNodeList datas = xmlNode.ChildNodes;
                for (int i = 0; i < datas.Count; i++)
                {
                    List<XmlNode> nodeList = new List<XmlNode>(datas[i].ChildNodes.Cast<XmlNode>());
                    XmlNode recordIDNode = (from item in nodeList
                                            where item.Name.Replace(" ", "") == "RecordId"
                                            select item).FirstOrDefault();
                    XmlNode productCategoryNode = (from item in nodeList
                                                   where item.Name.Replace(" ", "") == "ProductCategoryName"
                                                   select item).FirstOrDefault();

                    productCategoryWhrc = new ProductCategoryWhrc();
                    if (recordIDNode != null)
                    {
                        productCategoryWhrc.RecordId = Convert.ToInt64(recordIDNode.InnerText);
                    }
                    if (productCategoryNode != null)
                    {
                        productCategoryWhrc.ProductCategoryName = productCategoryNode.InnerText;
                    }
                    productCategoryList.Add(productCategoryWhrc);
                }
            }

            return productCategoryList;
        }

        private void AddDataRow(string name, string value, DataTable dt)
        {
            DataRow dr = dt.NewRow();
            dr["Name"] = name;
            dr["Value"] = value;
            dt.Rows.Add(dr);
        }

        #endregion

        #region Product Type

        public XmlDocument GetProductTypeXmlFromObject(ProductTypeWHRC productTypeWHRC)
        {
            DataTable dt = new DataTable();
            DataColumn dc1 = new DataColumn("Name");
            DataColumn dc2 = new DataColumn("Value");

            dt.Columns.Add(dc1);
            dt.Columns.Add(dc2);

            #region User Properties
            AddDataRow("ProductTypeName", productTypeWHRC.Name, dt);
            #endregion
            XmlDocument xmlDoc = new XmlDocument();
            DataSet dataSet = new DataSet();
            dataSet.Tables.Add(dt);
            string strXml = dataSet.GetXml();
            xmlDoc.LoadXml(strXml);

            return xmlDoc;
        }

        public List<ProductTypeWHRC> GetAllProductTypeWHRC(XmlNode xmlNode)
        {
            List<ProductTypeWHRC> productTypeList = new List<ProductTypeWHRC>();
            ProductTypeWHRC productTypeWhrc = null;

            if (xmlNode != null)
            {
                XmlNodeList datas = xmlNode.ChildNodes;
                for (int i = 0; i < datas.Count; i++)
                {
                    List<XmlNode> nodeList = new List<XmlNode>(datas[i].ChildNodes.Cast<XmlNode>());
                    XmlNode recordIDNode = (from item in nodeList
                                            where item.Name.Replace(" ", "") == "RecordId"
                                            select item).FirstOrDefault();
                    XmlNode productCategoryNode = (from item in nodeList
                                                   where item.Name.Replace(" ", "") == "ProductTypeName"
                                                   select item).FirstOrDefault();

                    productTypeWhrc = new ProductTypeWHRC();
                    if (recordIDNode != null)
                    {
                        productTypeWhrc.RecordId = Convert.ToInt64(recordIDNode.InnerText);
                    }
                    if (productCategoryNode != null)
                    {
                        productTypeWhrc.Name = productCategoryNode.InnerText;
                    }
                    productTypeList.Add(productTypeWhrc);
                }
            }

            return productTypeList;
        }

        #endregion

        #region Product Item

        public XmlDocument GetProductItemXmlFromObject(ProductItemWHRC productItemWHRC)
        {
            DataTable dt = new DataTable();
            DataColumn dc1 = new DataColumn("Name");
            DataColumn dc2 = new DataColumn("Value");

            dt.Columns.Add(dc1);
            dt.Columns.Add(dc2);

            #region product Item Properties

            //AddDataRow("ProductType", productItemWHRC.ProductType, dt);
            AddDataRow("ProductCategory", productItemWHRC.ProductCategory, dt);
            AddDataRow("ItemName", productItemWHRC.ItemName, dt);
            AddDataRow("SalePrice", Convert.ToString(productItemWHRC.SalePrice), dt);
            AddDataRow("Taxable", Convert.ToString(productItemWHRC.Taxable == true ? "Yes" : "No"), dt);
            AddDataRow("QuantityInStock",Convert.ToString(productItemWHRC.QuantityInStock), dt);
            AddDataRow("ReorderLevel", productItemWHRC.ReorderLevel, dt);
            AddDataRow("VendorName", productItemWHRC.VendorName, dt);
            AddDataRow("VendorItemNumber", productItemWHRC.VendorItemNumber, dt);
            AddDataRow("VendorPrice", Convert.ToString(productItemWHRC.VendorPrice), dt);
            AddDataRow("LastModifiedDate",Convert.ToString(productItemWHRC.LastModifiedDate), dt);
            AddDataRow("CreateDate", Convert.ToString(productItemWHRC.CreateDate), dt);
            AddDataRow("StaffInitials", productItemWHRC.StaffInitials, dt);
           //AddDataRow("Eligibleforshipping", Convert.ToString(productItemWHRC.EligibleForShipping), dt);
            AddDataRow("Eligibleforshipping", (productItemWHRC.EligibleForShipping == true ? "Yes" : "No"), dt);
            if (productItemWHRC.StatusOfProduct)
            {
                AddDataRow("StatusofProduct", "Active", dt);
            }
            else
            {
                AddDataRow("StatusofProduct", "Inactive", dt);
            }
            AddDataRow("ProductDescription", Convert.ToString(productItemWHRC.Description), dt);
            AddDataRow("ProductImageURL", Convert.ToString(productItemWHRC.ImgLink), dt);
            AddDataRow("ThumbnailURL", Convert.ToString(productItemWHRC.thumbnailUrl), dt);
            #endregion
            XmlDocument xmlDoc = new XmlDocument();
            DataSet dataSet = new DataSet();
            dataSet.Tables.Add(dt);
            string strXml = dataSet.GetXml();
            xmlDoc.LoadXml(strXml);

            return xmlDoc;
        }

        public List<ProductItemWHRC> GetAllProductItemWhrc(XmlNode xmlNode)
        {
            List<ProductItemWHRC> productItemList = new List<ProductItemWHRC>();
            ProductItemWHRC productItemWhrc = null;

            if (xmlNode != null)
            {
                XmlNodeList datas = xmlNode.ChildNodes;
                for (int i = 0; i < datas.Count; i++)
                {
                    List<XmlNode> nodeList = new List<XmlNode>(datas[i].ChildNodes.Cast<XmlNode>());
                    XmlNode recordIDNode = (from item in nodeList
                                            where item.Name.Replace(" ", "") == "RecordId"
                                            select item).FirstOrDefault();
                    //XmlNode productTypeNode = (from item in nodeList
                    //                           where item.Name.Replace(" ", "") == "ProductType"
                    //                               select item).FirstOrDefault();
                    XmlNode productCategoryNode = (from item in nodeList
                                                   where item.Name.Replace(" ", "") == "ProductCategory"
                                                   select item).FirstOrDefault();
                    XmlNode itemNameNode = (from item in nodeList
                                                   where item.Name.Replace(" ", "") == "ItemName"
                                                   select item).FirstOrDefault();
                    XmlNode salePriceNode = (from item in nodeList
                                                   where item.Name.Replace(" ", "") == "SalePrice"
                                                   select item).FirstOrDefault();
                    XmlNode taxableNode = (from item in nodeList
                                                   where item.Name.Replace(" ", "") == "Taxable"
                                                   select item).FirstOrDefault();
                    XmlNode quantityInStockNode = (from item in nodeList
                                                   where item.Name.Replace(" ", "") == "QuantityInStock"
                                                   select item).FirstOrDefault();
                    XmlNode reorderLevelNode = (from item in nodeList
                                                   where item.Name.Replace(" ", "") == "ReorderLevel"
                                                   select item).FirstOrDefault();
                    XmlNode vendorNameNode = (from item in nodeList
                                                   where item.Name.Replace(" ", "") == "VendorName"
                                                   select item).FirstOrDefault();
                    XmlNode vendorItemNumberNode = (from item in nodeList
                                                   where item.Name.Replace(" ", "") == "VendorItemNumber"
                                                   select item).FirstOrDefault();
                    XmlNode vendorPriceNode = (from item in nodeList
                                                   where item.Name.Replace(" ", "") == "VendorPrice"
                                                   select item).FirstOrDefault();
                    XmlNode lastModifiedDateNode = (from item in nodeList
                                                   where item.Name.Replace(" ", "") == "LastModifiedDate"
                                                   select item).FirstOrDefault();
                    XmlNode createDateNode = (from item in nodeList
                                                   where item.Name.Replace(" ", "") == "CreateDate"
                                                   select item).FirstOrDefault();
                    XmlNode staffInitialsNode = (from item in nodeList
                                                   where item.Name.Replace(" ", "") == "StaffInitials"
                                                   select item).FirstOrDefault();
                    XmlNode eligibleforShippingNode =  (from item in nodeList
                                                       where item.Name.Replace(" ", "") == "Eligibleforshipping"
                                                       select item).FirstOrDefault();
                    XmlNode statusNode = (from item in nodeList
                                          where item.Name.Replace(" ", "") == "StatusofProduct"
                                                       select item).FirstOrDefault();
                    XmlNode productDescriptionNode = (from item in nodeList
                                                  where item.Name.Replace(" ", "") == "ProductDescription"
                                          select item).FirstOrDefault();
                    XmlNode productImageUrlNode = (from item in nodeList
                                                   where item.Name.Replace(" ", "") == "ProductImageURL"
                                                 select item).FirstOrDefault();
                    XmlNode thumbnailURLNode = (from item in nodeList
                                                where item.Name.Replace(" ", "") == "ThumbnailURL"
                                                 select item).FirstOrDefault();


                    productItemWhrc = new ProductItemWHRC();
                    if (recordIDNode != null)
                    {
                        productItemWhrc.RecordId = Convert.ToInt64(recordIDNode.InnerText);
                    }
                    //if (productTypeNode != null)
                    //{
                    //    productItemWhrc.ProductType = productTypeNode.InnerText;
                    //}
                    if (productCategoryNode != null)
                    {
                        productItemWhrc.ProductCategory = productCategoryNode.InnerText;
                    }
                    if (itemNameNode != null)
                    {
                        productItemWhrc.ItemName = itemNameNode.InnerText;
                    }
                    if (salePriceNode != null)
                    {
                        productItemWhrc.SalePrice = Convert.ToDecimal(salePriceNode.InnerText.Replace("$",""));
                    }
                    if (taxableNode != null)
                    {
                        if (taxableNode.InnerText == "No")
                        {
                            productItemWhrc.Taxable = false; ;
                        }
                        else
                        {
                            productItemWhrc.Taxable = true;
                        }
                    }
                    if (quantityInStockNode != null)
                    {
                        productItemWhrc.QuantityInStock = quantityInStockNode.InnerText;
                        productItemWhrc.QuantityInStockInt =Convert.ToInt32(productItemWhrc.QuantityInStock);
                    }
                    if (reorderLevelNode != null)
                    {
                        productItemWhrc.ReorderLevel = reorderLevelNode.InnerText;
                    }
                    if (vendorNameNode != null)
                    {
                        productItemWhrc.VendorName = vendorNameNode.InnerText;
                    }
                    if (vendorItemNumberNode != null)
                    {
                        productItemWhrc.VendorItemNumber = vendorItemNumberNode.InnerText;
                    }
                    if (vendorPriceNode != null)
                    {
                        if (!string.IsNullOrEmpty(vendorPriceNode.InnerText))
                        {
                            productItemWhrc.VendorPrice = Convert.ToDecimal(vendorPriceNode.InnerText.Replace("$",""));
                        }
                        else
                        {
                            productItemWhrc.VendorPrice = 0;
                        }
                    }
                    if (lastModifiedDateNode != null)
                    {
                        if(!string.IsNullOrEmpty(lastModifiedDateNode.InnerText))
                        {
                            DateTime dateValue;
                            if (DateTime.TryParse(lastModifiedDateNode.InnerText, out dateValue))
                            {
                                productItemWhrc.LastModifiedDate = Convert.ToDateTime(lastModifiedDateNode.InnerText);
                            }
                            else
                            {
                                productItemWhrc.LastModifiedDate = Convert.ToDateTime("1/1/1900 12:00:00 AM");
                            }
                        }
                    }
                    if (createDateNode != null)
                    {
                        //productItemWhrc.CreateDate = Convert.ToDateTime(createDateNode.InnerText);
                        if (!string.IsNullOrEmpty(createDateNode.InnerText))
                        {
                            DateTime dateValue;
                            if (DateTime.TryParse(createDateNode.InnerText, out dateValue))
                            {
                                productItemWhrc.CreateDate = Convert.ToDateTime(createDateNode.InnerText);
                            }
                            else
                            {
                                productItemWhrc.CreateDate = Convert.ToDateTime("1/1/1900 12:00:00 AM");
                            }
                        }
                    }
                    if (staffInitialsNode != null)
                    {
                        productItemWhrc.StaffInitials = staffInitialsNode.InnerText;
                    }
                    if (eligibleforShippingNode != null)
                    {

                        productItemWhrc.EligibleForShipping = (eligibleforShippingNode.InnerText == "Yes" ? true : false);
                    }
                    if (statusNode != null)
                    {
                        if (statusNode.InnerText == "Active")
                        {
                            productItemWhrc.StatusOfProduct = true;
                        }
                        else if (statusNode.InnerText == "Inactive")
                        {
                            productItemWhrc.StatusOfProduct = false;
                        }
                        else
                        {
                            productItemWhrc.StatusOfProduct = false;
                        }
                    }
                    if (productDescriptionNode != null)
                    {
                        productItemWhrc.Description = productDescriptionNode.InnerText;
                    }
                    if (productImageUrlNode != null)
                    {
                        productItemWhrc.ImgLink = productImageUrlNode.InnerText;
                    }
                    if (thumbnailURLNode != null)
                    {
                        productItemWhrc.thumbnailUrl = thumbnailURLNode.InnerText;
                    }
                    productItemList.Add(productItemWhrc);
                }
            }

            return productItemList;
        }

        #endregion

    }
}
