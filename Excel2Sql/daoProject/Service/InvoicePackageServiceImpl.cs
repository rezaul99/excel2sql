﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using daoProject.Model;
using daoProject.Utilities;
using daoProject.Mapper;
using System.Data;

namespace daoProject.Service
{
    public class InvoicePackageServiceImpl
    {
        private InvoicePackageDataMapper dataMapper;

        public InvoicePackageServiceImpl()
        {
            dataMapper = new InvoicePackageDataMapper();
        }

        public void Add(InvoicePackage item)
        {
            dataMapper.Create(item);
        }

        public void Add(InvoicePackage item, System.Data.Odbc.OdbcConnection conn, System.Data.Odbc.OdbcTransaction tx)
        {
            dataMapper.Create(item, conn, tx);
        }

        public void Update(InvoicePackage item)
        {
            dataMapper.Update(item);
        }

        public bool Delete(long id)
        {
            return dataMapper.Delete(id);
        }

        public List<InvoicePackage> GetAll()
        {
            return dataMapper.GetAll();
        }

        
        public InvoicePackage GetById(long id)
        {
            return dataMapper.GetById(id);
        }

        public List<InvoicePackage> GetListByInvoiceId(long invoiceId)
        {
            return dataMapper.GetListByInvoiceId(invoiceId);
        }

        public bool DeleteByInvoiceId(long invoiceId)
        {
            return dataMapper.DeleteByInvoiceId(invoiceId);
        }


        public decimal CalculatePackageDiscount(Invoice invoice, List<ClassWHRC> classList, List<Section> sectionList, List<ProductItemWHRC> productList)
        {
            decimal packageDiscount = 0;

            List<PackageWHRC> packageWHRCList = new PackageWHRCServiceImpl().GetAll();
            if (packageWHRCList.Count > 0)
            {
                List<PackageWHRC> packageWHRCListClass = packageWHRCList.Where(x => (x.PackageType == daoProject.Infrastructure.PackageType.Class) && (x.Active == true)).ToList();
                List<PackageWHRC> packageWHRCListProduct = packageWHRCList.Where(x => (x.PackageType == daoProject.Infrastructure.PackageType.Product) && (x.Active == true)).ToList();

                if (invoice.ClassInvoiceItems.Count > 0)
                {
                    foreach (PackageWHRC package in packageWHRCListClass)
                    {
                        int packageIteration = PackageItreationForClass(package, invoice, classList, sectionList);
                        if (packageIteration > 0)
                        {
                            packageDiscount += packageIteration * package.DiscountAmount;
                        }
                    }
                }
                if (invoice.ProductInvoiceItems.Count > 0)
                {
                    foreach (PackageWHRC package in packageWHRCListProduct)
                    {
                        int packageIteration = PackageItreationForProduct(package, invoice, productList);
                        if (packageIteration > 0)
                        {
                            packageDiscount += packageIteration * package.DiscountAmount;
                        }
                    }
                }
            }
            return packageDiscount;
        }

        public decimal CalculatePackageDiscountForClass(Invoice invoice, List<ClassWHRC> classList, List<Section> sectionList)
        {
            decimal packageDiscount = 0;

            List<PackageWHRC> packageWHRCList = new PackageWHRCServiceImpl().GetAll();
            if (packageWHRCList.Count > 0)
            {
                List<PackageWHRC> packageWHRCListClass = packageWHRCList.Where(x => (x.PackageType == daoProject.Infrastructure.PackageType.Class) && (x.Active == true)).ToList();
                List<PackageWHRC> packageWHRCListProduct = packageWHRCList.Where(x => (x.PackageType == daoProject.Infrastructure.PackageType.Product) && (x.Active == true)).ToList();

                if (invoice.ClassInvoiceItems.Count > 0)
                {
                    foreach (PackageWHRC package in packageWHRCListClass)
                    {
                        int packageIteration = PackageItreationForClass(package, invoice, classList, sectionList);
                        if (packageIteration > 0)
                        {
                            packageDiscount += packageIteration * package.DiscountAmount;
                        }
                    }
                }
                //if (invoice.ProductInvoiceItems.Count > 0)
                //{
                //    foreach (PackageWHRC package in packageWHRCListProduct)
                //    {
                //        int packageIteration = PackageItreationForProduct(package, invoice, productList);
                //        if (packageIteration > 0)
                //        {
                //            packageDiscount += packageIteration * package.DiscountAmount;
                //        }
                //    }
                //}
            }
            return packageDiscount;
        }

        private int PackageItreationForClass(PackageWHRC package, Invoice invoice, List<ClassWHRC> classList, List<Section> sectionList)
        {
            int packageIteration = 0;

            List<int> packageItemList = new List<int>();
            foreach (PackageItemWHRC item in package.PackageItemsWHRC)
            {
                packageItemList.Add(0);
            }

            int packageItemCount = 0;
            foreach (PackageItemWHRC item in package.PackageItemsWHRC)
            {
                long classID = item.RecordId;
                ClassWHRC classwhrc = classList.Where(x => x.RecordID == item.RecordId).FirstOrDefault();
                List<Section> sectionForThisClass = sectionList.Where(x => x.ClassTitle == classwhrc.Title).ToList();

                foreach (ClassInvoiceItem classInvoiceItem in invoice.ClassInvoiceItems)
                {
                    foreach (Section section in sectionForThisClass)
                    {
                        if (classInvoiceItem.SectionId == section.RecordID)
                        {
                            packageItemList[packageItemCount] = packageItemList[packageItemCount] + 1;
                            break;
                        }
                    }
                }
                packageItemCount++;
            }

            packageItemList.Sort();
            packageIteration = packageItemList[0];

            return packageIteration;
        }

        private int PackageItreationForProduct(PackageWHRC package, Invoice invoice, List<ProductItemWHRC> productList)
        {
            int packageIteration = 0;

            List<int> packageItemList = new List<int>();
            foreach (PackageItemWHRC item in package.PackageItemsWHRC)
            {
                packageItemList.Add(0);
            }

            int packageItemCount = 0;
            foreach (PackageItemWHRC item in package.PackageItemsWHRC)
            {
                long productID = item.RecordId;
                foreach (ProductInvoiceItem productInvoiceItem in invoice.ProductInvoiceItems)
                {
                    if (item.RecordId == productInvoiceItem.ProductId)
                    {
                        int childpackageIteration = productInvoiceItem.Quantity/item.Quantity;
                        packageItemList[packageItemCount] = childpackageIteration;
                        break;
                    }
                }
                packageItemCount++;
            }

            packageItemList.Sort();
            packageIteration = packageItemList[0];

            return packageIteration;
        }

        public List<PackageWHRC> GetPackageNamesOnSingleInvoice(Invoice invoice, List<ClassWHRC> classList, List<Section> sectionList, List<ProductItemWHRC> productList)
        {
            //decimal packageDiscount = 0;
            List<PackageWHRC> packageNameList = new List<PackageWHRC>();

            List<PackageWHRC> packageWHRCList = new PackageWHRCServiceImpl().GetAll();
            if (packageWHRCList.Count > 0)
            {
                List<PackageWHRC> packageWHRCListClass = packageWHRCList.Where(x => (x.PackageType == daoProject.Infrastructure.PackageType.Class) && (x.Active == true)).ToList();
                List<PackageWHRC> packageWHRCListProduct = packageWHRCList.Where(x => (x.PackageType == daoProject.Infrastructure.PackageType.Product) && (x.Active == true)).ToList();

                if (invoice.ClassInvoiceItems.Count > 0)
                {
                    foreach (PackageWHRC package in packageWHRCListClass)
                    {
                        int packageIteration = PackageItreationForClass(package, invoice, classList, sectionList);
                        if (packageIteration > 0)
                        {
                            //packageDiscount += packageIteration * package.DiscountAmount;
                            packageNameList.Add(package);
                        }
                    }
                }
                if (invoice.ProductInvoiceItems.Count > 0)
                {
                    foreach (PackageWHRC package in packageWHRCListProduct)
                    {
                        int packageIteration = PackageItreationForProduct(package, invoice, productList);
                        if (packageIteration > 0)
                        {
                            //packageDiscount += packageIteration * package.DiscountAmount;
                            packageNameList.Add(package);
                        }
                    }
                }
            }
            return packageNameList;
        }
    }    
}
