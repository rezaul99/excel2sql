﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using daoProject.Model;
using daoProject.Utilities;
using daoProject.Mapper;
using daoProject.Infrastructure;

namespace daoProject.Service
{
    public class PackageItemWHRCServiceImpl
    {
        private PackageItemDataMapper dataMapper;

        public PackageItemWHRCServiceImpl()
        {
            dataMapper = new PackageItemDataMapper();
        }

        public bool getPackeageForDeleteClass(long classID)
        {
            List<PackageItemWHRC> getPackageForClass = new PackageItemDataMapper().GetPackageByIdToDelete(classID);
            bool statusPackage = false;
            if (getPackageForClass.Count > 0)
            {
                statusPackage = true;
            }
            return statusPackage;
        }

        public void Add(PackageItemWHRC item)
        {
            using (System.Data.Odbc.OdbcConnection conn = daoProject.Infrastructure.DBConnection.GetConnection())
            {
                conn.ConnectionTimeout = ConstantsGlobal.ConnectionTimeoutBypass;
                conn.Open();
                System.Data.Odbc.OdbcTransaction tx = conn.BeginTransaction();

                try
                {
                    dataMapper.Create(item, conn, tx);
                    tx.Commit();
                }
                catch (Exception ex1)
                {
                    try
                    {
                        tx.Rollback();
                        throw ex1;
                    }
                    catch (Exception ex2)
                    {

                    }
                }
            }
        }

        public PackageItemWHRC GetById(long id)
        {
            return dataMapper.GetById(id);
        }

        public List<PackageItemWHRC> GetByPackageId(long id)
        {
            return dataMapper.GetAllByPackageId(id);
        }

        public List<PackageItemWHRC> GetAll()
        {
            return dataMapper.GetAll();
        }

        public void DeleteByPackageId(long packageId)
        {
            dataMapper.DeleteByPackageId(packageId);
        }

        public List<PackageItemWHRC> GetPackageIdListByRecordId(long recordId)
        {
            return dataMapper.GetPackageIdByRecordId(recordId);
        }
    }
}
