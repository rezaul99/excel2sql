﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using daoProject.Model;
using daoProject.Utilities;
using daoProject.Mapper;
using daoProject.Infrastructure;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;
using daoProject.Service;
using System.Data;
using daoProject.WS;

namespace daoProject.Service
{
    public class ClassInvoiceItemServiceImpl
    {
        private ClassInvoiceItemDataMapper dataMapper;

        public ClassInvoiceItemServiceImpl()
        {
            dataMapper = new ClassInvoiceItemDataMapper();
        }

        public void Add(ClassInvoiceItem item)
        {
            dataMapper.Create(item);
        }

        public void Add(ClassInvoiceItem item, System.Data.Odbc.OdbcConnection conn, System.Data.Odbc.OdbcTransaction tx)
        {
            dataMapper.Create(item,conn,tx);
        }


        public void Update(ClassInvoiceItem item)
        {
            dataMapper.Update(item);
        }

        public void UpdateSection(ClassInvoiceItem item)
        {
            dataMapper.UpdateSection(item);
        }

        public void Delete(long id)
        {
             dataMapper.Delete(id);
        }
        public int GetNumberOfAttendingForUser(long invoiceID,long SectionID)
        {
            return dataMapper.GetNumberOfAttendingForUser(invoiceID, SectionID);
        }
        public decimal getItemAmountPrice(long id)
        {
            return dataMapper.getItemAmountPrice(id);
        }
        public List<ClassInvoiceItem> GetAll()
        {
            return dataMapper.GetAll();
        }

        public ClassInvoiceItem GetById(long id)
        {
            return dataMapper.GetById(id);
        }

        public ClassInvoiceItem GetBySectionId(long sectionID)
        {
            return dataMapper.GetBySectionId(sectionID);
        }

        public List<ClassInvoiceItem> GetAllBySectionId(long sectionID)
        {
            return dataMapper.GetAllBySectionId(sectionID);
        }
       
        public List<ClassEnrollmentWaitingHelper> GetEnrollmentAndWaitingListBySectionID(long sectionID)
        {
            return dataMapper.GetEnrollmentAndWaitingListBySectionID(sectionID);
        }
        public List<ClassInvoiceItem> GetClassListByUserId(long UserID)
        {
            return dataMapper.GetClassListByUserId(UserID);
        }
        public List<ClassInvoiceItem> GetListByInvoiceId(long invoiceId)
        {
            return dataMapper.GetListByInvoiceId(invoiceId);
        }
        public int GetTotalWaitingListBySectionId(long sectionId)
        {
            return dataMapper.GetTotalWaitingListBySectionId(sectionId);
        }
        public int GetTotalSignUPBySectionId(long sectionId)
        {
            return dataMapper.GetTotalSignUPBySectionId(sectionId);
        }
        //public List<ClassInvoiceItem> GetListByDateRange(DateTime fromDate, DateTime toDate)
        //{
        //    return dataMapper.GetListByDateRange(fromDate, toDate);
        //}

        //public List<ClassInvoiceItem> GetListByDateRangeAndSectionId(DateTime fromDate, DateTime toDate, long sectionId)
        //{
        //    return dataMapper.GetListByDateRangeAndSectionId(fromDate, toDate, sectionId);
        //}
        //public List<ClassInvoiceItem> GetListByDateRangeAndSectionId(long sectionId)
        //{
        //    return dataMapper.GetListByDateRangeAndSectionId(sectionId);
        //}

        public bool DeleteByInvoiceId(long invoiceId)
        {
            return dataMapper.DeleteByInvoiceId(invoiceId);
        }

        public void CancelClass(ClassInvoiceItem item)
        {
            dataMapper.CancelClass(item);
        }

        public void UpdateRefundProcessed(ClassInvoiceItem item)
        {
            dataMapper.UpdateRefundProcessed(item);
        }

        public int CalculateTotalSignUP(long sectionID)
        {
            return dataMapper.CalculateTotalSignUP(sectionID);
        }
        //CalculateTotalSignUPInCart
        public int CalculateTotalSignUPInCart(long sectionID)
        {
            return dataMapper.CalculateTotalSignUPInCart(sectionID);
        }

        public void UpdateEnrollmentWaitingStatus(ClassInvoiceItem classInvoiceItem)
        {
            dataMapper.UpdateEnrollmentWaitingStatus(classInvoiceItem);
        }

        public void UpdateEnrollmentCancelByClient(long classInvoiceItemID, bool isSignupCanceled, DateTime cancelDate)
        {
            dataMapper.UpdateEnrollmentCancelByClient(classInvoiceItemID, isSignupCanceled, cancelDate);
        }

        public void UpdateItemStatus(ClassInvoiceItem classInvoiceItem)
        {
            dataMapper.UpdateItemStatus(classInvoiceItem);
        }

        public void UpdateClassStatus(Invoice invoice)
        {
            List<ClassInvoiceItem> classInvoiceItemList = invoice.ClassInvoiceItems;
            if (classInvoiceItemList.Count > 0)
            {
                foreach (ClassInvoiceItem classInvoiceItem in classInvoiceItemList)
                {
                    if (!classInvoiceItem.IsInWaitingList)
                    {
                        classInvoiceItem.ClassInvoiceItemState = ClassInvoiceItemState.Ordered;
                        new ClassInvoiceItemServiceImpl().UpdateItemStatus(classInvoiceItem);
                    }
                }
            }
        }
        public void UpdateClassInvoiceId(ClassInvoiceItem item, long newInvoiceId)
        {
            dataMapper.UpdateClassInvoiceId(item, newInvoiceId);
        }
        private List<PackageWHRC> GetPackageNameListForInvoice(Invoice invoice)
        {
            WS.ToolService ts = new ToolService();
            XmlNode xmlNode = ts.GetApplicationData("ManageClass");
            List<ClassWHRC> classWHRCList = new ClassServiceImpl().GetAllClass(xmlNode);

            XmlNode xmlNode1 = ts.GetApplicationData("ManageSections");
            List<Section> sectionList = new ClassServiceImpl().GetAllSection(xmlNode1);

            XmlNode xmlNode2 = ts.GetApplicationData("ManageProductItems");
            List<ProductItemWHRC> producItemList = new ProductServiceImpl().GetAllProductItemWhrc(xmlNode2);

            List<PackageWHRC> packageWhrcForInvoice = new InvoicePackageServiceImpl().GetPackageNamesOnSingleInvoice(invoice, classWHRCList, sectionList, producItemList);
            return packageWhrcForInvoice;
        }
        public bool  ProcessofCancelSignupClass(long invoiceItemId)
        {
            bool noInvoiceItem = false;
            if (invoiceItemId > 0)
            {
                ClassInvoiceItem classInvoice = new ClassInvoiceItemServiceImpl().GetById(invoiceItemId);
                if (classInvoice != null)
                {
                    long invoiceId = classInvoice.InvoiceId;
                    Invoice invoice = new InvoiceServiceImpl().GetById(classInvoice.InvoiceId);
                    if (invoice != null)
                    {
                        if (invoice.OrderStatus == OrderState.Ordered)
                        {
                             int orderItemCount = invoice.ProductInvoiceItems.Count + invoice.RentalInvoiceItems.Count + invoice.MembershipInvoiceItems.Count + invoice.ClassInvoiceItems.Count;
                             List<InvoicePaymentDetail> invoicePaymentList = new InvoicePaymentServiceImpl().GetListByInvoiceId(invoice.Id);
                             InvoicePaymentDetail invoicePaymentDetailToUpdate = null;
                             WS.ToolService ts = new ToolService();
                             XmlNode xmlNode = ts.GetApplicationData("ManageSections");
                             XmlNode xmlNodeClass = ts.GetApplicationData("ManageClass");
                             List<Section> sectionList = new ClassServiceImpl().GetAllSection(xmlNode);
                             List<ClassWHRC> classWHRCList = new ClassServiceImpl().GetAllClass(xmlNodeClass);
                             Section section = sectionList.FirstOrDefault(x => x.RecordID == classInvoice.SectionId);
                             ClassWHRC classWHRC = null;
                             if (section != null)
                             {
                                 classWHRC = classWHRCList.FirstOrDefault(x => x.Title == section.ClassTitle);
                             }
                             if (orderItemCount > 1)
                             {
                                    noInvoiceItem = false;
                                    bool ClassByMedical = false;
                                    if (invoicePaymentList.Count > 1)
                                    {
                                        InvoicePaymentDetail invoicePaymentDetail = invoicePaymentList.FirstOrDefault(x => x.PaymentMode == PaymentType.Medical);
                                        if (invoicePaymentDetail != null)
                                        {
                                            invoicePaymentDetailToUpdate = invoicePaymentDetail;
                                            ClassByMedical = true;
                                        }
                                    }
                                    else
                                    {
                                        if (invoice.PaymentMode != PaymentType.CreditCard)
                                        {
                                            invoicePaymentDetailToUpdate = invoicePaymentList[0];
                                        }
                                    }
                                    if (invoicePaymentDetailToUpdate != null)
                                    {
                                        decimal currentMedicalAmount = invoicePaymentDetailToUpdate.Amount;
                                        decimal currentInvAmount = invoice.TotalAmount;
                                        decimal classPrice = 0;
                                        if (classInvoice.IsInWaitingList)
                                        {
                                            classPrice = 0;
                                        }
                                        else
                                        {
                                            classPrice = classInvoice.Price;
                                        }
                                        decimal updateAmountInvoicePayment = currentMedicalAmount - classPrice;
                                        decimal updateAmountInvoice = currentInvAmount - classPrice;
                                        decimal totalDiscountClass = 0;
                                       
                                        List<PackageWHRC> packageWhrcListClass = GetPackageNameListForInvoice(invoice).Where(x => (x.PackageType == PackageType.Class) && (x.Active == true)).ToList();
                                        if (packageWhrcListClass.Count > 0)
                                        {
                                            foreach (PackageWHRC item in packageWhrcListClass)
                                            {
                                                foreach (PackageItemWHRC Packageclass in item.PackageItemsWHRC)
                                                {
                                                    if (classWHRC != null)
                                                    {
                                                        if (Packageclass.RecordId == classWHRC.RecordID)
                                                        {
                                                            totalDiscountClass += item.DiscountAmount;
                                                            break;
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                        updateAmountInvoicePayment = updateAmountInvoicePayment + totalDiscountClass;
                                        if (ClassByMedical)
                                        {
                                            if (invoice.ClassInvoiceItems.Count > 1)
                                            {
                                                new InvoicePaymentServiceImpl().UpdateTotalAmountForMediCalMethod(updateAmountInvoicePayment, invoicePaymentDetailToUpdate.Id);
                                            }
                                            else
                                            {
                                                new InvoicePaymentServiceImpl().Delete(invoicePaymentDetailToUpdate.Id);
                                            }
                                        }
                                        else
                                        {
                                            new InvoicePaymentServiceImpl().UpdateTotalAmountForMediCalMethod(updateAmountInvoicePayment, invoicePaymentDetailToUpdate.Id);
                                        }
                                        //updateAmountInvoice = updateAmountInvoice - totalDiscountClass;
                                        new InvoiceServiceImpl().UpdateTotalAmount(updateAmountInvoice, invoice.Id);
                                        new ClassInvoiceItemServiceImpl().Delete(invoiceItemId);
                                    }
                             }
                             else
                             {
                                 if(invoice.PaymentMode != PaymentType.CreditCard)
                                 {
                                     noInvoiceItem = true;
                                    
                                         new ClassInvoiceItemServiceImpl().Delete(invoiceItemId);
                                         new InvoicePaymentServiceImpl().DeleteByInvoiceId(invoice.Id);
                                         new InvoiceServiceImpl().Delete(invoice.Id);                                    
                                 }
                             }
                            User user = new UserServiceImpl().GetUserById(invoice.ClientId);
                            EmailAddress EmailAddressObj = null;
                            List<EmailAddress> EmailAddressList = new EmailAddressServiceImpl().GetAll();
                            if (EmailAddressList.Count > 0)
                            {
                                EmailAddressObj = EmailAddressList[0];
                            }
                            if (user != null && invoice != null && classInvoice != null && EmailAddressObj != null && section != null)
                            {
                                new EmailServiceImpl().EmailForCancelSignupClassBeforeProcessingToMember(user, classInvoice, invoice, section);
                                // new EmailServiceImpl().EmailForCancelSignupClassBeforeProcessingToAdmin(user, classInvoice, invoice);
                                new EmailServiceImpl().EmailForCancelSignupClassBeforeProcessingToDefaultAdmin(user, EmailAddressObj, classInvoice, invoice, section);
                            }
                        }
                        else
                        {
                            new ClassInvoiceItemServiceImpl().UpdateEnrollmentCancelByClient(invoiceItemId, true, DateTime.Today);
                        }
                    }
                }
            }
            return noInvoiceItem;
        }

    }    
}
