﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using daoProject.Model;
using daoProject.Utilities;
using daoProject.Mapper;
using System.Data;
using System.Reflection;

namespace daoProject.Service
{
    public class LogDataServiceImpl
    {
        private LogDataMapper dataMapper;

        public LogDataServiceImpl()
        {
            dataMapper = new LogDataMapper();
        }

        public void Add(LogData item)
        {
            dataMapper.Create(item);
        }

        public void Add(LogData item, System.Data.Odbc.OdbcConnection conn, System.Data.Odbc.OdbcTransaction tx)
        {
            dataMapper.Create(item, conn, tx);
        }

        public void Update(LogData item)
        {
            dataMapper.Update(item);
        }

        public bool Delete(long id)
        {
            return dataMapper.Delete(id);
        }

        public List<LogData> GetAll()
        {
            return dataMapper.GetAll();
        }


        public LogData GetById(long id)
        {
            return dataMapper.GetById(id);
        }

        public List<LogData> GetListByObjectName(string objectName)
        {
            return dataMapper.GetListByObjectName(objectName);
        }

        public bool DeleteByInvoiceId(string objectName)
        {
            return dataMapper.DeleteByObjectName(objectName);
        }

        public void InsertForNew(object item)
        {
            List<LogData> logDataList = new List<LogData>();
            LogData logData = new LogData();
            Type t = item.GetType();
            if (t.Name == "ClassWHRC")
            {
                ClassWHRC classWHRC = (ClassWHRC)item;
                foreach (PropertyInfo pi in t.GetProperties())
                {
                    if (pi.Name != "UserID" && pi.Name.ToLower() != "recordid")
                    {
                        logData = new LogData();
                        logData.UserID = classWHRC.UserID;
                        logData.ObjectName = t.Name;
                        logData.FieldName = pi.Name;
                        logData.ControlValue = Convert.ToString(pi.GetValue(item, null));
                        logData.ActionDate = DateTime.Now;
                        logData.ActionName = daoProject.Infrastructure.UserAction.Add;
                        logData.RecordId = classWHRC.RecordID;
                        logDataList.Add(logData);
                    }
                }
            }
            else if (t.Name == "ClassTypeWHRC")
            {
                ClassTypeWHRC classTypeWHRC = (ClassTypeWHRC)item;
                foreach (PropertyInfo pi in t.GetProperties())
                {
                    if (pi.Name != "UserID" && pi.Name.ToLower() != "recordid")
                    {
                        logData = new LogData();
                        logData.UserID = classTypeWHRC.UserID;
                        logData.ObjectName = t.Name;
                        logData.FieldName = pi.Name;
                        logData.ControlValue = Convert.ToString(pi.GetValue(item, null));
                        logData.ActionDate = DateTime.Now;
                        logData.ActionName = daoProject.Infrastructure.UserAction.Add;
                        logData.RecordId = classTypeWHRC.RecordID; 
                        logDataList.Add(logData);
                    }
                }
            }
            else if (t.Name == "Instructor")
            {
                Instructor Instructor = (Instructor)item;
                foreach (PropertyInfo pi in t.GetProperties())
                {
                    if (pi.Name != "UserID" && pi.Name.ToLower() != "recordid")
                    {
                        logData = new LogData();
                        logData.UserID = Instructor.UserID;
                        logData.ObjectName = t.Name;
                        logData.FieldName = pi.Name;
                        logData.ControlValue = Convert.ToString(pi.GetValue(item, null));
                        logData.ActionDate = DateTime.Now;
                        logData.ActionName = daoProject.Infrastructure.UserAction.Add;
                        logData.RecordId = Instructor.RecordID;
                        logDataList.Add(logData);
                    }
                }
            }
            else if (t.Name == "ProductCategoryWhrc")
            {
                ProductCategoryWhrc ProductCategoryWhrc = (ProductCategoryWhrc)item;
                foreach (PropertyInfo pi in t.GetProperties())
                {
                    if (pi.Name != "UserID" && pi.Name.ToLower() != "id")
                    {
                        logData = new LogData();
                        logData.UserID = ProductCategoryWhrc.UserID;
                        logData.ObjectName = t.Name;
                        logData.FieldName = pi.Name;
                        logData.ControlValue = Convert.ToString(pi.GetValue(item, null));
                        logData.ActionDate = DateTime.Now;
                        logData.ActionName = daoProject.Infrastructure.UserAction.Add;
                        logData.RecordId = ProductCategoryWhrc.RecordId;
                        logDataList.Add(logData);
                    }
                }
            }
            else if (t.Name == "Location")
            {
                Location Location = (Location)item;
                foreach (PropertyInfo pi in t.GetProperties())
                {
                    if (pi.Name != "UserID" && pi.Name.ToLower() != "recordid")
                    {
                        logData = new LogData();
                        logData.UserID = Location.UserID;
                        logData.ObjectName = t.Name;
                        logData.FieldName = pi.Name;
                        logData.ControlValue = Convert.ToString(pi.GetValue(item, null));
                        logData.ActionDate = DateTime.Now;
                        logData.ActionName = daoProject.Infrastructure.UserAction.Add;
                        logData.RecordId = Location.RecordID;
                        logDataList.Add(logData);
                    }
                }
            }
            else if (t.Name == "PackageDiscountWHRC")
            {
                PackageDiscountWHRC PackageDiscountWHRC = (PackageDiscountWHRC)item;
                foreach (PropertyInfo pi in t.GetProperties())
                {
                    if (pi.Name != "UserID" && pi.Name.ToLower() != "recordid")
                    {
                        logData = new LogData();
                        logData.UserID = PackageDiscountWHRC.UserID;
                        logData.ObjectName = t.Name;
                        logData.FieldName = pi.Name;
                        logData.ControlValue = Convert.ToString(pi.GetValue(item, null));
                        logData.ActionDate = DateTime.Now;
                        logData.ActionName = daoProject.Infrastructure.UserAction.Add;
                        logData.RecordId = PackageDiscountWHRC.RecordID;
                        logDataList.Add(logData);
                    }
                }
            }
            else if (t.Name == "ManageSalesTaxRateWHRC")
            {
                ManageSalesTaxRateWHRC ManageSalesTaxRateWHRC = (ManageSalesTaxRateWHRC)item;
                foreach (PropertyInfo pi in t.GetProperties())
                {
                    if (pi.Name != "UserID" && pi.Name.ToLower() != "recordid")
                    {
                        logData = new LogData();
                        logData.UserID = ManageSalesTaxRateWHRC.UserID;
                        logData.ObjectName = t.Name;
                        logData.FieldName = pi.Name;
                        logData.ControlValue = Convert.ToString(pi.GetValue(item, null));
                        logData.ActionDate = DateTime.Now;
                        logData.ActionName = daoProject.Infrastructure.UserAction.Add;
                        logData.RecordId = ManageSalesTaxRateWHRC.RecordID;
                        logDataList.Add(logData);
                    }
                }
            }
            else if (t.Name == "MembershipWHRC")
            {
                MembershipWHRC MembershipWHRC = (MembershipWHRC)item;
                foreach (PropertyInfo pi in t.GetProperties())
                {
                    if (pi.Name != "UserID" && pi.Name.ToLower() != "recordid")
                    {
                        logData = new LogData();
                        logData.UserID = MembershipWHRC.UserID;
                        logData.ObjectName = t.Name;
                        logData.FieldName = pi.Name;
                        logData.ControlValue = Convert.ToString(pi.GetValue(item, null));
                        logData.ActionDate = DateTime.Now;
                        logData.ActionName = daoProject.Infrastructure.UserAction.Add;
                        logData.RecordId = MembershipWHRC.RecordID;
                        logDataList.Add(logData);
                    }
                }
            }
            else if (t.Name == "OtherDiscountWHRC")
            {
                OtherDiscountWHRC OtherDiscountWHRC = (OtherDiscountWHRC)item;
                foreach (PropertyInfo pi in t.GetProperties())
                {
                    if (pi.Name != "UserID" && pi.Name.ToLower() != "recordid")
                    {
                        logData = new LogData();
                        logData.UserID = OtherDiscountWHRC.UserID;
                        logData.ObjectName = t.Name;
                        logData.FieldName = pi.Name;
                        logData.ControlValue = Convert.ToString(pi.GetValue(item, null));
                        logData.ActionDate = DateTime.Now;
                        logData.ActionName = daoProject.Infrastructure.UserAction.Add;
                        logData.RecordId = OtherDiscountWHRC.RecordID;
                        logDataList.Add(logData);
                    }
                }
            }
            else if (t.Name == "PercentageDiscountWHRC")
            {
                PercentageDiscountWHRC PercentageDiscountWHRC = (PercentageDiscountWHRC)item;
                foreach (PropertyInfo pi in t.GetProperties())
                {
                    if (pi.Name != "UserID" && pi.Name.ToLower() != "recordid")
                    {
                        logData = new LogData();
                        logData.UserID = PercentageDiscountWHRC.UserID;
                        logData.ObjectName = t.Name;
                        logData.FieldName = pi.Name;
                        logData.ControlValue = Convert.ToString(pi.GetValue(item, null));
                        logData.ActionDate = DateTime.Now;
                        logData.ActionName = daoProject.Infrastructure.UserAction.Add;
                        logData.RecordId = PercentageDiscountWHRC.RecordID;
                        logDataList.Add(logData);
                    }
                }
            }
            else if (t.Name == "CreditCardPaymentSetup")
            {
                CreditCardPaymentSetup CreditCardPaymentSetup = (CreditCardPaymentSetup)item;
                foreach (PropertyInfo pi in t.GetProperties())
                {
                    if (pi.Name != "UserID" && pi.Name.ToLower() != "id")
                    {
                        logData = new LogData();
                        logData.UserID = CreditCardPaymentSetup.UserID;
                        logData.ObjectName = t.Name;
                        logData.FieldName = pi.Name;
                        logData.ControlValue = Convert.ToString(pi.GetValue(item, null));
                        logData.ActionDate = DateTime.Now;
                        logData.ActionName = daoProject.Infrastructure.UserAction.Add;
                        logData.RecordId = CreditCardPaymentSetup.Id;
                        logDataList.Add(logData);
                    }
                }
            }
            else if (t.Name == "ProductItemWHRC")
            {
                ProductItemWHRC ProductItemWHRC = (ProductItemWHRC)item;
                foreach (PropertyInfo pi in t.GetProperties())
                {
                    if (pi.Name != "UserID" && pi.Name.ToLower() != "recordid")
                    {
                        logData = new LogData();
                        logData.UserID = ProductItemWHRC.UserID;
                        logData.ObjectName = t.Name;
                        logData.FieldName = pi.Name;
                        logData.ControlValue = Convert.ToString(pi.GetValue(item, null));
                        logData.ActionDate = DateTime.Now;
                        logData.ActionName = daoProject.Infrastructure.UserAction.Add;
                        logData.RecordId = ProductItemWHRC.RecordId;
                        logDataList.Add(logData);
                    }
                }
            }
            else if (t.Name == "ProductTypeWHRC")
            {
                ProductTypeWHRC ProductTypeWHRC = (ProductTypeWHRC)item;
                foreach (PropertyInfo pi in t.GetProperties())
                {
                    if (pi.Name != "UserID" && pi.Name.ToLower() != "recordid")
                    {
                        logData = new LogData();
                        logData.UserID = ProductTypeWHRC.UserID;
                        logData.ObjectName = t.Name;
                        logData.FieldName = pi.Name;
                        logData.ControlValue = Convert.ToString(pi.GetValue(item, null));
                        logData.ActionDate = DateTime.Now;
                        logData.ActionName = daoProject.Infrastructure.UserAction.Add;
                        logData.RecordId = ProductTypeWHRC.RecordId;
                        logDataList.Add(logData);
                    }
                }
            }

            else if (t.Name == "RentalEquipmentWHRC")
            {
                RentalEquipmentWHRC RentalEquipmentWHRC = (RentalEquipmentWHRC)item;
                foreach (PropertyInfo pi in t.GetProperties())
                {
                    if (pi.Name != "UserID" && pi.Name.ToLower() != "recordid")
                    {
                        logData = new LogData();
                        logData.UserID = RentalEquipmentWHRC.UserID;
                        logData.ObjectName = t.Name;
                        logData.FieldName = pi.Name;
                        logData.ControlValue = Convert.ToString(pi.GetValue(item, null));
                        logData.ActionDate = DateTime.Now;
                        logData.ActionName = daoProject.Infrastructure.UserAction.Add;
                        logData.RecordId = RentalEquipmentWHRC.RecordID;
                        logDataList.Add(logData);
                    }
                }
            }
            else if (t.Name == "RentalEquipmentDelayFeeWHRC")
            {
                RentalEquipmentDelayFeeWHRC RentalEquipmentDelayFeeWHRC = (RentalEquipmentDelayFeeWHRC)item;
                foreach (PropertyInfo pi in t.GetProperties())
                {
                    if (pi.Name != "UserID" && pi.Name.ToLower() != "recordid")
                    {
                        logData = new LogData();
                        logData.UserID = RentalEquipmentDelayFeeWHRC.UserID;
                        logData.ObjectName = t.Name;
                        logData.FieldName = pi.Name;
                        logData.ControlValue = Convert.ToString(pi.GetValue(item, null));
                        logData.ActionDate = DateTime.Now;
                        logData.ActionName = daoProject.Infrastructure.UserAction.Add;
                        logData.RecordId = RentalEquipmentDelayFeeWHRC.RecordID;
                        logDataList.Add(logData);
                    }
                }
            }
            else if (t.Name == "RentalPeriodWHRC")
            {
                RentalPeriodWHRC RentalPeriodWHRC = (RentalPeriodWHRC)item;
                foreach (PropertyInfo pi in t.GetProperties())
                {
                    if (pi.Name != "UserID" && pi.Name.ToLower() != "recordid")
                    {
                        logData = new LogData();
                        logData.UserID = RentalPeriodWHRC.UserID;
                        logData.ObjectName = t.Name;
                        logData.FieldName = pi.Name;
                        logData.ControlValue = Convert.ToString(pi.GetValue(item, null));
                        logData.ActionDate = DateTime.Now;
                        logData.ActionName = daoProject.Infrastructure.UserAction.Add;
                        logData.RecordId = RentalPeriodWHRC.RecordID;
                        logDataList.Add(logData);
                    }
                }
            }
            else if (t.Name == "Section")
            {
                Section Section = (Section)item;
                foreach (PropertyInfo pi in t.GetProperties())
                {
                    if (pi.Name != "UserID" && pi.Name.ToLower() != "recordid")
                    {
                        logData = new LogData();
                        logData.UserID = Section.UserID;
                        logData.ObjectName = t.Name;
                        logData.FieldName = pi.Name;
                        logData.ControlValue = Convert.ToString(pi.GetValue(item, null));
                        logData.ActionDate = DateTime.Now;
                        logData.ActionName = daoProject.Infrastructure.UserAction.Add;
                        logData.RecordId = Section.RecordID;
                        logDataList.Add(logData);
                    }
                }
            }

            else if (t.Name == "StudentFlatRateWHRC")
            {
                StudentFlatRateWHRC StudentFlatRateWHRC = (StudentFlatRateWHRC)item;
                foreach (PropertyInfo pi in t.GetProperties())
                {
                    if (pi.Name != "UserID" && pi.Name.ToLower() != "recordid")
                    {
                        logData = new LogData();
                        logData.UserID = StudentFlatRateWHRC.UserID;
                        logData.ObjectName = t.Name;
                        logData.FieldName = pi.Name;
                        logData.ControlValue = Convert.ToString(pi.GetValue(item, null));
                        logData.ActionDate = DateTime.Now;
                        logData.ActionName = daoProject.Infrastructure.UserAction.Add;
                        logData.RecordId = StudentFlatRateWHRC.RecordID;
                        logDataList.Add(logData);
                    }
                }
            }
            else if (t.Name == "VendorWHRC")
            {
                VendorWHRC VendorWHRC = (VendorWHRC)item;
                foreach (PropertyInfo pi in t.GetProperties())
                {
                    if (pi.Name != "UserID" && pi.Name.ToLower() != "recordid")
                    {
                        logData = new LogData();
                        logData.UserID = VendorWHRC.UserID;
                        logData.ObjectName = t.Name;
                        logData.FieldName = pi.Name;
                        logData.ControlValue = Convert.ToString(pi.GetValue(item, null));
                        logData.ActionDate = DateTime.Now;
                        logData.ActionName = daoProject.Infrastructure.UserAction.Add;
                        logData.RecordId = VendorWHRC.RecordID;
                        logDataList.Add(logData);
                    }
                }
            }
            else if (t.Name == "ProductInvoiceItem")
            {
                ProductInvoiceItem ProductInvoiceItem = (ProductInvoiceItem)item;
                foreach (PropertyInfo pi in t.GetProperties())
                {
                    if (pi.Name != "UserID" && pi.Name.ToLower() != "recordid")
                    {
                        logData = new LogData();
                        logData.UserID = ProductInvoiceItem.UserID;
                        logData.ObjectName = t.Name;
                        logData.FieldName = pi.Name;
                        logData.ControlValue = Convert.ToString(pi.GetValue(item, null));
                        logData.ActionDate = DateTime.Now;
                        logData.ActionName = daoProject.Infrastructure.UserAction.Add;
                        logData.RecordId = ProductInvoiceItem.Id;
                        logDataList.Add(logData);
                    }
                }
            }
            else if (t.Name == "RentalInvoiceItem")
            {
                RentalInvoiceItem RentalInvoiceItem = (RentalInvoiceItem)item;
                foreach (PropertyInfo pi in t.GetProperties())
                {
                    if (pi.Name != "UserID" && pi.Name.ToLower() != "recordid")
                    {
                        logData = new LogData();
                        logData.UserID = RentalInvoiceItem.UserID;
                        logData.ObjectName = t.Name;
                        logData.FieldName = pi.Name;
                        logData.ControlValue = Convert.ToString(pi.GetValue(item, null));
                        logData.ActionDate = DateTime.Now;
                        logData.ActionName = daoProject.Infrastructure.UserAction.Add;
                        logData.RecordId = RentalInvoiceItem.Id;
                        logDataList.Add(logData);
                    }
                }
            }

            else if (t.Name == "ClassInvoiceItem")
            {
                ClassInvoiceItem ClassInvoiceItem = (ClassInvoiceItem)item;
                foreach (PropertyInfo pi in t.GetProperties())
                {
                    if (pi.Name != "UserID" && pi.Name.ToLower() != "recordid")
                    {
                        logData = new LogData();
                        logData.UserID = ClassInvoiceItem.UserID;
                        logData.ObjectName = t.Name;
                        logData.FieldName = pi.Name;
                        logData.ControlValue = Convert.ToString(pi.GetValue(item, null));
                        logData.ActionDate = DateTime.Now;
                        logData.ActionName = daoProject.Infrastructure.UserAction.Add;
                        logData.RecordId = ClassInvoiceItem.Id;
                        logDataList.Add(logData);
                    }
                }
            }
            else if (t.Name == "MembershipInvoiceItem")
            {
                MembershipInvoiceItem MembershipInvoiceItem = (MembershipInvoiceItem)item;
                foreach (PropertyInfo pi in t.GetProperties())
                {
                    if (pi.Name != "UserID" && pi.Name.ToLower() != "recordid")
                    {
                        logData = new LogData();
                        logData.UserID = MembershipInvoiceItem.UserID;
                        logData.ObjectName = t.Name;
                        logData.FieldName = pi.Name;
                        logData.ControlValue = Convert.ToString(pi.GetValue(item, null));
                        logData.ActionDate = DateTime.Now;
                        logData.ActionName = daoProject.Infrastructure.UserAction.Add;
                        logData.RecordId = MembershipInvoiceItem.Id;
                        logDataList.Add(logData);
                    }
                }
            }
            else if (t.Name == "Invoice")
            {
                Invoice Invoice = (Invoice)item;
                foreach (PropertyInfo pi in t.GetProperties())
                {
                    if (pi.Name != "UserID" && pi.Name.ToLower() != "recordid")
                    {
                        logData = new LogData();
                        logData.UserID = Invoice.ClientId;
                        logData.ObjectName = t.Name;
                        logData.FieldName = pi.Name;
                        logData.ControlValue = Convert.ToString(pi.GetValue(item, null));
                        logData.ActionDate = DateTime.Now;
                        logData.ActionName = daoProject.Infrastructure.UserAction.Add;
                        logData.RecordId = Invoice.Id;
                        logDataList.Add(logData);
                    }
                }
            }

            foreach (LogData logDataItem in logDataList)
            {
                this.Add(logDataItem);
            }
        }

        public void InsertForUpdate(object item, object oldItem)
        {
            List<LogData> logDataList = new List<LogData>();
            LogData logData = new LogData();
            Type t = item.GetType();
            Type tOLD = oldItem.GetType();

            if (t.Name == "ClassWHRC" && tOLD.Name == "ClassWHRC")
            {
                ClassWHRC ClassWHRC = (ClassWHRC)item;
                ClassWHRC ClassWHRCOLD = (ClassWHRC)oldItem;

                foreach(PropertyInfo pi in t.GetProperties())
                {
                    foreach(PropertyInfo pi1 in tOLD.GetProperties())
                    {
                        if (pi.Name == pi1.Name)
                        {
                            if (pi.GetValue(item, null) != pi1.GetValue(oldItem, null))
                            {
                                logData = new LogData();
                                logData.UserID = ClassWHRC.UserID;
                                logData.ObjectName = t.Name;
                                logData.FieldName = pi.Name;
                                logData.ControlValue = Convert.ToString(pi.GetValue(item, null));
                                logData.ActionDate = DateTime.Now;
                                logData.ActionName = daoProject.Infrastructure.UserAction.Update;
                                logData.RecordId = ClassWHRC.RecordID;
                                logDataList.Add(logData);
                            }
                            break;
                        }
                    }
                }
            }
            else if (t.Name == "ClassTypeWHRC" && tOLD.Name == "ClassTypeWHRC")
            {
                ClassTypeWHRC ClassTypeWHRC = (ClassTypeWHRC)item;
                ClassTypeWHRC ClassTypeWHRCOLD = (ClassTypeWHRC)oldItem;

                foreach (PropertyInfo pi in t.GetProperties())
                {
                    foreach (PropertyInfo pi1 in tOLD.GetProperties())
                    {
                        if (pi.Name == pi1.Name)
                        {
                            if (pi.GetValue(item, null) != pi1.GetValue(oldItem, null))
                            {
                                logData = new LogData();
                                logData.UserID = ClassTypeWHRC.UserID;
                                logData.ObjectName = t.Name;
                                logData.FieldName = pi.Name;
                                logData.ControlValue = Convert.ToString(pi.GetValue(item, null));
                                logData.ActionDate = DateTime.Now;
                                logData.ActionName = daoProject.Infrastructure.UserAction.Update;
                                logData.RecordId = ClassTypeWHRC.RecordID;
                                logDataList.Add(logData);
                            }
                            break;
                        }
                    }
                }
            }

            else if (t.Name == "Instructor" && tOLD.Name == "Instructor")
            {
                Instructor Instructor = (Instructor)item;
                Instructor InstructorOLD = (Instructor)oldItem;

                foreach (PropertyInfo pi in t.GetProperties())
                {
                    foreach (PropertyInfo pi1 in tOLD.GetProperties())
                    {
                        if (pi.Name == pi1.Name)
                        {
                            if (pi.GetValue(item, null) != pi1.GetValue(oldItem, null))
                            {
                                logData = new LogData();
                                logData.UserID = Instructor.UserID;
                                logData.ObjectName = t.Name;
                                logData.FieldName = pi.Name;
                                logData.ControlValue = Convert.ToString(pi.GetValue(item, null));
                                logData.ActionDate = DateTime.Now;
                                logData.ActionName = daoProject.Infrastructure.UserAction.Update;
                                logData.RecordId = Instructor.RecordID;
                                logDataList.Add(logData);
                            }
                            break;
                        }
                    }
                }
            }
            else if (t.Name == "Location" && tOLD.Name == "Location")
            {
                Location location = (Location)item;
                Location locationOLD = (Location)oldItem;

                foreach (PropertyInfo pi in t.GetProperties())
                {
                    foreach (PropertyInfo pi1 in tOLD.GetProperties())
                    {
                        if (pi.Name == pi1.Name)
                        {
                            if (pi.GetValue(item, null) != pi1.GetValue(oldItem, null))
                            {
                                logData = new LogData();
                                logData.UserID = location.UserID;
                                logData.ObjectName = t.Name;
                                logData.FieldName = pi.Name;
                                logData.ControlValue = Convert.ToString(pi.GetValue(item, null));
                                logData.ActionDate = DateTime.Now;
                                logData.ActionName = daoProject.Infrastructure.UserAction.Update;
                                logData.RecordId = location.RecordID;
                                logDataList.Add(logData);
                            }
                            break;
                        }
                    }
                }
            }
            else if (t.Name == "ManageSalesTaxRateWHRC" && tOLD.Name == "ManageSalesTaxRateWHRC")
            {
                ManageSalesTaxRateWHRC manageSalesTaxRateWHRC = (ManageSalesTaxRateWHRC)item;
                ManageSalesTaxRateWHRC manageSalesTaxRateWHRCOLD = (ManageSalesTaxRateWHRC)oldItem;

                foreach (PropertyInfo pi in t.GetProperties())
                {
                    foreach (PropertyInfo pi1 in tOLD.GetProperties())
                    {
                        if (pi.Name == pi1.Name)
                        {
                            if (pi.GetValue(item, null) != pi1.GetValue(oldItem, null))
                            {
                                logData = new LogData();
                                logData.UserID = manageSalesTaxRateWHRC.UserID;
                                logData.ObjectName = t.Name;
                                logData.FieldName = pi.Name;
                                logData.ControlValue = Convert.ToString(pi.GetValue(item, null));
                                logData.ActionDate = DateTime.Now;
                                logData.ActionName = daoProject.Infrastructure.UserAction.Update;
                                logData.RecordId = manageSalesTaxRateWHRC.RecordID;
                                logDataList.Add(logData);
                            }
                            break;
                        }
                    }
                }
            }
            else if (t.Name == "MembershipWHRC" && tOLD.Name == "MembershipWHRC")
            {
                MembershipWHRC membershipWHRC = (MembershipWHRC)item;
                MembershipWHRC membershipWHRCOLD = (MembershipWHRC)oldItem;

                foreach (PropertyInfo pi in t.GetProperties())
                {
                    foreach (PropertyInfo pi1 in tOLD.GetProperties())
                    {
                        if (pi.Name == pi1.Name)
                        {
                            if (pi.GetValue(item, null) != pi1.GetValue(oldItem, null))
                            {
                                logData = new LogData();
                                logData.UserID = membershipWHRC.UserID;
                                logData.ObjectName = t.Name;
                                logData.FieldName = pi.Name;
                                logData.ControlValue = Convert.ToString(pi.GetValue(item, null));
                                logData.ActionDate = DateTime.Now;
                                logData.ActionName = daoProject.Infrastructure.UserAction.Update;
                                logData.RecordId = membershipWHRC.RecordID;
                                logDataList.Add(logData);
                            }
                            break;
                        }
                    }
                }
            }
            else if (t.Name == "OtherDiscountWHRC" && tOLD.Name == "OtherDiscountWHRC")
            {
                OtherDiscountWHRC otherDiscountWHRC = (OtherDiscountWHRC)item;
                OtherDiscountWHRC otherDiscountWHRCOLD = (OtherDiscountWHRC)oldItem;

                foreach (PropertyInfo pi in t.GetProperties())
                {
                    foreach (PropertyInfo pi1 in tOLD.GetProperties())
                    {
                        if (pi.Name == pi1.Name)
                        {
                            if (pi.GetValue(item, null) != pi1.GetValue(oldItem, null))
                            {
                                logData = new LogData();
                                logData.UserID = otherDiscountWHRC.UserID;
                                logData.ObjectName = t.Name;
                                logData.FieldName = pi.Name;
                                logData.ControlValue = Convert.ToString(pi.GetValue(item, null));
                                logData.ActionDate = DateTime.Now;
                                logData.ActionName = daoProject.Infrastructure.UserAction.Update;
                                logData.RecordId = otherDiscountWHRC.RecordID;
                                logDataList.Add(logData);
                            }
                            break;
                        }
                    }
                }
            }
            else if (t.Name == "PackageDiscountWHRC" && tOLD.Name == "PackageDiscountWHRC")
            {
                PackageDiscountWHRC packageDiscountWHRC = (PackageDiscountWHRC)item;
                PackageDiscountWHRC packageDiscountWHRCOLD = (PackageDiscountWHRC)oldItem;

                foreach (PropertyInfo pi in t.GetProperties())
                {
                    foreach (PropertyInfo pi1 in tOLD.GetProperties())
                    {
                        if (pi.Name == pi1.Name)
                        {
                            if (pi.GetValue(item, null) != pi1.GetValue(oldItem, null))
                            {
                                logData = new LogData();
                                logData.UserID = packageDiscountWHRC.UserID;
                                logData.ObjectName = t.Name;
                                logData.FieldName = pi.Name;
                                logData.ControlValue = Convert.ToString(pi.GetValue(item, null));
                                logData.ActionDate = DateTime.Now;
                                logData.ActionName = daoProject.Infrastructure.UserAction.Update;
                                logData.RecordId = packageDiscountWHRC.RecordID;
                                logDataList.Add(logData);
                            }
                            break;
                        }
                    }
                }
            }
            else if (t.Name == "CreditCardPaymentSetup" && tOLD.Name == "CreditCardPaymentSetup")
            {
                CreditCardPaymentSetup CreditCardPaymentSetup = (CreditCardPaymentSetup)item;
                CreditCardPaymentSetup CreditCardPaymentSetupOLD = (CreditCardPaymentSetup)oldItem;

                foreach (PropertyInfo pi in t.GetProperties())
                {
                    foreach (PropertyInfo pi1 in tOLD.GetProperties())
                    {
                        if (pi.Name == pi1.Name)
                        {
                            if (pi.GetValue(item, null) != pi1.GetValue(oldItem, null))
                            {
                                logData = new LogData();
                                logData.UserID = CreditCardPaymentSetup.UserID;
                                logData.ObjectName = t.Name;
                                logData.FieldName = pi.Name;
                                logData.ControlValue = Convert.ToString(pi.GetValue(item, null));
                                logData.ActionDate = DateTime.Now;
                                logData.ActionName = daoProject.Infrastructure.UserAction.Update;
                                logData.RecordId = CreditCardPaymentSetup.Id;
                                logDataList.Add(logData);
                            }
                            break;
                        }
                    }
                }
            }
            else if (t.Name == "PercentageDiscountWHRC" && tOLD.Name == "PercentageDiscountWHRC")
            {
                PercentageDiscountWHRC PercentageDiscountWHRC = (PercentageDiscountWHRC)item;
                PercentageDiscountWHRC PercentageDiscountWHRCOLD = (PercentageDiscountWHRC)oldItem;

                foreach (PropertyInfo pi in t.GetProperties())
                {
                    foreach (PropertyInfo pi1 in tOLD.GetProperties())
                    {
                        if (pi.Name == pi1.Name)
                        {
                            if (pi.GetValue(item, null) != pi1.GetValue(oldItem, null))
                            {
                                logData = new LogData();
                                logData.UserID = PercentageDiscountWHRC.UserID;
                                logData.ObjectName = t.Name;
                                logData.FieldName = pi.Name;
                                logData.ControlValue = Convert.ToString(pi.GetValue(item, null));
                                logData.ActionDate = DateTime.Now;
                                logData.ActionName = daoProject.Infrastructure.UserAction.Update;
                                logData.RecordId = PercentageDiscountWHRC.RecordID;
                                logDataList.Add(logData);
                            }
                            break;
                        }
                    }
                }
            }
            
            else if (t.Name == "ProductCategoryWhrc" && tOLD.Name == "ProductCategoryWhrc")
            {
                ProductCategoryWhrc ProductCategoryWhrc = (ProductCategoryWhrc)item;
                ProductCategoryWhrc ProductCategoryWhrcOLD = (ProductCategoryWhrc)oldItem;

                foreach (PropertyInfo pi in t.GetProperties())
                {
                    foreach (PropertyInfo pi1 in tOLD.GetProperties())
                    {
                        if (pi.Name == pi1.Name)
                        {
                            if (pi.GetValue(item, null) != pi1.GetValue(oldItem, null))
                            {
                                logData = new LogData();
                                logData.UserID = ProductCategoryWhrc.UserID;
                                logData.ObjectName = t.Name;
                                logData.FieldName = pi.Name;
                                logData.ControlValue = Convert.ToString(pi.GetValue(item, null));
                                logData.ActionDate = DateTime.Now;
                                logData.ActionName = daoProject.Infrastructure.UserAction.Update;
                                logData.RecordId = ProductCategoryWhrc.RecordId;
                                logDataList.Add(logData);
                            }
                            break;
                        }
                    }
                }
            }

            else if (t.Name == "ProductItemWHRC" && tOLD.Name == "ProductItemWHRC")
            {
                ProductItemWHRC productItemWHRC = (ProductItemWHRC)item;
                ProductItemWHRC productItemWHRCOLD = (ProductItemWHRC)oldItem;

                foreach (PropertyInfo pi in t.GetProperties())
                {
                    foreach (PropertyInfo pi1 in tOLD.GetProperties())
                    {
                        if (pi.Name == pi1.Name)
                        {
                            if (pi.GetValue(item, null) != pi1.GetValue(oldItem, null))
                            {
                                logData = new LogData();
                                logData.UserID = productItemWHRC.UserID;
                                logData.ObjectName = t.Name;
                                logData.FieldName = pi.Name;
                                logData.ControlValue = Convert.ToString(pi.GetValue(item, null));
                                logData.ActionDate = DateTime.Now;
                                logData.ActionName = daoProject.Infrastructure.UserAction.Update;
                                logData.RecordId = productItemWHRC.RecordId;
                                logDataList.Add(logData);
                            }
                            break;
                        }
                    }
                }
            }

            else if (t.Name == "ProductTypeWHRC" && tOLD.Name == "ProductTypeWHRC")
            {
                ProductTypeWHRC productTypeWHRC = (ProductTypeWHRC)item;
                ProductTypeWHRC productTypeWHRCOLD = (ProductTypeWHRC)oldItem;

                foreach (PropertyInfo pi in t.GetProperties())
                {
                    foreach (PropertyInfo pi1 in tOLD.GetProperties())
                    {
                        if (pi.Name == pi1.Name)
                        {
                            if (pi.GetValue(item, null) != pi1.GetValue(oldItem, null))
                            {
                                logData = new LogData();
                                logData.UserID = productTypeWHRC.UserID;
                                logData.ObjectName = t.Name;
                                logData.FieldName = pi.Name;
                                logData.ControlValue = Convert.ToString(pi.GetValue(item, null));
                                logData.ActionDate = DateTime.Now;
                                logData.ActionName = daoProject.Infrastructure.UserAction.Update;
                                logData.RecordId = productTypeWHRC.RecordId;
                                logDataList.Add(logData);
                            }
                            break;
                        }
                    }
                }
            }

            else if (t.Name == "RentalEquipmentWHRC" && tOLD.Name == "RentalEquipmentWHRC")
            {
                RentalEquipmentWHRC rentalEquipmentWHRC = (RentalEquipmentWHRC)item;
                RentalEquipmentWHRC rentalEquipmentWHRCOLD = (RentalEquipmentWHRC)oldItem;

                foreach (PropertyInfo pi in t.GetProperties())
                {
                    foreach (PropertyInfo pi1 in tOLD.GetProperties())
                    {
                        if (pi.Name == pi1.Name)
                        {
                            if (pi.GetValue(item, null) != pi1.GetValue(oldItem, null))
                            {
                                logData = new LogData();
                                logData.UserID = rentalEquipmentWHRC.UserID;
                                logData.ObjectName = t.Name;
                                logData.FieldName = pi.Name;
                                logData.ControlValue = Convert.ToString(pi.GetValue(item, null));
                                logData.ActionDate = DateTime.Now;
                                logData.ActionName = daoProject.Infrastructure.UserAction.Update;
                                logData.RecordId = rentalEquipmentWHRC.RecordID;
                                logDataList.Add(logData);
                            }
                            break;
                        }
                    }
                }
            }
            else if (t.Name == "RentalEquipmentDelayFeeWHRC" && tOLD.Name == "RentalEquipmentDelayFeeWHRC")
            {
                RentalEquipmentDelayFeeWHRC rentalEquipmentDelayFeeWHRC = (RentalEquipmentDelayFeeWHRC)item;
                RentalEquipmentDelayFeeWHRC rentalEquipmentDelayFeeWHRCOLD = (RentalEquipmentDelayFeeWHRC)oldItem;

                foreach (PropertyInfo pi in t.GetProperties())
                {
                    foreach (PropertyInfo pi1 in tOLD.GetProperties())
                    {
                        if (pi.Name == pi1.Name)
                        {
                            if (pi.GetValue(item, null) != pi1.GetValue(oldItem, null))
                            {
                                logData = new LogData();
                                logData.UserID = rentalEquipmentDelayFeeWHRC.UserID;
                                logData.ObjectName = t.Name;
                                logData.FieldName = pi.Name;
                                logData.ControlValue = Convert.ToString(pi.GetValue(item, null));
                                logData.ActionDate = DateTime.Now;
                                logData.ActionName = daoProject.Infrastructure.UserAction.Update;
                                logData.RecordId = rentalEquipmentDelayFeeWHRC.RecordID;
                                logDataList.Add(logData);
                            }
                            break;
                        }
                    }
                }
            }
            else if (t.Name == "RentalPeriodWHRC" && tOLD.Name == "RentalPeriodWHRC")
            {
                RentalPeriodWHRC rentalPeriodWHRC = (RentalPeriodWHRC)item;
                RentalPeriodWHRC rentalPeriodWHRCOLD = (RentalPeriodWHRC)oldItem;

                foreach (PropertyInfo pi in t.GetProperties())
                {
                    foreach (PropertyInfo pi1 in tOLD.GetProperties())
                    {
                        if (pi.Name == pi1.Name)
                        {
                            if (pi.GetValue(item, null) != pi1.GetValue(oldItem, null))
                            {
                                logData = new LogData();
                                logData.UserID = rentalPeriodWHRC.UserID;
                                logData.ObjectName = t.Name;
                                logData.FieldName = pi.Name;
                                logData.ControlValue = Convert.ToString(pi.GetValue(item, null));
                                logData.ActionDate = DateTime.Now;
                                logData.ActionName = daoProject.Infrastructure.UserAction.Update;
                                logData.RecordId = rentalPeriodWHRC.RecordID;
                                logDataList.Add(logData);
                            }
                            break;
                        }
                    }
                }
            }
            else if (t.Name == "Section" && tOLD.Name == "Section")
            {
                Section Section = (Section)item;
                Section SectionOLD = (Section)oldItem;

                foreach (PropertyInfo pi in t.GetProperties())
                {
                    foreach (PropertyInfo pi1 in tOLD.GetProperties())
                    {
                        if (pi.Name == pi1.Name)
                        {
                            if (pi.GetValue(item, null) != pi1.GetValue(oldItem, null))
                            {
                                logData = new LogData();
                                logData.UserID = Section.UserID;
                                logData.ObjectName = t.Name;
                                logData.FieldName = pi.Name;
                                logData.ControlValue = Convert.ToString(pi.GetValue(item, null));
                                logData.ActionDate = DateTime.Now;
                                logData.ActionName = daoProject.Infrastructure.UserAction.Update;
                                logData.RecordId = Section.RecordID;
                                logDataList.Add(logData);
                            }
                            break;
                        }
                    }
                }
            }
            else if (t.Name == "StudentFlatRateWHRC" && tOLD.Name == "StudentFlatRateWHRC")
            {
                StudentFlatRateWHRC StudentFlatRateWHRC = (StudentFlatRateWHRC)item;
                StudentFlatRateWHRC StudentFlatRateWHRCOLD = (StudentFlatRateWHRC)oldItem;

                foreach (PropertyInfo pi in t.GetProperties())
                {
                    foreach (PropertyInfo pi1 in tOLD.GetProperties())
                    {
                        if (pi.Name == pi1.Name)
                        {
                            if (pi.GetValue(item, null) != pi1.GetValue(oldItem, null))
                            {
                                logData = new LogData();
                                logData.UserID = StudentFlatRateWHRC.UserID;
                                logData.ObjectName = t.Name;
                                logData.FieldName = pi.Name;
                                logData.ControlValue = Convert.ToString(pi.GetValue(item, null));
                                logData.ActionDate = DateTime.Now;
                                logData.ActionName = daoProject.Infrastructure.UserAction.Update;
                                logData.RecordId = StudentFlatRateWHRC.RecordID;
                                logDataList.Add(logData);
                            }
                            break;
                        }
                    }
                }
            }

            else if (t.Name == "VendorWHRC" && tOLD.Name == "VendorWHRC")
            {
                VendorWHRC vendorWHRC = (VendorWHRC)item;
                VendorWHRC vendorWHRCOLD = (VendorWHRC)oldItem;

                foreach (PropertyInfo pi in t.GetProperties())
                {
                    foreach (PropertyInfo pi1 in tOLD.GetProperties())
                    {
                        if (pi.Name == pi1.Name)
                        {
                            if (pi.GetValue(item, null) != pi1.GetValue(oldItem, null))
                            {
                                logData = new LogData();
                                logData.UserID = vendorWHRC.UserID;
                                logData.ObjectName = t.Name;
                                logData.FieldName = pi.Name;
                                logData.ControlValue = Convert.ToString(pi.GetValue(item, null));
                                logData.ActionDate = DateTime.Now;
                                logData.ActionName = daoProject.Infrastructure.UserAction.Update;
                                logData.RecordId = vendorWHRC.RecordID;
                                logDataList.Add(logData);
                            }
                            break;
                        }
                    }
                }
            }

            foreach (LogData logDataItem in logDataList)
            {
                this.Add(logDataItem);
            }
        }

        public void InsertForDelete(object item)
        {

        }

        public void DeleteLogOlderThanThisDate(DateTime date)
        {
            dataMapper.DeleteLogOlderThanThisDate(date);
        }
    }
}
