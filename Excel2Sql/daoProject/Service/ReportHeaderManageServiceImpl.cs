﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using daoProject.Mapper;
using daoProject.Model;

namespace daoProject.Service
{
    public class ReportHeaderManageServiceImpl
    {
        private ReportHeaderManageDataMapper dataMapper;

        public ReportHeaderManageServiceImpl()
        {
            dataMapper = new ReportHeaderManageDataMapper();
        }
        public List<ReportHeaderManager> GetAll()
        {
            return dataMapper.GetAll();
        }
        public ReportHeaderManager GetAllById(long Id)
        {
            return dataMapper.GetAllById(Id);
        }
        public void Add(ReportHeaderManager obj)
        {
            dataMapper.Add(obj);
        }
        public void Update(ReportHeaderManager obj)
        {
            dataMapper.Update(obj);
        }
    }
}
