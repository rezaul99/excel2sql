﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using daoProject.Model;
using daoProject.Utilities;
using daoProject.Mapper;
using System.Data;

namespace daoProject.Service
{
    public class MembershipInvoiceItemServiceImpl
    {
        private MembershipInvoiceItemDataMapper dataMapper;

        public MembershipInvoiceItemServiceImpl()
        {
            dataMapper = new MembershipInvoiceItemDataMapper();
        }

        public void Add(MembershipInvoiceItem item)
        {
            dataMapper.Create(item);
        }

        public void Add(MembershipInvoiceItem item, System.Data.Odbc.OdbcConnection conn, System.Data.Odbc.OdbcTransaction tx)
        {
            dataMapper.Create(item, conn, tx);
        }

        public void Update(MembershipInvoiceItem item)
        {
            dataMapper.Update(item);
        }

        public bool Delete(long id)
        {
            return dataMapper.Delete(id);
        }
        public decimal getItemAmountPrice(long id)
        {
            return dataMapper.getItemAmountPrice(id);
        }
        public List<MembershipInvoiceItem> GetAll()
        {
            return dataMapper.GetAll();
        }

        public DataSet GetMembershipByLastValidDate(DateTime lastValidDate)
        {
            return dataMapper.GetMembershipByLastValidDate(lastValidDate);
        }


        public MembershipInvoiceItem GetById(long id)
        {
            return dataMapper.GetById(id);
        }

        public List<MembershipInvoiceItem> GetListByInvoiceId(long invoiceId)
        {
            return dataMapper.GetListByInvoiceId(invoiceId);
        }

        public bool DeleteByInvoiceId(long invoiceId)
        {
            return dataMapper.DeleteByInvoiceId(invoiceId);
        }

        public List<MembershipInvoiceItem> GetListUserId(long userId)
        {
            return dataMapper.GetListByUserId(userId);
        }
        public void UpdateMemberInvoiceId(MembershipInvoiceItem item, long newInvoiceId)
        {
            dataMapper.UpdateMemberInvoiceId(item, newInvoiceId);
        }
    }
}
