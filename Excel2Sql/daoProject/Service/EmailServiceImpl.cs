﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using daoProject.Utilities;
using System.Configuration;
using System.Net.Mail;
using System.Data.Odbc;
using System.Web;
using daoProject.Model;
using daoProject.Infrastructure;
using System.Xml;
using daoProject.Service;
using daoProject.Utilities;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Drawing;
using System.Net.Mime;
using System.IO;
namespace daoProject.Service
{
    public class EmailServiceImpl
    {
        /// <summary>
        /// Check the Email format.
        /// </summary>
        /// <param name="EmailAddress"></param>
        /// <returns>true if Valid else false</returns>
        private LinkedResource MemberLogoRight { get; set; }
        private LinkedResource MemberLogoLeft { get; set; }
        private string footer { get; set; }

        private LinkedResource AdminLogoRight { get; set; }
        private LinkedResource AdminLogoLeft { get; set; }
       

        private string GenerateMemberFooter()
        {
            string path = System.Web.HttpContext.Current.Server.MapPath(@"images/logo.png"); // my logo is placed in images folder
            MemberLogoRight = new LinkedResource(path);
            MemberLogoRight.ContentId = "companylogo";

            string path2 = System.Web.HttpContext.Current.Server.MapPath(@"images/footer_ripple.jpg"); // my logo is placed in images folder
            MemberLogoLeft = new LinkedResource(path2);
            MemberLogoLeft.ContentId = "footer_ripple";
            // done HTML formatting in the next line to display my logo

            #region footer
            #region footer  details
            System.Text.StringBuilder footerDetails = new System.Text.StringBuilder(); ;
            footerDetails.Append("<table  cellspacing=0; style=\"width:600px; border:solid 1px #ccc; font: normal 12px 'lucida sans unicode'; text-align:left\">");
            footerDetails.Append("<tr style=\"border-bottom:none 0 #ccc; background-color: #BCE1EA;\">");
            footerDetails.Append("<td style=\"width:60%;\">");
            footerDetails.Append("<div id=\"footerLeft\" style=\" background-image: url(cid:footer_ripple);  background-repeat: no-repeat;\">");
            footerDetails.Append("<p>");
            footerDetails.Append("UCSF Women's Health Resource Center");
            footerDetails.Append("</p>");
            footerDetails.Append("<p>");
            footerDetails.Append("2356 Sutter Street, 1st Floor");
            footerDetails.Append("</p>");
            footerDetails.Append("<p>");
            footerDetails.Append("San Francisco, CA 94143-1750 (94115 for deliveries)");
            footerDetails.Append("</p>");
            footerDetails.Append("<p>");
            footerDetails.Append("Phone 415.353.2668");
            footerDetails.Append("</p>");
            footerDetails.Append("</div>");
            footerDetails.Append("</td>");
            footerDetails.Append("<td style=\"width:40%;\">");
            footerDetails.Append("<div id=\"footerRight\" style=\" background-image: url(cid:companylogo);  background-repeat: no-repeat;\">");
            footerDetails.Append(" <img src=\"cid:companylogo\" />");
            footerDetails.Append("</div>");
            footerDetails.Append("</td>");
            footerDetails.Append("</tr>");
            footerDetails.Append("</table>");

            footer = footerDetails.ToString();
            #endregion
            #endregion

            return footer;
        }

        private string GenerateAdminFooter()
        {
            string path = System.Web.HttpContext.Current.Server.MapPath(@"images/memberLogo.png"); // my logo is placed in images folder
            AdminLogoRight = new LinkedResource(path);
            AdminLogoRight.ContentId = "companylogo";

            string path2 = System.Web.HttpContext.Current.Server.MapPath(@"images/memberFooter_ripple.jpg"); // my logo is placed in images folder
            AdminLogoLeft = new LinkedResource(path2);
            AdminLogoLeft.ContentId = "footer_ripple";
            // done HTML formatting in the next line to display my logo

            #region footer
            #region footer  details
            System.Text.StringBuilder footerDetails = new System.Text.StringBuilder(); ;
            footerDetails.Append("<table  cellspacing=0; style=\"width:600px; border:solid 1px #ccc; font: normal 12px 'lucida sans unicode'; text-align:left\">");
            footerDetails.Append("<tr style=\"border-bottom:none 0 #ccc; background-color: #BCE1EA;\">");
            footerDetails.Append("<td style=\"width:60%;\">");
            footerDetails.Append("<div id=\"footerLeft\" style=\" background-image: url(cid:footer_ripple);  background-repeat: no-repeat;\">");
            footerDetails.Append("<p>");
            footerDetails.Append("UCSF Women's Health Resource Center");
            footerDetails.Append("</p>");
            footerDetails.Append("<p>");
            footerDetails.Append("2356 Sutter Street, 1st Floor");
            footerDetails.Append("</p>");
            footerDetails.Append("<p>");
            footerDetails.Append("San Francisco, CA 94143-1750 (94115 for deliveries)");
            footerDetails.Append("</p>");
            footerDetails.Append("<p>");
            footerDetails.Append("Phone 415.353.2668");
            footerDetails.Append("</p>");
            footerDetails.Append("</div>");
            footerDetails.Append("</td>");
            footerDetails.Append("<td style=\"width:40%;\">");
            footerDetails.Append("<div id=\"footerRight\" style=\" background-image: url(cid:companylogo);  background-repeat: no-repeat;\">");
            footerDetails.Append(" <img src=\"cid:companylogo\" />");
            footerDetails.Append("</div>");
            footerDetails.Append("</td>");
            footerDetails.Append("</tr>");
            footerDetails.Append("</table>");

            footer = footerDetails.ToString();
            #endregion
            #endregion

            return footer;
        }

        private static bool CheckEmail(string EmailAddress)
        {
            // All email pattern are available here
            string strPattern = "^([0-9a-zA-Z]([-.\\w]*[0-9a-zA-Z])*@([0-9a-zA-Z][-\\w]*[0-9a-zA-Z]\\.)+[a-zA-Z]{2,9})$";

            //match the input email to email pattern.
            if (System.Text.RegularExpressions.Regex.IsMatch(EmailAddress, strPattern))

            //return true if vaild
            { return true; }

            //return false if invaild.
            return false;
        }

        public void SendEmailForRegistrationCompletionWithPassword(string mailadd, User userObj, string temporaryPassword, string url)
        {
            try
            {
                //Email Validation
                if (CheckEmail(mailadd.ToString()) != false)
                {
                    Utility util = new Utility();
                    string[] info = util.SecurityInformaionEncrypt(Convert.ToString(userObj.Id));

                    //set the receiver address
                    string to = mailadd;
                  // string to = "rabeya@jijoty.org";

                    //set the content

                    string subject = "WHRC User Activation";

                    string fullUrl = string.Empty;
                    string fulUrlTemporary = string.Empty;
                    string idstring = HttpUtility.UrlEncode(info[0].ToString(), System.Text.Encoding.Default);
                    string keystring = HttpUtility.UrlEncode(info[1].ToString(), System.Text.Encoding.Default);
                    // Declear variable for configuration


                    //fullUrl = "http://" + url + "/Default.aspx?ID=" + idstring + "&Key=" + keystring;
                    fullUrl = url + "/whrc/Login.aspx?ID=" + idstring + "&Key=" + keystring;

                    //User usermodel = GetUserById(Id);
                    //Create the body of the Email
                    string body = "<BR/>" +
                        userObj.FirstName +
                        "<BR/><BR/>" +
                        "You have almost completed your registration! Your temporary password is " + temporaryPassword + "<BR/><BR/>All you need to do to " +
                        " is <a href='" + fullUrl.ToString() + "'>click here</a>  or you can copy the link below in your browser's address bar.<BR/><BR/>" +
                        "" + fullUrl.ToString() + "<BR/><BR/> " +
                        "Thank you" +
                        "<BR/>WHRC Support Team<BR/>" +
                        "<a href='http://" + url + "'>" + url + "</a>";

                     GenerateMemberFooter();
               
                AlternateView av1 = AlternateView.CreateAlternateViewFromString("<html><body>"+body+"</body></html>", null, MediaTypeNames.Text.Html);
                av1.LinkedResources.Add(MemberLogoRight);
                av1.LinkedResources.Add(MemberLogoLeft);
                MailHelper mailHelper = new MailHelper();
                mailHelper.GotImageLogo(av1);
                mailHelper.Send(to, subject, body);

                }
            }
            catch (Exception ex)
            {

            }
        }

        public void SendEmailForAdminForgotPassWordLink(string mailadd, long Id, string url)
        {
            try
            {
                // Email Validation
                if (CheckEmail(mailadd.ToString()) != false)
                {
                    Utility util = new Utility();
                    // Encrypt the password
                    string[] info = util.SecurityInformaionEncrypt(Convert.ToString(Id));

                    //set the receiver address
                    string to = mailadd;

                    //set the content

                    string subject = "Whrc Forgot Password";

                    string fullUrl = string.Empty;
                    string idstring = HttpUtility.UrlEncode(info[0].ToString(), System.Text.Encoding.Default);
                    string keystring = HttpUtility.UrlEncode(info[1].ToString(), System.Text.Encoding.Default);

                    //Check the value and get the value              

                    //fullUrl = "http://" + url + "/WPGT/PasswordReset.aspx?ID=" + idstring + "&Key=" + keystring;
                    fullUrl = url + "/whrcadmin/ResetPassword.aspx?ID=" + idstring + "&Key=" + keystring;

                    User usermodel = new UserServiceImpl().GetUserById(Id);
                    //Create the body of the Email
                    string body = "<BR/>" +
                        usermodel.FirstName +
                        "<BR/><BR/>" +
                       "You recently forgot your password for Jijoty. To  reactivate your account, please visit this link and follow the instructions." +
                            "<BR/>" + fullUrl + "<BR/>" +

                            "If you did not request this, then please click here <URL> to report the abuse." +
                        "<BR/><BR/>Thanks, Team";
                   GenerateAdminFooter();
                
                AlternateView av1 = AlternateView.CreateAlternateViewFromString("<html><body>" + body + "</body></html>", null, MediaTypeNames.Text.Html);
                av1.LinkedResources.Add(AdminLogoRight);
                av1.LinkedResources.Add(AdminLogoLeft);
                MailHelper mailHelper = new MailHelper();
                mailHelper.GotImageLogo(av1);
                mailHelper.Send(to, subject, body);


                }
            }
            catch (Exception ex)
            {

            }
        }

        public void SendEmailToMemberForForgotPassWordLink(string mailadd, long Id, string url)
        {
            try
            {
                // Email Validation
                if (CheckEmail(mailadd.ToString()) != false)
                {
                    Utility util = new Utility();
                    // Encrypt the password
                    string[] info = util.SecurityInformaionEncrypt(Convert.ToString(Id));

                    //set the receiver address
                   string to = mailadd;
                   //string to = "rabeya@jijoty.org";

                    //set the content

                    string subject = "Whrc Forgot Password";

                    string fullUrl = string.Empty;
                    string idstring = HttpUtility.UrlEncode(info[0].ToString(), System.Text.Encoding.Default);
                    string keystring = HttpUtility.UrlEncode(info[1].ToString(), System.Text.Encoding.Default);

                    //Check the value and get the value              

                    //fullUrl = "http://" + url + "/WPGT/PasswordReset.aspx?ID=" + idstring + "&Key=" + keystring;
                    fullUrl = url + "/whrc/ResetPassword.aspx?ID=" + idstring + "&Key=" + keystring;

                    User usermodel = new UserServiceImpl().GetUserById(Id);
                    //Create the body of the Email
                    if (usermodel != null)
                    {
                        string body = "<BR/>" +
                            usermodel.FirstName +
                            "<BR/><BR/>" +
                            "You recently forgot your password for Jijoty. To  reactivate your account, please visit this link and follow the instructions." +
                            "<BR/>" + fullUrl + "<BR/>" +

                            "If you did not request this, then please click here <URL> to report the abuse." +
                            "<BR/><BR/>Thanks, Team";

                        GenerateMemberFooter();

                        AlternateView av1 = AlternateView.CreateAlternateViewFromString("<html><body>" + body + "</body></html>", null, MediaTypeNames.Text.Html);
                        av1.LinkedResources.Add(MemberLogoRight);
                        av1.LinkedResources.Add(MemberLogoLeft);
                        MailHelper mailHelper = new MailHelper();
                        mailHelper.GotImageLogo(av1);
                        mailHelper.Send(to, subject, body);
                    }
                }
            }
            catch (Exception ex)
            {

            }
        }

        #region from member portal after purchase item

        public void SendEmailForAcknowledgementForOrderProcessFromMemberPortalToClient(User userObj, List<CartItem> cartItemList, List<OrderPaymentRefundHelper> orderPaymentRefundHelperList, Invoice invoiceObj, decimal totalDiscount,bool IsDisablePaymentMethodForZeroBalancedOrderItem)
        {
            string mailAddress = userObj.Email.ToString();
            bool isProduct = false;
            if (CheckEmail(mailAddress) != false)
            {
                string to = mailAddress;
                string subject = "Whrc Order Status";

                #region orderInfo
                        System.Text.StringBuilder invoiceDescription = new System.Text.StringBuilder(); ;
                        invoiceDescription.Append("<table style=\"width:600px; border:solid 1px #ccc; font: normal 12px 'lucida sans unicode'; text-align:left\">");
                        invoiceDescription.Append("<tr style=\"background:#cacaca\">");
                        invoiceDescription.Append("<th>");
                        invoiceDescription.Append("Order Item Type");
                        invoiceDescription.Append("</th>");
                        invoiceDescription.Append("<th>");
                        invoiceDescription.Append("Description");
                        invoiceDescription.Append("</th>");
                        foreach (var item in cartItemList)
                        {
                            if (item.InvoiceItemTypeDescription.ToString().Equals("Purchase"))
                            {
                                invoiceDescription.Append("<th>");
                                invoiceDescription.Append("Shipping Address");
                                invoiceDescription.Append("</th>");
                                break;
                            }
                        }
                        invoiceDescription.Append("<th>");
                        invoiceDescription.Append("Price");
                        invoiceDescription.Append("</th>");
                        invoiceDescription.Append("</tr>");
                        foreach (var item in cartItemList)
                        {
                            invoiceDescription.Append("<tr style=\"border-bottom:solid 1px #ccc\">");
                            invoiceDescription.Append("<td>");
                            if (item.InvoiceItemTypeDescription.ToString().Equals("Purchase"))
                            {
                                invoiceDescription.Append("Product");
                            }
                            else
                            {
                                invoiceDescription.Append(item.InvoiceItemTypeDescription.ToString());
                            }
                            invoiceDescription.Append("</td>");
                            invoiceDescription.Append("<td>");
                            invoiceDescription.Append(item.Description.ToString());
                            invoiceDescription.Append("</td>");
                            if (item.InvoiceItemTypeDescription.ToString().Equals("Purchase"))
                            {
                                invoiceDescription.Append("<td>");
                                invoiceDescription.Append(item.ShippingAddress);
                                invoiceDescription.Append("</td>");

                                invoiceDescription.Append("<td>");
                                invoiceDescription.Append(item.PriceString.ToString());
                                invoiceDescription.Append("</td>");
                                isProduct = true;
                            }
                            if (!(item.InvoiceItemTypeDescription.ToString().Equals("Purchase")))
                            {
                                if (isProduct)
                                {
                                    invoiceDescription.Append("<td>");
                                    invoiceDescription.Append("");
                                    invoiceDescription.Append("</td>");

                                    invoiceDescription.Append("<td>");
                                    invoiceDescription.Append(item.PriceString.ToString());
                                    invoiceDescription.Append("</td>");
                                }
                                else
                                {
                                    invoiceDescription.Append("<td>");//colspan=\"2\" style=\"text-align:right;\"
                                    invoiceDescription.Append(item.PriceString.ToString());
                                    invoiceDescription.Append("</td>");
                                }
                               
                            }
                            invoiceDescription.Append("</tr>");
                        }

                        invoiceDescription.Append("</table>");
                        string orderInfo = invoiceDescription.ToString();
                #endregion
                #region PaymentInfo1
                        System.Text.StringBuilder paymentInfoDetails = new System.Text.StringBuilder(); ;
                        paymentInfoDetails.Append("<table style=\"width:600px; border:solid 1px #ccc; font: normal 12px 'lucida sans unicode'; text-align:left\">");
                        paymentInfoDetails.Append("<tr style=\"background:#cacaca\">");
                        paymentInfoDetails.Append("<th>");
                        paymentInfoDetails.Append("Payment Mode");
                        paymentInfoDetails.Append("</th>");
                        paymentInfoDetails.Append("<th>");
                        paymentInfoDetails.Append("Payment Status");
                        paymentInfoDetails.Append("</th>");
                        paymentInfoDetails.Append("</tr>");
                        List<InvoicePaymentDetail> invoicePaymentDetailsList = new InvoicePaymentServiceImpl().GetListByInvoiceId(invoiceObj.Id);
                        foreach (var item1 in invoicePaymentDetailsList)
                        {
                            paymentInfoDetails.Append("<tr>");
                            paymentInfoDetails.Append("<td>");
                            paymentInfoDetails.Append(item1.PaymentMode.ToString());
                            paymentInfoDetails.Append("</td>");
                            paymentInfoDetails.Append("<td>");
                            paymentInfoDetails.Append(item1.PaymentStatus.ToString());
                            paymentInfoDetails.Append("</td>");
                            paymentInfoDetails.Append("</tr>");
                        }
                        paymentInfoDetails.Append("</table>");
                        string paymentInfoType = "";
                        if (IsDisablePaymentMethodForZeroBalancedOrderItem)
                        {
                            paymentInfoType = "";
                        }
                        else
                        {
                            paymentInfoType = paymentInfoDetails.ToString();
                        }
                        //string paymentInfoType = paymentInfoDetails.ToString();
                #endregion
                #region Paymentinfo2
                        System.Text.StringBuilder paymentDetails = new System.Text.StringBuilder(); ;
                        paymentDetails.Append("<table style=\"width:600px; border:solid 1px #ccc; font: normal 12px 'lucida sans unicode';  text-align:left\">");
                        paymentDetails.Append("<tr style=\"background:#cacaca\">");
                        paymentDetails.Append("<th>");
                        paymentDetails.Append("Payment");
                        paymentDetails.Append("</th>");
                        paymentDetails.Append("<th>");
                        paymentDetails.Append("Total Discount");
                        paymentDetails.Append("</th>");
                        paymentDetails.Append("<th>");
                        paymentDetails.Append("Refunds");
                        paymentDetails.Append("</th>");
                        paymentDetails.Append("</tr>");
                        foreach (var item in orderPaymentRefundHelperList)
                        {
                            paymentDetails.Append("<tr>");
                            paymentDetails.Append("<td>");
                            paymentDetails.Append(item.PaymentDetails.ToString());
                            paymentDetails.Append("</td>");
                            paymentDetails.Append("<td>");
                            if (IsDisablePaymentMethodForZeroBalancedOrderItem)
                            {
                                paymentDetails.Append("$0.00");
                            }
                            else
                            {
                                paymentDetails.Append("$ " + totalDiscount.ToString("0.00"));
                            }
                          //  paymentDetails.Append("$ " + totalDiscount.ToString("0.00"));
                            paymentDetails.Append("</td>");
                            paymentDetails.Append("<td>");
                            paymentDetails.Append(item.RefundDetails.ToString());
                            paymentDetails.Append("</td>");
                            paymentDetails.Append("</tr>");
                        }

                        paymentDetails.Append("</table>");
                        string paymentInfo = paymentDetails.ToString();
                #endregion
               
                #region mail body
                        string body = "<BR/>" +
                  "Dear " + userObj.FirstName.Trim() +
                  ", <BR/>" +
                  "Below is the summary of the items you have recently purchased using the WHRC service." +
                  "<BR/>" + "Here is the info for the items ordered and payment details" + "<BR/>" +

                  "<BR/>" + "View Payment" + "<BR/>" +
                  "<table style=\"width:600px; border:solid 1px #ccc; font: normal 12px 'lucida sans unicode';  text-align:left\">" +
                      "<tr style=\"background:#cacaca\">" +
                           "<th>" + "Invoice No" + "</th>" +
                           "<th>" + "Client Name" + "</th>" +
                           "<th>" + "Purchase Date" + "</th>" +
                           "<th>" + "Order Status" + "</th>" +

                      "</tr>" +

                      "<tr>" +
                           "<td>" + "INV-" + invoiceObj.Id.ToString().PadLeft(8, '0') + "</td>" +
                           "<td>" + userObj.FirstName + " " + userObj.LastName + "</td>" +
                           "<td>" + invoiceObj.PurchaseDate.ToString("MM/dd/yyy") + "</td>" +
                           "<td>" + "Not Processed" + "</td>" +
                      "</tr>" +

                  "</table>" +

                  "<BR/>" + "Order Items " + "<BR/>" +
                  orderInfo +


                  "<BR/>" + "Payment Details " + "<BR/>" +
                  paymentInfo +
                   "<BR/>" + "<BR/>" +
                   paymentInfoType +

                  "<BR/><BR/>Thank you for your order." +
                   "<BR/>" + "<BR/>" +
                      GenerateMemberFooter();
                #endregion
                AlternateView av1 = AlternateView.CreateAlternateViewFromString("<html><body>"+body+"</body></html>", null, MediaTypeNames.Text.Html);
                av1.LinkedResources.Add(MemberLogoRight);
                av1.LinkedResources.Add(MemberLogoLeft);
                MailHelper mailHelper = new MailHelper();
                mailHelper.GotImageLogo(av1);
                mailHelper.Send(to, subject, body);
            }
        }

        public void SendEmailForAcknowledgementForOrderProcessFromMemberPortalToAdmin(User adminUserObj, User userObj, List<CartItem> cartItemList, List<OrderPaymentRefundHelper> orderPaymentRefundHelperList, Invoice invoiceObj, decimal totalDiscount, bool IsDisablePaymentMethodForZeroBalancedOrderItem)
        {
            string mailAddress = adminUserObj.Email.ToString();
            bool isProduct = false;
            if (CheckEmail(mailAddress) != false)
            {
                string to = mailAddress;
                string subject = "Whrc Order Status and Payment Details ";

                #region orderInfo
                System.Text.StringBuilder invoiceDescription = new System.Text.StringBuilder(); ;
                invoiceDescription.Append("<table style=\"width:600px; border:solid 1px #ccc; font: normal 12px 'lucida sans unicode'; text-align:left\">");
                invoiceDescription.Append("<tr style=\"background:#cacaca\">");
                invoiceDescription.Append("<th>");
                invoiceDescription.Append("Order Item Type");
                invoiceDescription.Append("</th>");
                invoiceDescription.Append("<th>");
                invoiceDescription.Append("Description");
                invoiceDescription.Append("</th>");
                foreach (var item in cartItemList)
                {
                    if (item.InvoiceItemTypeDescription.ToString().Equals("Purchase"))
                    {
                        invoiceDescription.Append("<th>");
                        invoiceDescription.Append("Shipping Address");
                        invoiceDescription.Append("</th>");
                        break;
                    }
                }
                invoiceDescription.Append("<th>");
                invoiceDescription.Append("Price");
                invoiceDescription.Append("</th>");
                invoiceDescription.Append("</tr>");
                foreach (var item in cartItemList)
                {
                    invoiceDescription.Append("<tr style=\"border-bottom:solid 1px #ccc\">");
                    invoiceDescription.Append("<td>");
                    if (item.InvoiceItemTypeDescription.ToString().Equals("Purchase"))
                    {
                        invoiceDescription.Append("Product");
                    }
                    else
                    {
                        invoiceDescription.Append(item.InvoiceItemTypeDescription.ToString());
                    }
                    invoiceDescription.Append("</td>");
                    invoiceDescription.Append("<td>");
                    invoiceDescription.Append(item.Description.ToString());
                    invoiceDescription.Append("</td>");
                    if (item.InvoiceItemTypeDescription.ToString().Equals("Purchase"))
                    {
                        invoiceDescription.Append("<td>");
                        invoiceDescription.Append(item.ShippingAddress);
                        invoiceDescription.Append("</td>");

                        invoiceDescription.Append("<td>");
                        invoiceDescription.Append(item.PriceString.ToString());
                        invoiceDescription.Append("</td>");
                        isProduct = true;
                    }
                    if (!(item.InvoiceItemTypeDescription.ToString().Equals("Purchase")))
                    {
                        if (isProduct)
                        {
                            invoiceDescription.Append("<td>");
                            invoiceDescription.Append("");
                            invoiceDescription.Append("</td>");

                            invoiceDescription.Append("<td>");
                            invoiceDescription.Append(item.PriceString.ToString());
                            invoiceDescription.Append("</td>");
                        }
                        else
                        {
                            invoiceDescription.Append("<td>");//colspan=\"2\" style=\"text-align:right;\"
                            invoiceDescription.Append(item.PriceString.ToString());
                            invoiceDescription.Append("</td>");
                        }

                    }

                    invoiceDescription.Append("</tr>");
                }

                invoiceDescription.Append("</table>");
                string orderInfo = invoiceDescription.ToString();
                #endregion
                #region PaymentInfo1
                System.Text.StringBuilder paymentInfoDetails = new System.Text.StringBuilder(); ;
                paymentInfoDetails.Append("<table style=\"width:600px; border:solid 1px #ccc; font: normal 12px 'lucida sans unicode'; text-align:left\">");
                paymentInfoDetails.Append("<tr style=\"background:#cacaca\">");
                paymentInfoDetails.Append("<th>");
                paymentInfoDetails.Append("Payment Mode");
                paymentInfoDetails.Append("</th>");
                paymentInfoDetails.Append("<th>");
                paymentInfoDetails.Append("Payment Status");
                paymentInfoDetails.Append("</th>");
                paymentInfoDetails.Append("</tr>");
                List<InvoicePaymentDetail> invoicePaymentDetailsList = new InvoicePaymentServiceImpl().GetListByInvoiceId(invoiceObj.Id);
                foreach (var item1 in invoicePaymentDetailsList)
                {
                    paymentInfoDetails.Append("<tr>");
                    paymentInfoDetails.Append("<td>");
                    paymentInfoDetails.Append(item1.PaymentMode.ToString());
                    paymentInfoDetails.Append("</td>");
                    paymentInfoDetails.Append("<td>");
                    paymentInfoDetails.Append(item1.PaymentStatus.ToString());
                    paymentInfoDetails.Append("</td>");
                    paymentInfoDetails.Append("</tr>");
                }
                paymentInfoDetails.Append("</table>");
                string paymentInfoType = "";
                if (IsDisablePaymentMethodForZeroBalancedOrderItem)
                {
                    paymentInfoType = "";
                }
                else
                {
                     paymentInfoType = paymentInfoDetails.ToString();
                }
                #endregion
                #region Paymentinfo2
                System.Text.StringBuilder paymentDetails = new System.Text.StringBuilder(); ;
                paymentDetails.Append("<table style=\"width:600px; border:solid 1px #ccc; font: normal 12px 'lucida sans unicode';  text-align:left\">");
                paymentDetails.Append("<tr style=\"background:#cacaca\">");
                paymentDetails.Append("<th>");
                paymentDetails.Append("Payment");
                paymentDetails.Append("</th>");
                paymentDetails.Append("<th>");
                paymentDetails.Append("Total Discount");
                paymentDetails.Append("</th>");
                paymentDetails.Append("<th>");
                paymentDetails.Append("Refunds");
                paymentDetails.Append("</th>");
                paymentDetails.Append("</tr>");
                foreach (var item in orderPaymentRefundHelperList)
                {
                    paymentDetails.Append("<tr>");
                    paymentDetails.Append("<td>");
                    paymentDetails.Append(item.PaymentDetails.ToString());
                    paymentDetails.Append("</td>");
                    paymentDetails.Append("<td>");
                    if (IsDisablePaymentMethodForZeroBalancedOrderItem)
                    {
                        paymentDetails.Append("$0.00");
                    }
                    else
                    {
                        paymentDetails.Append("$ " + totalDiscount.ToString("0.00"));
                    }
                   // paymentDetails.Append("$ " + totalDiscount.ToString("0.00"));
                    paymentDetails.Append("</td>");
                    paymentDetails.Append("<td>");
                    paymentDetails.Append(item.RefundDetails.ToString());
                    paymentDetails.Append("</td>");
                    paymentDetails.Append("</tr>");
                }

                paymentDetails.Append("</table>");
                string paymentInfo = paymentDetails.ToString();
                #endregion
                #region mail body
                string body = "<BR/>" +
                  "Dear " + adminUserObj.FirstName +
                  ", <BR/>" +
                  "Below is the summary of the items " + userObj.FirstName + " has recently purchased using the WHRC service." +
                  "<BR/>" + "Here is the info for the items ordered and payment details" + "<BR/>" +

                  "<BR/>" + "View Payment" + "<BR/>" +
                  "<table style=\"width:600px; border:solid 1px #ccc; font: normal 12px 'lucida sans unicode';  text-align:left\">" +
                      "<tr style=\"background:#cacaca\">" +
                           "<th>" + "Invoice No" + "</th>" +
                           "<th>" + "Client Name" + "</th>" +
                           "<th>" + "Purchase Date" + "</th>" +
                           "<th>" + "Order Status" + "</th>" +

                      "</tr>" +

                      "<tr>" +
                           "<td>" + "INV-" + invoiceObj.Id.ToString().PadLeft(8, '0') + "</td>" +
                           "<td>" + userObj.FirstName + " " + userObj.LastName + "</td>" +
                           "<td>" + invoiceObj.PurchaseDate.ToString("MM/dd/yyy") + "</td>" +
                           "<td>" + "Not Processed" + "</td>" +
                      "</tr>" +

                  "</table>" +

                  "<BR/>" + "Order Items " + "<BR/>" +
                  orderInfo +


                  "<BR/>" + "Payment Details " + "<BR/>" +
                  paymentInfo +
                   "<BR/>" + "<BR/>" +
                   paymentInfoType +

                 "<BR/><BR/>Thank you." +
                   "<BR/>" + "<BR/>" +
                      GenerateMemberFooter();
                #endregion
                AlternateView av1 = AlternateView.CreateAlternateViewFromString("<html><body>" + body + "</body></html>", null, MediaTypeNames.Text.Html);
                av1.LinkedResources.Add(MemberLogoRight);
                av1.LinkedResources.Add(MemberLogoLeft);
                MailHelper mailHelper = new MailHelper();
                mailHelper.GotImageLogo(av1);
                mailHelper.Send(to, subject, body);
            }
        }

        public void SendEmailForAcknowledgementForOrderProcessFromMemberPortalToDefaultAdmin(EmailAddress EmailAddressObj, User userObj, List<CartItem> cartItemList, List<OrderPaymentRefundHelper> orderPaymentRefundHelperList, Invoice invoiceObj, decimal totalDiscount, bool IsDisablePaymentMethodForZeroBalancedOrderItem)
        {
            string mailAddress = EmailAddressObj.MailAddress.ToString();
            bool isProduct = false;
            if (CheckEmail(mailAddress) != false)
            {
                string to = mailAddress;
                string subject = "Whrc Order Status and Payment Details ";

                #region orderInfo
                System.Text.StringBuilder invoiceDescription = new System.Text.StringBuilder(); ;
                invoiceDescription.Append("<table style=\"width:600px; border:solid 1px #ccc; font: normal 12px 'lucida sans unicode'; text-align:left\">");
                invoiceDescription.Append("<tr style=\"background:#cacaca\">");
                invoiceDescription.Append("<th>");
                invoiceDescription.Append("Order Item Type");
                invoiceDescription.Append("</th>");
                invoiceDescription.Append("<th>");
                invoiceDescription.Append("Description");
                invoiceDescription.Append("</th>");
                foreach (var item in cartItemList)
                {
                    if (item.InvoiceItemTypeDescription.ToString().Equals("Purchase"))
                    {
                        invoiceDescription.Append("<th>");
                        invoiceDescription.Append("Shipping Address");
                        invoiceDescription.Append("</th>");
                        break;
                    }
                }
                invoiceDescription.Append("<th>");
                invoiceDescription.Append("Price");
                invoiceDescription.Append("</th>");
                invoiceDescription.Append("</tr>");
                foreach (var item in cartItemList)
                {
                    invoiceDescription.Append("<tr style=\"border-bottom:solid 1px #ccc\">");
                    invoiceDescription.Append("<td>");
                    if (item.InvoiceItemTypeDescription.ToString().Equals("Purchase"))
                    {
                        invoiceDescription.Append("Product");
                    }
                    else
                    {
                        invoiceDescription.Append(item.InvoiceItemTypeDescription.ToString());
                    }
                    invoiceDescription.Append("</td>");
                    invoiceDescription.Append("<td>");
                    invoiceDescription.Append(item.Description.ToString());
                    invoiceDescription.Append("</td>");
                    if (item.InvoiceItemTypeDescription.ToString().Equals("Purchase"))
                    {
                        invoiceDescription.Append("<td>");
                        invoiceDescription.Append(item.ShippingAddress);
                        invoiceDescription.Append("</td>");

                        invoiceDescription.Append("<td>");
                        invoiceDescription.Append(item.PriceString.ToString());
                        invoiceDescription.Append("</td>");
                        isProduct = true;
                    }
                    if (!(item.InvoiceItemTypeDescription.ToString().Equals("Purchase")))
                    {
                        if (isProduct)
                        {
                            invoiceDescription.Append("<td>");
                            invoiceDescription.Append("");
                            invoiceDescription.Append("</td>");

                            invoiceDescription.Append("<td>");
                            invoiceDescription.Append(item.PriceString.ToString());
                            invoiceDescription.Append("</td>");
                        }
                        else
                        {
                            invoiceDescription.Append("<td>");//colspan=\"2\" style=\"text-align:right;\"
                            invoiceDescription.Append(item.PriceString.ToString());
                            invoiceDescription.Append("</td>");
                        }

                    }

                    invoiceDescription.Append("</tr>");
                }

                invoiceDescription.Append("</table>");
                string orderInfo = invoiceDescription.ToString();
                #endregion
                #region PaymentInfo1
                System.Text.StringBuilder paymentInfoDetails = new System.Text.StringBuilder(); ;
                paymentInfoDetails.Append("<table style=\"width:600px; border:solid 1px #ccc; font: normal 12px 'lucida sans unicode'; text-align:left\">");
                paymentInfoDetails.Append("<tr style=\"background:#cacaca\">");
                paymentInfoDetails.Append("<th>");
                paymentInfoDetails.Append("Payment Mode");
                paymentInfoDetails.Append("</th>");
                paymentInfoDetails.Append("<th>");
                paymentInfoDetails.Append("Payment Status");
                paymentInfoDetails.Append("</th>");
                paymentInfoDetails.Append("</tr>");
                List<InvoicePaymentDetail> invoicePaymentDetailsList = new InvoicePaymentServiceImpl().GetListByInvoiceId(invoiceObj.Id);
                foreach (var item1 in invoicePaymentDetailsList)
                {
                    paymentInfoDetails.Append("<tr>");
                    paymentInfoDetails.Append("<td>");
                    paymentInfoDetails.Append(item1.PaymentMode.ToString());
                    paymentInfoDetails.Append("</td>");
                    paymentInfoDetails.Append("<td>");
                    paymentInfoDetails.Append(item1.PaymentStatus.ToString());
                    paymentInfoDetails.Append("</td>");
                    paymentInfoDetails.Append("</tr>");
                }
                paymentInfoDetails.Append("</table>");
                string paymentInfoType = "";
                if (IsDisablePaymentMethodForZeroBalancedOrderItem)
                {
                    paymentInfoType = "";
                }
                else
                {
                    paymentInfoType = paymentInfoDetails.ToString();
                }
                #endregion
                #region Paymentinfo2
                System.Text.StringBuilder paymentDetails = new System.Text.StringBuilder(); ;
                paymentDetails.Append("<table style=\"width:600px; border:solid 1px #ccc; font: normal 12px 'lucida sans unicode';  text-align:left\">");
                paymentDetails.Append("<tr style=\"background:#cacaca\">");
                paymentDetails.Append("<th>");
                paymentDetails.Append("Payment");
                paymentDetails.Append("</th>");
                paymentDetails.Append("<th>");
                paymentDetails.Append("Total Discount");
                paymentDetails.Append("</th>");
                paymentDetails.Append("<th>");
                paymentDetails.Append("Refunds");
                paymentDetails.Append("</th>");
                paymentDetails.Append("</tr>");
                foreach (var item in orderPaymentRefundHelperList)
                {
                    paymentDetails.Append("<tr>");
                    paymentDetails.Append("<td>");
                    paymentDetails.Append(item.PaymentDetails.ToString());
                    paymentDetails.Append("</td>");
                    paymentDetails.Append("<td>");
                    if (IsDisablePaymentMethodForZeroBalancedOrderItem)
                    {
                        paymentDetails.Append("$0.00");
                    }
                    else
                    {
                        paymentDetails.Append("$ " + totalDiscount.ToString("0.00"));
                    }
                    // paymentDetails.Append("$ " + totalDiscount.ToString("0.00"));
                    paymentDetails.Append("</td>");
                    paymentDetails.Append("<td>");
                    paymentDetails.Append(item.RefundDetails.ToString());
                    paymentDetails.Append("</td>");
                    paymentDetails.Append("</tr>");
                }

                paymentDetails.Append("</table>");
                string paymentInfo = paymentDetails.ToString();
                #endregion
                #region mail body
                string body = "<BR/>" +
                  "Dear " + "Admin" +
                  ", <BR/>" +
                  "Below is the summary of the items " + userObj.FirstName + " has recently purchased using the WHRC service." +
                  "<BR/>" + "Here is the info for the items ordered and payment details" + "<BR/>" +

                  "<BR/>" + "View Payment" + "<BR/>" +
                  "<table style=\"width:600px; border:solid 1px #ccc; font: normal 12px 'lucida sans unicode';  text-align:left\">" +
                      "<tr style=\"background:#cacaca\">" +
                           "<th>" + "Invoice No" + "</th>" +
                           "<th>" + "Client Name" + "</th>" +
                           "<th>" + "Purchase Date" + "</th>" +
                           "<th>" + "Order Status" + "</th>" +

                      "</tr>" +

                      "<tr>" +
                           "<td>" + "INV-" + invoiceObj.Id.ToString().PadLeft(8, '0') + "</td>" +
                           "<td>" + userObj.FirstName + " " + userObj.LastName + "</td>" +
                           "<td>" + invoiceObj.PurchaseDate.ToString("MM/dd/yyy") + "</td>" +
                           "<td>" + "Not Processed" + "</td>" +
                      "</tr>" +

                  "</table>" +

                  "<BR/>" + "Order Items " + "<BR/>" +
                  orderInfo +


                  "<BR/>" + "Payment Details " + "<BR/>" +
                  paymentInfo +
                   "<BR/>" + "<BR/>" +
                   paymentInfoType +

                 "<BR/><BR/>Thank you." +
                   "<BR/>" + "<BR/>" +
                      GenerateMemberFooter();
                #endregion
                AlternateView av1 = AlternateView.CreateAlternateViewFromString("<html><body>" + body + "</body></html>", null, MediaTypeNames.Text.Html);
                av1.LinkedResources.Add(MemberLogoRight);
                av1.LinkedResources.Add(MemberLogoLeft);
                MailHelper mailHelper = new MailHelper();
                mailHelper.GotImageLogo(av1);
                mailHelper.Send(to, subject, body);
            }
        }

        #endregion

        #region from mom mobile after purchase item

        public void SendEmailForAcknowledgementForOrderProcessFromMomMobilePortalToClient(User userObj, List<CartItem> cartItemList, List<OrderPaymentRefundHelper> orderPaymentRefundHelperList, Invoice invoiceObj, decimal totalDiscount,bool IsDisablePaymentMethodForZeroBalancedOrderItem)
        {
            string mailAddress = userObj.Email.ToString();
            bool isProduct = false;
            if (CheckEmail(mailAddress) != false)
            {
                string to = mailAddress;
                string subject = "Whrc Order Status";

                #region orderInfo
                System.Text.StringBuilder invoiceDescription = new System.Text.StringBuilder(); ;
                invoiceDescription.Append("<table style=\"width:600px; border:solid 1px #ccc; font: normal 12px 'lucida sans unicode'; text-align:left\">");
                invoiceDescription.Append("<tr style=\"background:#cacaca\">");
                invoiceDescription.Append("<th>");
                invoiceDescription.Append("Order Item Type");
                invoiceDescription.Append("</th>");
                invoiceDescription.Append("<th>");
                invoiceDescription.Append("Description");
                invoiceDescription.Append("</th>");
                foreach (var item in cartItemList)
                {
                    if (item.InvoiceItemTypeDescription.ToString().Equals("Purchase"))
                    {
                        invoiceDescription.Append("<th>");
                        invoiceDescription.Append("Shipping Address");
                        invoiceDescription.Append("</th>");
                        break;
                    }
                }
                invoiceDescription.Append("<th>");
                invoiceDescription.Append("Price");
                invoiceDescription.Append("</th>");
                invoiceDescription.Append("</tr>");
                foreach (var item in cartItemList)
                {
                    invoiceDescription.Append("<tr style=\"border-bottom:solid 1px #ccc\">");
                    invoiceDescription.Append("<td>");
                    if (item.InvoiceItemTypeDescription.ToString().Equals("Purchase"))
                    {
                        invoiceDescription.Append("Product");
                    }
                    else
                    {
                        invoiceDescription.Append(item.InvoiceItemTypeDescription.ToString());
                    }
                    invoiceDescription.Append("</td>");
                    invoiceDescription.Append("<td>");
                    invoiceDescription.Append(item.Description.ToString());
                    invoiceDescription.Append("</td>");
                    if (item.InvoiceItemTypeDescription.ToString().Equals("Purchase"))
                    {
                        invoiceDescription.Append("<td>");
                        invoiceDescription.Append(item.ShippingAddress);
                        invoiceDescription.Append("</td>");

                        invoiceDescription.Append("<td>");
                        invoiceDescription.Append(item.PriceString.ToString());
                        invoiceDescription.Append("</td>");
                        isProduct = true;
                    }
                    if (!(item.InvoiceItemTypeDescription.ToString().Equals("Purchase")))
                    {
                        if (isProduct)
                        {
                            invoiceDescription.Append("<td>");
                            invoiceDescription.Append("");
                            invoiceDescription.Append("</td>");

                            invoiceDescription.Append("<td>");
                            invoiceDescription.Append(item.PriceString.ToString());
                            invoiceDescription.Append("</td>");
                            
                        }
                        else
                        {
                            invoiceDescription.Append("<td>");//colspan=\"2\" style=\"text-align:right;\"
                            invoiceDescription.Append(item.PriceString.ToString());
                            invoiceDescription.Append("</td>");
                        }

                    }

                    invoiceDescription.Append("</tr>");
                }

                invoiceDescription.Append("</table>");
                string orderInfo = invoiceDescription.ToString();
                #endregion
                #region PaymentInfo1
                System.Text.StringBuilder paymentInfoDetails = new System.Text.StringBuilder(); ;
                paymentInfoDetails.Append("<table style=\"width:600px; border:solid 1px #ccc; font: normal 12px 'lucida sans unicode'; text-align:left\">");
                paymentInfoDetails.Append("<tr style=\"background:#cacaca\">");
                paymentInfoDetails.Append("<th>");
                paymentInfoDetails.Append("Payment Mode");
                paymentInfoDetails.Append("</th>");
                paymentInfoDetails.Append("<th>");
                paymentInfoDetails.Append("Payment Status");
                paymentInfoDetails.Append("</th>");
                paymentInfoDetails.Append("</tr>");
                List<InvoicePaymentDetail> invoicePaymentDetailsList = new InvoicePaymentServiceImpl().GetListByInvoiceId(invoiceObj.Id);
                foreach (var item1 in invoicePaymentDetailsList)
                {
                    paymentInfoDetails.Append("<tr>");
                    paymentInfoDetails.Append("<td>");
                    paymentInfoDetails.Append(item1.PaymentMode.ToString());
                    paymentInfoDetails.Append("</td>");
                    paymentInfoDetails.Append("<td>");
                    paymentInfoDetails.Append(item1.PaymentStatus.ToString());
                    paymentInfoDetails.Append("</td>");
                    paymentInfoDetails.Append("</tr>");
                }
                paymentInfoDetails.Append("</table>");
                string paymentInfoType = "";
                if (IsDisablePaymentMethodForZeroBalancedOrderItem)
                {
                    paymentInfoType = "";
                }
                else
                {
                    paymentInfoType = paymentInfoDetails.ToString();
                }
               // string paymentInfoType = paymentInfoDetails.ToString();
                #endregion
                #region Paymentinfo2
                System.Text.StringBuilder paymentDetails = new System.Text.StringBuilder(); ;
                paymentDetails.Append("<table style=\"width:600px; border:solid 1px #ccc; font: normal 12px 'lucida sans unicode';  text-align:left\">");
                paymentDetails.Append("<tr style=\"background:#cacaca\">");
                paymentDetails.Append("<th>");
                paymentDetails.Append("Payment");
                paymentDetails.Append("</th>");
                paymentDetails.Append("<th>");
                paymentDetails.Append("Total Discount");
                paymentDetails.Append("</th>");
                paymentDetails.Append("<th>");
                paymentDetails.Append("Refunds");
                paymentDetails.Append("</th>");
                paymentDetails.Append("</tr>");
                foreach (var item in orderPaymentRefundHelperList)
                {
                    paymentDetails.Append("<tr>");
                    paymentDetails.Append("<td>");
                    paymentDetails.Append(item.PaymentDetails.ToString());
                    paymentDetails.Append("</td>");
                    paymentDetails.Append("<td>");
                    if (IsDisablePaymentMethodForZeroBalancedOrderItem)
                    {
                        paymentDetails.Append("$0.00");
                    }
                    else
                    {
                        paymentDetails.Append("$ " + totalDiscount.ToString("0.00"));
                    }
                   // paymentDetails.Append("$ " + totalDiscount.ToString("0.00"));
                    paymentDetails.Append("</td>");
                    paymentDetails.Append("<td>");
                    paymentDetails.Append(item.RefundDetails.ToString());
                    paymentDetails.Append("</td>");
                    paymentDetails.Append("</tr>");
                }

                paymentDetails.Append("</table>");
                string paymentInfo = paymentDetails.ToString();
                #endregion
                #region mail body
                string body = "<BR/>" +
                  "Dear " + userObj.FirstName +
                  ", <BR/>" +
                  "below is the summary of the itmes you have recently purchased using the WHRC service. (This is an automated email. Do <em>NOT</em> reply to this message.)" +
                  "<BR/>" + "Here is the info for the order items and payment details" + "<BR/>" +

                  "<BR/>" + "View Payment" + "<BR/>" +
                  "<table style=\"width:600px; border:solid 1px #ccc; font: normal 12px 'lucida sans unicode';  text-align:left\">" +
                      "<tr style=\"background:#cacaca\">" +
                           "<th>" + "Invoice No" + "</th>" +
                           "<th>" + "Client Name" + "</th>" +
                           "<th>" + "Purchase Date" + "</th>" +
                           "<th>" + "Order Status" + "</th>" +

                      "</tr>" +

                      "<tr>" +
                           "<td>" + "INV-" + invoiceObj.Id.ToString().PadLeft(8, '0') + "</td>" +
                           "<td>" + userObj.FirstName + " " + userObj.LastName + "</td>" +
                           "<td>" + invoiceObj.PurchaseDate.ToString("MM/dd/yyy") + "</td>" +
                           "<td>" + "Not Processed" + "</td>" +
                      "</tr>" +

                  "</table>" +

                  "<BR/>" + "Order Items " + "<BR/>" +
                  orderInfo +


                  "<BR/>" + "Payment Details " + "<BR/>" +
                  paymentInfo +
                   "<BR/>" + "<BR/>" +
                   paymentInfoType +

                  "<BR/><BR/>Thank you." +
                   "<BR/>" + "<BR/>" +
                      GenerateAdminFooter();
                #endregion
                AlternateView av1 = AlternateView.CreateAlternateViewFromString("<html><body>" + body + "</body></html>", null, MediaTypeNames.Text.Html);
                av1.LinkedResources.Add(AdminLogoRight);
                av1.LinkedResources.Add(AdminLogoLeft);
                MailHelper mailHelper = new MailHelper();
                mailHelper.GotImageLogo(av1);
                mailHelper.Send(to, subject, body);
            }
        }

        public void SendEmailForAcknowledgementForOrderProcessFromMomMobileToAdmin(User adminUserObj, User userObj, List<CartItem> cartItemList, List<OrderPaymentRefundHelper> orderPaymentRefundHelperList, Invoice invoiceObj, decimal totalDiscount,bool IsDisablePaymentMethodForZeroBalancedOrderItem)
        {
            string mailAddress = adminUserObj.Email.ToString();
            bool isProduct = false;
            if (CheckEmail(mailAddress) != false)
            {
                string to = mailAddress;
                string subject = "Whrc Order Status and Payment Details ";

                #region orderInfo
                System.Text.StringBuilder invoiceDescription = new System.Text.StringBuilder(); ;
                invoiceDescription.Append("<table style=\"width:600px; border:solid 1px #ccc; font: normal 12px 'lucida sans unicode'; text-align:left\">");
                invoiceDescription.Append("<tr style=\"background:#cacaca\">");
                invoiceDescription.Append("<th>");
                invoiceDescription.Append("Order Item Type");
                invoiceDescription.Append("</th>");
                invoiceDescription.Append("<th>");
                invoiceDescription.Append("Description");
                invoiceDescription.Append("</th>");
                foreach (var item in cartItemList)
                {
                    if (item.InvoiceItemTypeDescription.ToString().Equals("Purchase"))
                    {
                        invoiceDescription.Append("<th>");
                        invoiceDescription.Append("Shipping Address");
                        invoiceDescription.Append("</th>");
                        break;
                    }
                }
                invoiceDescription.Append("<th>");
                invoiceDescription.Append("Price");
                invoiceDescription.Append("</th>");
                invoiceDescription.Append("</tr>");
                foreach (var item in cartItemList)
                {
                    invoiceDescription.Append("<tr style=\"border-bottom:solid 1px #ccc\">");
                    invoiceDescription.Append("<td>");
                    if (item.InvoiceItemTypeDescription.ToString().Equals("Purchase"))
                    {
                        invoiceDescription.Append("Product");
                    }
                    else
                    {
                        invoiceDescription.Append(item.InvoiceItemTypeDescription.ToString());
                    }
                    invoiceDescription.Append("</td>");
                    invoiceDescription.Append("<td>");
                    invoiceDescription.Append(item.Description.ToString());
                    invoiceDescription.Append("</td>");
                    if (item.InvoiceItemTypeDescription.ToString().Equals("Purchase"))
                    {
                        invoiceDescription.Append("<td>");
                        invoiceDescription.Append(item.ShippingAddress);
                        invoiceDescription.Append("</td>");

                        invoiceDescription.Append("<td>");
                        invoiceDescription.Append(item.PriceString.ToString());
                        invoiceDescription.Append("</td>");
                        isProduct = true;
                    }
                    if (!(item.InvoiceItemTypeDescription.ToString().Equals("Purchase")))
                    {
                        if (isProduct)
                        {
                            invoiceDescription.Append("<td>");
                            invoiceDescription.Append("");
                            invoiceDescription.Append("</td>");

                            invoiceDescription.Append("<td>");
                            invoiceDescription.Append(item.PriceString.ToString());
                            invoiceDescription.Append("</td>");
                        }
                        else
                        {
                            invoiceDescription.Append("<td>");//colspan=\"2\" style=\"text-align:right;\"
                            invoiceDescription.Append(item.PriceString.ToString());
                            invoiceDescription.Append("</td>");
                        }

                    }

                    invoiceDescription.Append("</tr>");
                }

                invoiceDescription.Append("</table>");
                string orderInfo = invoiceDescription.ToString();
                #endregion
                #region PaymentInfo1
                System.Text.StringBuilder paymentInfoDetails = new System.Text.StringBuilder(); ;
                paymentInfoDetails.Append("<table style=\"width:600px; border:solid 1px #ccc; font: normal 12px 'lucida sans unicode'; text-align:left\">");
                paymentInfoDetails.Append("<tr style=\"background:#cacaca\">");
                paymentInfoDetails.Append("<th>");
                paymentInfoDetails.Append("Payment Mode");
                paymentInfoDetails.Append("</th>");
                paymentInfoDetails.Append("<th>");
                paymentInfoDetails.Append("Payment Status");
                paymentInfoDetails.Append("</th>");
                paymentInfoDetails.Append("</tr>");
                List<InvoicePaymentDetail> invoicePaymentDetailsList = new InvoicePaymentServiceImpl().GetListByInvoiceId(invoiceObj.Id);
                foreach (var item1 in invoicePaymentDetailsList)
                {
                    paymentInfoDetails.Append("<tr>");
                    paymentInfoDetails.Append("<td>");
                    paymentInfoDetails.Append(item1.PaymentMode.ToString());
                    paymentInfoDetails.Append("</td>");
                    paymentInfoDetails.Append("<td>");
                    paymentInfoDetails.Append(item1.PaymentStatus.ToString());
                    paymentInfoDetails.Append("</td>");
                    paymentInfoDetails.Append("</tr>");
                }
                paymentInfoDetails.Append("</table>");
                string paymentInfoType = "";
                if (IsDisablePaymentMethodForZeroBalancedOrderItem)
                {
                    paymentInfoType = "";
                }
                else
                {
                    paymentInfoType = paymentInfoDetails.ToString();
                }
                //string paymentInfoType = paymentInfoDetails.ToString();
                #endregion
                #region Paymentinfo2
                System.Text.StringBuilder paymentDetails = new System.Text.StringBuilder(); ;
                paymentDetails.Append("<table style=\"width:600px; border:solid 1px #ccc; font: normal 12px 'lucida sans unicode';  text-align:left\">");
                paymentDetails.Append("<tr style=\"background:#cacaca\">");
                paymentDetails.Append("<th>");
                paymentDetails.Append("Payment");
                paymentDetails.Append("</th>");
                paymentDetails.Append("<th>");
                paymentDetails.Append("Total Discount");
                paymentDetails.Append("</th>");
                paymentDetails.Append("<th>");
                paymentDetails.Append("Refunds");
                paymentDetails.Append("</th>");
                paymentDetails.Append("</tr>");
                foreach (var item in orderPaymentRefundHelperList)
                {
                    paymentDetails.Append("<tr>");
                    paymentDetails.Append("<td>");
                    paymentDetails.Append(item.PaymentDetails.ToString());
                    paymentDetails.Append("</td>");
                    paymentDetails.Append("<td>");
                    if (IsDisablePaymentMethodForZeroBalancedOrderItem)
                    {
                         paymentDetails.Append("$0.00");
                    }
                    else
                    {
                         paymentDetails.Append("$ " + totalDiscount.ToString("0.00"));
                    }
                   // paymentDetails.Append("$ " + totalDiscount.ToString("0.00"));
                    paymentDetails.Append("</td>");
                    paymentDetails.Append("<td>");
                    paymentDetails.Append(item.RefundDetails.ToString());
                    paymentDetails.Append("</td>");
                    paymentDetails.Append("</tr>");
                }

                paymentDetails.Append("</table>");
                string paymentInfo = paymentDetails.ToString();
                #endregion
                #region mail body
                string body = "<BR/>" +
                  "Dear " + adminUserObj.FirstName +
                  ", <BR/>" +
                  "below is the summary of the itmes " + userObj.FirstName + " have recently purchased using the WHRC service. (This is an automated email. Do <em>NOT</em> reply to this message.)" +
                  "<BR/>" + "Here is the info for the order items and payment details" + "<BR/>" +

                  "<BR/>" + "View Payment" + "<BR/>" +
                  "<table style=\"width:600px; border:solid 1px #ccc; font: normal 12px 'lucida sans unicode';  text-align:left\">" +
                      "<tr style=\"background:#cacaca\">" +
                           "<th>" + "Invoice No" + "</th>" +
                           "<th>" + "Client Name" + "</th>" +
                           "<th>" + "Purchase Date" + "</th>" +
                           "<th>" + "Order Status" + "</th>" +

                      "</tr>" +

                      "<tr>" +
                           "<td>" + "INV-" + invoiceObj.Id.ToString().PadLeft(8, '0') + "</td>" +
                           "<td>" + userObj.FirstName + " " + userObj.LastName + "</td>" +
                           "<td>" + invoiceObj.PurchaseDate.ToString("MM/dd/yyy") + "</td>" +
                           "<td>" + "Not Processed" + "</td>" +
                      "</tr>" +

                  "</table>" +

                  "<BR/>" + "Order Items " + "<BR/>" +
                  orderInfo +


                  "<BR/>" + "Payment Details " + "<BR/>" +
                  paymentInfo +
                   "<BR/>" + "<BR/>" +
                   paymentInfoType +

                   "<BR/><BR/>Thank you." +
                   "<BR/>" + "<BR/>" +
                    GenerateAdminFooter();
                #endregion
                AlternateView av1 = AlternateView.CreateAlternateViewFromString("<html><body>" + body + "</body></html>", null, MediaTypeNames.Text.Html);
                av1.LinkedResources.Add(AdminLogoRight);
                av1.LinkedResources.Add(AdminLogoLeft);
                MailHelper mailHelper = new MailHelper();
                mailHelper.GotImageLogo(av1);
                mailHelper.Send(to, subject, body);
            }
        }

        public void SendEmailForAcknowledgementForOrderProcessFromMomMobileToDefaultAdmin(EmailAddress EmailAddressObj, User userObj, List<CartItem> cartItemList, List<OrderPaymentRefundHelper> orderPaymentRefundHelperList, Invoice invoiceObj, decimal totalDiscount, bool IsDisablePaymentMethodForZeroBalancedOrderItem)
        {
            string mailAddress = EmailAddressObj.MailAddress.ToString();
            bool isProduct = false;
            if (CheckEmail(mailAddress) != false)
            {
                string to = mailAddress;
                string subject = "Whrc Order Status and Payment Details ";

                #region orderInfo
                System.Text.StringBuilder invoiceDescription = new System.Text.StringBuilder(); ;
                invoiceDescription.Append("<table style=\"width:600px; border:solid 1px #ccc; font: normal 12px 'lucida sans unicode'; text-align:left\">");
                invoiceDescription.Append("<tr style=\"background:#cacaca\">");
                invoiceDescription.Append("<th>");
                invoiceDescription.Append("Order Item Type");
                invoiceDescription.Append("</th>");
                invoiceDescription.Append("<th>");
                invoiceDescription.Append("Description");
                invoiceDescription.Append("</th>");
                foreach (var item in cartItemList)
                {
                    if (item.InvoiceItemTypeDescription.ToString().Equals("Purchase"))
                    {
                        invoiceDescription.Append("<th>");
                        invoiceDescription.Append("Shipping Address");
                        invoiceDescription.Append("</th>");
                        break;
                    }
                }
                invoiceDescription.Append("<th>");
                invoiceDescription.Append("Price");
                invoiceDescription.Append("</th>");
                invoiceDescription.Append("</tr>");
                foreach (var item in cartItemList)
                {
                    invoiceDescription.Append("<tr style=\"border-bottom:solid 1px #ccc\">");
                    invoiceDescription.Append("<td>");
                    if (item.InvoiceItemTypeDescription.ToString().Equals("Purchase"))
                    {
                        invoiceDescription.Append("Product");
                    }
                    else
                    {
                        invoiceDescription.Append(item.InvoiceItemTypeDescription.ToString());
                    }
                    invoiceDescription.Append("</td>");
                    invoiceDescription.Append("<td>");
                    invoiceDescription.Append(item.Description.ToString());
                    invoiceDescription.Append("</td>");
                    if (item.InvoiceItemTypeDescription.ToString().Equals("Purchase"))
                    {
                        invoiceDescription.Append("<td>");
                        invoiceDescription.Append(item.ShippingAddress);
                        invoiceDescription.Append("</td>");

                        invoiceDescription.Append("<td>");
                        invoiceDescription.Append(item.PriceString.ToString());
                        invoiceDescription.Append("</td>");
                        isProduct = true;
                    }
                    if (!(item.InvoiceItemTypeDescription.ToString().Equals("Purchase")))
                    {
                        if (isProduct)
                        {
                            invoiceDescription.Append("<td>");
                            invoiceDescription.Append("");
                            invoiceDescription.Append("</td>");

                            invoiceDescription.Append("<td>");
                            invoiceDescription.Append(item.PriceString.ToString());
                            invoiceDescription.Append("</td>");
                        }
                        else
                        {
                            invoiceDescription.Append("<td>");//colspan=\"2\" style=\"text-align:right;\"
                            invoiceDescription.Append(item.PriceString.ToString());
                            invoiceDescription.Append("</td>");
                        }

                    }

                    invoiceDescription.Append("</tr>");
                }

                invoiceDescription.Append("</table>");
                string orderInfo = invoiceDescription.ToString();
                #endregion
                #region PaymentInfo1
                System.Text.StringBuilder paymentInfoDetails = new System.Text.StringBuilder(); ;
                paymentInfoDetails.Append("<table style=\"width:600px; border:solid 1px #ccc; font: normal 12px 'lucida sans unicode'; text-align:left\">");
                paymentInfoDetails.Append("<tr style=\"background:#cacaca\">");
                paymentInfoDetails.Append("<th>");
                paymentInfoDetails.Append("Payment Mode");
                paymentInfoDetails.Append("</th>");
                paymentInfoDetails.Append("<th>");
                paymentInfoDetails.Append("Payment Status");
                paymentInfoDetails.Append("</th>");
                paymentInfoDetails.Append("</tr>");
                List<InvoicePaymentDetail> invoicePaymentDetailsList = new InvoicePaymentServiceImpl().GetListByInvoiceId(invoiceObj.Id);
                foreach (var item1 in invoicePaymentDetailsList)
                {
                    paymentInfoDetails.Append("<tr>");
                    paymentInfoDetails.Append("<td>");
                    paymentInfoDetails.Append(item1.PaymentMode.ToString());
                    paymentInfoDetails.Append("</td>");
                    paymentInfoDetails.Append("<td>");
                    paymentInfoDetails.Append(item1.PaymentStatus.ToString());
                    paymentInfoDetails.Append("</td>");
                    paymentInfoDetails.Append("</tr>");
                }
                paymentInfoDetails.Append("</table>");
                string paymentInfoType = "";
                if (IsDisablePaymentMethodForZeroBalancedOrderItem)
                {
                    paymentInfoType = "";
                }
                else
                {
                    paymentInfoType = paymentInfoDetails.ToString();
                }
                //string paymentInfoType = paymentInfoDetails.ToString();
                #endregion
                #region Paymentinfo2
                System.Text.StringBuilder paymentDetails = new System.Text.StringBuilder(); ;
                paymentDetails.Append("<table style=\"width:600px; border:solid 1px #ccc; font: normal 12px 'lucida sans unicode';  text-align:left\">");
                paymentDetails.Append("<tr style=\"background:#cacaca\">");
                paymentDetails.Append("<th>");
                paymentDetails.Append("Payment");
                paymentDetails.Append("</th>");
                paymentDetails.Append("<th>");
                paymentDetails.Append("Total Discount");
                paymentDetails.Append("</th>");
                paymentDetails.Append("<th>");
                paymentDetails.Append("Refunds");
                paymentDetails.Append("</th>");
                paymentDetails.Append("</tr>");
                foreach (var item in orderPaymentRefundHelperList)
                {
                    paymentDetails.Append("<tr>");
                    paymentDetails.Append("<td>");
                    paymentDetails.Append(item.PaymentDetails.ToString());
                    paymentDetails.Append("</td>");
                    paymentDetails.Append("<td>");
                    if (IsDisablePaymentMethodForZeroBalancedOrderItem)
                    {
                        paymentDetails.Append("$0.00");
                    }
                    else
                    {
                        paymentDetails.Append("$ " + totalDiscount.ToString("0.00"));
                    }
                    // paymentDetails.Append("$ " + totalDiscount.ToString("0.00"));
                    paymentDetails.Append("</td>");
                    paymentDetails.Append("<td>");
                    paymentDetails.Append(item.RefundDetails.ToString());
                    paymentDetails.Append("</td>");
                    paymentDetails.Append("</tr>");
                }

                paymentDetails.Append("</table>");
                string paymentInfo = paymentDetails.ToString();
                #endregion
                #region mail body
                string body = "<BR/>" +
                  "Dear " + "Admin" +
                  ", <BR/>" +
                  "below is the summary of the itmes " + userObj.FirstName + " have recently purchased using the WHRC service. (This is an automated email. Do <em>NOT</em> reply to this message.)" +
                  "<BR/>" + "Here is the info for the order items and payment details" + "<BR/>" +

                  "<BR/>" + "View Payment" + "<BR/>" +
                  "<table style=\"width:600px; border:solid 1px #ccc; font: normal 12px 'lucida sans unicode';  text-align:left\">" +
                      "<tr style=\"background:#cacaca\">" +
                           "<th>" + "Invoice No" + "</th>" +
                           "<th>" + "Client Name" + "</th>" +
                           "<th>" + "Purchase Date" + "</th>" +
                           "<th>" + "Order Status" + "</th>" +

                      "</tr>" +

                      "<tr>" +
                           "<td>" + "INV-" + invoiceObj.Id.ToString().PadLeft(8, '0') + "</td>" +
                           "<td>" + userObj.FirstName + " " + userObj.LastName + "</td>" +
                           "<td>" + invoiceObj.PurchaseDate.ToString("MM/dd/yyy") + "</td>" +
                           "<td>" + "Not Processed" + "</td>" +
                      "</tr>" +

                  "</table>" +

                  "<BR/>" + "Order Items " + "<BR/>" +
                  orderInfo +


                  "<BR/>" + "Payment Details " + "<BR/>" +
                  paymentInfo +
                   "<BR/>" + "<BR/>" +
                   paymentInfoType +
  "<BR/><BR/>Thank you." +
                   "<BR/>" + "<BR/>" +
                      GenerateAdminFooter();
                #endregion
                AlternateView av1 = AlternateView.CreateAlternateViewFromString("<html><body>" + body + "</body></html>", null, MediaTypeNames.Text.Html);
                av1.LinkedResources.Add(AdminLogoRight);
                av1.LinkedResources.Add(AdminLogoLeft);
                MailHelper mailHelper = new MailHelper();
                mailHelper.GotImageLogo(av1);
                mailHelper.Send(to, subject, body);
            }
        }

        #endregion

        #region from admin portal after order processed
        public void SendEmailForAcknowledgementForOrderProcessFromAdminPortalToClient(List<InvoicePaymentDetail> invoicePaymentDetailsList, Invoice invoiceObj, User adminUserObj, User userObj, List<CartItem> cartItemList, decimal totalPackageDiscount, string ActualAmount, string totalPaidAmount, string netPayable, bool IsDisablePaymentMethodForZeroBalancedOrderItem)
        {
             string mailAddress = userObj.Email.ToString();
             bool isProduct = false;
            //string mailAddress = "rabeya@jijoty.org";

            string totalPrice = null;
            if (CheckEmail(mailAddress) != false)
            {
                string to = mailAddress;
                string subject = "Whrc Order Processed Acknowledgement";
                #region Order Item
                System.Text.StringBuilder invoiceDescription = new System.Text.StringBuilder(); ;
                invoiceDescription.Append("<table style=\"width:600px; border:solid 1px #ccc; font: normal 12px 'lucida sans unicode'; text-align:left\">");
                invoiceDescription.Append("<tr style=\"background:#cacaca\">");
                invoiceDescription.Append("<th>");
                invoiceDescription.Append("Order Item Type");
                invoiceDescription.Append("</th>");
                invoiceDescription.Append("<th>");
                invoiceDescription.Append("Description");
                invoiceDescription.Append("</th>");
                foreach (var item in cartItemList)
                {
                    if (item.InvoiceItemTypeDescription.ToString().Equals("Purchase"))
                    {
                        invoiceDescription.Append("<th>");
                        invoiceDescription.Append("Shipping Address");
                        invoiceDescription.Append("</th>");
                        break;
                    }
                }
                invoiceDescription.Append("<th>");
                invoiceDescription.Append("Price ($)");
                invoiceDescription.Append("</th>");
                invoiceDescription.Append("</tr>");
                foreach (var item in cartItemList)
                {
                    invoiceDescription.Append("<tr style=\"border-bottom:solid 1px #ccc\">");
                    invoiceDescription.Append("<td>");
                    if (item.InvoiceItemTypeDescription.ToString().Equals("Purchase"))
                    {
                        invoiceDescription.Append("Product");
                    }
                    else
                    {
                        invoiceDescription.Append(item.InvoiceItemTypeDescription.ToString());
                    }
                    invoiceDescription.Append("</td>");
                    invoiceDescription.Append("<td>");
                    invoiceDescription.Append(item.Description.ToString());
                    invoiceDescription.Append("</td>");
                   
                    if (item.InvoiceItemTypeDescription.ToString().Equals("Purchase"))
                    {
                        invoiceDescription.Append("<td>");
                        invoiceDescription.Append(item.ShippingAddress);
                        invoiceDescription.Append("</td>");

                        invoiceDescription.Append("<td>");
                        invoiceDescription.Append(item.PriceString.ToString());
                        invoiceDescription.Append("</td>");
                        isProduct = true;
                    }
                    if (!(item.InvoiceItemTypeDescription.ToString().Equals("Purchase")))
                    {
                        if (isProduct)
                        {
                            invoiceDescription.Append("<td>");
                            invoiceDescription.Append("");
                            invoiceDescription.Append("</td>");

                            invoiceDescription.Append("<td>");
                            invoiceDescription.Append(item.PriceString.ToString());
                            invoiceDescription.Append("</td>");
                        }
                        else
                        {
                            invoiceDescription.Append("<td>");//colspan=\"2\" style=\"text-align:right;\"
                            invoiceDescription.Append(item.PriceString.ToString());
                            invoiceDescription.Append("</td>");
                        }

                    }
                    totalPrice = item.Price.ToString();
                    invoiceDescription.Append("</tr>");
                }
                invoiceDescription.Append("</table>");
                string orderInfo = invoiceDescription.ToString();
                #endregion
                #region paymentmode
                System.Text.StringBuilder paymentMode = new System.Text.StringBuilder(); ;
                paymentMode.Append("<table style=\"width:600px; border:solid 1px #ccc; font: normal 12px 'lucida sans unicode'; text-align:left\">");
                paymentMode.Append("<tr style=\"background:#cacaca\">");
                paymentMode.Append("<th>");
                paymentMode.Append("Payment Mode");
                paymentMode.Append("</th>");
                paymentMode.Append("</tr>");
                int count = 0;
                foreach (var item1 in invoicePaymentDetailsList)
                {
                    paymentMode.Append("<tr style=\"border-bottom:solid 1px #ccc\">");
                    paymentMode.Append("<td>");
                    if (count > 0)
                    {
                        paymentMode.Append("<br/>");
                    }
                    if (item1.PaymentMode == PaymentType.Medical)
                    {
                        string medicalNumber = item1.MedicalNumberOrNationalId.ToString();
                        paymentMode.Append(item1.PaymentMode.ToString());
                        paymentMode.Append("<br/>Medical Number - ");
                        paymentMode.Append(medicalNumber);
                    }
                    else if (item1.PaymentMode == PaymentType.Check)
                    {
                        string CheckNumber = item1.CheckNumber.ToString();
                        paymentMode.Append(item1.PaymentMode.ToString());
                        paymentMode.Append("<br/>Check Number - ");
                        paymentMode.Append(CheckNumber);
                    }
                    else
                    {
                        paymentMode.Append(item1.PaymentMode.ToString());
                    }
                    paymentMode.Append("</td>");
                    paymentMode.Append("</tr>");
                    count++;
                }
                paymentMode.Append("</table>");
                string paymentModeInfo = "";
                string PaidStatus = "";
                if (IsDisablePaymentMethodForZeroBalancedOrderItem)
                {
                    paymentModeInfo = "";
                    PaidStatus = "";
                }
                else
                {
                    paymentModeInfo = paymentMode.ToString();
                    PaidStatus = "Paid";
                }
                #endregion
                #region payment Info1 
                System.Text.StringBuilder payemtInfoList = new System.Text.StringBuilder(); ;
                payemtInfoList.Append("<table style=\"width:600px; border:solid 1px #ccc; font: normal 12px 'lucida sans unicode'; text-align:left\">");
                payemtInfoList.Append("<tr style=\"background:#cacaca\">");
                payemtInfoList.Append("<th>");
                payemtInfoList.Append("Payment Date");
                payemtInfoList.Append("</th>");
                payemtInfoList.Append("<th>");
                payemtInfoList.Append("Patient Receipt Number");
                payemtInfoList.Append("</th>");
                payemtInfoList.Append("</tr>");
                foreach (var item in invoicePaymentDetailsList)
                {
                        DateTime date = item.PaymentDate.Date;
                        payemtInfoList.Append("<tr style=\"border-bottom:solid 1px #ccc\">");
                        payemtInfoList.Append("<td>");
                        payemtInfoList.Append(date.ToString("MM/dd/yyy"));
                        payemtInfoList.Append("</td>");
                        payemtInfoList.Append("<td>");
                        payemtInfoList.Append(item.PatientReceiptNumber.ToString());
                        payemtInfoList.Append("</td>");
                        payemtInfoList.Append("</tr>");
                        break;
                }
                payemtInfoList.Append("</table>");
                string payemtInfo = payemtInfoList.ToString();
                #endregion
                string PackageDiscount = null;
                if (IsDisablePaymentMethodForZeroBalancedOrderItem)
                {
                    PackageDiscount = "$0.00";
                }
                else
                {
                    if (invoiceObj.PackageDiscount == 0)
                    {
                        PackageDiscount = totalPackageDiscount.ToString("0.00");
                    }
                    else
                    {
                        PackageDiscount = invoiceObj.PackageDiscount.ToString("0.00");
                    }
                }
                if (invoiceObj.PaymentMode == PaymentType.CreditCard)
                {
                    totalPaidAmount = invoiceObj.PaidAmount.ToString("0.00");
                    netPayable = "0.00";
                }
                string paidAmount = "";
                string test = "0.00";
                string totalpaid = totalPaidAmount.Substring(0, 4);
                // if(!totalPaidAmount.Equals("\"0.00\""))
                if (!(totalpaid.Equals(test)))
               // if (!totalPaidAmount.Equals("\"0.00\""))
                {
                    paidAmount = totalPaidAmount;
                }
                else
                {
                    paidAmount = netPayable;
                }
                #region mail body
                string body = "<BR/>" +
                         "Dear "+userObj.FirstName.Trim() +
                         ", <BR/>" +
                         "Below is the summary of the items you have recently purchased using the WHRC service.Your order has been processed and reviewed by " + adminUserObj.FirstName + " " + adminUserObj.LastName + "." +
                         "<BR/>" + "Here is the info for the items ordered and payment details" + "<BR/>" +

                         "<BR/>" + "View Payment" + "<BR/>" +
                         "<table style=\"width:600px; border:solid 1px #ccc; font: normal 12px 'lucida sans unicode';  text-align:left\">" +
                             "<tr style=\"background:#cacaca\">" +
                                  "<th>" + "Invoice No" + "</th>" +
                                  "<th>" + "Client Name" + "</th>" +
                                  "<th>" + "Purchase Date" + "</th>" +
                                  "<th>" + "Order Status" + "</th>" +
                                  "<th>" + "Payment Status" + "</th>" +
                             "</tr>" +

                             "<tr>" +
                                  "<td>" + "INV-" + invoiceObj.Id.ToString().PadLeft(8, '0') + "</td>" +
                                  "<td>" + userObj.FirstName + " " + userObj.LastName + "</td>" +
                                  "<td>" + invoiceObj.PurchaseDate.ToString("MM/dd/yyy") + "</td>" +
                                  "<td>" + "Processed" + "</td>" +
                                  "<td>" + PaidStatus + "</td>" +
                             "</tr>" +

                         "</table>" +

                         "<BR/>" + "Order Items " + "<BR/>" +
                         orderInfo +
                         "<BR/>" + "Payment Details " + "<BR/>" +
                          "<table style=\"width:600px; border:solid 1px #ccc; font: normal 12px 'lucida sans unicode';  text-align:left\">" +
                             "<tr style=\"background:#cacaca\">" +
                                  "<th>" + "Actual Amount" + "</th>" +
                                  "<th>" + "Discount Message" + "</th>" +
                                  "<th>" + "Package Discount" + "</th>" +
                                  "<th>" + "Discount Amount" + "</th>" +
                                  "<th>" + "Total Paid" + "</th>" +
                                  "<th>" + "Net Payable" + "</th>" +
                             "</tr>" +
                             "<tr>" +
                                  "<td>" + "$ "+ ActualAmount + "</td>" +
                                  "<td>" + invoiceObj.ClientRequestedDiscountMsg + "</td>" +
                                  "<td>" + "$ " + PackageDiscount + "</td>" +
                                  "<td>" + "$ " + invoiceObj.Discount.ToString("0.00") + "</td>" +
                                   "<td>" + "$ " + paidAmount + "</td>" +
                                   "<td>" + "$ " + "0.00" + "</td>" +//totalPaidAmount
                             "</tr>" +

                         "</table>" +
                         payemtInfo +
                         "<BR/><BR/>"+
                         paymentModeInfo+
                          "<BR/><BR/>Thank you for your order." +
                   "<BR/>" + "<BR/>" +
                     GenerateAdminFooter();
                #endregion
                AlternateView av1 = AlternateView.CreateAlternateViewFromString("<html><body>" + body + "</body></html>", null, MediaTypeNames.Text.Html);
                av1.LinkedResources.Add(AdminLogoRight);
                av1.LinkedResources.Add(AdminLogoLeft);
                MailHelper mailHelper = new MailHelper();
                mailHelper.GotImageLogo(av1);
                mailHelper.Send(to, subject, body);
            }
        }

        public void SendEmailForAcknowledgementForOrderProcessFromAdminPortalToAdmin(List<InvoicePaymentDetail> invoicePaymentDetailsList, Invoice invoiceObj, User adminUserObj, User userObj, List<CartItem> cartItemList, decimal totalPackageDiscount, string ActualAmount, string totalPaidAmount, string netPayable, bool IsDisablePaymentMethodForZeroBalancedOrderItem)
        {
            string mailAddress = adminUserObj.Email.ToString();
            bool isProduct = false;
            //string mailAddress = "rabeya@jijoty.org";

            string totalPrice = null;
            if (CheckEmail(mailAddress) != false)
            {
                string to = mailAddress;
                string subject = "Whrc Order Processed Acknowledgement";
                #region Order Item
                System.Text.StringBuilder invoiceDescription = new System.Text.StringBuilder(); ;
                invoiceDescription.Append("<table style=\"width:600px; border:solid 1px #ccc; font: normal 12px 'lucida sans unicode'; text-align:left\">");
                invoiceDescription.Append("<tr style=\"background:#cacaca\">");
                invoiceDescription.Append("<th>");
                invoiceDescription.Append("Order Item Type");
                invoiceDescription.Append("</th>");
                invoiceDescription.Append("<th>");
                invoiceDescription.Append("Description");
                invoiceDescription.Append("</th>");
                foreach (var item in cartItemList)
                {
                    if (item.InvoiceItemTypeDescription.ToString().Equals("Purchase"))
                    {
                        invoiceDescription.Append("<th>");
                        invoiceDescription.Append("Shipping Address");
                        invoiceDescription.Append("</th>");
                        break;
                    }
                }
                invoiceDescription.Append("<th>");
                invoiceDescription.Append("Price ($)");
                invoiceDescription.Append("</th>");
                invoiceDescription.Append("</tr>");
                foreach (var item in cartItemList)
                {
                    invoiceDescription.Append("<tr style=\"border-bottom:solid 1px #ccc\">");
                    invoiceDescription.Append("<td>");
                    if (item.InvoiceItemTypeDescription.ToString().Equals("Purchase"))
                    {
                        invoiceDescription.Append("Product");
                    }
                    else
                    {
                        invoiceDescription.Append(item.InvoiceItemTypeDescription.ToString());
                    }
                    invoiceDescription.Append("</td>");
                    invoiceDescription.Append("<td>");
                    invoiceDescription.Append(item.Description.ToString());
                    invoiceDescription.Append("</td>");
                    if (item.InvoiceItemTypeDescription.ToString().Equals("Purchase"))
                    {
                        invoiceDescription.Append("<td>");
                        invoiceDescription.Append(item.ShippingAddress);
                        invoiceDescription.Append("</td>");

                        invoiceDescription.Append("<td>");
                        invoiceDescription.Append(item.PriceString.ToString());
                        invoiceDescription.Append("</td>");
                        isProduct = true;
                    }
                    if (!(item.InvoiceItemTypeDescription.ToString().Equals("Purchase")))
                    {
                        if (isProduct)
                        {
                            invoiceDescription.Append("<td>");
                            invoiceDescription.Append("");
                            invoiceDescription.Append("</td>");

                            invoiceDescription.Append("<td>");
                            invoiceDescription.Append(item.PriceString.ToString());
                            invoiceDescription.Append("</td>");
                        }
                        else
                        {
                            invoiceDescription.Append("<td>");//colspan=\"2\" style=\"text-align:right;\"
                            invoiceDescription.Append(item.PriceString.ToString());
                            invoiceDescription.Append("</td>");
                        }

                    }
                    totalPrice = item.Price.ToString();
                    invoiceDescription.Append("</tr>");
                }
                invoiceDescription.Append("</table>");
                string orderInfo = invoiceDescription.ToString();
                #endregion
                #region paymentmode
                System.Text.StringBuilder paymentMode = new System.Text.StringBuilder(); ;
                paymentMode.Append("<table style=\"width:600px; border:solid 1px #ccc; font: normal 12px 'lucida sans unicode'; text-align:left\">");
                paymentMode.Append("<tr style=\"background:#cacaca\">");
                paymentMode.Append("<th>");
                paymentMode.Append("Payment Mode");
                paymentMode.Append("</th>");
                paymentMode.Append("</tr>");
                int count = 0;
                foreach (var item1 in invoicePaymentDetailsList)
                {
                    paymentMode.Append("<tr style=\"border-bottom:solid 1px #ccc\">");
                    paymentMode.Append("<td>");
                    if (count > 0)
                    {
                        paymentMode.Append("<br/>");
                    }
                    if (item1.PaymentMode == PaymentType.Medical)
                    {
                        string medicalNumber = item1.MedicalNumberOrNationalId.ToString();
                        paymentMode.Append(item1.PaymentMode.ToString());
                        paymentMode.Append("<br/>medicalNumber - ");
                        paymentMode.Append(medicalNumber);
                    }
                    else if (item1.PaymentMode == PaymentType.Check)
                    {
                        string CheckNumber = item1.CheckNumber.ToString();
                        paymentMode.Append(item1.PaymentMode.ToString());
                        paymentMode.Append("<br/>Check Number - ");
                        paymentMode.Append(CheckNumber);
                    }
                    else
                    {
                        paymentMode.Append(item1.PaymentMode.ToString());
                    }
                    paymentMode.Append("</td>");
                    paymentMode.Append("</tr>");
                    count++;
                }
                paymentMode.Append("</table>");
                string paymentModeInfo = "";
                string PaidStatus = "";
                if (IsDisablePaymentMethodForZeroBalancedOrderItem)
                {
                    paymentModeInfo = "";
                    PaidStatus = "";
                }
                else
                {
                    paymentModeInfo = paymentMode.ToString();
                    PaidStatus = "Paid";
                }
                //string paymentModeInfo = paymentMode.ToString();
                #endregion
                #region payment Info1
                System.Text.StringBuilder payemtInfoList = new System.Text.StringBuilder(); ;
                payemtInfoList.Append("<table style=\"width:600px; border:solid 1px #ccc; font: normal 12px 'lucida sans unicode'; text-align:left\">");
                payemtInfoList.Append("<tr style=\"background:#cacaca\">");
                payemtInfoList.Append("<th>");
                payemtInfoList.Append("Payment Date");
                payemtInfoList.Append("</th>");
                payemtInfoList.Append("<th>");
                payemtInfoList.Append("Patient Receipt Number");
                payemtInfoList.Append("</th>");
                payemtInfoList.Append("</tr>");
                foreach (var item in invoicePaymentDetailsList)
                {
                    DateTime date = item.PaymentDate.Date;
                    payemtInfoList.Append("<tr style=\"border-bottom:solid 1px #ccc\">");
                    payemtInfoList.Append("<td>");
                    payemtInfoList.Append(date.ToString("MM/dd/yyy"));
                    payemtInfoList.Append("</td>");
                    payemtInfoList.Append("<td>");
                    payemtInfoList.Append(item.PatientReceiptNumber.ToString());
                    payemtInfoList.Append("</td>");
                    payemtInfoList.Append("</tr>");
                    break;
                }
                payemtInfoList.Append("</table>");
                string payemtInfo = payemtInfoList.ToString();
                #endregion
                string PackageDiscount = null;
                if (IsDisablePaymentMethodForZeroBalancedOrderItem)
                {
                    PackageDiscount = "$0.00";
                }
                else
                {
                    if (invoiceObj.PackageDiscount == 0)
                    {
                        PackageDiscount = totalPackageDiscount.ToString("0.00");
                    }
                    else
                    {
                        PackageDiscount = invoiceObj.PackageDiscount.ToString("0.00");
                    }
                }
                if (invoiceObj.PaymentMode == PaymentType.CreditCard)
                {
                    totalPaidAmount = invoiceObj.PaidAmount.ToString("0.00");
                    netPayable = "0.00";
                }
                string paidAmount="";
                string test="0.00";
                string totalpaid = totalPaidAmount.Substring(0, 4);
               // if(!totalPaidAmount.Equals("\"0.00\""))

                if (!(totalpaid.Equals(test)))
                  {
                       paidAmount = totalPaidAmount;
                  }
                  else
                  {
                       paidAmount=netPayable;
                  }
                #region mail body
                string body = "<BR/>" +
                         "Dear " + adminUserObj.FirstName +
                         ", <BR/>" +
                         "Below is the summary of the items " + userObj.FirstName + "  has recently purchased using the WHRC service.It has been processed and reviewed by " + adminUserObj.FirstName + " " + adminUserObj .LastName+ "." +
                         "<BR/>" + "Here is the info for the items ordered and payment details" + "<BR/>" +

                         "<BR/>" + "View Payment" + "<BR/>" +
                         "<table style=\"width:600px; border:solid 1px #ccc; font: normal 12px 'lucida sans unicode';  text-align:left\">" +
                             "<tr style=\"background:#cacaca\">" +
                                  "<th>" + "Invoice No" + "</th>" +
                                  "<th>" + "Client Name" + "</th>" +
                                  "<th>" + "Purchase Date" + "</th>" +
                                  "<th>" + "Order Status" + "</th>" +
                                  "<th>" + "Payment Status" + "</th>" +
                             "</tr>" +

                             "<tr>" +
                                  "<td>" + "INV-" + invoiceObj.Id.ToString().PadLeft(8, '0') + "</td>" +
                                  "<td>" + userObj.FirstName + " " + userObj.LastName + "</td>" +
                                  "<td>" + invoiceObj.PurchaseDate.ToString("MM/dd/yyy") + "</td>" +
                                  "<td>" + "Processed" + "</td>" +
                                  "<td>" + PaidStatus + "</td>" +
                             "</tr>" +

                         "</table>" +

                         "<BR/>" + "Order Items " + "<BR/>" +
                         orderInfo +
                         "<BR/>" + "Payment Details " + "<BR/>" +
                          "<table style=\"width:600px; border:solid 1px #ccc; font: normal 12px 'lucida sans unicode';  text-align:left\">" +
                             "<tr style=\"background:#cacaca\">" +
                                  "<th>" + "Actual Amount" + "</th>" +
                                  "<th>" + "Discount Message" + "</th>" +
                                  "<th>" + "Package Discount" + "</th>" +
                                  "<th>" + "Discount Amount" + "</th>" +
                                  "<th>" + "Total Paid" + "</th>" +
                                  "<th>" + "Net Payable" + "</th>" +
                             "</tr>" +
                             "<tr>" +
                                  "<td>" + "$ " + ActualAmount + "</td>" +
                                  "<td>" + invoiceObj.ClientRequestedDiscountMsg + "</td>" +
                                  "<td>" + "$ " + PackageDiscount + "</td>" +
                                  "<td>" + "$ " + invoiceObj.Discount.ToString("0.00") + "</td>" +
                                  "<td>" + "$ " + paidAmount + "</td>" +
                                  "<td>" + "$ " + "0.00" + "</td>" +//totalPaidAmount
                             "</tr>" +

                         "</table>" +
                         payemtInfo +
                         "<BR/><BR/>" +
                         paymentModeInfo +
                          "<BR/><BR/>Thank you." +
                   "<BR/>" + "<BR/>" +
                     GenerateAdminFooter();
                #endregion
                AlternateView av1 = AlternateView.CreateAlternateViewFromString("<html><body>" + body + "</body></html>", null, MediaTypeNames.Text.Html);
                av1.LinkedResources.Add(AdminLogoRight);
                av1.LinkedResources.Add(AdminLogoLeft);
                MailHelper mailHelper = new MailHelper();
                mailHelper.GotImageLogo(av1);
                mailHelper.Send(to, subject, body);
            }
        }

        public void SendEmailForAcknowledgementForOrderProcessFromAdminPortalToAdminsGotMail(List<InvoicePaymentDetail> invoicePaymentDetailsList, Invoice invoiceObj, User AdminUserGotMail, User adminUserObj, User userObj, List<CartItem> cartItemList, decimal totalPackageDiscount, string ActualAmount, string totalPaidAmount, string netPayable, bool IsDisablePaymentMethodForZeroBalancedOrderItem)
        {
            string mailAddress = AdminUserGotMail.Email.ToString();
            bool isProduct = false;
            //string mailAddress = "rabeya@jijoty.org";

            string totalPrice = null;
            if (CheckEmail(mailAddress) != false)
            {
                string to = mailAddress;
                string subject = "Whrc Order Processed Acknowledgement";
                #region Order Item
                System.Text.StringBuilder invoiceDescription = new System.Text.StringBuilder(); ;
                invoiceDescription.Append("<table style=\"width:600px; border:solid 1px #ccc; font: normal 12px 'lucida sans unicode'; text-align:left\">");
                invoiceDescription.Append("<tr style=\"background:#cacaca\">");
                invoiceDescription.Append("<th>");
                invoiceDescription.Append("Order Item Type");
                invoiceDescription.Append("</th>");
                invoiceDescription.Append("<th>");
                invoiceDescription.Append("Description");
                invoiceDescription.Append("</th>");
                foreach (var item in cartItemList)
                {
                    if (item.InvoiceItemTypeDescription.ToString().Equals("Purchase"))
                    {
                        invoiceDescription.Append("<th>");
                        invoiceDescription.Append("Shipping Address");
                        invoiceDescription.Append("</th>");
                        break;
                    }
                }
                invoiceDescription.Append("<th>");
                invoiceDescription.Append("Price ($)");
                invoiceDescription.Append("</th>");
                invoiceDescription.Append("</tr>");
                foreach (var item in cartItemList)
                {
                    invoiceDescription.Append("<tr style=\"border-bottom:solid 1px #ccc\">");
                    invoiceDescription.Append("<td>");
                    if (item.InvoiceItemTypeDescription.ToString().Equals("Purchase"))
                    {
                        invoiceDescription.Append("Product");
                    }
                    else
                    {
                        invoiceDescription.Append(item.InvoiceItemTypeDescription.ToString());
                    }
                    invoiceDescription.Append("</td>");
                    invoiceDescription.Append("<td>");
                    invoiceDescription.Append(item.Description.ToString());
                    invoiceDescription.Append("</td>");
                    if (item.InvoiceItemTypeDescription.ToString().Equals("Purchase"))
                    {
                        invoiceDescription.Append("<td>");
                        invoiceDescription.Append(item.ShippingAddress);
                        invoiceDescription.Append("</td>");

                        invoiceDescription.Append("<td>");
                        invoiceDescription.Append(item.PriceString.ToString());
                        invoiceDescription.Append("</td>");
                        isProduct = true;
                    }
                    if (!(item.InvoiceItemTypeDescription.ToString().Equals("Purchase")))
                    {
                        if (isProduct)
                        {
                            invoiceDescription.Append("<td>");
                            invoiceDescription.Append("");
                            invoiceDescription.Append("</td>");

                            invoiceDescription.Append("<td>");
                            invoiceDescription.Append(item.PriceString.ToString());
                            invoiceDescription.Append("</td>");
                        }
                        else
                        {
                            invoiceDescription.Append("<td>");//colspan=\"2\" style=\"text-align:right;\"
                            invoiceDescription.Append(item.PriceString.ToString());
                            invoiceDescription.Append("</td>");
                        }

                    }
                    totalPrice = item.Price.ToString();
                    invoiceDescription.Append("</tr>");
                }
                invoiceDescription.Append("</table>");
                string orderInfo = invoiceDescription.ToString();
                #endregion
                #region paymentmode
                System.Text.StringBuilder paymentMode = new System.Text.StringBuilder(); ;
                paymentMode.Append("<table style=\"width:600px; border:solid 1px #ccc; font: normal 12px 'lucida sans unicode'; text-align:left\">");
                paymentMode.Append("<tr style=\"background:#cacaca\">");
                paymentMode.Append("<th>");
                paymentMode.Append("Payment Mode");
                paymentMode.Append("</th>");
                paymentMode.Append("</tr>");
                int count = 0;
                foreach (var item1 in invoicePaymentDetailsList)
                {
                    paymentMode.Append("<tr style=\"border-bottom:solid 1px #ccc\">");
                    paymentMode.Append("<td>");
                    if (count > 0)
                    {
                        paymentMode.Append("<br/>");
                    }
                    if (item1.PaymentMode == PaymentType.Medical)
                    {
                        string medicalNumber = item1.MedicalNumberOrNationalId.ToString();
                        paymentMode.Append(item1.PaymentMode.ToString());
                        paymentMode.Append("<br/>medicalNumber - ");
                        paymentMode.Append(medicalNumber);
                    }
                    else if (item1.PaymentMode == PaymentType.Check)
                    {
                        string CheckNumber = item1.CheckNumber.ToString();
                        paymentMode.Append(item1.PaymentMode.ToString());
                        paymentMode.Append("<br/>Check Number - ");
                        paymentMode.Append(CheckNumber);
                    }
                    else
                    {
                        paymentMode.Append(item1.PaymentMode.ToString());
                    }
                    paymentMode.Append("</td>");
                    paymentMode.Append("</tr>");
                    count++;
                }
                paymentMode.Append("</table>");
                string paymentModeInfo = "";
                string PaidStatus = "";
                if (IsDisablePaymentMethodForZeroBalancedOrderItem)
                {
                    paymentModeInfo = "";
                    PaidStatus = "";
                }
                else
                {
                    paymentModeInfo = paymentMode.ToString();
                    PaidStatus = "Paid";
                }
                //string paymentModeInfo = paymentMode.ToString();
                #endregion
                #region payment Info1
                System.Text.StringBuilder payemtInfoList = new System.Text.StringBuilder(); ;
                payemtInfoList.Append("<table style=\"width:600px; border:solid 1px #ccc; font: normal 12px 'lucida sans unicode'; text-align:left\">");
                payemtInfoList.Append("<tr style=\"background:#cacaca\">");
                payemtInfoList.Append("<th>");
                payemtInfoList.Append("Payment Date");
                payemtInfoList.Append("</th>");
                payemtInfoList.Append("<th>");
                payemtInfoList.Append("Patient Receipt Number");
                payemtInfoList.Append("</th>");
                payemtInfoList.Append("</tr>");
                foreach (var item in invoicePaymentDetailsList)
                {
                    DateTime date = item.PaymentDate.Date;
                    payemtInfoList.Append("<tr style=\"border-bottom:solid 1px #ccc\">");
                    payemtInfoList.Append("<td>");
                    payemtInfoList.Append(date.ToString("MM/dd/yyy"));
                    payemtInfoList.Append("</td>");
                    payemtInfoList.Append("<td>");
                    payemtInfoList.Append(item.PatientReceiptNumber.ToString());
                    payemtInfoList.Append("</td>");
                    payemtInfoList.Append("</tr>");
                    break;
                }
                payemtInfoList.Append("</table>");
                string payemtInfo = payemtInfoList.ToString();
                #endregion
                string PackageDiscount = null;
                if (IsDisablePaymentMethodForZeroBalancedOrderItem)
                {
                    PackageDiscount = "$0.00";
                }
                else
                {
                    if (invoiceObj.PackageDiscount == 0)
                    {
                        PackageDiscount = totalPackageDiscount.ToString("0.00");
                    }
                    else
                    {
                        PackageDiscount = invoiceObj.PackageDiscount.ToString("0.00");
                    }
                }
                if (invoiceObj.PaymentMode == PaymentType.CreditCard)
                {
                    totalPaidAmount = invoiceObj.PaidAmount.ToString("0.00");
                    netPayable = "0.00";
                }
                string paidAmount = "";
                string test = "0.00";
                string totalpaid = totalPaidAmount.Substring(0, 4);
                // if(!totalPaidAmount.Equals("\"0.00\""))

                if (!(totalpaid.Equals(test)))
                {
                    paidAmount = totalPaidAmount;
                }
                else
                {
                    paidAmount = netPayable;
                }
                #region mail body
                string body = "<BR/>" +
                         "Dear " + AdminUserGotMail.FirstName +
                         ", <BR/>" +
                         "Below is the summary of the items " + userObj.FirstName + "  has recently purchased using the WHRC service.It has been processed and reviewed by " + adminUserObj.FirstName + " " + adminUserObj.LastName + "." +
                         "<BR/>" + "Here is the info for the items ordered and payment details" + "<BR/>" +

                         "<BR/>" + "View Payment" + "<BR/>" +
                         "<table style=\"width:600px; border:solid 1px #ccc; font: normal 12px 'lucida sans unicode';  text-align:left\">" +
                             "<tr style=\"background:#cacaca\">" +
                                  "<th>" + "Invoice No" + "</th>" +
                                  "<th>" + "Client Name" + "</th>" +
                                  "<th>" + "Purchase Date" + "</th>" +
                                  "<th>" + "Order Status" + "</th>" +
                                  "<th>" + "Payment Status" + "</th>" +
                             "</tr>" +

                             "<tr>" +
                                  "<td>" + "INV-" + invoiceObj.Id.ToString().PadLeft(8, '0') + "</td>" +
                                  "<td>" + userObj.FirstName + " " + userObj.LastName + "</td>" +
                                  "<td>" + invoiceObj.PurchaseDate.ToString("MM/dd/yyy") + "</td>" +
                                  "<td>" + "Processed" + "</td>" +
                                  "<td>" + PaidStatus + "</td>" +
                             "</tr>" +

                         "</table>" +

                         "<BR/>" + "Order Items " + "<BR/>" +
                         orderInfo +
                         "<BR/>" + "Payment Details " + "<BR/>" +
                          "<table style=\"width:600px; border:solid 1px #ccc; font: normal 12px 'lucida sans unicode';  text-align:left\">" +
                             "<tr style=\"background:#cacaca\">" +
                                  "<th>" + "Actual Amount" + "</th>" +
                                  "<th>" + "Discount Message" + "</th>" +
                                  "<th>" + "Package Discount" + "</th>" +
                                  "<th>" + "Discount Amount" + "</th>" +
                                  "<th>" + "Total Paid" + "</th>" +
                                  "<th>" + "Net Payable" + "</th>" +
                             "</tr>" +
                             "<tr>" +
                                  "<td>" + "$ " + ActualAmount + "</td>" +
                                  "<td>" + invoiceObj.ClientRequestedDiscountMsg + "</td>" +
                                  "<td>" + "$ " + PackageDiscount + "</td>" +
                                  "<td>" + "$ " + invoiceObj.Discount.ToString("0.00") + "</td>" +
                                  "<td>" + "$ " + paidAmount + "</td>" +
                                  "<td>" + "$ " + "0.00" + "</td>" +//totalPaidAmount
                             "</tr>" +

                         "</table>" +
                         payemtInfo +
                         "<BR/><BR/>" +
                         paymentModeInfo +
                          "<BR/><BR/>Thank you." +
                   "<BR/>" + "<BR/>" +
                      GenerateAdminFooter();
                #endregion
                AlternateView av1 = AlternateView.CreateAlternateViewFromString("<html><body>" + body + "</body></html>", null, MediaTypeNames.Text.Html);
                av1.LinkedResources.Add(AdminLogoRight);
                av1.LinkedResources.Add(AdminLogoLeft);
                MailHelper mailHelper = new MailHelper();
                mailHelper.GotImageLogo(av1);
                mailHelper.Send(to, subject, body);
            }
        }

        public void SendEmailForAcknowledgementForOrderProcessFromAdminPortalToDefaultAdmin(List<InvoicePaymentDetail> invoicePaymentDetailsList, Invoice invoiceObj, EmailAddress EmailAddressObj, User adminUserObj, User userObj, List<CartItem> cartItemList, decimal totalPackageDiscount, string ActualAmount, string totalPaidAmount, string netPayable, bool IsDisablePaymentMethodForZeroBalancedOrderItem)
        {
            string mailAddress = EmailAddressObj.MailAddress.ToString();
            bool isProduct = false;
            //string mailAddress = "rabeya@jijoty.org";

            string totalPrice = null;
            if (CheckEmail(mailAddress) != false)
            {
                string to = mailAddress;
                string subject = "Whrc Order Processed Acknowledgement";
                #region Order Item
                System.Text.StringBuilder invoiceDescription = new System.Text.StringBuilder(); ;
                invoiceDescription.Append("<table style=\"width:600px; border:solid 1px #ccc; font: normal 12px 'lucida sans unicode'; text-align:left\">");
                invoiceDescription.Append("<tr style=\"background:#cacaca\">");
                invoiceDescription.Append("<th>");
                invoiceDescription.Append("Order Item Type");
                invoiceDescription.Append("</th>");
                invoiceDescription.Append("<th>");
                invoiceDescription.Append("Description");
                invoiceDescription.Append("</th>");
                foreach (var item in cartItemList)
                {
                    if (item.InvoiceItemTypeDescription.ToString().Equals("Purchase"))
                    {
                        invoiceDescription.Append("<th>");
                        invoiceDescription.Append("Shipping Address");
                        invoiceDescription.Append("</th>");
                        break;
                    }
                }
                invoiceDescription.Append("<th>");
                invoiceDescription.Append("Price ($)");
                invoiceDescription.Append("</th>");
                invoiceDescription.Append("</tr>");
                foreach (var item in cartItemList)
                {
                    invoiceDescription.Append("<tr style=\"border-bottom:solid 1px #ccc\">");
                    invoiceDescription.Append("<td>");
                    if (item.InvoiceItemTypeDescription.ToString().Equals("Purchase"))
                    {
                        invoiceDescription.Append("Product");
                    }
                    else
                    {
                        invoiceDescription.Append(item.InvoiceItemTypeDescription.ToString());
                    }
                    invoiceDescription.Append("</td>");
                    invoiceDescription.Append("<td>");
                    invoiceDescription.Append(item.Description.ToString());
                    invoiceDescription.Append("</td>");

                    if (item.InvoiceItemTypeDescription.ToString().Equals("Purchase"))
                    {
                        invoiceDescription.Append("<td>");
                        invoiceDescription.Append(item.ShippingAddress);
                        invoiceDescription.Append("</td>");

                        invoiceDescription.Append("<td>");
                        invoiceDescription.Append(item.PriceString.ToString());
                        invoiceDescription.Append("</td>");
                        isProduct = true;
                    }
                    if (!(item.InvoiceItemTypeDescription.ToString().Equals("Purchase")))
                    {
                        if (isProduct)
                        {
                            invoiceDescription.Append("<td>");
                            invoiceDescription.Append("");
                            invoiceDescription.Append("</td>");

                            invoiceDescription.Append("<td>");
                            invoiceDescription.Append(item.PriceString.ToString());
                            invoiceDescription.Append("</td>");
                        }
                        else
                        {
                            invoiceDescription.Append("<td>");//colspan=\"2\" style=\"text-align:right;\"
                            invoiceDescription.Append(item.PriceString.ToString());
                            invoiceDescription.Append("</td>");
                        }

                    }
                    totalPrice = item.Price.ToString();
                    invoiceDescription.Append("</tr>");
                }
                invoiceDescription.Append("</table>");
                string orderInfo = invoiceDescription.ToString();
                #endregion
                #region paymentmode
                System.Text.StringBuilder paymentMode = new System.Text.StringBuilder(); ;
                paymentMode.Append("<table style=\"width:600px; border:solid 1px #ccc; font: normal 12px 'lucida sans unicode'; text-align:left\">");
                paymentMode.Append("<tr style=\"background:#cacaca\">");
                paymentMode.Append("<th>");
                paymentMode.Append("Payment Mode");
                paymentMode.Append("</th>");
                paymentMode.Append("</tr>");
                int count = 0;
                foreach (var item1 in invoicePaymentDetailsList)
                {
                    paymentMode.Append("<tr style=\"border-bottom:solid 1px #ccc\">");
                    paymentMode.Append("<td>");
                    if (count > 0)
                    {
                        paymentMode.Append("<br/>");
                    }
                    if (item1.PaymentMode == PaymentType.Medical)
                    {
                        string medicalNumber = item1.MedicalNumberOrNationalId.ToString();
                        paymentMode.Append(item1.PaymentMode.ToString());
                        paymentMode.Append("<br/>medicalNumber - ");
                        paymentMode.Append(medicalNumber);
                    }
                    else if (item1.PaymentMode == PaymentType.Check)
                    {
                        string CheckNumber = item1.CheckNumber.ToString();
                        paymentMode.Append(item1.PaymentMode.ToString());
                        paymentMode.Append("<br/>Check Number - ");
                        paymentMode.Append(CheckNumber);
                    }
                    else
                    {
                        paymentMode.Append(item1.PaymentMode.ToString());
                    }
                    paymentMode.Append("</td>");
                    paymentMode.Append("</tr>");
                    count++;
                }
                paymentMode.Append("</table>");
                string paymentModeInfo = "";
                string PaidStatus = "";
                if (IsDisablePaymentMethodForZeroBalancedOrderItem)
                {
                    paymentModeInfo = "";
                    PaidStatus = "";
                }
                else
                {
                    paymentModeInfo = paymentMode.ToString();
                    PaidStatus = "Paid";
                }
                //string paymentModeInfo = paymentMode.ToString();
                #endregion
                #region payment Info1
                System.Text.StringBuilder payemtInfoList = new System.Text.StringBuilder(); ;
                payemtInfoList.Append("<table style=\"width:600px; border:solid 1px #ccc; font: normal 12px 'lucida sans unicode'; text-align:left\">");
                payemtInfoList.Append("<tr style=\"background:#cacaca\">");
                payemtInfoList.Append("<th>");
                payemtInfoList.Append("Payment Date");
                payemtInfoList.Append("</th>");
                payemtInfoList.Append("<th>");
                payemtInfoList.Append("Patient Receipt Number");
                payemtInfoList.Append("</th>");
                payemtInfoList.Append("</tr>");
                foreach (var item in invoicePaymentDetailsList)
                {
                    DateTime date = item.PaymentDate.Date;
                    payemtInfoList.Append("<tr style=\"border-bottom:solid 1px #ccc\">");
                    payemtInfoList.Append("<td>");
                    payemtInfoList.Append(date.ToString("MM/dd/yyy"));
                    payemtInfoList.Append("</td>");
                    payemtInfoList.Append("<td>");
                    payemtInfoList.Append(item.PatientReceiptNumber.ToString());
                    payemtInfoList.Append("</td>");
                    payemtInfoList.Append("</tr>");
                    break;
                }
                payemtInfoList.Append("</table>");
                string payemtInfo = payemtInfoList.ToString();
                #endregion
                string PackageDiscount = null;
                if (IsDisablePaymentMethodForZeroBalancedOrderItem)
                {
                    PackageDiscount = "$0.00";
                }
                else
                {
                    if (invoiceObj.PackageDiscount == 0)
                    {
                        PackageDiscount = totalPackageDiscount.ToString("0.00");
                    }
                    else
                    {
                        PackageDiscount = invoiceObj.PackageDiscount.ToString("0.00");
                    }
                }
                if (invoiceObj.PaymentMode == PaymentType.CreditCard)
                {
                    totalPaidAmount = invoiceObj.PaidAmount.ToString("0.00");
                    netPayable = "0.00";
                }
                string paidAmount = "";
                string test = "0.00";
                string totalpaid = totalPaidAmount.Substring(0, 4);
                // if(!totalPaidAmount.Equals("\"0.00\""))

                if (!(totalpaid.Equals(test)))
                {
                    paidAmount = totalPaidAmount;
                }
                else
                {
                    paidAmount = netPayable;
                }
                #region mail body
                string body = "<BR/>" +
                         "Dear " + "Admin" +
                         ", <BR/>" +
                         "Below is the summary of the items " + userObj.FirstName + "  has recently purchased using the WHRC service.It has been processed and reviewed by " + adminUserObj.FirstName + " " + adminUserObj.LastName + "." +
                         "<BR/>" + "Here is the info for the items ordered and payment details" + "<BR/>" +

                         "<BR/>" + "View Payment" + "<BR/>" +
                         "<table style=\"width:600px; border:solid 1px #ccc; font: normal 12px 'lucida sans unicode';  text-align:left\">" +
                             "<tr style=\"background:#cacaca\">" +
                                  "<th>" + "Invoice No" + "</th>" +
                                  "<th>" + "Client Name" + "</th>" +
                                  "<th>" + "Purchase Date" + "</th>" +
                                  "<th>" + "Order Status" + "</th>" +
                                  "<th>" + "Payment Status" + "</th>" +
                             "</tr>" +

                             "<tr>" +
                                  "<td>" + "INV-" + invoiceObj.Id.ToString().PadLeft(8, '0') + "</td>" +
                                  "<td>" + userObj.FirstName + " " + userObj.LastName + "</td>" +
                                  "<td>" + invoiceObj.PurchaseDate.ToString("MM/dd/yyy") + "</td>" +
                                  "<td>" + "Processed" + "</td>" +
                                  "<td>" + PaidStatus + "</td>" +
                             "</tr>" +

                         "</table>" +

                         "<BR/>" + "Order Items " + "<BR/>" +
                         orderInfo +
                         "<BR/>" + "Payment Details " + "<BR/>" +
                          "<table style=\"width:600px; border:solid 1px #ccc; font: normal 12px 'lucida sans unicode';  text-align:left\">" +
                             "<tr style=\"background:#cacaca\">" +
                                  "<th>" + "Actual Amount" + "</th>" +
                                  "<th>" + "Discount Message" + "</th>" +
                                  "<th>" + "Package Discount" + "</th>" +
                                  "<th>" + "Discount Amount" + "</th>" +
                                  "<th>" + "Total Paid" + "</th>" +
                                  "<th>" + "Net Payable" + "</th>" +
                             "</tr>" +
                             "<tr>" +
                                  "<td>" + "$ " + ActualAmount + "</td>" +
                                  "<td>" + invoiceObj.ClientRequestedDiscountMsg + "</td>" +
                                  "<td>" + "$ " + PackageDiscount + "</td>" +
                                  "<td>" + "$ " + invoiceObj.Discount.ToString("0.00") + "</td>" +
                                  "<td>" + "$ " + paidAmount + "</td>" +
                                  "<td>" + "$ " + "0.00" + "</td>" +//totalPaidAmount
                             "</tr>" +

                         "</table>" +
                         payemtInfo +
                         "<BR/><BR/>" +
                         paymentModeInfo +
                          "<BR/><BR/>Thank you." +
                   "<BR/>" + "<BR/>" +
                    GenerateAdminFooter();
                #endregion
                AlternateView av1 = AlternateView.CreateAlternateViewFromString("<html><body>" + body + "</body></html>", null, MediaTypeNames.Text.Html);
                av1.LinkedResources.Add(AdminLogoRight);
                av1.LinkedResources.Add(AdminLogoLeft);
                MailHelper mailHelper = new MailHelper();
                mailHelper.GotImageLogo(av1);
                mailHelper.Send(to, subject, body);
            }
        }
        #endregion

        #region from admin portal after change section the acknowledgement

        public void EmailSendForSectionChangeAcknowledgrmentToMember(User adminUserObj, User memberUserObj, Section newSectionObj, Section oldSectionObj, Invoice invoice)
        {
            string mailAddress = memberUserObj.Email.ToString();

            if (CheckEmail(mailAddress) != false)
            {
                string to = mailAddress;
                string subject = "Whrc Section Channge Ackowledgement";

                #region section
                System.Text.StringBuilder sectionDescription = new System.Text.StringBuilder(); ;
                sectionDescription.Append("<table style=\"width:600px; border:solid 1px #ccc; font: normal 12px 'lucida sans unicode'; text-align:left\">");
                sectionDescription.Append("<tr style=\"background:#cacaca\">");

                #region table column
                sectionDescription.Append("<th>");
                sectionDescription.Append("Day of Week");
                sectionDescription.Append("</th>");

                sectionDescription.Append("<th>");
                sectionDescription.Append("Start Date");
                sectionDescription.Append("</th>");

                sectionDescription.Append("<th>");
                sectionDescription.Append("Start Time");
                sectionDescription.Append("</th>");

                sectionDescription.Append("<th>");
                sectionDescription.Append("Instructor");
                sectionDescription.Append("</th>");

                sectionDescription.Append("<th>");
                sectionDescription.Append("Location");
                sectionDescription.Append("</th>");

                sectionDescription.Append("</tr>");
                #endregion

                #region table row
                sectionDescription.Append("<tr style=\"border-bottom:solid 1px #ccc\">");

                sectionDescription.Append("<td>");
                sectionDescription.Append(newSectionObj.DayOfWeek.ToString());
                sectionDescription.Append("</td>");
                sectionDescription.Append("<td>");
                sectionDescription.Append(newSectionObj.StartDate.ToString("MM/dd/yyy"));
                sectionDescription.Append("</td>");
                sectionDescription.Append("<td>");
                sectionDescription.Append(Convert.ToString(newSectionObj.StartTime));
                sectionDescription.Append("</td>");
                sectionDescription.Append("<td>");
                sectionDescription.Append(newSectionObj.Instructor.ToString());
                sectionDescription.Append("</td>");
                sectionDescription.Append("<td>");
                sectionDescription.Append(newSectionObj.Locations.ToString());
                sectionDescription.Append("</td>");

                sectionDescription.Append("</tr>");
                #endregion

                sectionDescription.Append("</table>");
                string section = sectionDescription.ToString();
                #endregion
                string oldclassDescription = oldSectionObj.ClassTitle + "( " + oldSectionObj.DayOfWeek + " - " + oldSectionObj.StartDate.ToString("MM/dd/yyy") + " )";

                #region mail body
                string body = "<BR/>" +
                  "Dear " + memberUserObj.FirstName +
                  ", <BR/> <BR/>" +
                   "The class " + oldclassDescription + " of Order No: " + ": INV-" + invoice.Id.ToString().PadLeft(8, '0') + " placed on " + invoice.PurchaseDate + 
                   " <BR/>is changed to the section below using the WHRC service by " + adminUserObj.FirstName + " " + adminUserObj.LastName +
                   ". (This is an automated email. Do <em>NOT</em> reply to this message.)" +
                   "<BR/> <BR/>" + "Here is the info of the changed section in details" + "<BR/> <BR/>" +
                    section +

                  "<BR/><BR/>Thank you." +
                   "<BR/>" + "<BR/>" +
                      GenerateAdminFooter();
                #endregion
                AlternateView av1 = AlternateView.CreateAlternateViewFromString("<html><body>" + body + "</body></html>", null, MediaTypeNames.Text.Html);
                av1.LinkedResources.Add(AdminLogoRight);
                av1.LinkedResources.Add(AdminLogoLeft);
                MailHelper mailHelper = new MailHelper();
                mailHelper.GotImageLogo(av1);
                mailHelper.Send(to, subject, body);
            }
        }

        public void EmailSendForSectionChangeAcknowledgrmentToDefaltAdmin(EmailAddress EmailAddressObj, User adminUserObj, User memberUserObj, Section newSectionObj, Section oldSectionObj, Invoice invoice)
        {
            string mailAddress = EmailAddressObj.MailAddress.ToString();

            if (CheckEmail(mailAddress) != false)
            {
                string to = mailAddress;
                string subject = "Whrc Section Channge Ackowledgement";

                #region section
                System.Text.StringBuilder sectionDescription = new System.Text.StringBuilder(); ;
                sectionDescription.Append("<table style=\"width:600px; border:solid 1px #ccc; font: normal 12px 'lucida sans unicode'; text-align:left\">");
                sectionDescription.Append("<tr style=\"background:#cacaca\">");

                #region table column
                sectionDescription.Append("<th>");
                sectionDescription.Append("Day of Week");
                sectionDescription.Append("</th>");

                sectionDescription.Append("<th>");
                sectionDescription.Append("Start Date");
                sectionDescription.Append("</th>");

                sectionDescription.Append("<th>");
                sectionDescription.Append("Start Time");
                sectionDescription.Append("</th>");

                sectionDescription.Append("<th>");
                sectionDescription.Append("Instructor");
                sectionDescription.Append("</th>");

                sectionDescription.Append("<th>");
                sectionDescription.Append("Location");
                sectionDescription.Append("</th>");

                sectionDescription.Append("</tr>");
                #endregion

                #region table row
                sectionDescription.Append("<tr style=\"border-bottom:solid 1px #ccc\">");

                sectionDescription.Append("<td>");
                sectionDescription.Append(newSectionObj.DayOfWeek.ToString());
                sectionDescription.Append("</td>");
                sectionDescription.Append("<td>");
                sectionDescription.Append(newSectionObj.StartDate.ToString("MM/dd/yyy"));
                sectionDescription.Append("</td>");
                sectionDescription.Append("<td>");
                sectionDescription.Append(Convert.ToString(newSectionObj.StartTime));
                sectionDescription.Append("</td>");
                sectionDescription.Append("<td>");
                sectionDescription.Append(newSectionObj.Instructor.ToString());
                sectionDescription.Append("</td>");
                sectionDescription.Append("<td>");
                sectionDescription.Append(newSectionObj.Locations.ToString());
                sectionDescription.Append("</td>");

                sectionDescription.Append("</tr>");
                #endregion

                sectionDescription.Append("</table>");
                string section = sectionDescription.ToString();
                #endregion
                string oldclassDescription = oldSectionObj.ClassTitle + "( " + oldSectionObj.DayOfWeek + " - " + oldSectionObj.StartDate.ToString("MM/dd/yyy") + " )";

                #region mail body
                string body = "<BR/>" +
                  "Dear " + "Admin" +
                  ", <BR/> <BR/>" +
                   "The class " + oldclassDescription + " of Order No: " + ": INV-" + invoice.Id.ToString().PadLeft(8, '0') + " placed on " + invoice.PurchaseDate +
                   " <BR/>is changed to the section below using the WHRC service by " + adminUserObj.FirstName + " " + adminUserObj.LastName +
                   ". (This is an automated email. Do <em>NOT</em> reply to this message.)" +
                   "<BR/> <BR/>" + "Here is the info of the changed section in details" + "<BR/> <BR/>" +
                    section +

                  "<BR/><BR/>Thank you." +
                   "<BR/>" + "<BR/>" +
                     GenerateAdminFooter();
                #endregion
                AlternateView av1 = AlternateView.CreateAlternateViewFromString("<html><body>" + body + "</body></html>", null, MediaTypeNames.Text.Html);
                av1.LinkedResources.Add(AdminLogoRight);
                av1.LinkedResources.Add(AdminLogoLeft);
                MailHelper mailHelper = new MailHelper();
                mailHelper.GotImageLogo(av1);
                mailHelper.Send(to, subject, body);
            }
        }
       
        #endregion

        #region from member portal for canseling class signUp b4 processing
        public void EmailForCancelSignupClassBeforeProcessingToMember(User user, ClassInvoiceItem classInvoice, Invoice invoice, Section sectionObj)
        {
            string mailAddress = user.Email.ToString();

            if (CheckEmail(mailAddress) != false)
            {
                string to = mailAddress;
                string subject = "Whrc Class Cancel Acknowledgement";

                #region class
                System.Text.StringBuilder sectionDescription = new System.Text.StringBuilder(); ;
                sectionDescription.Append("<table style=\"width:600px; border:solid 1px #ccc; font: normal 12px 'lucida sans unicode'; text-align:left\">");
                sectionDescription.Append("<tr style=\"background:#cacaca\">");

                #region table column
                sectionDescription.Append("<th>");
                sectionDescription.Append("Day of Week");
                sectionDescription.Append("</th>");

                sectionDescription.Append("<th>");
                sectionDescription.Append("Start Date");
                sectionDescription.Append("</th>");

                sectionDescription.Append("<th>");
                sectionDescription.Append("Start Time");
                sectionDescription.Append("</th>");

                sectionDescription.Append("<th>");
                sectionDescription.Append("Instructor");
                sectionDescription.Append("</th>");

                sectionDescription.Append("<th>");
                sectionDescription.Append("Location");
                sectionDescription.Append("</th>");

                sectionDescription.Append("</tr>");
                #endregion

                #region table row
                sectionDescription.Append("<tr style=\"border-bottom:solid 1px #ccc\">");

                sectionDescription.Append("<td>");
                sectionDescription.Append(sectionObj.DayOfWeek.ToString());
                sectionDescription.Append("</td>");
                sectionDescription.Append("<td>");
                sectionDescription.Append(sectionObj.StartDate.ToString("MM/dd/yyy"));
                sectionDescription.Append("</td>");
                sectionDescription.Append("<td>");
                sectionDescription.Append(Convert.ToString(sectionObj.StartTime));
                sectionDescription.Append("</td>");
                sectionDescription.Append("<td>");
                sectionDescription.Append(sectionObj.Instructor.ToString());
                sectionDescription.Append("</td>");
                sectionDescription.Append("<td>");
                sectionDescription.Append(sectionObj.Locations.ToString());
                sectionDescription.Append("</td>");

                sectionDescription.Append("</tr>");
                #endregion

                sectionDescription.Append("</table>");
                string section = sectionDescription.ToString();
                #endregion
                string classDescription = sectionObj.ClassTitle + "( " + sectionObj.DayOfWeek + " - " + sectionObj.StartDate.ToString("MM/dd/yyy") + " )";

                #region mail body
                string body = "<BR/>" +
                  "Dear " + user.FirstName +
                  ", <BR/> <BR/>" +
                   "The class " + classDescription + " of Order No: " + "INV-" + invoice.Id.ToString().PadLeft(8, '0') + " placed on " + invoice.PurchaseDate +
                   " <BR/>is removed from your ordered item(s) using the WHRC service."+
                   " (This is an automated email. Do <em>NOT</em> reply to this message.)" +
                  "<BR/> <BR/>" + "Here is the section details of the removed class " + sectionObj.ClassTitle + "<BR/> <BR/>" +
                    section +

                  "<BR/><BR/>Thank you." +
                   "<BR/>" + "<BR/>" +
                      GenerateMemberFooter();
                #endregion
                AlternateView av1 = AlternateView.CreateAlternateViewFromString("<html><body>" + body + "</body></html>", null, MediaTypeNames.Text.Html);
                av1.LinkedResources.Add(MemberLogoRight);
                av1.LinkedResources.Add(MemberLogoLeft);
                MailHelper mailHelper = new MailHelper();
                mailHelper.GotImageLogo(av1);
                mailHelper.Send(to, subject, body);
            }
        }
        //public void EmailForCancelSignupClassBeforeProcessingToAdmin(User user, ClassInvoiceItem classInvoice,Invoice invoice)
        //{
        //}
        public void EmailForCancelSignupClassBeforeProcessingToDefaultAdmin(User user, EmailAddress EmailAddressObj, ClassInvoiceItem classInvoice, Invoice invoice, Section sectionObj)
        {
            string mailAddress = EmailAddressObj.MailAddress.ToString();

            if (CheckEmail(mailAddress) != false)
            {
                string to = mailAddress;
                string subject = "Whrc Class Cancel Acknowledgement";

                #region class
                System.Text.StringBuilder sectionDescription = new System.Text.StringBuilder(); ;
                sectionDescription.Append("<table style=\"width:600px; border:solid 1px #ccc; font: normal 12px 'lucida sans unicode'; text-align:left\">");
                sectionDescription.Append("<tr style=\"background:#cacaca\">");

                #region table column
                sectionDescription.Append("<th>");
                sectionDescription.Append("Day of Week");
                sectionDescription.Append("</th>");

                sectionDescription.Append("<th>");
                sectionDescription.Append("Start Date");
                sectionDescription.Append("</th>");

                sectionDescription.Append("<th>");
                sectionDescription.Append("Start Time");
                sectionDescription.Append("</th>");

                sectionDescription.Append("<th>");
                sectionDescription.Append("Instructor");
                sectionDescription.Append("</th>");

                sectionDescription.Append("<th>");
                sectionDescription.Append("Location");
                sectionDescription.Append("</th>");

                sectionDescription.Append("</tr>");
                #endregion

                #region table row
                sectionDescription.Append("<tr style=\"border-bottom:solid 1px #ccc\">");

                sectionDescription.Append("<td>");
                sectionDescription.Append(sectionObj.DayOfWeek.ToString());
                sectionDescription.Append("</td>");
                sectionDescription.Append("<td>");
                sectionDescription.Append(sectionObj.StartDate.ToString("MM/dd/yyy"));
                sectionDescription.Append("</td>");
                sectionDescription.Append("<td>");
                sectionDescription.Append(Convert.ToString(sectionObj.StartTime));
                sectionDescription.Append("</td>");
                sectionDescription.Append("<td>");
                sectionDescription.Append(sectionObj.Instructor.ToString());
                sectionDescription.Append("</td>");
                sectionDescription.Append("<td>");
                sectionDescription.Append(sectionObj.Locations.ToString());
                sectionDescription.Append("</td>");

                sectionDescription.Append("</tr>");
                #endregion

                sectionDescription.Append("</table>");
                string section = sectionDescription.ToString();
                #endregion
                string classDescription = sectionObj.ClassTitle + "( " + sectionObj.DayOfWeek + " - " + sectionObj.StartDate.ToString("MM/dd/yyy") + " )";

                #region mail body
                string body = "<BR/>" +
                  "Dear " +"Admin" +
                  ", <BR/> <BR/>" +
                   "The class " + classDescription + " of Order No: " + "INV-" + invoice.Id.ToString().PadLeft(8, '0') + " placed on " + invoice.PurchaseDate +
                   " <BR/>is removed from the ordered item(s) of " + user.FirstName + " using the WHRC service by WHRC member " + user.FirstName + " " + user.LastName + "." +
                   " (This is an automated email. Do <em>NOT</em> reply to this message.)" +
                   "<BR/> <BR/>" + "Here is the section details of the removed class " + sectionObj.ClassTitle + "<BR/> <BR/>" +
                    section +

                  "<BR/><BR/>Thank you." +
                   "<BR/>" + "<BR/>" +
                      GenerateMemberFooter();
                #endregion
                AlternateView av1 = AlternateView.CreateAlternateViewFromString("<html><body>" + body + "</body></html>", null, MediaTypeNames.Text.Html);
                av1.LinkedResources.Add(MemberLogoRight);
                av1.LinkedResources.Add(MemberLogoLeft);
                MailHelper mailHelper = new MailHelper();
                mailHelper.GotImageLogo(av1);
                mailHelper.Send(to, subject, body);
            }
        }

        #endregion

        #region reset password mail
        public void SendEmailForResetpasswordToClient(string mailadd, User userObj, string url)
        {
            try
            {
                if (CheckEmail(mailadd.ToString()) != false)
                {
                    Utility util = new Utility();
                    string[] info = util.SecurityInformaionEncrypt(Convert.ToString(userObj.Id));
                    string to = mailadd;
                    //string to = "rabeya@jijoty.org";
                    string subject = "Your new WHRC account.";
                    string fullUrl = string.Empty;
                    string fulUrlTemporary = string.Empty;
                    string idstring = HttpUtility.UrlEncode(info[0].ToString(), System.Text.Encoding.Default);
                    string keystring = HttpUtility.UrlEncode(info[1].ToString(), System.Text.Encoding.Default);
                    fullUrl = url + "/whrc/ResetPassword.aspx?ID=" + idstring + "&Key=" + keystring;
                    string body = "<BR/>" +
                        "Hello " +userObj.FirstName +
                        ",<BR/><BR/>" +
                        "Welcome to WHRC! Your registration process is almost complete. A WHRC Client Account has been created as per your request." + "<BR/><BR/>All you need to do to " +
                        " is <a href='" + fullUrl.ToString() + "'>click here</a>  or copy the link below in your browser's address bar to complete your registration.<BR/><BR/>" +
                        "" + fullUrl.ToString() + "<BR/><BR/> " +
                        "Thank you" +
                        "<BR/>WHRC Support Team<BR/>" +
                        "<a href='http://" + url + "'>" + url + "</a>";
                    GenerateMemberFooter();

                    AlternateView av1 = AlternateView.CreateAlternateViewFromString("<html><body>" + body + "</body></html>", null, MediaTypeNames.Text.Html);
                    av1.LinkedResources.Add(MemberLogoRight);
                    av1.LinkedResources.Add(MemberLogoLeft);
                    MailHelper mailHelper = new MailHelper();
                    mailHelper.GotImageLogo(av1);
                    mailHelper.Send(to, subject, body);
                }
            }
            catch (Exception ex)
            {
            }
        }

        public void SendEmailForResetpasswordToAdmin(string mailadd, User userObj, string url)
        {
            try
            {
                if (CheckEmail(mailadd.ToString()) != false)
                {
                    Utility util = new Utility();
                    string[] info = util.SecurityInformaionEncrypt(Convert.ToString(userObj.Id));
                    string to = mailadd;
                    //string to = "rabeya@jijoty.org";
                    string subject = "Your new WHRC account.";
                    string fullUrl = string.Empty;
                    string fulUrlTemporary = string.Empty;
                    string idstring = HttpUtility.UrlEncode(info[0].ToString(), System.Text.Encoding.Default);
                    string keystring = HttpUtility.UrlEncode(info[1].ToString(), System.Text.Encoding.Default);
                    fullUrl = url + "/whrcadmin/ResetPassword.aspx?ID=" + idstring + "&Key=" + keystring;
                    string body = "<BR/>" +
                        "Hello " + userObj.FirstName +
                        ",<BR/><BR/>" +
                        "Welcome to WHRC! Your registration process is almost complete. A WHRC Admin Account has been created as per your request." + "<BR/><BR/>All you need to do to " +
                        " is <a href='" + fullUrl.ToString() + "'>click here</a>  or copy the link below in your browser's address bar to complete your registration.<BR/><BR/>" +
                        "" + fullUrl.ToString() + "<BR/><BR/> " +
                        "Thank you" +
                        "<BR/>WHRC Support Team<BR/>" +
                        "<a href='http://" + url + "'>" + url + "</a>";
                   GenerateAdminFooter();
               
                AlternateView av1 = AlternateView.CreateAlternateViewFromString("<html><body>" + body + "</body></html>", null, MediaTypeNames.Text.Html);
                av1.LinkedResources.Add(AdminLogoRight);
                av1.LinkedResources.Add(AdminLogoLeft);
                MailHelper mailHelper = new MailHelper();
                mailHelper.GotImageLogo(av1);
                mailHelper.Send(to, subject, body);
                }
            }
            catch (Exception ex)
            {
            }
        }
        #endregion
    }
}
