﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using daoProject.Model;
using daoProject.Utilities;
using daoProject.Mapper;
using daoProject.Infrastructure;
namespace daoProject.Service
{
    public class RentalInvoiceItemServiceImpl
    {
        private RentalInvoiceItemDataMapper dataMapper;

        public RentalInvoiceItemServiceImpl()
        {
            dataMapper = new RentalInvoiceItemDataMapper();
        }

        public void Add(RentalInvoiceItem item)
        {
            dataMapper.Create(item);
        }
        public void Add(RentalInvoiceItem item, System.Data.Odbc.OdbcConnection conn, System.Data.Odbc.OdbcTransaction tx)
        {
            dataMapper.Create(item,conn, tx);
        }

        public void Update(RentalInvoiceItem item)
        {
            dataMapper.Update(item);
        }

        public bool Delete(long id)
        {
            return dataMapper.Delete(id);
        }
        public decimal getItemAmountPrice(long id)
        {
            decimal i = dataMapper.getItemAmountPrice(id);
            decimal j = dataMapper.getItemDepositAmountPrice(id);
            decimal totalAmount = i+j;
            return totalAmount;
        }
        public List<RentalInvoiceItem> GetAll()
        {
            return dataMapper.GetAll();
        }

        public RentalInvoiceItem GetById(long id)
        {
            return dataMapper.GetById(id);
        }

        public List<RentalInvoiceItem> GetListByInvoiceId(long invoiceId)
        {
            return dataMapper.GetListByInvoiceId(invoiceId);
        }

        public bool DeleteByInvoiceId(long invoiceId)
        {
            return dataMapper.DeleteByInvoiceId(invoiceId);
        }

        public void DeleteByRentalId(long RentalId)
        {
             dataMapper.DeleteByRentalid(RentalId);
        }
        public void ReturnItem(RentalInvoiceItem item)
        {
            dataMapper.ReturnItem(item);
        }


        public void UpdateRefundProcessed(RentalInvoiceItem item)
        {
            dataMapper.UpdateRefundProcessed(item);
        }
        public void UpdateRegularfee(RentalInvoiceItem item)
        {
            dataMapper.UpdateRegularfee(item);
        }
        public void UpdateRentalExtensionRequest(RentalInvoiceItem item)
        {
            dataMapper.UpdateRentalExtensionRequest(item);
        }
        public void UpdateRentalStatus(Invoice invoice)
        {
            List<RentalInvoiceItem> RentalInvoiceItemList = invoice.RentalInvoiceItems;
            if (RentalInvoiceItemList.Count > 0)
            {
                foreach (RentalInvoiceItem RentalInvoiceItemObj in RentalInvoiceItemList)
                {
                   // if (!RentalInvoiceItemObj.IsInWaitingList)
                    //{
                    RentalInvoiceItemObj.RentalItemStatus = ReturnStatus.Rented;
                    new RentalInvoiceItemServiceImpl().UpdateItemStatus(RentalInvoiceItemObj);
                   // }
                }
            }
        }

        public void UpdateItemStatus(RentalInvoiceItem RentalInvoiceItemObj)
        {
            dataMapper.UpdateRentalItemStatus(RentalInvoiceItemObj);
        }
        public void UpdateRentalInvoiceId(RentalInvoiceItem item, long newInvoiceId)
        {
            dataMapper.UpdateRentalInvoiceId(item,newInvoiceId);
        }
    }
}
