﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Drawing;
using System.Web;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

namespace daoProject.Service
{
    public class ImageServiceImpl
    {
        /// <summary>
        /// Upload a file
        /// </summary>
        /// <param name="filUpload">File Uploda control name</param>
        /// <returns>Physical Location of the File</returns>
        public string[] ImageUpload(FileUpload filUpload,string SubDirectory)
        {
            // Initialize variables
            string[] imagePath = {"",""};
            string sSavePath;
            string sThumbExtension;
            string sThumbBigExtension;
            int intThumbWidth;
            int intThumbHeight;

            // Set constant values
            //string leftPart = HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority);
            //sSavePath = leftPart + "/ProductImages/";
           // sSavePath = "~/images/productImages/";
            sSavePath = "~/images/" + SubDirectory + "/";
            sThumbExtension = "_thumb";
            sThumbBigExtension = "_bigThumb";
            intThumbWidth = 100;
            intThumbHeight = 120;

            // If file field isn’t empty
            if (filUpload.PostedFile.ContentLength != 0)
            {

                // Check file size (mustn’t be 0)
                HttpPostedFile myFile = filUpload.PostedFile;
                int nFileLen = myFile.ContentLength;
                if (nFileLen > 1048576)
                {
                    // lblOutput.Text = "There wasn't any file uploaded.";
                    string[] errorMessage = {"Exceed Image Limit"};
                    return (errorMessage);
                }

                string fileType = System.IO.Path.GetExtension(myFile.FileName);
                if (fileType == ".jpeg" || fileType == ".png" || fileType == ".jpg" || fileType == ".gif")
                {
                    // Check file extension (must be JPG)
                    //if (System.IO.Path.GetExtension(myFile.FileName).ToLower() != ".jpg")
                    //{
                    //    //lblOutput.Text = "The file must have an extension of JPG";
                    //    return (string.Empty);
                    //}

                    // Read file into a data stream
                    byte[] myData = new Byte[nFileLen];
                    myFile.InputStream.Read(myData, 0, nFileLen);

                    // Make sure a duplicate file doesn’t exist.  If it does, keep on appending an incremental numeric until it is unique
                    string sFilename = System.IO.Path.GetFileName(myFile.FileName);

                    int file_append = 0;
                    while (System.IO.File.Exists(System.Web.HttpContext.Current.Server.MapPath(sSavePath + sFilename)))
                    {
                        file_append++;
                        sFilename = System.IO.Path.GetFileNameWithoutExtension(myFile.FileName) + file_append.ToString() + fileType;
                    }

                    // Save the stream to disk
                    System.IO.FileStream newFile = new System.IO.FileStream(System.Web.HttpContext.Current.Server.MapPath(sSavePath + sFilename), System.IO.FileMode.Create);
                    newFile.Write(myData, 0, myData.Length);
                    newFile.Close();

                     /**** MyOModifiedrginalImage *****/
                    System.Drawing.Image.GetThumbnailImageAbort myCallBack = new System.Drawing.Image.GetThumbnailImageAbort(ThumbnailCallback);
                    Bitmap myBitmap;
                    try
                    {
                        myBitmap = new Bitmap(System.Web.HttpContext.Current.Server.MapPath(sSavePath + sFilename));

                        // If jpg file is a jpeg, create a thumbnail filename that is unique.
                        file_append = 0;
                        string sThumbBigFile = System.IO.Path.GetFileNameWithoutExtension(myFile.FileName) + sThumbBigExtension + fileType;
                        while (System.IO.File.Exists(System.Web.HttpContext.Current.Server.MapPath(sSavePath + sThumbBigFile)))
                        {
                            file_append++;
                            sThumbBigFile = System.IO.Path.GetFileNameWithoutExtension(myFile.FileName) + file_append.ToString() + sThumbBigExtension + fileType;
                        }

                        // Save thumbnail and output it onto the webpage
                        System.Drawing.Image myThumbnailBig = myBitmap.GetThumbnailImage(200, 150, myCallBack, IntPtr.Zero);

                        // If we want to create Thumb then this code should be opend
                        myThumbnailBig.Save(System.Web.HttpContext.Current.Server.MapPath(sSavePath + sThumbBigFile));

                        //myThumbnailBig.Dispose();
                        //myBitmap.Dispose();
                        /**** end *****/

                    // Check whether the file is really a JPEG by opening it
                    //System.Drawing.Image.GetThumbnailImageAbort myCallBack = new System.Drawing.Image.GetThumbnailImageAbort(ThumbnailCallback);
                    //Bitmap myBitmap;
                    //try
                    //{
                    //    myBitmap = new Bitmap(System.Web.HttpContext.Current.Server.MapPath(sSavePath + sFilename));

                        // If jpg file is a jpeg, create a thumbnail filename that is unique.
                        file_append = 0;
                        string sThumbFile = System.IO.Path.GetFileNameWithoutExtension(myFile.FileName) + sThumbExtension + fileType;
                        while (System.IO.File.Exists(System.Web.HttpContext.Current.Server.MapPath(sSavePath + sThumbFile)))
                        {
                            file_append++;
                            sThumbFile = System.IO.Path.GetFileNameWithoutExtension(myFile.FileName) + file_append.ToString() + sThumbExtension + fileType;
                        }

                        // Save thumbnail and output it onto the webpage
                        System.Drawing.Image myThumbnail = myBitmap.GetThumbnailImage(intThumbWidth, intThumbHeight, myCallBack, IntPtr.Zero);

                        // If we want to create Thumb then this code should be opend
                        myThumbnail.Save(System.Web.HttpContext.Current.Server.MapPath(sSavePath + sThumbFile));

                        //If want Thumb Picture
                        string thumbImagepath = sThumbFile;

                        string bigThumbnailImagepath = sThumbBigFile;
                        //If want Actual picture
                        //string originalImagepath = sSavePath + myFile.FileName;

                        // Destroy objects
                        myThumbnailBig.Dispose();
                        myThumbnail.Dispose();
                        myBitmap.Dispose();
                        
                        imagePath[0] = thumbImagepath;
                        imagePath[1] = bigThumbnailImagepath;

                    }

                    catch (ArgumentException errArgument)
                    {
                        System.IO.File.Delete(System.Web.HttpContext.Current.Server.MapPath(sSavePath + sFilename));
                    }
                }
                else
                {
                    string[] errorMessage = { "Invalid File Type" };
                    return errorMessage;
                }
            }
            return imagePath;
        }

        /// <summary>
        /// For dispose only
        /// </summary>
        /// <returns></returns>
        private bool ThumbnailCallback()
        {
            return false;
        }
    }
}
