﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace daoProject.Service
{
    public class AmazonFPSException : Exception
    {
        private String message = null;

        /// <summary>
        /// Constructs AmazonFPSException with message
        /// </summary>
        /// <param name="message">Overview of error</param>
        public AmazonFPSException(String message)
        {
            this.message = message;
        }

        /// <summary>
        /// Gets error message
        /// </summary>
        public override String Message
        {
            get { return this.message; }
        }
    }
}
