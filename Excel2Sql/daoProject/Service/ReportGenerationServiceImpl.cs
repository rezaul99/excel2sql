﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using daoProject.Model;
using daoProject.Mapper;
using ReportLibrary.Model;
namespace daoProject.Service
{
    public class ReportGenerationServiceImpl
    {
        private ReportGenerationDataMapper dataMapper;

        public ReportGenerationServiceImpl()
        {
            dataMapper = new ReportGenerationDataMapper();
        }

        #region Insertion Object
        public void AddColumn(ReportAttributes ColumnObj)
        {
            dataMapper.AddColumn(ColumnObj);
        }
        public void AddReportInformaton(ReportDetails RdlcGenerateObj)
        {
            dataMapper.AddReportInformaton(RdlcGenerateObj);
        }
        public void AddTable(ReportClassObjects TableInfoObj)
        {
            dataMapper.AddTable(TableInfoObj);
        }
        public void AddReportObjectRelation(ReportObjectRelation reportObjectRelation)
        {
            dataMapper.AddReportObjectRelation(reportObjectRelation);
        }
        #endregion

        #region Get  object List
        public List<ReportAttributes> GetColumnListByTableId(long tableId) //use
        {
            return dataMapper.GetColumnListByTableId(tableId);
        }
        public List<ReportDetails> GetAllReports() //use
        {
            return dataMapper.GetAllReports();
        }
        public List<ReportClassObjects> GetTableListByDataBaseID(long dataBaseId) //use
        {
            return dataMapper.GetTableListByDataBaseID(dataBaseId);
        }
        public List<ReportObjectRelation>  GetAllReportObjectRelationList()
        {
            return dataMapper.GetAllReportObjectRelationList();
        }
        #endregion

        #region Get Object
        public ReportDetails GetReportById(long RdlcId)  //use
        {
            return dataMapper.GetReportById(RdlcId);
        }

        public ReportObjectRelation GetReportObjectRelationByObjectsId(string Table1Name, string Table2Name)  //use
        {
            return dataMapper.GetReportObjectRelationByObjectsId(Table1Name, Table2Name);
        }

        #endregion
    }
}
