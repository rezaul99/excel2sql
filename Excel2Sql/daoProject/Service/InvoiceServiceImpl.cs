﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using daoProject.Model;
using daoProject.Utilities;
using daoProject.Mapper;
using daoProject.Infrastructure;
using System.Xml;
using daoProject.WS;

namespace daoProject.Service
{
    public class InvoiceServiceImpl
    {
        private InvoiceDataMapper dataMapper;

        public InvoiceServiceImpl()
        {
            dataMapper = new InvoiceDataMapper();
        }
       
        public void Add(Invoice item)
        {
            using (System.Data.Odbc.OdbcConnection conn = daoProject.Infrastructure.DBConnection.GetConnection())
            {
                conn.ConnectionTimeout = ConstantsGlobal.ConnectionTimeoutBypass;
                conn.Open();
                System.Data.Odbc.OdbcTransaction tx = conn.BeginTransaction();

                try
                {
                    dataMapper.Create(item, conn, tx);

                    if (item.Id > 0)
                    {
                        ProductInvoiceItemServiceImpl productInvoiceItemServiceImpl = new ProductInvoiceItemServiceImpl();
                        foreach (ProductInvoiceItem childItem in item.ProductInvoiceItems)
                        {
                            childItem.InvoiceId = item.Id;
                            productInvoiceItemServiceImpl.Add(childItem, conn, tx);
                            if (item.ClientId!= null)
                            {
                                childItem.UserID = item.ClientId;
                            }
                            new LogDataServiceImpl().InsertForNew(childItem);
                        }

                        RentalInvoiceItemServiceImpl rentalInvoiceItemServiceImpl = new RentalInvoiceItemServiceImpl();
                        foreach (RentalInvoiceItem childItem in item.RentalInvoiceItems)
                        {
                            childItem.InvoiceId = item.Id;
                            rentalInvoiceItemServiceImpl.Add(childItem, conn, tx);
                            if (item.ClientId != null)
                            {
                                childItem.UserID = item.ClientId;
                            }
                            new LogDataServiceImpl().InsertForNew(childItem);
                        }

                        ClassInvoiceItemServiceImpl classInvoiceItemServiceImpl = new ClassInvoiceItemServiceImpl();
                        foreach (ClassInvoiceItem childItem in item.ClassInvoiceItems)
                        {
                            childItem.InvoiceId = item.Id;
                            classInvoiceItemServiceImpl.Add(childItem, conn, tx);
                            if (item.ClientId != null)
                            {
                                childItem.UserID = item.ClientId;
                            }
                            new LogDataServiceImpl().InsertForNew(childItem);
                        }

                        MembershipInvoiceItemServiceImpl membershipInvoiceItemServiceImpl = new MembershipInvoiceItemServiceImpl();
                        foreach (MembershipInvoiceItem childItem in item.MembershipInvoiceItems)
                        {
                            childItem.InvoiceId = item.Id;
                            membershipInvoiceItemServiceImpl.Add(childItem, conn, tx);
                            if (item.ClientId != null)
                            {
                                childItem.UserID = item.ClientId;
                            }
                            new LogDataServiceImpl().InsertForNew(childItem);
                        }
                    }

                    tx.Commit();
                }
                catch (Exception ex1)
                {
                    try
                    {
                        tx.Rollback();

                        throw ex1;
                    }
                    catch (Exception ex2)
                    {

                    }
                }
            }
        }

        public void Update(Invoice item, bool includingChilds)
        {
            using (System.Data.Odbc.OdbcConnection conn = daoProject.Infrastructure.DBConnection.GetConnection())
            {
                conn.ConnectionTimeout = ConstantsGlobal.ConnectionTimeoutBypass;
                conn.Open();
                System.Data.Odbc.OdbcTransaction tx = conn.BeginTransaction();

                try
                {
                    // Update this
                    dataMapper.Update(item, conn, tx);

                    if (includingChilds)
                    {
                        ProductInvoiceItemServiceImpl productInvoiceItemServiceImpl = new ProductInvoiceItemServiceImpl();
                        RentalInvoiceItemServiceImpl rentalInvoiceItemServiceImpl = new RentalInvoiceItemServiceImpl();
                        ClassInvoiceItemServiceImpl classInvoiceItemServiceImpl = new ClassInvoiceItemServiceImpl();
                        MembershipInvoiceItemServiceImpl membershipInvoiceItemServiceImpl = new MembershipInvoiceItemServiceImpl();

                        // Delete all previous childs
                        productInvoiceItemServiceImpl.DeleteByInvoiceId(item.Id);
                        rentalInvoiceItemServiceImpl.DeleteByInvoiceId(item.Id);
                        classInvoiceItemServiceImpl.DeleteByInvoiceId(item.Id);
                        membershipInvoiceItemServiceImpl.DeleteByInvoiceId(item.Id);

                        // Add all new childs
                        foreach (ProductInvoiceItem childItem in item.ProductInvoiceItems)
                        {
                            childItem.InvoiceId = item.Id;
                            productInvoiceItemServiceImpl.Add(childItem, conn, tx);
                        }
                        foreach (RentalInvoiceItem childItem in item.RentalInvoiceItems)
                        {
                            childItem.InvoiceId = item.Id;
                            rentalInvoiceItemServiceImpl.Add(childItem, conn, tx);
                        }
                        foreach (ClassInvoiceItem childItem in item.ClassInvoiceItems)
                        {
                            childItem.InvoiceId = item.Id;
                            classInvoiceItemServiceImpl.Add(childItem, conn, tx);
                        }
                        foreach (MembershipInvoiceItem childItem in item.MembershipInvoiceItems)
                        {
                            childItem.InvoiceId = item.Id;
                            membershipInvoiceItemServiceImpl.Add(childItem, conn, tx);
                        }
                    }

                    tx.Commit();
                }
                catch (Exception ex1)
                {
                    try
                    {
                        tx.Rollback();

                        throw ex1;
                    }
                    catch (Exception ex2)
                    {

                    }
                }
            }
        }
        public void UpdateAmount(Invoice invoice)
        {
            dataMapper.UpdateAmount(invoice);
        }
        public void UpdateTotalAmount(decimal updateAmountInvoice,long invoiceId)
        {
            dataMapper.UpdateTotalAmount(updateAmountInvoice,invoiceId);
        }

        public bool Delete(long id)
        {
            return dataMapper.Delete(id);
        }

        public List<Invoice> GetAll()
        {
            return dataMapper.GetAll();
        }

        public Invoice GetById(long id)
        {
            return dataMapper.GetById(id);
        }

        public List<Invoice> GetListByClientId(long clientId)
        {
            return dataMapper.GetListByClientId(clientId);
        }

        public List<Invoice> GetInvoiceListByOrderStatus(OrderState orderState)
        {
            return dataMapper.GetInvoiceListByOrderStatus(orderState);
        }

        public List<Invoice> GetInvoiceListForProcess()
        {
            return dataMapper.GetInvoiceListForProcess();
        }

        public List<Invoice>  GetInvoiceListForReProcess()
        {
            return dataMapper.GetInvoiceListForReProcess();
        }

        public List<Invoice> GetInvoiceListByClientName(string clientName)
        {
            return dataMapper.GetInvoiceListByClientName(clientName);
        }

        public List<Invoice> GetInvoiceListNeedReprocessingByClientName(string clientName)
        {
            return dataMapper.GetInvoiceListNeedReprocessingByClientName(clientName);
        }

        public List<Invoice> GetInvoiceListByOrderProcessStatus(bool isProcessed)
        {
            return dataMapper.GetInvoiceListByOrderProcessStatus(isProcessed);
        }

        public List<Invoice> GetInvoiceListByOrderProcessStatusAndClientName(bool isProcessed, string clientName)
        {
            return dataMapper.GetInvoiceListByOrderProcessStatusAndClientName(isProcessed,clientName);
        }

        public List<Invoice> GetInvoiceListForClientByOrderStatus(long clientID, OrderState orderState)
        {
            return dataMapper.GetInvoiceListForClientByOrderStatus(clientID, orderState);
        }

        public List<Invoice> GetInvoiceByClientIDAndOrderType(long clientID, OrderState orderState, InvoiceType invoiceType)
        {
            return dataMapper.GetInvoiceByClientIDAndOrderType(clientID, orderState, invoiceType);
        }

        //public List<Invoice> GetInvoiceByClientIDAndOrderType(long clientID, OrderState orderState)
        //{
        //    return dataMapper.GetInvoiceByClientIDAndOrderType(clientID, orderState);
        //}

        public void AddToCart(object invoiceItem)
        {
            Type invoiceItemType = invoiceItem.GetType();

            switch (invoiceItemType.Name)
            {
                case "ClassInvoiceItem":
                    {
                        ClassInvoiceItem childItem = (ClassInvoiceItem)invoiceItem;
                        ClassInvoiceItemServiceImpl classInvoiceItemServiceImpl = new ClassInvoiceItemServiceImpl();
                        classInvoiceItemServiceImpl.Add(childItem);
                    }
                    break;
                case "ProductInvoiceItem":
                    {
                        ProductInvoiceItem childItem = (ProductInvoiceItem)invoiceItem;
                        ProductInvoiceItemServiceImpl productInvoiceItemServiceImpl = new ProductInvoiceItemServiceImpl();

                        List<ProductInvoiceItem> productInvoiceItems = productInvoiceItemServiceImpl.GetListByInvoiceId(childItem.InvoiceId);
                        ProductInvoiceItem invoiceItemPrev = productInvoiceItems.FirstOrDefault(x => x.ProductId == childItem.ProductId);

                        if (invoiceItemPrev == null)
                        {
                            productInvoiceItemServiceImpl.Add(childItem);
                        }
                        else
                        {
                            invoiceItemPrev.Quantity += childItem.Quantity;
                            invoiceItemPrev.SubTotal = childItem.UnitPrice * invoiceItemPrev.Quantity;
                            invoiceItemPrev.TaxAmount += childItem.TaxAmount;
                            invoiceItemPrev.Price = invoiceItemPrev.SubTotal + invoiceItemPrev.TaxAmount;

                            productInvoiceItemServiceImpl.Update(invoiceItemPrev);
                        }
                    }
                    break;
                case "RentalInvoiceItem":
                    {
                        RentalInvoiceItemServiceImpl rentalInvoiceItemServiceImpl = new RentalInvoiceItemServiceImpl();
                        RentalInvoiceItem childItem = (RentalInvoiceItem)invoiceItem;
                        rentalInvoiceItemServiceImpl.Add(childItem);
                    }
                    break;
                case "MembershipInvoiceItem":
                    {
                        MembershipInvoiceItemServiceImpl membershipInvoiceItemServiceImpl = new MembershipInvoiceItemServiceImpl();
                        MembershipInvoiceItem childItem = (MembershipInvoiceItem)invoiceItem;
                        membershipInvoiceItemServiceImpl.Add(childItem);
                    }
                    break;
                default:
                    break;
            }
        }

        public void RemoveFromCart(long invoiceItemId, InvoiceItemType invoiceItemType)
        {
            switch (invoiceItemType)
            {
                case InvoiceItemType.Class:
                    {
                        new ClassInvoiceItemServiceImpl().Delete(invoiceItemId);
                    }
                    break;
                case InvoiceItemType.Purchase:
                    {
                        new ProductInvoiceItemServiceImpl().Delete(invoiceItemId);
                    }
                    break;
                case InvoiceItemType.Rental:
                    {
                        new RentalInvoiceItemServiceImpl().Delete(invoiceItemId);
                    }
                    break;
                case InvoiceItemType.Membership:
                    {
                        new MembershipInvoiceItemServiceImpl().Delete(invoiceItemId);
                    }
                    break;
                default:
                    break;
            }
        }

        public decimal getItemPriceAmount(long invoiceItemId, InvoiceItemType invoiceItemType)
        {
            decimal itemPriceAmount = 0;
            switch (invoiceItemType)
            {
                case InvoiceItemType.Class:
                    {
                        itemPriceAmount = new ClassInvoiceItemServiceImpl().getItemAmountPrice(invoiceItemId);
                    }
                    break;
                case InvoiceItemType.Purchase:
                    {
                        itemPriceAmount = new ProductInvoiceItemServiceImpl().getItemAmountPrice(invoiceItemId);
                    }
                    break;
                case InvoiceItemType.Rental:
                    {
                        itemPriceAmount = new RentalInvoiceItemServiceImpl().getItemAmountPrice(invoiceItemId);
                    }
                    break;
                case InvoiceItemType.Membership:
                    {
                        itemPriceAmount = new MembershipInvoiceItemServiceImpl().getItemAmountPrice(invoiceItemId);
                    }
                    break;
                default:
                    break;
            }
            return itemPriceAmount;
        }

        public void ApplyDiscount(Invoice invoice)
        {
            new InvoiceDataMapper().ApplyDiscount(invoice);
        }

        public void MakePayment(Invoice invoice)
        {
            new InvoiceDataMapper().MakePayment(invoice);
        }

        public void UpdateTotalRefundAmount(Invoice invoice)
        {
            new InvoiceDataMapper().UpdateTotalRefundAmount(invoice);
        }

        public void UpdateTotalRefundAmount(decimal totalRefundedAmount, long invoiceID)
        {
            new InvoiceDataMapper().UpdateTotalRefundAmount(totalRefundedAmount, invoiceID);
        }

        public void UpdateStaffInitialOrderProcessByAdmin(User userObj, Invoice invoiceObj)
        {
            new InvoiceDataMapper().UpdateStaffInitialOrderProcessByAdmin(userObj, invoiceObj);
        }

        public List<Invoice> GetInvoiceByStaffIdForCart(long staffId, OrderState orderState)
        {
            return dataMapper.GetInvoiceByStaffIdForCart(staffId, orderState);
        }

        public void UpdateInvoiceClientID(long  invoiceID, long clientID)
        {
            new InvoiceDataMapper().UpdateInvoiceClientID(invoiceID, clientID);
        }

        public void UpdateCreditCardPayment(Invoice invoice)
        {
            new InvoiceDataMapper().UpdateCreditCardPayment(invoice);
        }

        public long GetClassMedicalPaymentCount(long clientID)
        {
            return new InvoiceDataMapper().GetClassMedicalPaymentCount(clientID);
        }

        public void UpdatePaidAmount(Invoice invoice)
        {
            new InvoiceDataMapper().UpdatePaidAmount(invoice);
        }

        public void UpdateCheckNumber(long invoiceID,string checkNumber)
        {
            new InvoiceDataMapper().UpdateCheckNumber(invoiceID, checkNumber);
        }

        public void UpdateMedicalNumber(long invoiceID, string medicalNumber)
        {
            new InvoiceDataMapper().UpdateMedicalNumber(invoiceID, medicalNumber);
        }

        #region incart invoice for a user updated

        public void UpdateUserInvoiceInCart(Invoice newInvoice)
        {
            List<Invoice> oldInvoiceListInCart = new InvoiceServiceImpl().GetListByClientId(newInvoice.ClientId);
            if (oldInvoiceListInCart.Count > 1)
            {
                Invoice oldInvoiceInCart = oldInvoiceListInCart.FirstOrDefault(x => x.OrderStatus == OrderState.InCart);
                if (oldInvoiceInCart != null)
                {
                    long newInvoiceId = newInvoice.Id;
                    if (newInvoiceId != oldInvoiceInCart.Id)
                    {
                        #region Class
                        List<ClassInvoiceItem> newClassInvoiceItemList = newInvoice.ClassInvoiceItems;
                        List<ClassInvoiceItem> oldClassInvoiceItemList = oldInvoiceInCart.ClassInvoiceItems;// new ClassInvoiceItemDataMapper().GetListByInvoiceId(oldInvoiceInCart.Id);
                        if (oldClassInvoiceItemList.Count > 0)
                        {
                            foreach (ClassInvoiceItem item in oldClassInvoiceItemList)
                            {
                                ClassInvoiceItem classInvoiceItem = newClassInvoiceItemList.FirstOrDefault(x => x.SectionId == item.SectionId);
                                if (classInvoiceItem != null)
                                {
                                    new ClassInvoiceItemServiceImpl().Delete(item.Id);
                                }
                                else
                                {
                                    new ClassInvoiceItemServiceImpl().UpdateClassInvoiceId(item, newInvoiceId);
                                }
                            }
                        }
                        #endregion

                        #region Product
                        List<ProductInvoiceItem> ProductInvoiceItemList = oldInvoiceInCart.ProductInvoiceItems;// new ProductInvoiceItemServiceImpl().GetListByInvoiceId(oldInvoiceInCart.Id);
                        List<ProductInvoiceItem> newProductInvoiceItemList = newInvoice.ProductInvoiceItems;
                        if (ProductInvoiceItemList.Count > 0)
                        {
                            foreach (ProductInvoiceItem item in ProductInvoiceItemList)
                            {
                                ProductInvoiceItem productInvoiceItem = newProductInvoiceItemList.FirstOrDefault(x => x.ProductId == item.ProductId);
                                if (productInvoiceItem != null)
                                {
                                    int newProductQuantity = productInvoiceItem.Quantity + item.Quantity;
                                    new ProductInvoiceItemServiceImpl().UpdateProductQuantity(productInvoiceItem, newProductQuantity);
                                    new ProductInvoiceItemServiceImpl().Delete(item.Id);
                                }
                                else
                                {
                                    new ProductInvoiceItemServiceImpl().UpdateProductInvoiceId(item, newInvoiceId);
                                }
                            }
                        }
                        #endregion

                        #region rental
                        List<RentalInvoiceItem> rentalInvoiceItemList = oldInvoiceInCart.RentalInvoiceItems;// new RentalInvoiceItemServiceImpl().GetListByInvoiceId(oldInvoiceInCart.Id);
                        List<RentalInvoiceItem> newRentalInvoiceItemList = newInvoice.RentalInvoiceItems;
                        if (rentalInvoiceItemList.Count > 0)
                        {
                            foreach (RentalInvoiceItem item in rentalInvoiceItemList)
                            {
                                RentalInvoiceItem rentalInvoiceItem = newRentalInvoiceItemList.FirstOrDefault(x => x.RentalEquipmentID == item.RentalEquipmentID);
                                if (rentalInvoiceItem != null)
                                {
                                    new RentalInvoiceItemServiceImpl().Delete(item.Id);
                                }
                                else
                                {
                                    new RentalInvoiceItemServiceImpl().UpdateRentalInvoiceId(item, newInvoiceId);
                                }
                            }
                        }
                        #endregion

                        #region Membership
                        List<MembershipInvoiceItem> memberInvoiceItemList = oldInvoiceInCart.MembershipInvoiceItems;// new MembershipInvoiceItemServiceImpl().GetListByInvoiceId(oldInvoiceInCart.Id);
                        if (memberInvoiceItemList.Count > 0)
                        {
                            foreach (MembershipInvoiceItem item in memberInvoiceItemList)
                            {
                                new MembershipInvoiceItemServiceImpl().UpdateMemberInvoiceId(item, newInvoiceId);
                            }
                        }
                        #endregion

                        new InvoiceServiceImpl().Delete(oldInvoiceInCart.Id);
                    }
                }
            }
        }

        #endregion

        #region after order successfully update invoice and invoice payment

        public void UpdateInvoiceAndInvoicePayment(Invoice invoice)
        {
            //after amazn payment with medical and credit card, for two times refresh page, there is insert again invoicepayment,
            InvoicePaymentDetail invoicePayment = new InvoicePaymentDetail();
            invoicePayment.InvoiceID = invoice.Id;
            invoicePayment.UserId = invoice.ClientId;
            invoicePayment.PaymentMode = PaymentType.CreditCard;
            invoicePayment.Amount = GetPaidAmount(invoice);
            invoicePayment.PaymentDate = DateTime.Today;
            invoicePayment.TransactionStatus = CreditCardTransactionStatus.NA;
            invoicePayment.TransactionID = "";
            invoicePayment.TransactionDetails = "";
            invoicePayment.PatientReceiptNumber = "";
            invoicePayment.PaymentStatus = PaymentStatus.Paid;
            invoicePayment.CheckNumber = "";
            new InvoicePaymentServiceImpl().Add(invoicePayment);

            decimal packageDicsount = CalculatePackageDiscount(invoice);
            //invoice.PaidAmount = invoice.NetPayable - packageDicsount;
            invoice.PaidAmount = invoicePayment.Amount;// GetPaidAmount(invoice); ;
            invoice.PackageDiscount = packageDicsount;

            UpdateInvoiceStatus(invoice);
            new ClassInvoiceItemServiceImpl().UpdateClassStatus(invoice);

            #region email
             #region Email Sending
                bool IsDisablePaymentMethodForZeroBalancedOrderItem = false;
                List<InvoicePaymentDetail> InvoicePaymentDetailList = new List<InvoicePaymentDetail>();
                InvoicePaymentDetailList.Add(invoicePayment);

                long userID = invoicePayment.UserId;
                User userObj = new UserServiceImpl().GetUserById(userID);
                bool fromEmail = true;
                List<CartItem> cartItemList = GetCartOrderItems(invoice, fromEmail);
                List<OrderPaymentRefundHelper> orderPaymentRefundHelperList = GetPaymentDetails(invoice);
                decimal totalDiscount = SetValueForEmail(invoice);
                EmailAddress EmailAddressObj = null;
                List<EmailAddress> EmailAddressList = new EmailAddressServiceImpl().GetAll();
                if (EmailAddressList.Count > 0)
                {
                    EmailAddressObj = EmailAddressList[0];
                }
                IsDisablePaymentMethodForZeroBalancedOrderItem = CalculateTotalDue(invoice);
                if (userObj != null && (cartItemList.Count > 0 || orderPaymentRefundHelperList.Count > 0 || invoice != null) && invoicePayment != null)
                {
                    List<User> adminUserObjList = new UserServiceImpl().GetAllAdminUsers();
                    //userObjList = userObjList.Where(x=>x.UserType == UserType.SuperAdmin);
                    if (adminUserObjList.Count > 0)
                    {
                        foreach (User adminUserObj in adminUserObjList)
                        {
                            if (adminUserObj.ReceiveEmailNotification == IsReceiveEmailNotification.Yes)
                            {
                                new EmailServiceImpl().SendEmailForAcknowledgementForOrderProcessFromMemberPortalToAdmin(adminUserObj, userObj, cartItemList, orderPaymentRefundHelperList, invoice, totalDiscount, IsDisablePaymentMethodForZeroBalancedOrderItem);

                            }
                        }
                    }
                    new EmailServiceImpl().SendEmailForAcknowledgementForOrderProcessFromMemberPortalToClient(userObj, cartItemList, orderPaymentRefundHelperList, invoice, totalDiscount, IsDisablePaymentMethodForZeroBalancedOrderItem);
                    if (EmailAddressObj != null)
                    {
                        new EmailServiceImpl().SendEmailForAcknowledgementForOrderProcessFromMemberPortalToDefaultAdmin(EmailAddressObj, userObj, cartItemList, orderPaymentRefundHelperList, invoice, totalDiscount, IsDisablePaymentMethodForZeroBalancedOrderItem);
                    }
                }
                #endregion                
            #endregion
        }

        public decimal SetValueForEmail(Invoice invoiceObj)
        {
                decimal totalDiscount = 0;
                if (invoiceObj != null)
                {
                    if (invoiceObj.ClassInvoiceItems.Count > 0 || invoiceObj.ProductInvoiceItems.Count > 0)
                    {
                        WS.ToolService ts = new ToolService();
                        XmlNode xmlNode = ts.GetApplicationData("ManageClass");
                        List<ClassWHRC> classWHRCList = new ClassServiceImpl().GetAllClass(xmlNode);

                        XmlNode xmlNode1 = ts.GetApplicationData("ManageSections");
                        List<Section> sectionList = new ClassServiceImpl().GetAllSection(xmlNode1);

                        XmlNode xmlNode2 = ts.GetApplicationData("ManageProductItems");
                        List<ProductItemWHRC> producItemList = new ProductServiceImpl().GetAllProductItemWhrc(xmlNode2);

                        totalDiscount = new InvoicePackageServiceImpl().CalculatePackageDiscount(invoiceObj, classWHRCList, sectionList, producItemList);
                    }
                }
                return totalDiscount;
        }

        public decimal GetPaidAmount(Invoice invoice)
        {
            decimal totalPaidAmount = 0;
            decimal totalDiscountClass = 0;
            decimal totalDiscountProduct = 0;
            decimal packageDicsount = CalculatePackageDiscount(invoice);
            List<PackageWHRC> packageWhrcListClass = GetPackageNameListForInvoice(invoice).Where(x => (x.PackageType == PackageType.Class) && (x.Active == true)).ToList();
            List<PackageWHRC> packageWhrcListProduct = GetPackageNameListForInvoice(invoice).Where(x => (x.PackageType == PackageType.Product) && (x.Active == true)).ToList();
            if (packageWhrcListClass.Count > 0)
            {
                foreach (PackageWHRC item in packageWhrcListClass)
                {
                    totalDiscountClass += item.DiscountAmount;
                }
            }
            if (packageWhrcListProduct.Count > 0)
            {
                foreach (PackageWHRC item in packageWhrcListProduct)
                {
                    totalDiscountProduct += item.DiscountAmount;
                }
            }
            decimal paymentAmountWithoutClass = 0;
            decimal allPackageDiscount = totalDiscountClass + totalDiscountProduct;
            if (IsClassPaidUsingMedical())
            {
                paymentAmountWithoutClass = GetOtherDue();
                totalPaidAmount = paymentAmountWithoutClass - totalDiscountProduct;
                //if (packageDicsount > paymentAmountWithoutClass)
                // {
                // totalPaidAmount = 0;
                // }
                // else
                // {
                //  totalPaidAmount = paymentAmountWithoutClass - packageDicsount;
                // }
            }
            else
            {
                totalPaidAmount = invoice.NetPayable - packageDicsount;
                //if (paymentAmountWithoutClasse > allPackageDiscount)
                //{
                //    totalPaidAmount = paymentAmountWithoutClasse - allPackageDiscount;
                //}
                //else
                //{
                //    totalPaidAmount = 0;
                //}
            }
            return totalPaidAmount;
        }

        public  Dictionary<string, decimal> GetPackageList(Invoice invoice)
        {
            bool isClassPaidUsingMedical = IsClassPaidUsingMedical();
            Dictionary<string, decimal> packageDetails = new Dictionary<string, decimal>();
            List<PackageWHRC> packageWhrcListClass = GetPackageNameListForInvoice(invoice).Where(x => (x.PackageType == PackageType.Class) && (x.Active == true)).ToList();
            List<PackageWHRC> packageWhrcListProduct = GetPackageNameListForInvoice(invoice).Where(x => (x.PackageType == PackageType.Product) && (x.Active == true)).ToList();
            if (!isClassPaidUsingMedical)
            {
                if (packageWhrcListClass.Count > 0)
                {
                    foreach (PackageWHRC item in packageWhrcListClass.Distinct())
                    {
                        //  packageDetails.Add(item.PackageDescription, item.DiscountAmount);
                        packageDetails.Add(item.PackageName + " " + item.PackageDescription, item.DiscountAmount);
                    }
                }
            }
            if (packageWhrcListProduct.Count > 0)
            {
                foreach (PackageWHRC item in packageWhrcListProduct.Distinct())
                {
                   // packageDetails.Add(item.PackageDescription, item.DiscountAmount);
                   // packages.Add(item.PackageName, packageDetails);
                    packageDetails.Add(item.PackageName + " " + item.PackageDescription, item.DiscountAmount);
                }
            }
            return packageDetails;
        }

        private bool IsClassPaidUsingMedical()
        {
            bool result = false;
            long clientOrSelectedClientId = 0;
            Invoice invoice = null;
            if (System.Web.HttpContext.Current.Session["ClientID"] != null)
            {
                clientOrSelectedClientId = Convert.ToInt64(System.Web.HttpContext.Current.Session["ClientID"]);
                invoice = GetInvoiceInCart(clientOrSelectedClientId);
            }
            if (invoice != null)
            {
                List<InvoicePaymentDetail> invoicePaymentDetailsList = new InvoicePaymentServiceImpl().GetListByInvoiceId(invoice.Id);
                InvoicePaymentDetail invoicePaymentDetailForClass = invoicePaymentDetailsList.FirstOrDefault(x => x.PaymentMode == PaymentType.Medical);
                if (invoicePaymentDetailForClass != null)
                {
                    result = true;
                }
                else
                {
                    result = false;
                }
            }
            return result;
        }

        private Invoice GetInvoiceInCart(long clientID)
        {
            Invoice invoice = null;
            List<Invoice> invoiceList = new InvoiceServiceImpl().GetListByClientId(clientID);
            invoice = (from item in invoiceList
                       where item.OrderStatus == OrderState.InCart
                       select item).FirstOrDefault();
            return invoice;
        }

        private decimal GetOtherDue()
        {
            decimal totalDue = 0;
            // decimal totalDiscount = 0;
            //  decimal totalAmount = 0;
            int totalItem = 0;
            long clientID = Convert.ToInt64(System.Web.HttpContext.Current.Session["ClientID"]);
            Invoice invoice = GetInvoiceInCart(clientID);

            if (invoice != null)
            {
                foreach (ProductInvoiceItem item in invoice.ProductInvoiceItems)
                {
                    totalDue += item.Price;
                    totalItem++;
                }

                foreach (RentalInvoiceItem item in invoice.RentalInvoiceItems)
                {
                    totalDue += item.RPTotal + item.DepositAmount;
                    totalItem++;
                }
                foreach (MembershipInvoiceItem item in invoice.MembershipInvoiceItems)
                {
                    totalDue += item.Price;
                    totalItem++;
                }
            }

            //List<PackageWHRC> packageWhrcList = GetPackageNameListForInvoice(invoice).Where(x => x.PackageType == PackageType.Class).ToList();
            //if (packageWhrcList.Count > 0)
            //{
            //    foreach (PackageWHRC item in packageWhrcList)
            //    {
            //            totalDiscount += item.DiscountAmount;
            //    }
            //}
            //totalAmount = totalDue - totalDiscount;
            return totalDue;
        }

        private List<PackageWHRC> GetPackageNameListForInvoice(Invoice invoice)
        {
            //decimal totalDiscount = 0;

            WS.ToolService ts = new ToolService();
            XmlNode xmlNode = ts.GetApplicationData("ManageClass");
            List<ClassWHRC> classWHRCList = new ClassServiceImpl().GetAllClass(xmlNode);

            XmlNode xmlNode1 = ts.GetApplicationData("ManageSections");
            List<Section> sectionList = new ClassServiceImpl().GetAllSection(xmlNode1);

            XmlNode xmlNode2 = ts.GetApplicationData("ManageProductItems");
            List<ProductItemWHRC> producItemList = new ProductServiceImpl().GetAllProductItemWhrc(xmlNode2);

            //totalDiscount = new InvoicePackageServiceImpl().CalculatePackageDiscount(invoice, classWHRCList, sectionList, producItemList);
            List<PackageWHRC> packageWhrcForInvoice = new InvoicePackageServiceImpl().GetPackageNamesOnSingleInvoice(invoice, classWHRCList, sectionList, producItemList);
            return packageWhrcForInvoice;
        }

        private decimal CalculatePackageDiscount(Invoice invoice)
        {
            decimal totalDiscount = 0;

            WS.ToolService ts = new ToolService();
            XmlNode xmlNode = ts.GetApplicationData("ManageClass");
            List<ClassWHRC> classWHRCList = new ClassServiceImpl().GetAllClass(xmlNode);

            XmlNode xmlNode1 = ts.GetApplicationData("ManageSections");
            List<Section> sectionList = new ClassServiceImpl().GetAllSection(xmlNode1);

            XmlNode xmlNode2 = ts.GetApplicationData("ManageProductItems");
            List<ProductItemWHRC> producItemList = new ProductServiceImpl().GetAllProductItemWhrc(xmlNode2);

            totalDiscount = new InvoicePackageServiceImpl().CalculatePackageDiscount(invoice, classWHRCList, sectionList, producItemList);
            return totalDiscount;
        }

        private void UpdateInvoiceStatus(Invoice invoice)
        {
            long clientOrSelectedClientId = 0;
            if (System.Web.HttpContext.Current.Session["ClientID"] != null)
            {
                clientOrSelectedClientId = Convert.ToInt64(System.Web.HttpContext.Current.Session["ClientID"]);
            }
            else if (System.Web.HttpContext.Current.Session["AdminID"] != null)
            {
                if (System.Web.HttpContext.Current.Session["SelectedUserId"] != null)
                {
                    clientOrSelectedClientId = Convert.ToInt64(System.Web.HttpContext.Current.Session["SelectedUserId"]);
                }
            }

            long shipmentAddressID = 0;
            invoice.OrderStatus = OrderState.Ordered;
            invoice.ShippingAddressId = shipmentAddressID;
            invoice.PurchaseDate = DateTime.Now;
            invoice.PaymentMode = PaymentType.CreditCard;
            invoice.ClientRequestedDiscountMsg = "";
            invoice.ClientId = clientOrSelectedClientId;

           // UpdateShipmentAddressForProductInvoice(invoice);// Update Shipment Address For Product Invoice
          //  UpdateDefaultShippingAddress();
            bool isChildUpdatable = false;

            new InvoiceServiceImpl().Update(invoice, isChildUpdatable);
        }

       
        #endregion

        public List<CartItem> GetCartOrderItems(Invoice invoice, bool fromEmail)
        {
            List<CartItem> cartItems = new List<CartItem>();
            decimal totalAmount = 0;

            if (invoice != null)
            {
                CartItem cartItem = null;
                foreach (ProductInvoiceItem item in invoice.ProductInvoiceItems)
                {
                    cartItem = new CartItem();
                    cartItem.InvoiceItemType = daoProject.Infrastructure.InvoiceItemType.Purchase;
                    cartItem.InvoiceItemTypeDescription = "Purchase";
                    cartItem.PurchaseDate = invoice.PurchaseDate;
                    cartItem.Description = GetPurchaseDescription(item);
                    cartItem.InvoiceItemId = item.Id;
                    cartItem.InvoiceItemIdAndInvoiceItemType = Convert.ToString(item.Id) + "#" + Convert.ToString(Convert.ToInt32(daoProject.Infrastructure.InvoiceItemType.Purchase));
                    cartItem.Price = item.Price;
                    cartItem.PriceString = "$" + item.Price.ToString("0.00");
                    totalAmount += cartItem.Price;
                    if (fromEmail)
                    {
                        if (item.ShippingAddressId == 0)
                        {
                            cartItem.ShippingAddress = "Pick from Store";
                        }
                        else
                        {
                            ShippingAddress shippingAddress = GetShippingAddressById(item.ShippingAddressId);
                            if (shippingAddress != null)
                            {
                                cartItem.ShippingAddress = shippingAddress.DetailDescription;
                            }
                        }
                    }
                    // cartItem.Quantity = 1;
                    cartItems.Add(cartItem);
                }
                foreach (ClassInvoiceItem item in invoice.ClassInvoiceItems)
                {
                    cartItem = new CartItem();
                    cartItem.InvoiceItemType = daoProject.Infrastructure.InvoiceItemType.Class;
                    cartItem.InvoiceItemTypeDescription = "Class";
                    cartItem.PurchaseDate = invoice.PurchaseDate;
                    cartItem.Description = GetClassDescription(item);
                    cartItem.InvoiceItemId = item.Id;
                    cartItem.InvoiceItemIdAndInvoiceItemType = Convert.ToString(item.Id) + "#" + Convert.ToString(Convert.ToInt32(daoProject.Infrastructure.InvoiceItemType.Class));
                    if (item.IsInWaitingList)
                    {
                        cartItem.Price = 0;
                    }
                    else
                    {
                        cartItem.Price = item.Price;
                    }
                    cartItem.PriceString = "$" + cartItem.Price.ToString("0.00");
                    totalAmount += cartItem.Price;
                    // cartItem.Quantity = 1;
                    cartItems.Add(cartItem);
                }
                foreach (RentalInvoiceItem item in invoice.RentalInvoiceItems)
                {
                    cartItem = new CartItem();
                    cartItem.InvoiceItemType = daoProject.Infrastructure.InvoiceItemType.Rental;
                    cartItem.InvoiceItemTypeDescription = "Rental";
                    cartItem.PurchaseDate = invoice.PurchaseDate;
                    cartItem.Description = GetRentalDescription(item);
                    cartItem.InvoiceItemId = item.Id;
                    cartItem.InvoiceItemIdAndInvoiceItemType = Convert.ToString(item.Id) + "#" + Convert.ToString(Convert.ToInt32(daoProject.Infrastructure.InvoiceItemType.Rental));
                    cartItem.Price = item.RPTotal;
                    cartItem.PriceString = "$" + item.RPTotal.ToString("0.00") + "+(deposit: " + item.DepositAmount.ToString("0.00") + ")";
                    totalAmount += cartItem.Price + item.DepositAmount;
                    // cartItem.Quantity = 1;
                    cartItems.Add(cartItem);
                }
                foreach (MembershipInvoiceItem item in invoice.MembershipInvoiceItems)
                {
                    cartItem = new CartItem();
                    cartItem.InvoiceItemType = daoProject.Infrastructure.InvoiceItemType.Membership;
                    cartItem.InvoiceItemTypeDescription = "Membership";
                    cartItem.PurchaseDate = invoice.PurchaseDate;
                    cartItem.Description = GetMembershipDescription(item);
                    cartItem.InvoiceItemId = item.Id;
                    cartItem.InvoiceItemIdAndInvoiceItemType = Convert.ToString(item.Id) + "#" + Convert.ToString(Convert.ToInt32(daoProject.Infrastructure.InvoiceItemType.Membership));
                    cartItem.Price = item.Price;
                    cartItem.PriceString = "$" + item.Price.ToString("0.00");
                    totalAmount += cartItem.Price;
                    // cartItem.Quantity = 1;
                    cartItems.Add(cartItem);
                }
            }
            return cartItems;
        }

        private string GetPurchaseDescription(ProductInvoiceItem productInvoiceItem)
        {
            string purchaseDescription = "";
            List<ProductItemWHRC> poductItemList = null;

            if (System.Web.HttpContext.Current.Session["ProductTableClientOrder"] == null)
            {
                WS.ToolService ts = new ToolService();
                XmlNode xmlNode = ts.GetApplicationData("ManageProductitems");
                poductItemList = new ProductServiceImpl().GetAllProductItemWhrc(xmlNode);

                System.Web.HttpContext.Current.Session["ProductTableClientOrder"] = poductItemList;
            }
            else
            {
                poductItemList = (List<ProductItemWHRC>)System.Web.HttpContext.Current.Session["ProductTableClientOrder"];
            }

            foreach (ProductItemWHRC item in poductItemList)
            {
                if (item.RecordId == productInvoiceItem.ProductId)
                {
                    purchaseDescription = item.ItemName + ", quantity: " + productInvoiceItem.Quantity.ToString();
                    break;
                }
            }
            return purchaseDescription;
        }

        private string GetClassDescription(ClassInvoiceItem classInvoiceItem)
        {
            string purchaseDescription = "";
            List<Section> sections = null;

            WS.ToolService ts = new ToolService();
            XmlNode xmlNode = ts.GetApplicationData("ManageSections");
            sections = new ClassServiceImpl().GetAllSection(xmlNode);

            foreach (Section item in sections)
            {
                if (item.RecordID == classInvoiceItem.SectionId)
                {
                    purchaseDescription = item.ClassTitle + ", " + item.DayOfWeek + ", start date: " + item.StartDate.ToString("MM/dd/yyy") + ", " + item.Locations;
                    if (classInvoiceItem.IsInWaitingList)
                    {
                        purchaseDescription += " ,<strong>In Waiting List</strong>";
                    }
                    break;
                }
            }
            return purchaseDescription;
        }

        private string GetRentalDescription(RentalInvoiceItem rentalInvoiceItem)
        {
            string purchaseDescription = "";
            List<RentalEquipmentWHRC> rentalEquipmentList = null;

            if (System.Web.HttpContext.Current.Session["RentalTableOrderProcess"] == null)
            {
                WS.ToolService ts = new ToolService();
                XmlNode xmlNode = ts.GetApplicationData("MgeRentalEquipment");
                rentalEquipmentList = new RentalServiceImpl().GetAllRentalEquipment(xmlNode);

                System.Web.HttpContext.Current.Session["RentalTableOrderProcess"] = rentalEquipmentList;
            }
            else
            {
                rentalEquipmentList = (List<RentalEquipmentWHRC>)System.Web.HttpContext.Current.Session["RentalTableOrderProcess"];
            }

            foreach (RentalEquipmentWHRC item in rentalEquipmentList)
            {
                if (item.RecordID == rentalInvoiceItem.RentalEquipmentID)
                {
                    purchaseDescription = "Serial #: " + item.Serial + ", " + item.ModelName + ", period " + rentalInvoiceItem.REStartingDate.ToString("MM/dd/yyyy") + " to " + rentalInvoiceItem.RPEndingDate.ToString("MM/dd/yyyy");
                    break;
                }
            }
            return purchaseDescription;
        }

        private string GetMembershipDescription(MembershipInvoiceItem membershipInvoiceItem)
        {
            string purchaseDescription = "";
            List<MembershipWHRC> memberships = null;

            if (System.Web.HttpContext.Current.Session["MembershipTableOrderProcess"] == null)
            {
                WS.ToolService ts = new ToolService();
                XmlNode xmlNode = ts.GetApplicationData("WHRCMemberships");
                memberships = new VendorMemberTaxServiceImpl().GetAllMembership(xmlNode);


                System.Web.HttpContext.Current.Session["MembershipTableOrderProcess"] = memberships;
            }
            else
            {
                memberships = (List<MembershipWHRC>)System.Web.HttpContext.Current.Session["MembershipTableOrderProcess"];
            }

            foreach (MembershipWHRC item in memberships)
            {
                if (item.RecordID == membershipInvoiceItem.MembershipId)
                {
                    purchaseDescription = item.NameofMembership + ", from " + membershipInvoiceItem.DateJoined.ToString("MM/dd/yyy") + " to " + membershipInvoiceItem.DateValidTill.ToString("MM/dd/yyy");
                    break;
                }
            }

            return purchaseDescription;
        }

        private ShippingAddress GetShippingAddressById(long id)
        {
            ShippingAddress shippingAddressObj = new ShippingAddressServiceImpl().GetById(id);
            return shippingAddressObj;
        }

        public List<OrderPaymentRefundHelper> GetPaymentDetails(Invoice invoice)
        {
            List<OrderPaymentRefundHelper> orderPaymentRefundHelperList = new List<OrderPaymentRefundHelper>();
            OrderPaymentRefundHelper orderPaymentRefundHelper = null;

            if (invoice != null)
            {
                orderPaymentRefundHelper = new OrderPaymentRefundHelper();
                orderPaymentRefundHelper.InvoiceID = invoice.Id;
                orderPaymentRefundHelper.PaymentDetails = PreparePaymentDetails(invoice);
                orderPaymentRefundHelper.RefundDetails = PrepareRefundDetails(invoice);

                orderPaymentRefundHelperList.Add(orderPaymentRefundHelper);
            }
            return orderPaymentRefundHelperList;
        }

        private string PreparePaymentDetails(Invoice invoice)
        {
            string paymentDetails = "";
            paymentDetails = "Total Amount: $" + invoice.TotalAmount.ToString("0.00") + "<br/>";
            decimal overdueAmount = 0;
            foreach (RentalInvoiceItem item in invoice.RentalInvoiceItems)
            {
                overdueAmount += item.OverdueCharge;
            }
            paymentDetails += "Overdue amount: $" + overdueAmount.ToString("0.00") + "<br/>";
            return paymentDetails;
        }

        private string PrepareRefundDetails(Invoice invoice)
        {
            string refundDetails = "-";

            if (invoice.TotalRefundAmount > 0)
            {
                if (invoice.ProductInvoiceItems.Count > 0)
                {
                    decimal productRefundAmount = 0;
                    foreach (ProductInvoiceItem item in invoice.ProductInvoiceItems)
                    {
                        productRefundAmount += item.RefundAmount;
                    }
                    refundDetails = "Refund for return product: $" + productRefundAmount.ToString("0.00") + "</br>";
                }
                if (invoice.ClassInvoiceItems.Count > 0)
                {
                    decimal classCalcelRefundAmount = 0;
                    foreach (ClassInvoiceItem item in invoice.ClassInvoiceItems)
                    {
                        classCalcelRefundAmount += item.RefundAmount;
                    }
                    refundDetails += "Refund for cancelling class signup: $" + classCalcelRefundAmount.ToString("0.00") + "</br>";
                }

                if (invoice.RentalInvoiceItems.Count > 0)
                {
                    decimal rentalRefundAmount = 0;
                    decimal rentalDepositRefundAmount = 0;
                    foreach (RentalInvoiceItem item in invoice.RentalInvoiceItems)
                    {
                        rentalRefundAmount += item.RefundAmount;
                        rentalDepositRefundAmount += item.DepositAmount;
                    }
                    refundDetails += "Refund for rental deposit: $" + rentalDepositRefundAmount.ToString("0.00") + "</br>";
                    refundDetails += "Refund for ending rental: $" + rentalRefundAmount.ToString("0.00") + "</br>";
                }
            }

            if (invoice.Discount > 0 || invoice.PackageDiscount > 0)
            {
                decimal totalDiscount = invoice.Discount + invoice.PackageDiscount;
                refundDetails += "Total discount amount: $" + totalDiscount.ToString("0.00") + "</br>";
            }

            return refundDetails;
        }

        private bool CalculateTotalDue(Invoice invoice)
        {
            decimal totalDue = 0;

            bool IsDisablePaymentMethodForZeroBalancedOrderItem = false;
            if (invoice != null)
            {
                foreach (ProductInvoiceItem item in invoice.ProductInvoiceItems)
                {
                    totalDue += item.Price;
                }

                foreach (RentalInvoiceItem item in invoice.RentalInvoiceItems)
                {
                    totalDue += item.RPTotal + item.DepositAmount;
                }
                foreach (MembershipInvoiceItem item in invoice.MembershipInvoiceItems)
                {
                    totalDue += item.Price;
                }
                foreach (ClassInvoiceItem item in invoice.ClassInvoiceItems)
                {
                    if (!item.IsInWaitingList)
                    {
                        totalDue += item.Price;
                    }
                }
            }
            if (!(totalDue > 0))
            {
                IsDisablePaymentMethodForZeroBalancedOrderItem = true;
            }
            return IsDisablePaymentMethodForZeroBalancedOrderItem;
        }

    }
}
