﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using daoProject.Model;
using daoProject.Utilities;
using daoProject.Mapper;

namespace daoProject.Service
{
    public class ProductInvoiceItemServiceImpl
    {
        private ProductInvoiceItemDataMapper dataMapper;

        public ProductInvoiceItemServiceImpl()
        {
            dataMapper = new ProductInvoiceItemDataMapper();
        }

        public void Add(ProductInvoiceItem item)
        {
            dataMapper.Create(item);
        }
        public void Add(ProductInvoiceItem item, System.Data.Odbc.OdbcConnection conn, System.Data.Odbc.OdbcTransaction tx)
        {
            dataMapper.Create(item, conn, tx);
        }

        public void Update(ProductInvoiceItem item)
        {
            dataMapper.Update(item);
        }

        public bool Delete(long id)
        {
            return dataMapper.Delete(id);
        }
        public decimal getItemAmountPrice(long id)
        {
            return dataMapper.getItemAmountPrice(id);
        }
        public List<ProductInvoiceItem> GetAll()
        {
            return dataMapper.GetAll();
        }

        public ProductInvoiceItem GetById(long id)
        {
            return dataMapper.GetById(id);
        }

        public List<ProductInvoiceItem> GetListByInvoiceId(long invoiceId)
        {
            return dataMapper.GetListByInvoiceId(invoiceId);
        }

        public bool DeleteByInvoiceId(long invoiceId)
        {
            return dataMapper.DeleteByInvoiceId(invoiceId);
        }

        public void ReturnItem(ProductInvoiceItem item)
        {
            dataMapper.ReturnItem(item);
        }

        public void UpdateRefundProcessed(ProductInvoiceItem item)
        {
            dataMapper.UpdateRefundProcessed(item);
        }

        public void UpdateShipmentAddress(ProductInvoiceItem item)
        {
            dataMapper.UpdateShipmentAddress(item);
        }
        public void UpdateProductInvoiceId(ProductInvoiceItem item, long newInvoiceId)
        {
            dataMapper.UpdateProductInvoiceId(item,newInvoiceId);
        }
        public void UpdateProductQuantity(ProductInvoiceItem productInvoiceItem,int newProductQuantity)
        {
            dataMapper.UpdateProductQuantity(productInvoiceItem, newProductQuantity);
        }
    }
}
