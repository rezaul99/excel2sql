﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using daoProject.Model;
using System.Data;
using System.Data.OleDb;
using daoProject.Infrastructure;
using daoProject.Utilities;
using System.Xml;
using daoProject.WS;
using System.Configuration;
using System.Data.Odbc;

namespace daoProject.Service
{
    public class ExcelToSqlServiceImpl
    {
        public void InsertToDb(string fileName, string sheetName)
        {
            DataSet ds = ExcelToSqlConversion(fileName, sheetName);
            Insert(ds);
        }
        public DataTable AccessToSqlConversion(string path, string sheetName)
        {
            using (OdbcConnection myConnection = new OdbcConnection())
            {
                string connectionString = @"Driver={Microsoft Access Driver (*.mdb, *.accdb)};Dbq=F:\ATT2000.MDB;Uid=Admin;Pwd=;";
                myConnection.ConnectionString = connectionString;
                myConnection.Open();
                DataSet dsExecute = new DataSet();

                // This property create instance of OdbcDataAdapter
                OdbcDataAdapter daExecute = new OdbcDataAdapter();

                OdbcCommand cmd = new OdbcCommand(@"select * from AttParam");
                cmd.Connection = myConnection;
                //Sqlcommand as a select command
                daExecute.SelectCommand = cmd;

                //Accept the change
                daExecute.AcceptChangesDuringFill = false;

                //Fill Data from DataAdapter to DataSet
                daExecute.Fill(dsExecute, "Data");

                DataTable dt = dsExecute.Tables[0];

                // Close the OdbcConnection
                myConnection.Close();

                return dt;
            }
        }
        public DataSet ExcelToSqlConversion(string path, string sheetName)
        {            
            OleDbConnection dbConnection = new OleDbConnection();
            string connectionString = string.Format("Provider=Microsoft.ACE.OLEDB.12.0; Data Source={0}; Extended Properties=\"Excel 12.0;HDR=Yes;IMEX=1\";", path);
      //      string connectionString = ConfigurationManager.ConnectionStrings["xls"].ToString();
            dbConnection = new OleDbConnection(connectionString);
           // dbConnection = new OleDbConnection(ConfigurationManager.Configuration.WebConfigurationManager.ConnectionStrings["xls"].ConnectionString);
            dbConnection.Open();
            OleDbCommand select = new OleDbCommand();
            select.Connection = dbConnection;
            sheetName = "Sheet1$";
            select.CommandText = "SELECT * FROM ["+ sheetName +"]";
            OleDbDataReader reader = select.ExecuteReader();
            while (reader.Read())
            {
             //   listBox1.Items.Add(reader[1].ToString() + "," + reader[2].ToString());
            }
            dbConnection.Close();

           
            /**** 2nd Way***/
            dbConnection.ConnectionString = string.Format("Provider=Microsoft.ACE.OLEDB.12.0; Data Source={0}; Extended Properties=\"Excel 12.0;HDR=Yes;IMEX=1\";", path);
            StringBuilder stbQuery = new StringBuilder();
            stbQuery.Append("SELECT * FROM ["+ sheetName+"]");
            OleDbDataAdapter adp = new OleDbDataAdapter(stbQuery.ToString(), dbConnection);
            DataSet dsXLS = new DataSet();
            adp.Fill(dsXLS);
            return dsXLS;
            
        }
        private void Insert(DataSet ds)
        {
            WS.ToolService ts = new ToolService();
            XmlNode xmlNode = ts.GetApplicationData("ManageSections");

            List<Section> sectionAll = new List<Section>();
            List<Section> sectionOne = new List<Section>();
            Section section = null;

            List<ClassInvoiceItem> classInvoiceItemList = new List<ClassInvoiceItem>();
            //classInvoiceItemList = new ClassInvoiceItemServiceImpl().GetAll();
            sectionAll = new ClassServiceImpl().GetAllSection(xmlNode);

            if (ds.Tables["Table"].Rows.Count > 0)
            {
                for (int i = 0; i < ds.Tables["Table"].Rows.Count; i++)
                {
                    

                    //if (Convert.ToString(ds.Tables["Table"].Rows[i].ItemArray[11]) == "Sabrina Rascone" && Convert.ToString(ds.Tables["Table"].Rows[i].ItemArray[3]) == "6/23/2009 12:00:00 AM")
                    {
                        DateTime dt = Convert.ToDateTime("6/23/2009 12:00:00 AM");
                        sectionOne = sectionAll.Where(x => x.ClassTitle == Convert.ToString(ds.Tables["Table"].Rows[i].ItemArray[1])).ToList();
                        sectionOne = sectionAll.Where(x => x.StartDate == Convert.ToDateTime(ds.Tables["Table"].Rows[i].ItemArray[3])).ToList();// && x.StartDate == dt).ToList();
                        if (sectionOne.Count > 0)
                        {
                            int p = 0;
                        }
                    }                    
                }
            }           
        }
    }
}
