﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using daoProject.Mapper;
using daoProject.Model;

namespace daoProject.Service
{
    public class EmailAddressServiceImpl
    {
        private EmailAddressDataMapper dataMapper;

        public EmailAddressServiceImpl()
        {
            dataMapper = new EmailAddressDataMapper();
        }
        public List<EmailAddress> GetAll()
        {
            return dataMapper.GetAll();
        }
        public EmailAddress GetById(long Id)
        {
            return dataMapper.GetById(Id);
        }
        public void Add(EmailAddress obj)
        {
            dataMapper.Add(obj);
        }
        public void Update(EmailAddress obj)
        {
            dataMapper.Update(obj);
        }
    }
}
