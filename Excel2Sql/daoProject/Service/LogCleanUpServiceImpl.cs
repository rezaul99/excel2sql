﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using daoProject.Model;
using daoProject.Utilities;
using daoProject.Mapper;
using System.Data;
using System.Xml;
using daoProject.Infrastructure;


namespace daoProject.Service
{
   public class LogCleanUpServiceImpl
    {
       public List<LogCleanUpDaysWHRC> GetAllLogCleanUpDays(XmlNode xmlNode)
       {
           List<LogCleanUpDaysWHRC> logDayList = new List<LogCleanUpDaysWHRC>();
           LogCleanUpDaysWHRC logCleanUpDaysWHRC = null;

           if (xmlNode != null)
           {
               XmlNodeList datas = xmlNode.ChildNodes;
               for (int i = 0; i < datas.Count; i++)
               {
                   List<XmlNode> nodeList = new List<XmlNode>(datas[i].ChildNodes.Cast<XmlNode>());
                   XmlNode recordIDNode = (from item in nodeList
                                           where item.Name.Replace(" ", "") == "RecordId"
                                           select item).FirstOrDefault();
                   XmlNode logDaysNode = (from item in nodeList
                                          where item.Name.Replace(" ", "") == "LogCleanupdays"
                                               select item).FirstOrDefault();


                   logCleanUpDaysWHRC = new LogCleanUpDaysWHRC();
                   logCleanUpDaysWHRC.RecordID = Convert.ToInt64(recordIDNode.InnerText);
                   if (logDaysNode.InnerText != "")
                   {
                       logCleanUpDaysWHRC.LogCleanUpDays = Convert.ToInt32(logDaysNode.InnerText);
                   }

                   logDayList.Add(logCleanUpDaysWHRC);
               }
           }
           return logDayList;
       }

       public XmlDocument InsertManageSalesTaxRate(LogCleanUpDaysWHRC logCleanUpDaysWHRC)
       {
           DataTable dt = new DataTable();
           DataColumn dc1 = new DataColumn("Name");
           DataColumn dc2 = new DataColumn("Value");

           dt.Columns.Add(dc1);
           dt.Columns.Add(dc2);

           AddDataRow("LogCleanupdays", Convert.ToString(logCleanUpDaysWHRC.LogCleanUpDays), dt);

           XmlDocument xmlDoc = new XmlDocument();
           DataSet dataSet = new DataSet();
           dataSet.Tables.Add(dt);
           string strXml = dataSet.GetXml();
           xmlDoc.LoadXml(strXml);

           return xmlDoc;
       }

       private void AddDataRow(string name, string value, DataTable dt)
       {
           DataRow dr = dt.NewRow();
           dr["Name"] = name;
           dr["Value"] = value;
           dt.Rows.Add(dr);
       }
    }

  
}
