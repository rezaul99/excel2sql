﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using daoProject.Mapper;
using daoProject.Model;
using System.Xml;
using daoProject.Infrastructure;
using System.Collections;
using daoProject.Utilities;
using System.Collections.Generic;



namespace daoProject.Service
{
    public class ReportServiceImpl
    {
        private ReportDataMapper dataMapper;

        public ReportServiceImpl()
        {
            dataMapper = new ReportDataMapper();
        }

        public DataTable GetAllSignupClassesByDateRange(DateTime startDate, DateTime endDate)
        {
            return dataMapper.GetAllSignupClassesByDateRange(startDate, endDate);
        }

        public List<RptClassMedical> GetClassEnrollmentMediCalList(XmlNode xmlNode, DateTime fromDate, DateTime toDate)
        {
           

            List<RptClassMedical> rptClassMedicalList = new List<RptClassMedical>();
            
            List<Invoice> invoiceListMedical = dataMapper.GetInvoiceMedicalSignups(fromDate, toDate);

            List<ClassInvoiceItem> allSignups = dataMapper.GetAllClassSignips(fromDate, toDate);
            DataTable dt = GetClassSectionData(xmlNode);
            List<DataRow> dtRowList = dt.Select().ToList();

            foreach (ClassInvoiceItem item in allSignups)
            {
                DataRow row = dtRowList.FirstOrDefault(x => Convert.ToInt64(x["RecordID"]) == item.SectionId);
                if (row != null)
                {
                    RptClassMedical rptClassMedical = new RptClassMedical();
                    rptClassMedical.ClassType = Convert.ToString(row["ClassType"]);
                    rptClassMedical.Title = Convert.ToString(row["Title"]);
                    DateTime date = Convert.ToDateTime(row["StartDate"]);
                    rptClassMedical.StartDate = date.ToString("MM/dd/yyy");
                    rptClassMedical.Section = Convert.ToString(row["DayofWeek"]) + " | " + Convert.ToString(row["StartDate"]);

                    rptClassMedical.MaxSize = Convert.ToInt32(row["MaximumSize"]);

                    //Here, Enrollment is used for TotalEnrolled
                    rptClassMedical.Enrollment = CalculateTotalEnrolled(item.SectionId, allSignups); //Convert.ToInt32(row["MaximumSize"]);
                    //rptClassMedical.MediCalSignups = CalculateMedicalSignup(item.InvoiceId, invoiceListMedical);
                    rptClassMedical.MediCalSignups = CalculateMedicalSignup(item.InvoiceId);

                    rptClassMedicalList.Add(rptClassMedical);
                }
            }
            return rptClassMedicalList;
        }

        public List<RptClassAll> GetClassEnrollmentAll(XmlNode xmlNode, DateTime fromDate, DateTime toDate)
        {
            List<RptClassAll> classEnrollmentList = new List<RptClassAll>();
            RptClassAll rptClassAll = new RptClassAll();

            List<ClassInvoiceItem> allSignups = dataMapper.GetAllClassSignips(fromDate, toDate);
            DataTable dt = GetClassSectionData(xmlNode);
            List<DataRow> dtRowList = dt.Select().ToList();
            int totalEnrolled = 0;

            foreach (ClassInvoiceItem item in allSignups)
            {
                DataRow row = dtRowList.FirstOrDefault(x => Convert.ToInt64(x["RecordID"]) == item.SectionId);
                if (row != null)
                {
                    
                    rptClassAll = new RptClassAll();
                    rptClassAll.ClassType = Convert.ToString(row["ClassType"]);
                    rptClassAll.Title = Convert.ToString(row["Title"]);
                    DateTime date = Convert.ToDateTime(row["StartDate"]);
                    rptClassAll.SectionCode = Convert.ToString(row["DayofWeek"]) + " | " + date.ToString("MM/dd/yyy");
                    rptClassAll.MaxSize = Convert.ToInt32(row["MaximumSize"]);
                    rptClassAll.SpacesAvailable = CalculateAvailable(item.SectionId, rptClassAll.MaxSize, allSignups);
                    //rptClassAll.Enrollment = rptClassAll.MaxSize - rptClassAll.SpacesAvailable;
                    totalEnrolled = item.NumberAttending;
                    rptClassAll.Enrollment = totalEnrolled;
                    classEnrollmentList.Add(rptClassAll);
                }
            }
            return classEnrollmentList;
        }

        public List<RptClassAll> GetClassEnrollmentBySectionId(XmlNode xmlNode, long sectionId)
        {
            List<RptClassAll> classEnrollmentList = new List<RptClassAll>();
            RptClassAll rptClassAll = new RptClassAll();

            DataTable dtAttending = dataMapper.GetClassAttendingsGroupBySectionId();
            List<DataRow> dtAttendingList = dtAttending.Select().ToList().FindAll(x => Convert.ToInt64(x["SectionId"]) == sectionId);

            DataTable dt = GetClassSectionData(xmlNode);
            List<DataRow> dtRowlist = dt.Select().ToList();

            foreach (DataRow item in dtAttendingList)
            {
                DataRow row = dtRowlist.FirstOrDefault(x => Convert.ToInt64(x["RecordID"]) == Convert.ToInt64(item["SectionId"]));

                if (row != null)
                {
                    rptClassAll = new RptClassAll();
                    rptClassAll.ClassType = Convert.ToString(row["ClassType"]);
                    rptClassAll.Title = Convert.ToString(row["Title"]);
                    rptClassAll.SectionCode = Convert.ToString(row["DayofWeek"]) + " | " + Convert.ToString(row["StartDate"]);
                    rptClassAll.MaxSize = Convert.ToInt32(row["MaximumSize"]);
                    rptClassAll.Enrollment = Convert.ToInt32(item["TotalAttending"]);
                    rptClassAll.SpacesAvailable = rptClassAll.MaxSize - rptClassAll.Enrollment;
                    classEnrollmentList.Add(rptClassAll);
                }
            }
            return classEnrollmentList;
        }

        //public DataTable GetClassAttendanceBySectionId(XmlNode xmlNode, long sectionId)
        //{
        //    DataTable dtToReturn = new DataTable();

        //    try
        //    {
        //        dtToReturn.Columns.Add("Month");
        //        dtToReturn.Columns.Add("ClassName");
        //        dtToReturn.Columns.Add("Section");
        //        dtToReturn.Columns.Add("NoOfAttendee", typeof(int));

        //        DataTable dtClassSection = GetClassSectionData(xmlNode);
        //        List<DataRow> drListClassSection = dtClassSection.Select().ToList();

        //        DataTable dt = dataMapper.GetClassAttendanceBySectionId(sectionId);

        //        foreach (DataRow dr in dt.Rows)
        //        {
        //            DataRow drClassSection = drListClassSection.FirstOrDefault(x => Convert.ToInt64(x["RecordID"]) == Convert.ToInt64(dr["SectionId"]));

        //            DataRow drToReturn = dtToReturn.NewRow();

        //            if (drClassSection != null)
        //            {
        //                drToReturn["Month"] = Convert.ToDateTime(drClassSection["StartDate"]).ToString("MMMM yyyy");
        //                drToReturn["ClassName"] = drClassSection["Title"];
        //                drToReturn["Section"] = Convert.ToString(drClassSection["DayofWeek"]) + " | " + Convert.ToString(drClassSection["StartDate"])/* + " | " + Convert.ToString(drClassSection["MaximumSize"])*/;
        //            }
        //            drToReturn["NoOfAttendee"] = Convert.ToInt32(dr["NoOfAttendee"]);

        //            dtToReturn.Rows.Add(drToReturn);
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        dtToReturn = new DataTable();
        //    }

        //    return dtToReturn;
        //}

        public DataTable GetClassAttendanceBySectionId(long sectionId)
        {
            DataTable dtToReturn = new DataTable();

            try
            {
                dtToReturn.Columns.Add("SignupDate");
                dtToReturn.Columns.Add("OnWaitList");
                dtToReturn.Columns.Add("Client");
                dtToReturn.Columns.Add("NoAttending", typeof(int));
                dtToReturn.Columns.Add("DateCancelled");
                dtToReturn.Columns.Add("ReasonCancelled");

                DataTable dt = dataMapper.GetClassAttendanceBySectionId(sectionId);

                foreach (DataRow dr in dt.Rows)
                {
                    DataRow drToReturn = dtToReturn.NewRow();

                    bool isClassCanceled = Convert.ToBoolean(dr["IsClassCanceled"]);
                    bool isInWaitingList = Convert.ToBoolean(dr["IsInWaitingList"]);
                    int numberAttending = Convert.ToInt32(dr["NumberAttending"]);
                    bool isClassCanceledByClient = Convert.ToBoolean(dr["IsClassCanceledByClient"]);

                    drToReturn["SignupDate"] = Convert.ToDateTime(dr["SignupDate"]).ToString("MM/dd/yyyy");
                    drToReturn["OnWaitList"] = isInWaitingList ? "Wait List" : "";
                    drToReturn["Client"] = Convert.ToString(dr["LastName"]) + ", " + Convert.ToString(dr["FirstName"]);
                    drToReturn["NoAttending"] = !isClassCanceled ? numberAttending : 0;
                    drToReturn["DateCancelled"] = isClassCanceled ? Convert.ToDateTime(dr["CancelDate"]).ToString("MM/dd/yyyy") : "";
                    drToReturn["ReasonCancelled"] = (isClassCanceled && isClassCanceledByClient) ? "Client Cancelled" : (isClassCanceled ? "Admin Cancelled" : "");

                    dtToReturn.Rows.Add(drToReturn);
                }
            }
            catch (Exception ex)
            {
                dtToReturn = new DataTable();
            }

            return dtToReturn;
        }

        public DataTable GetClassAttendanceSignUPBySectionId(long sectionId)
        {
            DataTable dtToReturn = new DataTable();
            try
            {
                dtToReturn.Columns.Add("ClientId");
                dtToReturn.Columns.Add("ClientDetails");
                dtToReturn.Columns.Add("ClientOtherDetails");

                DataTable dt = dataMapper.GetClassAttendanceSignUPBySectionId(sectionId);
                foreach (DataRow dr in dt.Rows)
                {
                    string waitingListStatus = "";
                    if (Convert.ToInt32(dr["IsInWaitingList"]) != 0)
                    {
                        waitingListStatus = "In Waiting List.";
                    }
                    DataRow drToReturn = dtToReturn.NewRow();
                    drToReturn["ClientId"] = Convert.ToString(dr["Id"]);
                    drToReturn["ClientDetails"] = Convert.ToString(dr["LastName"]) + ", " + Convert.ToString(dr["FirstName"]) +
                        System.Environment.NewLine + Convert.ToString(dr["Address1"]) + ", " + Convert.ToString(dr["AD1City"])
                        + System.Environment.NewLine + Convert.ToString(dr["AD1State"]) + ", " + Convert.ToString(dr["AD2Zip"])
                        + System.Environment.NewLine + Convert.ToString(dr["Phone"])
                        + System.Environment.NewLine + System.Environment.NewLine + Convert.ToString(dr["Email"]);

                        drToReturn["ClientOtherDetails"] = "Date of Birth :  " + Convert.ToDateTime(dr["DOB"]).ToString("MM/dd/yyyy")
                                                         + "    Signup Date :  " + Convert.ToDateTime(dr["SignupDate"]).ToString("MM/dd/yyyy")
                                        + System.Environment.NewLine +
                                                       " Due Date :  " + Convert.ToDateTime(dr["DueDate"]).ToString("MM/dd/yyyy")
                                                        + "     Number Attending :      " + Convert.ToString(dr["NumberAttending"]) + ",  " + waitingListStatus
                                        + System.Environment.NewLine +
                                                        "Insurance :  " + Convert.ToString(dr["Insurance"]);
                    
                    dtToReturn.Rows.Add(drToReturn);
                }
            }
            catch (Exception ex)
            {
                dtToReturn = new DataTable();
            }

            return dtToReturn;
        }

        public DataTable GetClassFillRateBySectionId(XmlNode xmlNode, long sectionId,int MaxSize)
        {
            DataTable dtToReturn = new DataTable();

            try
            {
                dtToReturn.Columns.Add("Month");
                dtToReturn.Columns.Add("ClassName");
                dtToReturn.Columns.Add("Section");
                dtToReturn.Columns.Add("FillRate", typeof(decimal));

                DataTable dtClassSection = GetClassSectionData(xmlNode);
                List<DataRow> drListClassSection = dtClassSection.Select().ToList();

                DataTable dt = dataMapper.GetClassFillRateBySectionId(sectionId);

                foreach (DataRow dr in dt.Rows)
                {
                    DataRow drClassSection = drListClassSection.FirstOrDefault(x => Convert.ToInt64(x["RecordID"]) == Convert.ToInt64(dr["SectionId"]));

                    DataRow drToReturn = dtToReturn.NewRow();

                    if (drClassSection != null)
                    {
                        drToReturn["Month"] = Convert.ToDateTime(drClassSection["StartDate"]).ToString("MMMM yyyy");
                        drToReturn["ClassName"] = drClassSection["Title"];
                        drToReturn["Section"] = Convert.ToString(drClassSection["DayofWeek"]) + " | " + Convert.ToString(drClassSection["StartDate"])/* + " | " + Convert.ToString(drClassSection["MaximumSize"])*/;
                    }
                    //drToReturn["FillRate"] = Convert.ToInt32(dr["FillRate"]);
                    #region fill rate
                    int numOfAttenee = Convert.ToInt32(dr["FillRate"]);
                    drToReturn["FillRate"] = (numOfAttenee * 100) / MaxSize;
                    #endregion

                    dtToReturn.Rows.Add(drToReturn);
                }
            }
            catch (Exception ex)
            {
                dtToReturn = new DataTable();
            }

            return dtToReturn;
        }

        private int CalculateAvailable(long sectionID, int maximumSize, List<ClassInvoiceItem> classInvoiceItemList)
        {
            int availableSeat = maximumSize;

            if (classInvoiceItemList.Count > 0)
            {
                classInvoiceItemList = (from item in classInvoiceItemList
                                        where item.SectionId == sectionID
                                        select item).ToList();

                int totalEnrolled = 0;
                foreach (ClassInvoiceItem item in classInvoiceItemList)
                {
                    totalEnrolled += item.NumberAttending;
                }

                availableSeat = maximumSize - totalEnrolled;
            }

            return availableSeat;
        }

        //private int CalculateMedicalSignup(long invoiceId, List<Invoice> invoiceListMedical)
        //{
        //    int medicalSignups = 0;

        //    invoiceListMedical = invoiceListMedical.Where(x => x.Id == invoiceId).ToList();
        //    medicalSignups = invoiceListMedical.Count();
        //    return medicalSignups;
        //}
        private int CalculateMedicalSignup(long invoiceId)
        {
            int medicalSignups = 0;

            medicalSignups = dataMapper.GetMedicalSignupExistsByInvoiceId(invoiceId) ? 1 : 0;

            return medicalSignups;
        }

        private int CalculateTotalEnrolled(long sectionID, List<ClassInvoiceItem> classInvoiceItemList)
        {
            int totalEnrolled = 0;

            if (classInvoiceItemList.Count > 0)
            {
                classInvoiceItemList = (from item in classInvoiceItemList
                                        where item.SectionId == sectionID
                                        select item).ToList();

                foreach (ClassInvoiceItem item in classInvoiceItemList)
                {
                    totalEnrolled += item.NumberAttending;
                }
            }
            return totalEnrolled;
        }

        public List<RptMemberShip> GetMembershipList(DateTime lastValidDate)
        {
            List<RptMemberShip> rptMemberShipList = new List<RptMemberShip>();
            RptMemberShip rptMemberShip = null;

            DataSet ds = new MembershipInvoiceItemServiceImpl().GetMembershipByLastValidDate(lastValidDate);

            if (ds != null)
            {
                foreach (DataRow row in ds.Tables[0].Rows)
                {
                    rptMemberShip = new RptMemberShip();

                    DateTime validtillDate = Convert.ToDateTime(Convert.ToString(row["DateValidTill"]));
                    rptMemberShip.Month = validtillDate.ToString("MMM") + " " + validtillDate.Year.ToString();
                    rptMemberShip.LastName = Convert.ToString(row["LastName"]);
                    rptMemberShip.FirstName = Convert.ToString(row["FirstName"]);
                    rptMemberShip.LastDateValid = validtillDate.ToString("MM/dd/yyy");

                    rptMemberShipList.Add(rptMemberShip);
                }
            }

            return rptMemberShipList;
        }

        public List<RptClientPayment> GetClientPayment()
        {
            List<RptClientPayment> rptClientPaymentList = new List<RptClientPayment>();
            RptClientPayment rptClientPayment = null;

            List<User> userList = new UserServiceImpl().GetAllUsers();

            if (userList != null)
            {
                foreach (User item in userList)
                {
                    rptClientPayment = new RptClientPayment();


                    rptClientPayment.LastName = item.LastName;
                    rptClientPayment.FirstName = item.FirstName;
                    rptClientPayment.Phone = item.phone;
                    rptClientPayment.AccountBalance = 0;
                    rptClientPayment.description = item.Notes;

                    rptClientPaymentList.Add(rptClientPayment);
                }
            }

            return rptClientPaymentList;
        }

        private DataTable GetClassSectionData(XmlNode xmlNode)
        {

            DataTable dt = new DataTable();
            DataColumn dc1 = new DataColumn("ClassType");
            DataColumn dc2 = new DataColumn("Title");
            DataColumn dc3 = new DataColumn("Fee");
            DataColumn dc4 = new DataColumn("RecordID");
            DataColumn dc5 = new DataColumn("SectionCode");
            DataColumn dc6 = new DataColumn("StartDate");
            DataColumn dc7 = new DataColumn("MaximumSize");
            DataColumn dc8 = new DataColumn("DayofWeek");

            dt.Columns.Add(dc1);
            dt.Columns.Add(dc2);
            dt.Columns.Add(dc3);
            dt.Columns.Add(dc4);
            dt.Columns.Add(dc5);
            dt.Columns.Add(dc6);
            dt.Columns.Add(dc7);
            dt.Columns.Add(dc8);

            if (xmlNode != null)
            {
                XmlNodeList datas = xmlNode.ChildNodes;
                DataRow dr = dt.NewRow();
                for (int i = 0; i < datas.Count; i++)
                {
                    List<XmlNode> nodeList = new List<XmlNode>(datas[i].ChildNodes.Cast<XmlNode>());
                    XmlNode classTypeNode = (from item in nodeList
                                             where item.Name.Replace(" ", "") == "ClassType"
                                             select item).FirstOrDefault();
                    XmlNode titleNode = (from item in nodeList
                                         where item.Name.Replace(" ", "") == "Title"
                                         select item).FirstOrDefault();

                    XmlNode feeNode = (from item in nodeList
                                       where item.Name.Replace(" ", "") == "Fee"
                                       select item).FirstOrDefault();

                    XmlNode recordIDNode = (from item in nodeList
                                            where item.Name.Replace(" ", "") == "RecordID"
                                            select item).FirstOrDefault();

                    XmlNode sectionCodeNode = (from item in nodeList
                                               where item.Name.Replace(" ", "") == "SectionCode"
                                               select item).FirstOrDefault();

                    XmlNode startDateNode = (from item in nodeList
                                             where item.Name.Replace(" ", "") == "StartDate"
                                             select item).FirstOrDefault();

                    XmlNode maximumSizenode = (from item in nodeList
                                               where item.Name.Replace(" ", "") == "MaximumSize"
                                               select item).FirstOrDefault();

                    XmlNode daysOfWeekNode = (from item in nodeList
                                              where item.Name.Replace(" ", "") == "DayofWeek"
                                              select item).FirstOrDefault();

                    dr = dt.NewRow();
                    dr["ClassType"] = classTypeNode.InnerText;
                    dr["Title"] = titleNode.InnerText;
                    dr["Fee"] = feeNode.InnerText;
                    dr["RecordID"] = recordIDNode.InnerText;
                    //dr["SectionCode"] = sectionCodeNode.InnerText;
                    dr["SectionCode"] = "";
                    dr["StartDate"] = startDateNode.InnerText;
                    dr["MaximumSize"] = maximumSizenode.InnerText;
                    dr["DayofWeek"] = (daysOfWeekNode != null ? daysOfWeekNode.InnerText : "");

                    dt.Rows.Add(dr);
                }
            }

            return dt;
        }

        //public DataTable GetAllClasses(XmlNode xmlNode)
        //{
        //    DataTable dt = new DataTable();
        //    DataColumn dc1 = new DataColumn("recordID");
        //    DataColumn dc2 = new DataColumn("Title");
        //    DataColumn dc3 = new DataColumn("Year");
        //    DataColumn dc4 = new DataColumn("Fee");
        //    DataColumn dc5 = new DataColumn("Description");

        //    dt.Columns.Add(dc1);
        //    dt.Columns.Add(dc2);
        //    dt.Columns.Add(dc3);
        //    dt.Columns.Add(dc4);
        //    dt.Columns.Add(dc5);

        //    if (xmlNode != null)
        //    {
        //        XmlNodeList datas = xmlNode.ChildNodes;
        //        for (int i = 0; i < datas.Count; i++)
        //        {
        //            List<XmlNode> nodeList = new List<XmlNode>(datas[i].ChildNodes.Cast<XmlNode>());
        //            XmlNode recordIDNode = (from item in nodeList
        //                                    where item.Name.Replace(" ", "") == "RecordId"
        //                                    select item).FirstOrDefault();
        //            XmlNode titleNode = (from item in nodeList
        //                                 where item.Name.Replace(" ", "") == "Title"
        //                                 select item).FirstOrDefault();

        //            XmlNode yearNode = (from item in nodeList
        //                                where item.Name.Replace(" ", "") == "Year"
        //                                select item).FirstOrDefault();

        //            XmlNode feeNode = (from item in nodeList
        //                               where item.Name.Replace(" ", "") == "Fee"
        //                               select item).FirstOrDefault();


        //            DataRow dr = dt.NewRow();
        //            dr["recordID"] = recordIDNode.InnerText;
        //            dr["Title"] = titleNode.InnerText;
        //            dr["Year"] = yearNode.InnerText;
        //            dr["Fee"] = feeNode.InnerText;

        //            dr["Description"] = Convert.ToString(dr["Title"]) + " | " + Convert.ToString(dr["Fee"]);

        //            dt.Rows.Add(dr);
        //        }
        //    }

        //    //List<DataRow> drl = dt.Select("", "Year desc, Title").ToList();

        //    return dt;
        //}

        public DataTable GetSectionsByClassTitle(XmlNode xmlNode, string selectedClass)
        {
            DataTable dt = new DataTable();
            DataColumn dc1 = new DataColumn("RecordID");
            DataColumn dc2 = new DataColumn("SectionCode");
            DataColumn dc3 = new DataColumn("StartDate");
            DataColumn dc4 = new DataColumn("NumberofMeetings");
            DataColumn dc5 = new DataColumn("StartTime");
            DataColumn dc6 = new DataColumn("MaximumSize");
            DataColumn dc7 = new DataColumn("ClassTitle");
            DataColumn dc8 = new DataColumn("Available");
            DataColumn dc9 = new DataColumn("DayofWeek");
            DataColumn dc10 = new DataColumn("SectionDisplayText");

            dt.Columns.Add(dc1);
            dt.Columns.Add(dc2);
            dt.Columns.Add(dc3);
            dt.Columns.Add(dc4);
            dt.Columns.Add(dc5);
            dt.Columns.Add(dc6);
            dt.Columns.Add(dc7);
            dt.Columns.Add(dc8);
            dt.Columns.Add(dc9);
            dt.Columns.Add(dc10);

            List<ClassInvoiceItem> classInvoiceItemList = new List<ClassInvoiceItem>();
            classInvoiceItemList = new ClassInvoiceItemServiceImpl().GetAll();

            if (xmlNode != null)
            {
                XmlNodeList datas = xmlNode.ChildNodes;
                DataRow dr = dt.NewRow();
                long availableCount = 0;
                for (int i = 0; i < datas.Count; i++)
                {
                    List<XmlNode> nodeList = new List<XmlNode>(datas[i].ChildNodes.Cast<XmlNode>());
                    XmlNode recordIDNode = (from item in nodeList
                                            where item.Name.Replace(" ", "") == "RecordId"
                                            select item).FirstOrDefault();
                    XmlNode sectionCodeNode = (from item in nodeList
                                               where item.Name.Replace(" ", "") == "SectionCode"
                                               select item).FirstOrDefault();

                    XmlNode startDateNode = (from item in nodeList
                                             where item.Name.Replace(" ", "") == "StartDate"
                                             select item).FirstOrDefault();

                    XmlNode numberofMeetingNode = (from item in nodeList
                                                   where item.Name.Replace(" ", "") == "NumberofMeetings"
                                                   select item).FirstOrDefault();

                    XmlNode startTimeNode = (from item in nodeList
                                             where item.Name.Replace(" ", "") == "StartTime"
                                             select item).FirstOrDefault();

                    XmlNode maximumSizeNode = (from item in nodeList
                                               where item.Name.Replace(" ", "") == "MaximumSize"
                                               select item).FirstOrDefault();

                    XmlNode classTitleNode = (from item in nodeList
                                              where item.Name.Replace(" ", "") == "ClassTitle"
                                              select item).FirstOrDefault();

                    XmlNode daysOfWeekNode = (from item in nodeList
                                              where item.Name.Replace(" ", "") == "DayofWeek"
                                              select item).FirstOrDefault();

                    dr = dt.NewRow();
                    dr["RecordID"] = recordIDNode.InnerText;
                    //dr["SectionCode"] = sectionCodeNode.InnerText;
                    dr["SectionCode"] = "";
                    dr["StartDate"] = startDateNode.InnerText;
                    dr["NumberofMeetings"] = numberofMeetingNode.InnerText;
                    dr["StartTime"] = startTimeNode.InnerText;
                    dr["MaximumSize"] = maximumSizeNode.InnerText;
                    dr["ClassTitle"] = classTitleNode.InnerText;
                    dr["DayofWeek"] = (daysOfWeekNode != null ? daysOfWeekNode.InnerText : "");

                    availableCount = CalculateAvailable(Convert.ToInt64(dr["RecordID"]), Convert.ToInt32(dr["MaximumSize"]), classInvoiceItemList);
                    dr["Available"] = availableCount;
                    DateTime date = Convert.ToDateTime(dr["StartDate"]);

                    dr["SectionDisplayText"] = Convert.ToString(dr["DayofWeek"]) + " | " + date.ToString("MM/dd/yyy") + " | " + Convert.ToString(dr["MaximumSize"]);

                    if (classTitleNode.InnerText == selectedClass)
                    {
                        dt.Rows.Add(dr);
                    }
                }
            }

            return dt;
        }
       
        //public List<RptWaitingList> GetWaitingListCollection(XmlNode xmlNode, DateTime fromDate, DateTime toDate)
        //{
        //    List<RptWaitingList> waitingListCollection = new List<RptWaitingList>();

        //    DataTable dt = GetClassSectionData(xmlNode);

        //    //List<ClassInvoiceItem> classInvoiceItemList = new ClassInvoiceItemServiceImpl().GetListByDateRange(fromDate, toDate).FindAll(x => x.IsInWaitingList);
        //    DataTable dtWL = dataMapper.GetTotalWaitingList(fromDate, toDate);

        //    foreach (DataRow drWL in dtWL.Rows)
        //    {
        //        foreach (DataRow row in dt.Rows)
        //        {
        //            if (Convert.ToInt64(drWL["SectionId"]) == Convert.ToInt64(row["RecordID"]))
        //            {
        //                RptWaitingList rptWaitingList = new RptWaitingList();
        //                rptWaitingList.ClassType = Convert.ToString(row["ClassType"]);
        //                rptWaitingList.Title = Convert.ToString(row["Title"]);
        //                rptWaitingList.SectionCode = Convert.ToString(row["DayofWeek"]) + " | " + Convert.ToString(row["StartDate"]);
        //                rptWaitingList.InWaitingList = Convert.ToInt32(drWL["TotalWaiting"]);
        //                waitingListCollection.Add(rptWaitingList);
        //                break;
        //            }
        //        }
        //    }

        //    return waitingListCollection;
        //}
        public List<RptWaitingList> GetWaitingListCollection(XmlNode xmlNode, long sectionId)
        {
            List<RptWaitingList> waitingListCollection = new List<RptWaitingList>();

            DataTable dt = GetClassSectionData(xmlNode);

            //List<ClassInvoiceItem> classInvoiceItemList = new ClassInvoiceItemServiceImpl().GetListByDateRange(fromDate, toDate).FindAll(x => x.IsInWaitingList);
            DataTable dtWL = dataMapper.GetTotalWaitingList(sectionId);

            foreach (DataRow drWL in dtWL.Rows)
            {
                foreach (DataRow row in dt.Rows)
                {
                    if (Convert.ToInt64(drWL["SectionId"]) == Convert.ToInt64(row["RecordID"]))
                    {
                        RptWaitingList rptWaitingList = new RptWaitingList();
                        rptWaitingList.ClassType = Convert.ToString(row["ClassType"]);
                        rptWaitingList.Title = Convert.ToString(row["Title"]);
                        rptWaitingList.SectionCode = Convert.ToString(row["DayofWeek"]) + " | " + Convert.ToString(row["StartDate"]);
                        rptWaitingList.InWaitingList = Convert.ToInt32(drWL["TotalWaiting"]);
                        waitingListCollection.Add(rptWaitingList);
                        break;
                    }
                }
            }

            return waitingListCollection;
        }

        public List<ProductItemWHRC> GetAllProductItems(XmlNode xmlNode)
        {
            List<ProductItemWHRC> allProducts = new ProductServiceImpl().GetAllProductItemWhrc(xmlNode);

            return allProducts;
        }

        public List<ProductItemWHRC> GetProductItemsSold(XmlNode xmlNode, DateTime fromDate, DateTime toDate)
        {
            List<ProductItemWHRC> productsSold = new List<ProductItemWHRC>();

            try
            {
                List<ProductItemWHRC> allProducts = new ProductServiceImpl().GetAllProductItemWhrc(xmlNode);

                DataTable dt = dataMapper.GetTotalSold(fromDate, toDate);

                foreach (DataRow dr in dt.Rows)
                {
                    long productId = Convert.ToInt64(dr["productID"]);
                    int totalSold = Convert.ToInt32(dr["totalsold"]);
                    
                    ProductItemWHRC product = allProducts.Find(x => x.RecordId == productId);

                    ProductItemWHRC productItemWHRC = new ProductItemWHRC();
                    productItemWHRC.ProductCategory = product.ProductCategory;
                    productItemWHRC.ProductType = product.ProductType;
                    productItemWHRC.ItemName = product.ItemName;
                    productItemWHRC.QuantitySold = totalSold;

                    productsSold.Add(productItemWHRC);
                }
            }
            catch (Exception ex)
            {
                productsSold = new List<ProductItemWHRC>();
            }

            return productsSold;
        }

        public List<ProductItemWHRC> GetProductItemsNotSold(XmlNode xmlNode, DateTime fromDate, DateTime toDate)
        {
            List<ProductItemWHRC> productsNotSold = new List<ProductItemWHRC>();

            try
            {
                List<ProductItemWHRC> allProducts = new ProductServiceImpl().GetAllProductItemWhrc(xmlNode);

                DataTable dt = dataMapper.GetTotalSold(fromDate, toDate);
                List<DataRow> drList = dt.Select().ToList();

                foreach (ProductItemWHRC product in allProducts)
                {
                    if (!drList.Exists(x => Convert.ToInt64(x["productID"]) == product.RecordId))
                    {
                        ProductItemWHRC productItemWHRC = new ProductItemWHRC();
                        productItemWHRC.ProductCategory = product.ProductCategory;
                        productItemWHRC.ProductType = product.ProductType;
                        productItemWHRC.ItemName = product.ItemName;
                        productItemWHRC.QuantityInStockInt = Convert.ToInt32(product.QuantityInStock);

                        productsNotSold.Add(productItemWHRC);
                    }
                }
            }
            catch (Exception ex)
            {
                productsNotSold = new List<ProductItemWHRC>();
            }

            return productsNotSold;
        }

        public List<ProductItemWHRC> GetProductItemsNeedReorder(XmlNode xmlNode, DateTime fromDate, DateTime toDate)
        {
            List<ProductItemWHRC> productsNeedReorder = new List<ProductItemWHRC>();

            try
            {
                List<ProductItemWHRC> allProducts = new ProductServiceImpl().GetAllProductItemWhrc(xmlNode);

                DataTable dt = dataMapper.GetTotalSold(fromDate, toDate);
                List<DataRow> drList = dt.Select().ToList();

                foreach (ProductItemWHRC product in allProducts)
                {
                    int reorderLevel = Convert.ToInt32(product.ReorderLevel);
                    int quantityInStock = Convert.ToInt32(product.QuantityInStock);
                    if (quantityInStock <= reorderLevel)
                    {
                        ProductItemWHRC productItemWHRC = new ProductItemWHRC();
                        productItemWHRC.ProductCategory = product.ProductCategory;
                        productItemWHRC.ProductType = product.ProductType;
                        productItemWHRC.ItemName = product.ItemName;
                        productItemWHRC.ReorderLevelInt = reorderLevel;
                        productItemWHRC.QuantityInStockInt = quantityInStock;

                        productsNeedReorder.Add(productItemWHRC);
                    }
                }
            }
            catch (Exception ex)
            {
                productsNeedReorder = new List<ProductItemWHRC>();
            }

            return productsNeedReorder;
        }

        public List<RentalEquipmentWHRC> GetAllRentalEquipments(XmlNode xmlNode)
        {
            List<RentalEquipmentWHRC> rentalEquipmentWHRCs = new List<RentalEquipmentWHRC>();

            try
            {
                rentalEquipmentWHRCs = new RentalServiceImpl().GetAllRentalEquipment(xmlNode);

                DataTable dt = dataMapper.GetRentalEquipments();
                List<DataRow> rentalInvoiceItems = dt.Select().ToList();

                foreach (RentalEquipmentWHRC rentalEquipmentWHRC in rentalEquipmentWHRCs)
                {
                    DataRow dr = rentalInvoiceItems.FirstOrDefault(x => Convert.ToInt64(x["RentalEquipmentID"]) == rentalEquipmentWHRC.RecordID);
                    if (dr != null)
                    {
                        rentalEquipmentWHRC.ConditionWhenReturned = EnumHelper.GetRentalStatus((ReturnStatus)Convert.ToInt32(dr["RentalItemStatus"]));
                        rentalEquipmentWHRC.Client = Convert.ToString(dr["LastName"]);
                        DateTime returnedDate = Convert.ToDateTime(dr["ReturnDate"]);
                        rentalEquipmentWHRC.DateReturnedToVendor = (returnedDate.Year != 1900 ? returnedDate.ToString("MM/dd/yyyy") : "");
                    }
                }
            }
            catch (Exception ex)
            {
                rentalEquipmentWHRCs = new List<RentalEquipmentWHRC>();
            }

            return rentalEquipmentWHRCs;
        }

        public DataTable GetAllRentalOverdueEquipments(XmlNode xmlNode)
        {
            DataTable dtToReturn = new DataTable();
            
            try
            {
                dtToReturn.Columns.Add("LastName");
                dtToReturn.Columns.Add("FirstName");
                dtToReturn.Columns.Add("StartDate");
                dtToReturn.Columns.Add("DueDate");
                dtToReturn.Columns.Add("Serial");
                dtToReturn.Columns.Add("VendorName");
                dtToReturn.Columns.Add("PhoneType");
                dtToReturn.Columns.Add("AreaCode");
                dtToReturn.Columns.Add("Phone");

                List<RentalEquipmentWHRC>  rentalEquipmentWHRCs = new RentalServiceImpl().GetAllRentalEquipment(xmlNode);

                DataTable dt = dataMapper.GetAllRentalOverdueEquipments();

                foreach (DataRow dr in dt.Rows)
                {
                    DataRow drToReturn = dtToReturn.NewRow();

                    drToReturn["LastName"] = dr["LastName"];
                    drToReturn["FirstName"] = dr["FirstName"];
                    drToReturn["StartDate"] = Convert.ToDateTime(dr["RPStartingDate"]).ToString("MM/dd/yyyy");
                    drToReturn["DueDate"] = Convert.ToDateTime(dr["RPEndingDate"]).ToString("MM/dd/yyyy");
                    long rentalEquipmentID = Convert.ToInt64(dr["RentalEquipmentID"]);
                    RentalEquipmentWHRC rentalEquipmentWHR = rentalEquipmentWHRCs.FirstOrDefault(x => x.RecordID == Convert.ToInt64(dr["RentalEquipmentID"]));
                    if (rentalEquipmentWHR != null)
                    {
                        drToReturn["Serial"] = rentalEquipmentWHR.Serial;
                        drToReturn["VendorName"] = rentalEquipmentWHR.VendorName;
                    }
                    drToReturn["PhoneType"] = dr["PhoneType"];
                    drToReturn["AreaCode"] = dr["AreaCode"];
                    drToReturn["Phone"] = dr["Phone"];

                    dtToReturn.Rows.Add(drToReturn);
                }
            }
            catch (Exception ex)
            {
                dtToReturn = new DataTable();
            }

            return dtToReturn;
        }

        public DataTable GetAllClientFeeOwed()
        {
            DataTable dtToReturn = new DataTable();

            try
            {
                dtToReturn.Columns.Add("LastName");
                dtToReturn.Columns.Add("FirstName");
                dtToReturn.Columns.Add("AreaCode");
                dtToReturn.Columns.Add("Phone");
                //dtToReturn.Columns.Add("AccountBalance", Type.GetType("System.Decimal"));
                dtToReturn.Columns.Add("Date");
                dtToReturn.Columns.Add("AmountOwed");

                #region AmountOwed
                //List<Invoice> invoiceobjList = new InvoiceServiceImpl().GetAll();
                //List<ClassInvoiceItem> classInvoiceItemList1 = new List<ClassInvoiceItem>();
                //List<ClassInvoiceItem> classInvoiceItemList2 = new List<ClassInvoiceItem>();
                //bool OrderStatusTrack = false;
                //long classItemId =0;
                //foreach (Invoice item in invoiceobjList)
                //{
                //    classInvoiceItemList1 = new ClassInvoiceItemServiceImpl().GetListByInvoiceId(item.Id);
                //    foreach (ClassInvoiceItem classItem in classInvoiceItemList1)
                //    {
                //        classInvoiceItemList2.Add(classItem);
                //    }
                //}
                //foreach (ClassInvoiceItem classItem in classInvoiceItemList2)
                //{
                //    if (classItem.ClassInvoiceItemState == ClassInvoiceItemState.RequireReprocessing)
                //    {
                //        classItemId = classItem.Id;
                //        OrderStatusTrack = true;
                //    }
                //}
                #endregion

                // DataTable dt = dataMapper.GetClientFeeOwedDataTable(classItemId, OrderStatusTrack);
                //DataTable dt = dataMapper.GetClientFeeOwedDataTable(classItemId);
                DataTable dt = dataMapper.GetClientFeeOwedDataTable();
                foreach (DataRow dr in dt.Rows)
                {
                    DataRow drToReturn = dtToReturn.NewRow();

                    drToReturn["LastName"] = Convert.ToString(dr["LastName"]);
                    drToReturn["FirstName"] = Convert.ToString(dr["FirstName"]);
                    drToReturn["AreaCode"] = Convert.ToString(dr["AreaCode"]);
                    drToReturn["Phone"] = Convert.ToString(dr["Phone"]);
                    DateTime date = Convert.ToDateTime(dr["Date"]);
                    drToReturn["Date"] = date.ToString("MM/dd/yyy");
                    drToReturn["AmountOwed"] = Convert.ToString(dr["AmountOwed"]);
                    dtToReturn.Rows.Add(drToReturn);
                }
            }
            catch (Exception ex)
            {
                dtToReturn = new DataTable();
            }

            return dtToReturn;
        }

        public DataTable GetRevenueTaxReceived(DateTime fromDate, DateTime toDate)
        {
            DataTable dtToReturn = new DataTable();

            try
            {
                fromDate = new DateTime(fromDate.Year, 1, 1);
                toDate = new DateTime(toDate.Year, 12, 31);

                dtToReturn.Columns.Add("Year");
                dtToReturn.Columns.Add("Month");
                dtToReturn.Columns.Add("SalesTax", Type.GetType("System.Decimal"));

                DataTable dtRental = dataMapper.GetRevenueTaxReceivedForRental(fromDate, toDate);
                List<DataRow> drCollectionRental = dtRental.Select().ToList();

                DataTable dtProduct = dataMapper.GetRevenueTaxReceivedForProduct(fromDate, toDate);
                List<DataRow> drCollectionProduct = dtProduct.Select().ToList();

                for (int y = fromDate.Year; y <= toDate.Year; y++)
                {
                    for (int i = 1; i <= 12; i++)
                    {
                        List<DataRow> drRentals = drCollectionRental.FindAll(x => Convert.ToDateTime(x["PaymentDate"]).Year == y && Convert.ToDateTime(x["PaymentDate"]).Month == i);
                        decimal sum_i_rental = 0;
                        if (drRentals != null)
                        {
                            sum_i_rental = drRentals.Sum(x => Convert.ToDecimal(x["RPSalesTax"]));
                        }

                        List<DataRow> drProducts = drCollectionProduct.FindAll(x => Convert.ToDateTime(x["PaymentDate"]).Year == y && Convert.ToDateTime(x["PaymentDate"]).Month == i);
                        decimal sum_i_product = 0;
                        if (drRentals != null)
                        {
                            sum_i_product = drProducts.Sum(x => Convert.ToDecimal(x["TaxAmount"]));
                        }

                        DataRow drToReturn = dtToReturn.NewRow();
                        drToReturn["Year"] = Convert.ToString(y);
                        drToReturn["Month"] = new DateTime(1900, i, 1).ToString("MMMM");
                        if (sum_i_rental + sum_i_product != 0)
                        {
                            drToReturn["SalesTax"] = sum_i_rental + sum_i_product;
                        }
                        dtToReturn.Rows.Add(drToReturn);
                    }
                }
            }
            catch (Exception ex)
            {
                dtToReturn = new DataTable();
            }

            return dtToReturn;
        }

        public DataTable GetRevenueDeposit(DateTime fromDate, DateTime toDate)
        {
            DataTable dtToReturn = new DataTable();

            try
            {
                fromDate = new DateTime(fromDate.Year, 1, 1);
                toDate = new DateTime(toDate.Year, 12, 31);

                dtToReturn.Columns.Add("Year");
                dtToReturn.Columns.Add("Month");
                dtToReturn.Columns.Add("Deposit", Type.GetType("System.Decimal"));

                DataTable dtRental = dataMapper.GetRevenueDeposit(fromDate, toDate);
                List<DataRow> drCollectionRental = dtRental.Select().ToList();

                for (int y = fromDate.Year; y <= toDate.Year; y++)
                {
                    for (int i = 1; i <= 12; i++)
                    {
                        List<DataRow> drRentals = drCollectionRental.FindAll(x => Convert.ToDateTime(x["PaymentDate"]).Year == y && Convert.ToDateTime(x["PaymentDate"]).Month == i);
                        decimal sum_i_rental = 0;
                        if (drRentals != null)
                        {
                            sum_i_rental = drRentals.Sum(x => Convert.ToDecimal(x["DepositAmount"]));
                        }

                        DataRow drToReturn = dtToReturn.NewRow();
                        drToReturn["Year"] = Convert.ToString(y);
                        drToReturn["Month"] = new DateTime(1900, i, 1).ToString("MMMM");
                        if (sum_i_rental != 0)
                        {
                            drToReturn["Deposit"] = sum_i_rental;
                        }
                        dtToReturn.Rows.Add(drToReturn);
                    }
                }
            }
            catch (Exception ex)
            {
                dtToReturn = new DataTable();
            }

            return dtToReturn;
        }

        public DataTable GetRevenueBreakdownSummary(DateTime fromDate, DateTime toDate)
        {
            DataTable dtToReturn = new DataTable();

            try
            {
                fromDate = new DateTime(fromDate.Year, 1, 1);
                toDate = new DateTime(toDate.Year, 12, 31);

                dtToReturn.Columns.Add("Year");
                dtToReturn.Columns.Add("Month");
                dtToReturn.Columns.Add("Class", Type.GetType("System.Decimal"));
                dtToReturn.Columns.Add("Membership", Type.GetType("System.Decimal"));
                dtToReturn.Columns.Add("Purchase", Type.GetType("System.Decimal"));
                dtToReturn.Columns.Add("Deposit", Type.GetType("System.Decimal"));
                dtToReturn.Columns.Add("RentalFee", Type.GetType("System.Decimal"));

                DataTable dtClass = dataMapper.GetRevenueBreakdownSummaryClass(fromDate, toDate);
                List<DataRow> drCollectionClass = dtClass.Select().ToList();

                DataTable dtMembership = dataMapper.GetRevenueBreakdownSummaryMembership(fromDate, toDate);
                List<DataRow> drCollectionMembership = dtMembership.Select().ToList();

                DataTable dtPurchase = dataMapper.GetRevenueBreakdownSummaryProduct(fromDate, toDate);
                List<DataRow> drCollectionPurchase = dtPurchase.Select().ToList();

                DataTable dtRental = dataMapper.GetRevenueBreakdownSummaryRental(fromDate, toDate);
                List<DataRow> drCollectionRental = dtRental.Select().ToList();

                for (int y = fromDate.Year; y <= toDate.Year; y++)
                {
                    for (int i = 1; i <= 12; i++)
                    {
                        List<DataRow> drListClass = drCollectionClass.FindAll(x => Convert.ToDateTime(x["PaymentDate"]).Year == y && Convert.ToDateTime(x["PaymentDate"]).Month == i);
                        decimal sum_i_class = 0;
                        if (drListClass != null)
                        {
                            sum_i_class = drListClass.Sum(x => Convert.ToDecimal(x["Class"]));
                        }

                        List<DataRow> drListMembership = drCollectionMembership.FindAll(x => Convert.ToDateTime(x["PaymentDate"]).Year == y && Convert.ToDateTime(x["PaymentDate"]).Month == i);
                        decimal sum_i_membership = 0;
                        if (drListMembership != null)
                        {
                            sum_i_membership = drListMembership.Sum(x => Convert.ToDecimal(x["Membership"]));
                        }

                        List<DataRow> drListPurchase = drCollectionPurchase.FindAll(x => Convert.ToDateTime(x["PaymentDate"]).Year == y && Convert.ToDateTime(x["PaymentDate"]).Month == i);
                        decimal sum_i_purchase = 0;
                        if (drListPurchase != null)
                        {
                            sum_i_purchase = drListPurchase.Sum(x => Convert.ToDecimal(x["Purchase"]));
                        }

                        List<DataRow> drListRental = drCollectionRental.FindAll(x => Convert.ToDateTime(x["PaymentDate"]).Year == y && Convert.ToDateTime(x["PaymentDate"]).Month == i);
                        decimal sum_i_rental_deposit = 0;
                        decimal sum_i_rental_fee = 0;
                        if (drListRental != null)
                        {
                            sum_i_rental_deposit = drListRental.Sum(x => Convert.ToDecimal(x["Deposit"]));
                            sum_i_rental_fee = drListRental.Sum(x => Convert.ToDecimal(x["RentalFee"]));
                        }

                        DataRow drToReturn = dtToReturn.NewRow();
                        drToReturn["Year"] = Convert.ToString(y);
                        drToReturn["Month"] = new DateTime(1900, i, 1).ToString("MMMM");
                        if (sum_i_class != 0)
                        {
                            drToReturn["Class"] = sum_i_class;
                        }
                        if (sum_i_membership != 0)
                        {
                            drToReturn["Membership"] = sum_i_membership;
                        }
                        if (sum_i_purchase != 0)
                        {
                            drToReturn["Purchase"] = sum_i_purchase;
                        }
                        if (sum_i_rental_deposit != 0)
                        {
                            drToReturn["Deposit"] = sum_i_rental_deposit;
                        }
                        if (sum_i_rental_fee != 0)
                        {
                            drToReturn["RentalFee"] = sum_i_rental_fee;
                        }
                        dtToReturn.Rows.Add(drToReturn);
                    }
                }
            }
            catch (Exception ex)
            {
                dtToReturn = new DataTable();
            }

            return dtToReturn;
        }

        public DataTable GetRevenueBreakdownClassDataTable(XmlNode xmlNode, DateTime fromDate, DateTime toDate)
        {
            DataTable dtToReturn = new DataTable();

            try
            {
                fromDate = new DateTime(fromDate.Year, fromDate.Month, 1);
                toDate = new DateTime(toDate.Year, toDate.Month + 1, 1).AddDays(-1);

                dtToReturn.Columns.Add("Date");
                dtToReturn.Columns.Add("ClassTitle");
                dtToReturn.Columns.Add("ClassTotal", Type.GetType("System.Decimal"));

                DataTable dtFromXml = GetClassSectionData(xmlNode);

                DataTable dt = dataMapper.GetRevenueBreakdownClassDataTable(fromDate, toDate);

                var drCollection = dtFromXml.Select().ToList();
                foreach (DataRow dr in dt.Rows)
                {
                    DataRow dr1 = drCollection.FirstOrDefault(x => Convert.ToInt64(x["RecordID"]) == Convert.ToInt64(dr["SectionId"]));

                    DataRow drToReturn = dtToReturn.NewRow();

                    drToReturn["Date"] = Convert.ToDateTime(dr["PaymentDate"]).ToString("MMMM yyyy");
                    drToReturn["ClassTitle"] = dr1["Title"];
                    drToReturn["ClassTotal"] = Convert.ToDecimal(dr["Price"]);

                    dtToReturn.Rows.Add(drToReturn);
                }
            }
            catch (Exception ex)
            {
                dtToReturn = new DataTable();
            }

            return dtToReturn;
        }

        public DataTable GetRevenueBreakdownProductItemDataTable(XmlNode xmlNode)
        {
            DataTable dtToReturn = new DataTable();

            try
            {
                dtToReturn.Columns.Add("Month");
                dtToReturn.Columns.Add("ProductType");
                dtToReturn.Columns.Add("MonthlyTotal", Type.GetType("System.Decimal"));

                List<ProductItemWHRC> allProducts = new ProductServiceImpl().GetAllProductItemWhrc(xmlNode);

                DataTable dt = dataMapper.GetRevenueBreakdownProductItemDataTable();

                foreach (DataRow dr in dt.Rows)
                {
                    DataRow drToReturn = dtToReturn.NewRow();

                    ProductItemWHRC product = allProducts.Find(x => x.RecordId == Convert.ToInt64(dr["ProductId"]));
                    drToReturn["ProductType"] = product.ProductType;
                    drToReturn["Month"] = Convert.ToDateTime(dr["PaymentDate"]).ToString("MMMM yyyy");
                    drToReturn["MonthlyTotal"] = Convert.ToDecimal(dr["Price"]);

                    dtToReturn.Rows.Add(drToReturn);
                }
            }
            catch (Exception ex)
            {
                dtToReturn = new DataTable();
            }

            return dtToReturn;
        }

        public DataTable GetRevenueBreakdownClientCredit(DateTime fromDate, DateTime toDate)
        {
            DataTable dtToReturn = new DataTable();

            try
            {
                fromDate = new DateTime(fromDate.Year, 1, 1);
                toDate = new DateTime(toDate.Year, 12, 31);

                dtToReturn.Columns.Add("Month");
                dtToReturn.Columns.Add("CancellationOrReturn", Type.GetType("System.Decimal"));
                dtToReturn.Columns.Add("DepositReturn", Type.GetType("System.Decimal"));
                dtToReturn.Columns.Add("Discount", Type.GetType("System.Decimal"));
                dtToReturn.Columns.Add("Package", Type.GetType("System.Decimal"));
                dtToReturn.Columns.Add("Payment", Type.GetType("System.Decimal"));

                DataTable dtClassCC = dataMapper.GetRevenueBreakdownClientCreditClass(fromDate, toDate);
                List<DataRow> drCollectionClassCC = dtClassCC.Select().ToList();

                DataTable dtPurchaseCC = dataMapper.GetRevenueBreakdownClientCreditProduct(fromDate, toDate);
                List<DataRow> drCollectionPurchaseCC = dtPurchaseCC.Select().ToList();

                DataTable dtRentalCC = dataMapper.GetRevenueBreakdownClientCreditRental(fromDate, toDate);
                List<DataRow> drCollectionRentalCC = dtRentalCC.Select().ToList();

                DataTable dtDiscountCC = dataMapper.GetRevenueBreakdownClientCreditDiscount(fromDate, toDate);
                List<DataRow> drCollectionDiscountCC = dtDiscountCC.Select().ToList();

                DataTable dtPackageDiscountCC = dataMapper.GetRevenueBreakdownClientCreditPackageDiscount(fromDate, toDate);
                List<DataRow> drCollectionPackageDiscountCC = dtPackageDiscountCC.Select().ToList();

                //+ Payment
                DataTable dtClass = dataMapper.GetRevenueBreakdownSummaryClass(fromDate, toDate);
                List<DataRow> drCollectionClass = dtClass.Select().ToList();

                DataTable dtMembership = dataMapper.GetRevenueBreakdownSummaryMembership(fromDate, toDate);
                List<DataRow> drCollectionMembership = dtMembership.Select().ToList();

                DataTable dtPurchase = dataMapper.GetRevenueBreakdownSummaryProduct(fromDate, toDate);
                List<DataRow> drCollectionPurchase = dtPurchase.Select().ToList();

                DataTable dtRental = dataMapper.GetRevenueBreakdownSummaryRental(fromDate, toDate);
                List<DataRow> drCollectionRental = dtRental.Select().ToList();
                //- Payment

                for (int y = fromDate.Year; y <= toDate.Year; y++)
                {
                    for (int i = 1; i <= 12; i++)
                    {
                        List<DataRow> drListClassCC = drCollectionClassCC.FindAll(x => Convert.ToDateTime(x["PaymentDate"]).Year == y && Convert.ToDateTime(x["PaymentDate"]).Month == i);
                        decimal sum_i_classCC = 0;
                        if (drListClassCC != null)
                        {
                            sum_i_classCC = drListClassCC.Sum(x => Convert.ToDecimal(x["Class"]));
                        }

                        List<DataRow> drListPurchaseCC = drCollectionPurchaseCC.FindAll(x => Convert.ToDateTime(x["PaymentDate"]).Year == y && Convert.ToDateTime(x["PaymentDate"]).Month == i);
                        decimal sum_i_purchaseCC = 0;
                        if (drListPurchaseCC != null)
                        {
                            sum_i_purchaseCC = drListPurchaseCC.Sum(x => Convert.ToDecimal(x["Purchase"]));
                        }

                        List<DataRow> drListRentalCC = drCollectionRentalCC.FindAll(x => Convert.ToDateTime(x["PaymentDate"]).Year == y && Convert.ToDateTime(x["PaymentDate"]).Month == i);
                        decimal sum_i_rental_depositCC = 0;
                        if (drListRentalCC != null)
                        {
                            sum_i_rental_depositCC = drListRentalCC.Sum(x => Convert.ToDecimal(x["Deposit"]));
                        }

                        List<DataRow> drListDiscountCC = drCollectionDiscountCC.FindAll(x => Convert.ToDateTime(x["PaymentDate"]).Year == y && Convert.ToDateTime(x["PaymentDate"]).Month == i);
                        decimal sum_i_discountCC = 0;
                        if (drListDiscountCC != null)
                        {
                            sum_i_discountCC = drListDiscountCC.Sum(x => Convert.ToDecimal(x["Discount"]));
                        }

                        List<DataRow> drListPackageDiscountCC = drCollectionPackageDiscountCC.FindAll(x => Convert.ToDateTime(x["PaymentDate"]).Year == y && Convert.ToDateTime(x["PaymentDate"]).Month == i);
                        decimal sum_i_packagediscountCC = 0;
                        if (drListPackageDiscountCC != null)
                        {
                            sum_i_packagediscountCC = drListPackageDiscountCC.Sum(x => Convert.ToDecimal(x["PackageDiscount"]));
                        }

                        //+ Payment
                        List<DataRow> drListClass = drCollectionClass.FindAll(x => Convert.ToDateTime(x["PaymentDate"]).Year == y && Convert.ToDateTime(x["PaymentDate"]).Month == i);
                        decimal sum_i_class = 0;
                        if (drListClass != null)
                        {
                            sum_i_class = drListClass.Sum(x => Convert.ToDecimal(x["Class"]));
                        }

                        List<DataRow> drListMembership = drCollectionMembership.FindAll(x => Convert.ToDateTime(x["PaymentDate"]).Year == y && Convert.ToDateTime(x["PaymentDate"]).Month == i);
                        decimal sum_i_membership = 0;
                        if (drListMembership != null)
                        {
                            sum_i_membership = drListMembership.Sum(x => Convert.ToDecimal(x["Membership"]));
                        }

                        List<DataRow> drListPurchase = drCollectionPurchase.FindAll(x => Convert.ToDateTime(x["PaymentDate"]).Year == y && Convert.ToDateTime(x["PaymentDate"]).Month == i);
                        decimal sum_i_purchase = 0;
                        if (drListPurchase != null)
                        {
                            sum_i_purchase = drListPurchase.Sum(x => Convert.ToDecimal(x["Purchase"]));
                        }

                        List<DataRow> drListRental = drCollectionRental.FindAll(x => Convert.ToDateTime(x["PaymentDate"]).Year == y && Convert.ToDateTime(x["PaymentDate"]).Month == i);
                        decimal sum_i_rental_deposit = 0;
                        decimal sum_i_rental_fee = 0;
                        if (drListRental != null)
                        {
                            sum_i_rental_deposit = drListRental.Sum(x => Convert.ToDecimal(x["Deposit"]));
                            sum_i_rental_fee = drListRental.Sum(x => Convert.ToDecimal(x["RentalFee"]));
                        }
                        //- Payment


                        DataRow drToReturn = dtToReturn.NewRow();
                        drToReturn["Month"] = new DateTime(1900, i, 1).ToString("MMMM") + " " + Convert.ToString(y);
                        if (sum_i_classCC + sum_i_purchaseCC != 0)
                        {
                            drToReturn["CancellationOrReturn"] = sum_i_classCC + sum_i_purchaseCC;
                        }
                        if (sum_i_rental_depositCC != 0)
                        {
                            drToReturn["DepositReturn"] = sum_i_rental_depositCC;
                        }
                        if (sum_i_discountCC != 0)
                        {
                            drToReturn["Discount"] = sum_i_discountCC;
                        }
                        if (sum_i_packagediscountCC != 0)
                        {
                            drToReturn["Package"] = sum_i_packagediscountCC;
                        }
                        if (sum_i_class + sum_i_membership + sum_i_purchase + sum_i_rental_deposit + sum_i_rental_fee != 0)
                        {
                            drToReturn["Payment"] = sum_i_class + sum_i_membership + sum_i_purchase + sum_i_rental_deposit + sum_i_rental_fee;
                        }
                        dtToReturn.Rows.Add(drToReturn);
                    }
                }
            }
            catch (Exception ex)
            {
                dtToReturn = new DataTable();
            }

            return dtToReturn;
        }

        public DataTable GetRevenueMOMMobileSummary(DateTime fromDate, DateTime toDate)
        {
            DataTable dtToReturn = new DataTable();

            try
            {
                fromDate = new DateTime(fromDate.Year, 1, 1);
                toDate = new DateTime(toDate.Year, 12, 31);

                dtToReturn.Columns.Add("Year");
                dtToReturn.Columns.Add("Month");
                dtToReturn.Columns.Add("Class", Type.GetType("System.Decimal"));
                dtToReturn.Columns.Add("Membership", Type.GetType("System.Decimal"));
                dtToReturn.Columns.Add("Purchase", Type.GetType("System.Decimal"));
                dtToReturn.Columns.Add("Deposit", Type.GetType("System.Decimal"));
                dtToReturn.Columns.Add("RentalFee", Type.GetType("System.Decimal"));

                DataTable dtClass = dataMapper.GetRevenueMOMMobileSummaryClass(fromDate, toDate);
                List<DataRow> drCollectionClass = dtClass.Select().ToList();

                DataTable dtMembership = dataMapper.GetRevenueMOMMobileSummaryMembership(fromDate, toDate);
                List<DataRow> drCollectionMembership = dtMembership.Select().ToList();

                DataTable dtPurchase = dataMapper.GetRevenueMOMMobileSummaryProduct(fromDate, toDate);
                List<DataRow> drCollectionPurchase = dtPurchase.Select().ToList();

                DataTable dtRental = dataMapper.GetRevenueMOMMobileSummaryRental(fromDate, toDate);
                List<DataRow> drCollectionRental = dtRental.Select().ToList();

                for (int y = fromDate.Year; y <= toDate.Year; y++)
                {
                    for (int i = 1; i <= 12; i++)
                    {
                        List<DataRow> drListClass = drCollectionClass.FindAll(x => Convert.ToDateTime(x["PaymentDate"]).Year == y && Convert.ToDateTime(x["PaymentDate"]).Month == i);
                        decimal sum_i_class = 0;
                        if (drListClass != null)
                        {
                            sum_i_class = drListClass.Sum(x => Convert.ToDecimal(x["Class"]));
                        }

                        List<DataRow> drListMembership = drCollectionMembership.FindAll(x => Convert.ToDateTime(x["PaymentDate"]).Year == y && Convert.ToDateTime(x["PaymentDate"]).Month == i);
                        decimal sum_i_membership = 0;
                        if (drListMembership != null)
                        {
                            sum_i_membership = drListMembership.Sum(x => Convert.ToDecimal(x["Membership"]));
                        }

                        List<DataRow> drListPurchase = drCollectionPurchase.FindAll(x => Convert.ToDateTime(x["PaymentDate"]).Year == y && Convert.ToDateTime(x["PaymentDate"]).Month == i);
                        decimal sum_i_purchase = 0;
                        if (drListPurchase != null)
                        {
                            sum_i_purchase = drListPurchase.Sum(x => Convert.ToDecimal(x["Purchase"]));
                        }

                        List<DataRow> drListRental = drCollectionRental.FindAll(x => Convert.ToDateTime(x["PaymentDate"]).Year == y && Convert.ToDateTime(x["PaymentDate"]).Month == i);
                        decimal sum_i_rental_deposit = 0;
                        decimal sum_i_rental_fee = 0;
                        if (drListRental != null)
                        {
                            sum_i_rental_deposit = drListRental.Sum(x => Convert.ToDecimal(x["Deposit"]));
                            sum_i_rental_fee = drListRental.Sum(x => Convert.ToDecimal(x["RentalFee"]));
                        }

                        DataRow drToReturn = dtToReturn.NewRow();
                        drToReturn["Year"] = Convert.ToString(y);
                        drToReturn["Month"] = new DateTime(1900, i, 1).ToString("MMMM");
                        if (sum_i_class != 0)
                        {
                            drToReturn["Class"] = sum_i_class;
                        }
                        if (sum_i_membership != 0)
                        {
                            drToReturn["Membership"] = sum_i_membership;
                        }
                        if (sum_i_purchase != 0)
                        {
                            drToReturn["Purchase"] = sum_i_purchase;
                        }
                        if (sum_i_rental_deposit != 0)
                        {
                            drToReturn["Deposit"] = sum_i_rental_deposit;
                        }
                        if (sum_i_rental_fee != 0)
                        {
                            drToReturn["RentalFee"] = sum_i_rental_fee;
                        }
                        dtToReturn.Rows.Add(drToReturn);
                    }
                }
            }
            catch (Exception ex)
            {
                dtToReturn = new DataTable();
            }

            return dtToReturn;
        }

        public DataTable GetRevenueMOMMobileProductItemDataTable(XmlNode xmlNode)
        {
            DataTable dtToReturn = new DataTable();

            try
            {
                dtToReturn.Columns.Add("Month");
                dtToReturn.Columns.Add("ProductType");
                dtToReturn.Columns.Add("MonthlyTotal", Type.GetType("System.Decimal"));

                List<ProductItemWHRC> allProducts = new ProductServiceImpl().GetAllProductItemWhrc(xmlNode);

                DataTable dt = dataMapper.GetRevenueMOMMobileProductItemDataTable();

                foreach (DataRow dr in dt.Rows)
                {
                    DataRow drToReturn = dtToReturn.NewRow();

                    ProductItemWHRC product = allProducts.Find(x => x.RecordId == Convert.ToInt64(dr["ProductId"]));
                    drToReturn["ProductType"] = product.ProductType;
                    drToReturn["Month"] = Convert.ToDateTime(dr["PaymentDate"]).ToString("MMMM yyyy");
                    drToReturn["MonthlyTotal"] = Convert.ToDecimal(dr["Price"]);

                    dtToReturn.Rows.Add(drToReturn);
                }
            }
            catch (Exception ex)
            {
                dtToReturn = new DataTable();
            }

            return dtToReturn;
        }

        public DataTable GetRevenueMOMMobileRentalDataTable(XmlNode xmlNode, DateTime fromDate, DateTime toDate)
        {
            DataTable dtToReturn = new DataTable();

            try
            {
                fromDate = new DateTime(fromDate.Year, fromDate.Month, 1);
                toDate = new DateTime(toDate.Year, toDate.Month + 1, 1).AddDays(-1);

                dtToReturn.Columns.Add("Date");
                dtToReturn.Columns.Add("ModelName");
                dtToReturn.Columns.Add("ModelTotal", Type.GetType("System.Decimal"));

                List<RentalEquipmentWHRC> rentalEquipmentWHRCs = new RentalServiceImpl().GetAllRentalEquipment(xmlNode);

                DataTable dt = dataMapper.GetRevenueMOMMobileRentalDataTable(fromDate, toDate);

                foreach (DataRow dr in dt.Rows)
                {
                    DataRow drToReturn = dtToReturn.NewRow();

                    drToReturn["Date"] = Convert.ToDateTime(dr["PaymentDate"]).ToString("MMMM yyyy");

                    RentalEquipmentWHRC rentalEquipmentWHRC = rentalEquipmentWHRCs.FirstOrDefault(x => x.RecordID == Convert.ToInt64(dr["RentalEquipmentID"]));
                    if (rentalEquipmentWHRC != null)
                    {
                        drToReturn["ModelName"] = rentalEquipmentWHRC.ModelName;
                    }

                    drToReturn["ModelTotal"] = Convert.ToDecimal(dr["Price"]);

                    dtToReturn.Rows.Add(drToReturn);
                }
            }
            catch (Exception ex)
            {
                dtToReturn = new DataTable();
            }

            return dtToReturn;
        }
    }
}
