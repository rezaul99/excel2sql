﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using daoProject.Model;
using System.Data;
using daoProject.Mapper;


namespace daoProject.Service
{
    public class SystemDataBaseServiceImpl 
    {
        private SystemDataBaseDataMapper dataMapper;

        public SystemDataBaseServiceImpl()
        {
            dataMapper = new SystemDataBaseDataMapper();
        }

        public DataTable GenerateTableValues(List<ReportAttributes> columnList, string tableName, string databaseName)
        {
            return dataMapper.GenerateTableValues(columnList, tableName, databaseName);
        }

        public List<ReportClassObjects> GetTableListByDataBaseName(string databaseName)
        {
            return dataMapper.GetTableListByDataBaseName(databaseName);
        }

        public List<ReportAttributes> GetColumnListByTableName(string databaseName, string TableName)
        {
            return dataMapper.GetColumnListByTableName(databaseName, TableName);
        }
    }
}
