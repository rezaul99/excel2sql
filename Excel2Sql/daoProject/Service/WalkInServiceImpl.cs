﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using daoProject.Model;
using daoProject.Utilities;
using daoProject.Mapper;
using System.Data;
using System.Xml;
using daoProject.Infrastructure;

namespace daoProject.Service
{
    public class WalkInServiceImpl
    {
        public List<WalkIn> GetAllUsers()
        {
            WalkInDataMapper walkInUserDataMapper = new WalkInDataMapper();
            List<WalkIn> userList = walkInUserDataMapper.GetAll();
            return userList;
        }
        public void InsertUser(WalkIn walkInUserObj)
        {
            WalkInDataMapper walkInUserDataMapper = new WalkInDataMapper();
            walkInUserDataMapper.Create(walkInUserObj);
        }

        public WalkIn GetUserById(long userId)
        {
            return new WalkInDataMapper().GetUserById(userId);
        }  

        public List<WalkIn> GetUserListBySearchInput(string searchInput)
        {
            WalkInDataMapper walkInDataMapper = new WalkInDataMapper();
            return walkInDataMapper.SearchUserByInput(searchInput);
        }
    }
}
