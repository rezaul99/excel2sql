﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using daoProject.Model;
using System.Globalization;
using System.Security.Cryptography;

namespace daoProject.Service
{
    public class PaymentGatewaySystemServiceImpl
    {
        public void DoPayment(Invoice invoice)
        {
            PaypalIntegration paypalIntegration = new PaypalIntegration();
            Uri gatewayUrl = new Uri(paypalIntegration.AmazonPaymentUrl);
            decimal amount = new InvoiceServiceImpl().GetPaidAmount(invoice);
            PaymentGatewayPost post = new PaymentGatewayPost();
            post.FormName = "SimplePay";
            post.Url = gatewayUrl.ToString();
            post.Method = "POST";
            post.Add("immediateReturn", "1");
            post.Add(AmazonPaymentsHelper.SIGNATURE_VERSION_KEYNAME, AmazonPaymentsHelper.SIGNATURE_VERSION_2);
            post.Add(AmazonPaymentsHelper.SIGNATURE_METHOD_KEYNAME, AmazonPaymentsHelper.HMAC_SHA256_ALGORITHM);
            post.Add("accessKey", paypalIntegration.creditCardPaymentSetupForAmazon.MarchentLoginID);
            post.Add("amount", String.Format(CultureInfo.InvariantCulture, "USD {0:0.00}", amount));
            post.Add("description", string.Format("{0} - {1}", "Order some items from WHRC" , "INV-" + invoice.Id.ToString().PadLeft(8, '0')));
           // post.Add("description", GetOrderDetails(invoice));
            //    /*
            //    * Your Amazon Payments account ID. This parameter is not used
            //    * and should not be present if you sign the button using your secret key.
            //    * For more information, see Using Access Identifiers on Amazon User Guide Website).
            //    * */
           // post.Add("amazonPaymentsAccountId", paypalIntegration.AccessKeyID);

            post.Add("returnUrl", paypalIntegration.returnURL); //where to return after finishing with transaction

            //    //Amazon can send additional posts to this url in our website
            //    //to notify us about particular transaction state
            //    //we should handle it and update our records in database accordingly
                post.Add("ipnUrl", paypalIntegration.instantPaymentNotificationURL);

                post.Add("processImmediate", "1"); //0 will reserve the payment, 1 will process it immediatelly
               // post.Add("referenceId", "INV-" + invoice.Id.ToString().PadLeft(8, '0'));
                post.Add("referenceId",invoice.Id.ToString());
                post.Add("abandonUrl", paypalIntegration.returnURL); //same as return url
                string signature = GetParametersSignature(post, gatewayUrl); //BuildSignature(paypalIntegration.SecretKey, post.ListParams);
                post.Add(AmazonPaymentsHelper.SIGNATURE_KEYNAME, signature);

                //make the post
                post.Post();
            }

        public string GetOrderDetails(Invoice invoice)
        {
            bool fromEmail = false;
            List<CartItem> cartList = new InvoiceServiceImpl().GetCartOrderItems(invoice, fromEmail);
            List<OrderPaymentRefundHelper> orderPaymentRefundHelperList = new InvoiceServiceImpl().GetPaymentDetails(invoice);
            StringBuilder orderDetails = new StringBuilder();

            foreach (CartItem cartItem in cartList)
            {
                string orders = cartItem.InvoiceItemTypeDescription.ToString() + ", " +
                               cartItem.PriceString +". ";
                orderDetails.Append(orders);
            }
            string paymentDetails = "";
            foreach (var item in orderPaymentRefundHelperList)
            {
                paymentDetails += ", " + item.PaymentDetails.Replace("<br/>", ", ");
            }

            foreach (var item in orderPaymentRefundHelperList)
            {
                paymentDetails += ", " + item.RefundDetails.Replace("<br/>", ", ");
            }
           // orderDetails.Append(paymentDetails);
            return orderDetails.ToString();
        }
        public static string GetParametersSignature(PaymentGatewayPost post, Uri gatewayUrl)
        {
            PaypalIntegration paypalIntegration = new PaypalIntegration();
            IDictionary<string,string> param=post.ListParams.AllKeys.ToDictionary(k => k, v => post.ListParams[v]);
            return AmazonPaymentsHelper.SignParameters
            (
            param,// post.ListParams.AllKeys.ToDictionary(k => k, v => parameters[v]),
            paypalIntegration.creditCardPaymentSetupForAmazon.MarchentTransactionKey,
            post.Method,
            gatewayUrl.Host,
            gatewayUrl.AbsolutePath
            );
        }
        public static string BuildSignature(string secretKey, System.Collections.Specialized.NameValueCollection parameters)
        {
            StringBuilder sortedParams =  new StringBuilder(@"/cobranded//-ui/actions/start");
           // IDictionary<string, string> parameterList = (IDictionary<string, string>)parameters;
          //  List<KeyValuePair<string, string>> parameterLst = ((Dictionary<string, string>)parameterList).ToList();
           // bool first = true;
            Dictionary<string, string> a = parameters.AllKeys.ToDictionary(k => k, v => parameters[v]);
            foreach (KeyValuePair<string, string> var in a)
            {
                sortedParams.Append(var.Key.Trim()); 
                //sortedParams.AppendFormat("{2}{0}={1}", var.Key.Trim()); 
                   //PrepareUrlEncode//(var.Value), (first ? "?" : "&"));
               // first = false;
            }

            return CalculateHMAC(secretKey, sortedParams.ToString());
        }
        internal static string CalculateHMAC(String secretKey, String parametersToHash)
        {
            ASCIIEncoding ae = new ASCIIEncoding();
            HMACSHA1 signature = new HMACSHA1(ae.GetBytes(secretKey));
            return Convert.ToBase64String(signature.ComputeHash(
                           ae.GetBytes(parametersToHash.ToCharArray())));
        }

       
    }
}
