﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using daoProject.Model;
using daoProject.Utilities;
using daoProject.Mapper;

namespace daoProject.Service
{
    public class SystemSetupServiceImpl
    {
        private SystemSetupDataMapper dataMapper;

        public SystemSetupServiceImpl()
        {
            dataMapper = new SystemSetupDataMapper();
        }

        public void Add(SystemSetup item)
        {
            dataMapper.Create(item);
        }

        public void Update(SystemSetup item)
        {
            dataMapper.Update(item);
        }

        public bool Delete(long id)
        {
            return dataMapper.Delete(id);
        }

        public List<SystemSetup> GetAll()
        {
            return dataMapper.GetAll();
        }

        public SystemSetup GetById(long id)
        {
            return dataMapper.GetById(id);
        }
    }
}
