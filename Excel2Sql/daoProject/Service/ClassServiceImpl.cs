﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using daoProject.Model;
using daoProject.Utilities;
using daoProject.Mapper;
using System.Data;
using System.Xml;
using daoProject.Infrastructure;


namespace daoProject.Service
{
    public class ClassServiceImpl
    {
        #region Class
        public List<ClassWHRC> GetAllClass(XmlNode xmlNode)
        {
            List<ClassWHRC> classList = new List<ClassWHRC>();

            ClassWHRC classWHRC = null;

            if (xmlNode != null)
            {
                XmlNodeList datas = xmlNode.ChildNodes;
                for (int i = 0; i < datas.Count; i++)
                {
                    List<XmlNode> nodeList = new List<XmlNode>(datas[i].ChildNodes.Cast<XmlNode>());
                    XmlNode recordIDNode = (from item in nodeList
                                            where item.Name.Replace(" ", "") == "RecordId"
                                            select item).FirstOrDefault();
                    XmlNode titleNode = (from item in nodeList
                                         where item.Name.Replace(" ", "") == "Title"
                                         select item).FirstOrDefault();

                    XmlNode yearNode = (from item in nodeList
                                        where item.Name.Replace(" ", "") == "Year"
                                        select item).FirstOrDefault();

                    XmlNode feeNode = (from item in nodeList
                                       where item.Name.Replace(" ", "") == "Fee"
                                       select item).FirstOrDefault();

                    XmlNode sectionCodePrefixNode = (from item in nodeList
                                       where item.Name.Replace(" ", "") == "SectionCodePrefix"
                                       select item).FirstOrDefault();

                    XmlNode classTypeNode = (from item in nodeList
                                       where item.Name.Replace(" ", "") == "ClassType"
                                       select item).FirstOrDefault();

                    XmlNode publishhisClassNode = (from item in nodeList
                                             where item.Name.Replace(" ", "") == "Publishthisclass"
                                             select item).FirstOrDefault();

                    XmlNode classDescriptionNode = (from item in nodeList
                                                    where item.Name.Replace(" ", "") == "ClassDescription"
                                                   select item).FirstOrDefault();

                    classWHRC = new ClassWHRC();
                    classWHRC.RecordID = Convert.ToInt64(recordIDNode.InnerText);
                    classWHRC.Title = titleNode.InnerText;
                    classWHRC.Year =Convert.ToInt64(yearNode.InnerText);
                    //classWHRC.SectionCodePrefix = sectionCodePrefixNode.InnerText;
                    classWHRC.SectionCodePrefix = "";

                    if (publishhisClassNode != null)
                    {
                        if (publishhisClassNode.InnerText == "No")
                        {
                            classWHRC.PublishThisClass = false;
                        }
                        else if (publishhisClassNode.InnerText == "Yes")
                        {
                            classWHRC.PublishThisClass = true;
                        }
                        else
                        {
                            classWHRC.PublishThisClass = false;
                        }
                    }

                    if (classDescriptionNode != null)
                    {
                        classWHRC.ClassDescription = classDescriptionNode.InnerText;
                    }

                    if (feeNode.InnerText != "")
                    {
                        classWHRC.Fee = Convert.ToDecimal(feeNode.InnerText);
                    }
                    classWHRC.ClassType = classTypeNode.InnerText;
                    classWHRC.Description = classWHRC.Year + " | " + classWHRC.Title + " |  $" + classWHRC.Fee.ToString("0.00");

                    classList.Add(classWHRC);
                }
            }

            classList = classList.OrderBy(x => x.Title).OrderByDescending(x => x.Year).ToList();

            return classList;
        }
        
        public XmlDocument InsertClass(ClassWHRC classWHRC)
        {
            DataTable dt = new DataTable();
            DataColumn dc1 = new DataColumn("Name");
            DataColumn dc2 = new DataColumn("Value");

            dt.Columns.Add(dc1);
            dt.Columns.Add(dc2);

            AddDataRow("Title", classWHRC.Title, dt);
            AddDataRow("ClassType", classWHRC.ClassType, dt);
            AddDataRow("Year", classWHRC.Year.ToString(), dt);
            //AddDataRow("SectionCodePrefix", classWHRC.SectionCodePrefix, dt);
            AddDataRow("Fee", classWHRC.Fee.ToString(), dt);
            AddDataRow("ClassDescription", classWHRC.ClassDescription, dt);
            if (classWHRC.PublishThisClass)
            {
                AddDataRow("Publishthisclass", "Yes", dt);
            }
            else
            {
                AddDataRow("Publishthisclass", "No", dt);
            }

            XmlDocument xmlDoc = new XmlDocument();
            DataSet dataSet = new DataSet();
            dataSet.Tables.Add(dt);
            string strXml = dataSet.GetXml();
            xmlDoc.LoadXml(strXml);

            return xmlDoc;
        }

        public XmlDocument UpdateClass(ClassWHRC classWHRC)
        {
            XmlDocument xmlDocument = null;

            return xmlDocument;
        }

        public void DeleteClass(long recordID)
        {

        }
        #endregion

        #region Section
        public List<Section> GetAllSection(XmlNode xmlNode)
        {
            List<Section> sectionList = new List<Section>();
            Section section = null;

            if (xmlNode != null)
            {
                XmlNodeList datas = xmlNode.ChildNodes;
                long availableCount = 0;
                for (int i = 0; i < datas.Count; i++)
                {
                    List<XmlNode> nodeList = new List<XmlNode>(datas[i].ChildNodes.Cast<XmlNode>());
                    XmlNode recordIDNode = (from item in nodeList
                                            where item.Name.Replace(" ", "") == "RecordId"
                                            select item).FirstOrDefault();
                    XmlNode sectionCodeNode = (from item in nodeList
                                               where item.Name.Replace(" ", "") == "SectionCode"
                                               select item).FirstOrDefault();

                    XmlNode startDateNode = (from item in nodeList
                                             where item.Name.Replace(" ", "") == "StartDate"
                                             select item).FirstOrDefault();

                    XmlNode numberofMeetingNode = (from item in nodeList
                                                   where item.Name.Replace(" ", "") == "NumberofMeetings"
                                                   select item).FirstOrDefault();

                    XmlNode startTimeNode = (from item in nodeList
                                             where item.Name.Replace(" ", "") == "StartTime"
                                             select item).FirstOrDefault();

                    XmlNode endTimeNode = (from item in nodeList
                                           where item.Name.Replace(" ", "") == "EndTime"
                                           select item).FirstOrDefault();

                    XmlNode maximumSizeNode = (from item in nodeList
                                               where item.Name.Replace(" ", "") == "MaximumSize"
                                               select item).FirstOrDefault();

                    XmlNode classTitleNode = (from item in nodeList
                                              where item.Name.Replace(" ", "") == "ClassTitle"
                                              select item).FirstOrDefault();

                    XmlNode instructorNode = (from item in nodeList
                                              where item.Name.Replace(" ", "") == "Instructor"
                                              select item).FirstOrDefault();

                    XmlNode locationsNode = (from item in nodeList
                                              where item.Name.Replace(" ", "") == "Locations"
                                              select item).FirstOrDefault();

                    XmlNode pubListThisSectionNode = (from item in nodeList
                                                      where item.Name.Replace(" ", "") == "Publishthissection"
                                                      select item).FirstOrDefault();

                    XmlNode daysOfWeekNode = (from item in nodeList
                                              where item.Name.Replace(" ", "") == "DayofWeek"
                                              select item).FirstOrDefault();

                    section = new Section();
                    section.RecordID = Convert.ToInt64(recordIDNode.InnerText);
                    //section.SectionCode = sectionCodeNode.InnerText;
                    section.SectionCode = "";
                    section.StartDate = Convert.ToDateTime(startDateNode.InnerText);
                    section.StartDateString = section.StartDate.ToString("MM/dd/yyy");
                    section.NumberofMeetings = Convert.ToInt64(numberofMeetingNode.InnerText);
                    section.StartTime = startTimeNode.InnerText;
                    section.EndTime = endTimeNode.InnerText;
                    section.MaximumSize = Convert.ToInt32(maximumSizeNode.InnerText);
                    section.ClassTitle = classTitleNode.InnerText;
                    section.Instructor = instructorNode.InnerText;
                    section.Locations = locationsNode.InnerText;

                    if (pubListThisSectionNode != null)
                    {
                        if (pubListThisSectionNode.InnerText == "No")
                        {
                            section.PublishThisSection = false;
                        }
                        else if (pubListThisSectionNode.InnerText == "Yes")
                        {
                            section.PublishThisSection = true;
                        }
                        else
                        {
                            section.PublishThisSection = false;
                        }
                    }

                    if (daysOfWeekNode != null)
                    {
                        if (!string.IsNullOrEmpty(daysOfWeekNode.InnerText))
                        {
                            section.DayOfWeek = daysOfWeekNode.InnerText;
                        }
                        else
                        {
                            section.DayOfWeek = "";
                        }
                    }

                    //availableCount = CalculateAvailable(section.RecordID, section.MaximumSize, classInvoiceItemList);
                    //section.Available = availableCount;

                    sectionList.Add(section);
                }
            }

            return sectionList;
        }

        public XmlDocument InsertSection(Section section)
        {
            DataTable dt = new DataTable();
            DataColumn dc1 = new DataColumn("Name");
            DataColumn dc2 = new DataColumn("Value");

            dt.Columns.Add(dc1);
            dt.Columns.Add(dc2);

            AddDataRow("ClassTitle", section.ClassTitle, dt);
            AddDataRow("DayofWeek", section.DayOfWeek, dt);
            AddDataRow("StartDate", section.StartDate.ToString(), dt);
            AddDataRow("StartTime", section.StartTime, dt);
            AddDataRow("EndTime", section.EndTime, dt);
            AddDataRow("NumberofMeetings", section.NumberofMeetings.ToString(), dt);
            AddDataRow("MaximumSize", section.MaximumSize.ToString(), dt);
            AddDataRow("Instructor", section.Instructor, dt);
            AddDataRow("Locations", section.Locations, dt);
            if (section.PublishThisSection)
            {
                AddDataRow("Publishthissection", "Yes", dt);
            }
            else
            {
                AddDataRow("Publishthissection", "No", dt);
            }
            if(section.PublishThisSection)
            {
                AddDataRow("Publishthissection", "Yes", dt);
            }
            else
            {
                AddDataRow("Publishthissection", "No", dt);    
            }            

            XmlDocument xmlDoc = new XmlDocument();
            DataSet dataSet = new DataSet();
            dataSet.Tables.Add(dt);
            string strXml = dataSet.GetXml();
            xmlDoc.LoadXml(strXml);

            return xmlDoc;
        }

        public XmlDocument UpdateSection(Section section)
        {
            XmlDocument xmlDocument = null;

            return xmlDocument;
        }

        public void DeleteSection(long recordID)
        {

        }
        #endregion

        #region ClassType
        public List<ClassTypeWHRC> GetAllClassType(XmlNode xmlNode)
        {
            List<ClassTypeWHRC> classTypeList = new List<ClassTypeWHRC>();
            ClassTypeWHRC classTypeWHRC = null;

            if (xmlNode != null)
            {
                XmlNodeList datas = xmlNode.ChildNodes;
                for (int i = 0; i < datas.Count; i++)
                {
                    List<XmlNode> nodeList = new List<XmlNode>(datas[i].ChildNodes.Cast<XmlNode>());
                    XmlNode recordIDNode = (from item in nodeList
                                            where item.Name.Replace(" ", "") == "RecordId"
                                            select item).FirstOrDefault();
                    XmlNode classTypeNameNode = (from item in nodeList
                                         where item.Name.Replace(" ", "") == "ClassTypeName"
                                         select item).FirstOrDefault();

                    classTypeWHRC = new ClassTypeWHRC();
                    classTypeWHRC.RecordID = Convert.ToInt64(recordIDNode.InnerText);
                    classTypeWHRC.ClassTypeName =classTypeNameNode.InnerText;

                    classTypeList.Add(classTypeWHRC);
                }
            }

            return classTypeList;
        }

        public XmlDocument InsertClassType(ClassTypeWHRC classType)
        {
            DataTable dt = new DataTable();
            DataColumn dc1 = new DataColumn("Name");
            DataColumn dc2 = new DataColumn("Value");

            dt.Columns.Add(dc1);
            dt.Columns.Add(dc2);

            AddDataRow("ClassTypeName", classType.ClassTypeName, dt);
            
            XmlDocument xmlDoc = new XmlDocument();
            DataSet dataSet = new DataSet();
            dataSet.Tables.Add(dt);
            string strXml = dataSet.GetXml();
            xmlDoc.LoadXml(strXml);

            return xmlDoc;
        }

        private void AddDataRow(string name, string value, DataTable dt)
        {
            DataRow dr = dt.NewRow();
            dr["Name"] = name;
            dr["Value"] = value;
            dt.Rows.Add(dr);
        }

        public XmlDocument UpdateClassType(ClassTypeWHRC classType)
        {
            XmlDocument xmlDocument = null;

            return xmlDocument;
        }

        public void DeleteClassType(long recordID)
        {

        }
        #endregion

        #region Instructor
        public List<Instructor> GetAllInstructor(XmlNode xmlNode)
        {
            List<Instructor> instructorList = new List<Instructor>();
            Instructor instructor = null;

            if (xmlNode != null)
            {
                XmlNodeList datas = xmlNode.ChildNodes;
                for (int i = 0; i < datas.Count; i++)
                {
                    List<XmlNode> nodeList = new List<XmlNode>(datas[i].ChildNodes.Cast<XmlNode>());
                    XmlNode recordIDNode = (from item in nodeList
                                            where item.Name.Replace(" ", "") == "RecordId"
                                            select item).FirstOrDefault();
                    XmlNode instructorNameNode = (from item in nodeList
                                                 where item.Name.Replace(" ", "") == "InstructorName"
                                                 select item).FirstOrDefault();
                    XmlNode Designation = (from item in nodeList
                                           where item.Name.Replace(" ", "") == "Designation"
                                                  select item).FirstOrDefault();
                    XmlNode Position = (from item in nodeList
                                        where item.Name.Replace(" ", "") == "Position"
                                                  select item).FirstOrDefault();
                    XmlNode DetailsLink = (from item in nodeList
                                           where item.Name.Replace(" ", "") == "DetailsLink"
                                                  select item).FirstOrDefault();
                    XmlNode ThumbnailURL = (from item in nodeList
                                            where item.Name.Replace(" ", "") == "ThumbnailURL"
                                                  select item).FirstOrDefault();
                    XmlNode ImageURL = (from item in nodeList
                                        where item.Name.Replace(" ", "") == "ImageURL"
                                                  select item).FirstOrDefault();

                    instructor = new Instructor();
                    instructor.RecordID = Convert.ToInt64(recordIDNode.InnerText);
                    instructor.InstructorName = instructorNameNode.InnerText;
                    if (Designation!=null)
                        instructor.Designation = Designation.InnerText;
                    if (DetailsLink != null)
                        instructor.DetailsViewLink = DetailsLink.InnerText;
                    if (Position != null)
                        instructor.Position = Position.InnerText;
                    if (ThumbnailURL != null)
                        instructor.ThumbnailURL = ThumbnailURL.InnerText;
                    if (ImageURL != null)
                        instructor.ImageURL = ImageURL.InnerText;

                    instructorList.Add(instructor);
                }
            }

            return instructorList;
        }

        public XmlDocument InsertInstructor(Instructor instructor)
        {
            DataTable dt = new DataTable();
            DataColumn dc1 = new DataColumn("Name");
            DataColumn dc2 = new DataColumn("Value");

            dt.Columns.Add(dc1);
            dt.Columns.Add(dc2);

            AddDataRow("InstructorName", instructor.InstructorName, dt);
            AddDataRow("Designation", instructor.Designation, dt);
            AddDataRow("Position", instructor.Position, dt);
            AddDataRow("DetailsLink", instructor.DetailsViewLink, dt);
            AddDataRow("ThumbnailURL", instructor.ThumbnailURL, dt);
            AddDataRow("ImageURL", instructor.ImageURL, dt);

            XmlDocument xmlDoc = new XmlDocument();
            DataSet dataSet = new DataSet();
            dataSet.Tables.Add(dt);
            string strXml = dataSet.GetXml();
            xmlDoc.LoadXml(strXml);

            return xmlDoc;
        }

        public XmlDocument UpdateInstructor(Instructor instructor)
        {
            XmlDocument xmlDocument = null;

            return xmlDocument;
        }

        public void DeleteInstructor(long recordID)
        {

        }
        #endregion

        #region Location
        public List<Location> GetAllLocation(XmlNode xmlNode)
        {
            List<Location> locationnList = new List<Location>();
            Location location = null;

            if (xmlNode != null)
            {
                XmlNodeList datas = xmlNode.ChildNodes;
                for (int i = 0; i < datas.Count; i++)
                {
                    List<XmlNode> nodeList = new List<XmlNode>(datas[i].ChildNodes.Cast<XmlNode>());
                    XmlNode recordIDNode = (from item in nodeList
                                            where item.Name.Replace(" ", "") == "RecordId"
                                            select item).FirstOrDefault();
                    XmlNode locationDetailsNode = (from item in nodeList
                                                  where item.Name.Replace(" ", "") == "LocationDetails"
                                                  select item).FirstOrDefault();

                    location = new  Location();
                    location.RecordID = Convert.ToInt64(recordIDNode.InnerText);
                    location.LocationDetails = locationDetailsNode.InnerText;

                    locationnList.Add(location);
                }
            }

            return locationnList;
        }

        public XmlDocument InsertLocation(Location location)
        {
            DataTable dt = new DataTable();
            DataColumn dc1 = new DataColumn("Name");
            DataColumn dc2 = new DataColumn("Value");

            dt.Columns.Add(dc1);
            dt.Columns.Add(dc2);

            AddDataRow("LocationDetails", location.LocationDetails, dt);

            XmlDocument xmlDoc = new XmlDocument();
            DataSet dataSet = new DataSet();
            dataSet.Tables.Add(dt);
            string strXml = dataSet.GetXml();
            xmlDoc.LoadXml(strXml);

            return xmlDoc;
        }

        public XmlDocument UpdateLocation(Location location)
        {
            XmlDocument xmlDocument = null;

            return xmlDocument;
        }

        public void DeleteLocation(long recordID)
        {

        }
        #endregion

    }
}