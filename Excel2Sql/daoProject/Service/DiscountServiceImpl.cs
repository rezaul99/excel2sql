﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using daoProject.Model;
using daoProject.Utilities;
using daoProject.Mapper;
using System.Data;
using System.Xml;
using daoProject.Infrastructure;


namespace daoProject.Service
{
    public class DiscountServiceImpl
    {
        #region Package Discount
        public List<PackageDiscountWHRC> GetAllPackageDiscount(XmlNode xmlNode)
        {
            List<PackageDiscountWHRC> packageDiscountList = new List<PackageDiscountWHRC>();
            PackageDiscountWHRC packageDiscountWHRC = null;

            if (xmlNode != null)
            {
                XmlNodeList datas = xmlNode.ChildNodes;
                for (int i = 0; i < datas.Count; i++)
                {
                    List<XmlNode> nodeList = new List<XmlNode>(datas[i].ChildNodes.Cast<XmlNode>());
                    XmlNode recordIDNode = (from item in nodeList
                                            where item.Name.Replace(" ", "") == "RecordId"
                                            select item).FirstOrDefault();
                    XmlNode packageNameNode = (from item in nodeList
                                          where item.Name.Replace(" ", "") == "PackageName"
                                         select item).FirstOrDefault();

                    XmlNode discountAmountNode = (from item in nodeList
                                              where item.Name.Replace(" ", "") == "DiscountAmount"
                                        select item).FirstOrDefault();


                    packageDiscountWHRC = new PackageDiscountWHRC();
                    packageDiscountWHRC.RecordID = Convert.ToInt64(recordIDNode.InnerText);
                    packageDiscountWHRC.PackageName = packageNameNode.InnerText;
                    if (discountAmountNode.InnerText != "")
                    {
                        packageDiscountWHRC.DiscountAmount =Convert.ToDecimal(discountAmountNode.InnerText);
                    }

                    packageDiscountList.Add(packageDiscountWHRC);
                }
            }
            return packageDiscountList;
        }

        public XmlDocument InsertPackageDiscount(PackageDiscountWHRC packageDiscountWHRC)
        {
            DataTable dt = new DataTable();
            DataColumn dc1 = new DataColumn("Name");
            DataColumn dc2 = new DataColumn("Value");

            dt.Columns.Add(dc1);
            dt.Columns.Add(dc2);

            AddDataRow("PackageName", packageDiscountWHRC.PackageName, dt);
            AddDataRow("DepositAmount", packageDiscountWHRC.DiscountAmount.ToString(), dt);

            XmlDocument xmlDoc = new XmlDocument();
            DataSet dataSet = new DataSet();
            dataSet.Tables.Add(dt);
            string strXml = dataSet.GetXml();
            xmlDoc.LoadXml(strXml);

            return xmlDoc;
        }

        public XmlDocument UpdatePackageDiscount(PackageDiscountWHRC packageDiscountWHRC)
        {
            XmlDocument xmlDocument = null;

            return xmlDocument;
        }

        public void DeletePackageDiscount(long recordID)
        {

        }
        #endregion

        #region Percentage Discount
        public List<PercentageDiscountWHRC> GetAllPercentageDiscount(XmlNode xmlNode)
        {
            List<PercentageDiscountWHRC> percentageDiscountList = new List<PercentageDiscountWHRC>();
            PercentageDiscountWHRC percentageDiscountWHRC = null;

            if (xmlNode != null)
            {
                XmlNodeList datas = xmlNode.ChildNodes;
                for (int i = 0; i < datas.Count; i++)
                {
                    List<XmlNode> nodeList = new List<XmlNode>(datas[i].ChildNodes.Cast<XmlNode>());
                    XmlNode recordIDNode = (from item in nodeList
                                            where item.Name.Replace(" ", "") == "RecordId"
                                            select item).FirstOrDefault();
                    XmlNode disountTypeNode = (from item in nodeList
                                               where item.Name.Replace(" ", "") == "DiscountType"
                                               select item).FirstOrDefault();

                    XmlNode percentageNode = (from item in nodeList
                                                  where item.Name.Replace(" ", "") == "Percentage"
                                                  select item).FirstOrDefault();


                    percentageDiscountWHRC = new PercentageDiscountWHRC();
                    percentageDiscountWHRC.RecordID = Convert.ToInt64(recordIDNode.InnerText);
                    percentageDiscountWHRC.DisountType = disountTypeNode.InnerText;
                    if (percentageNode.InnerText != "")
                    {
                        percentageDiscountWHRC.Percentage = Convert.ToDecimal(percentageNode.InnerText);
                    }

                    percentageDiscountList.Add(percentageDiscountWHRC);
                }
            }
            return percentageDiscountList;
        }

        public XmlDocument InsertPercentageDiscount(PercentageDiscountWHRC percentageDiscountWHRC)
        {
            DataTable dt = new DataTable();
            DataColumn dc1 = new DataColumn("Name");
            DataColumn dc2 = new DataColumn("Value");

            dt.Columns.Add(dc1);
            dt.Columns.Add(dc2);

            AddDataRow("DiscountType", percentageDiscountWHRC.DisountType, dt);
            AddDataRow("Percentage", percentageDiscountWHRC.Percentage.ToString(), dt);

            XmlDocument xmlDoc = new XmlDocument();
            DataSet dataSet = new DataSet();
            dataSet.Tables.Add(dt);
            string strXml = dataSet.GetXml();
            xmlDoc.LoadXml(strXml);

            return xmlDoc;
        }

        public XmlDocument UpdatePercentageDiscount(PercentageDiscountWHRC percentageDiscountWHRC)
        {
            XmlDocument xmlDocument = null;

            return xmlDocument;
        }

        public void DeletePercentageDiscount(long recordID)
        {

        }
        #endregion

        #region Other Discount
        public List<OtherDiscountWHRC> GetAllOtherDiscount(XmlNode xmlNode)
        {
            List<OtherDiscountWHRC> OtherDiscountList = new List<OtherDiscountWHRC>();
            OtherDiscountWHRC otherDiscountWHRC = null;

            if (xmlNode != null)
            {
                XmlNodeList datas = xmlNode.ChildNodes;
                for (int i = 0; i < datas.Count; i++)
                {
                    List<XmlNode> nodeList = new List<XmlNode>(datas[i].ChildNodes.Cast<XmlNode>());
                    XmlNode recordIDNode = (from item in nodeList
                                            where item.Name.Replace(" ", "") == "RecordId"
                                            select item).FirstOrDefault();
                    XmlNode disountTypeNode = (from item in nodeList
                                               where item.Name.Replace(" ", "") == "DiscountType"
                                               select item).FirstOrDefault();
                    XmlNode discountModeNode = (from item in nodeList
                                               where item.Name.Replace(" ", "") == "DiscountMode"
                                               select item).FirstOrDefault();
                    XmlNode discountValueNode = (from item in nodeList
                                                where item.Name.Replace(" ", "") == "DiscountValue"
                                                select item).FirstOrDefault();


                    otherDiscountWHRC = new OtherDiscountWHRC();
                    otherDiscountWHRC.RecordID = Convert.ToInt64(recordIDNode.InnerText);
                    otherDiscountWHRC.DisountType = disountTypeNode.InnerText;
                    otherDiscountWHRC.DisountMode = discountModeNode.InnerText;
                    otherDiscountWHRC.DiscountValue =Convert.ToDecimal(discountValueNode.InnerText);
                    
                    OtherDiscountList.Add(otherDiscountWHRC);
                }
            }
            return OtherDiscountList;
        }

        public XmlDocument InsertOtherDiscount(OtherDiscountWHRC otherDiscountWHRC)
        {
            DataTable dt = new DataTable();
            DataColumn dc1 = new DataColumn("Name");
            DataColumn dc2 = new DataColumn("Value");

            dt.Columns.Add(dc1);
            dt.Columns.Add(dc2);

            AddDataRow("DiscountType", otherDiscountWHRC.DisountType, dt);
            AddDataRow("DiscountMode", otherDiscountWHRC.DisountMode, dt);
            AddDataRow("DiscountValue", otherDiscountWHRC.DiscountValue.ToString(), dt);

            XmlDocument xmlDoc = new XmlDocument();
            DataSet dataSet = new DataSet();
            dataSet.Tables.Add(dt);
            string strXml = dataSet.GetXml();
            xmlDoc.LoadXml(strXml);

            return xmlDoc;
        }

        public XmlDocument UpdateOtherDiscount(OtherDiscountWHRC otherDiscountWHRC)
        {
            XmlDocument xmlDocument = null;

            return xmlDocument;
        }

        public void DeleteOtherDiscount(long recordID)
        {

        }
        #endregion

        private void AddDataRow(string name, string value, DataTable dt)
        {
            DataRow dr = dt.NewRow();
            dr["Name"] = name;
            dr["Value"] = value;
            dt.Rows.Add(dr);
        }

    }
}