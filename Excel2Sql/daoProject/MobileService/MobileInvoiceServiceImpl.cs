﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using daoProject.Model;
using System.Xml;
using daoProject.Infrastructure;
using daoProject.Service;
using System.Globalization;
using System.Net;
using System.IO;
using System.Web;
using daoProject.Mapper;

namespace daoProject.MobileService
{
    public class MobileInvoiceServiceImpl
    {
       // Invoice

        public bool InsertInvoiceFromMobileApp(string xml)
        {
            bool result = false;
            try
            {
                XmlDocument xmlDoc = new XmlDocument();
                xmlDoc.LoadXml(xml);

                XmlNode invoiceNode = xmlDoc.SelectSingleNode("/Whrc/Invoice");

                if (invoiceNode != null)
                {
                    Invoice invoice = new Invoice();

                    XmlNode clientIdNode = invoiceNode.SelectSingleNode("ClientId");
                    if (clientIdNode != null)
                    {
                        invoice.ClientId = Convert.ToInt64(clientIdNode.InnerText);
                    }

                    XmlNode totalAmountNode = invoiceNode.SelectSingleNode("TotalAmount");
                    if (totalAmountNode != null)
                    {
                        invoice.TotalAmount = Convert.ToDecimal(totalAmountNode.InnerText);
                    }

                    invoice.PurchaseDate = DateTime.Now;
                    invoice.OrderStatus = OrderState.Ordered;
                    invoice.InvoiceType = InvoiceType.General;

                    XmlNode modeNode = xmlDoc.SelectSingleNode("/Whrc/Invoice/DualMode");                
                  
                    if (modeNode != null)
                    {
                        string mode = modeNode.InnerText;

                        XmlNode paymentDetailstNode = xmlDoc.SelectSingleNode("/Whrc/Invoice/PaymentDetails");
                        if (paymentDetailstNode != null)
                        {
                            invoice.PaymentMode = GetInvoicePaymentType(paymentDetailstNode, mode);   
                        }
                        new InvoiceServiceImpl().Add(invoice);

                        #region Insert Product Class Rental Membership
                        XmlNode productListNode = xmlDoc.SelectSingleNode("/Whrc/ProductList");
                        if (productListNode != null)
                        {
                            InsertProducts(productListNode, invoice);
                        }

                        XmlNode classListNode = xmlDoc.SelectSingleNode("/Whrc/ClassList");
                        if (classListNode != null)
                        {
                            InsertClasses(classListNode, invoice);
                        }

                        XmlNode rentalListNode = xmlDoc.SelectSingleNode("/Whrc/RentalList");
                        if (rentalListNode != null)
                        {
                            InsertRentals(rentalListNode, invoice);
                        }

                        XmlNode membershipNode = xmlDoc.SelectSingleNode("/Whrc/Membership");
                        if (membershipNode != null)
                        {
                            InsertMembership(membershipNode, invoice);
                        }
                        #endregion

                        if (paymentDetailstNode != null)
                        {
                            if (InsertInvoicePayments(paymentDetailstNode, mode, invoice))
                            {
                                result = true;
                            }
                            else
                            {
                                result = false;
                            }
                        }
                    }
                    new ClassInvoiceItemServiceImpl().UpdateClassStatus(invoice);
                    new RentalInvoiceItemServiceImpl().UpdateRentalStatus(invoice);
                }
            }
            catch (Exception ex)
            {
                return result;
                throw;
            }
            return result;
        }

        public void InsertProducts(XmlNode productListNode,Invoice invoiceObj)
        {
            if (productListNode != null)
            {
                XmlNodeList products = productListNode.SelectNodes("Product");
                List<ProductInvoiceItem> productInvoiceItemList = new List<ProductInvoiceItem>();
                foreach (XmlNode productNode in products)
                {
                    if (productNode != null)
                    {
                        ProductInvoiceItem productInvoiceItem = new ProductInvoiceItem();
                        productInvoiceItem.InvoiceId = invoiceObj.Id;

                        XmlNode idNode = productNode.SelectSingleNode("Id");
                        if (idNode != null)
                        {
                            productInvoiceItem.ProductId = Convert.ToInt64(idNode.InnerText);
                        }

                        XmlNode quantityNode = productNode.SelectSingleNode("Quantity");
                        if (quantityNode != null)
                        {
                            productInvoiceItem.Quantity = Convert.ToInt32(quantityNode.InnerText);
                        }

                        XmlNode priceNode = productNode.SelectSingleNode("Price");
                        if (priceNode != null)
                        {
                            productInvoiceItem.Price = Convert.ToDecimal(priceNode.InnerText);
                        }

                        XmlNode taxAmountNode = productNode.SelectSingleNode("TaxAmount");
                        if (taxAmountNode != null)
                        {
                            productInvoiceItem.TaxAmount = Convert.ToDecimal(taxAmountNode.InnerText);
                        }

                        XmlNode subtotalNode = productNode.SelectSingleNode("SubTotal");
                        if (subtotalNode != null)
                        {
                            productInvoiceItem.SubTotal = Convert.ToDecimal(subtotalNode.InnerText);
                        }

                        new InvoiceServiceImpl().AddToCart(productInvoiceItem);
                        productInvoiceItemList.Add(productInvoiceItem);
                    }
                }
                invoiceObj.ProductInvoiceItems = productInvoiceItemList;
            }
        }

        public void InsertClasses(XmlNode classtListNode, Invoice invoiceObj)
        {
            try
            {
                if (classtListNode != null)
                {
                    XmlNodeList classList = classtListNode.SelectNodes("Class");
                    List<ClassInvoiceItem> classInvoiceItemList = new List<ClassInvoiceItem>();
                    foreach (XmlNode classNode in classList)
                    {
                        if (classNode != null)
                        {
                            ClassInvoiceItem classInvoiceItem = new ClassInvoiceItem();
                            classInvoiceItem.InvoiceId = invoiceObj.Id;

                            XmlNode sectionIdNode = classNode.SelectSingleNode("SectionId");
                            if (sectionIdNode != null)
                            {
                                classInvoiceItem.SectionId = Convert.ToInt64(sectionIdNode.InnerText);
                            }

                            XmlNode signupDateNode = classNode.SelectSingleNode("SignupDate");
                            if (signupDateNode != null)
                            {
                                DateTime signUpDateTime =  DateTime.ParseExact(signupDateNode.InnerText.Trim(),"dd/MM/yyyy", null);
                                classInvoiceItem.SignupDate = Convert.ToDateTime(signUpDateTime.ToString("MM/dd/yyyy"));
                            }

                            XmlNode numberAttendingNode = classNode.SelectSingleNode("NumberAttending");
                            if (numberAttendingNode != null)
                            {
                                classInvoiceItem.NumberAttending = Convert.ToInt32(numberAttendingNode.InnerText);
                            }

                            XmlNode priceNode = classNode.SelectSingleNode("Price");
                            if (priceNode != null)
                            {
                                classInvoiceItem.Price = Convert.ToDecimal(priceNode.InnerText);
                            }
                            classInvoiceItem.Description = "";

                            new InvoiceServiceImpl().AddToCart(classInvoiceItem);
                            classInvoiceItemList.Add(classInvoiceItem);
                        }
                    }
                    invoiceObj.ClassInvoiceItems = classInvoiceItemList;
                }
            }
            catch (Exception ex)
            {

                throw;
            }
        }

        public void InsertRentals(XmlNode rentalListNode, Invoice invoiceObj)
        {
            try
            {
                if (rentalListNode != null)
                {
                    XmlNodeList rentalList = rentalListNode.SelectNodes("Rental");
                    List<RentalInvoiceItem> rentalInvoiceItemList = new List<RentalInvoiceItem>();
                    foreach (XmlNode rentalNode in rentalList)                    
                    {
                        if (rentalNode != null)
                        {
                            RentalInvoiceItem rentalInvoiceItemObj = new RentalInvoiceItem();
                          
                            rentalInvoiceItemObj.InvoiceId = invoiceObj.Id; 

                            #region Rental Equipment
                            rentalInvoiceItemObj.AgreementNo = "";

                            XmlNode rentalEquipmentIdNode = rentalNode.SelectSingleNode("RentalEquipmentId");
                            if (rentalEquipmentIdNode != null)
                            {
                                rentalInvoiceItemObj.RentalEquipmentID = Convert.ToInt64(rentalEquipmentIdNode.InnerText);
                            }

                            XmlNode rEStartingDateNode = rentalNode.SelectSingleNode("REStartingDate");
                            if (rEStartingDateNode != null)
                            {
                                DateTime restartingDate = DateTime.ParseExact(rEStartingDateNode.InnerText.Trim(), "dd/MM/yyyy", null);
                                rentalInvoiceItemObj.REStartingDate = Convert.ToDateTime(restartingDate.ToString("MM/dd/yyyy"));
                            }

                            XmlNode rEndingDateNode = rentalNode.SelectSingleNode("REEndingDate");
                            if (rEndingDateNode != null)
                            {
                                DateTime reEndingDate = DateTime.ParseExact(rEndingDateNode.InnerText.Trim(), "dd/MM/yyyy", null);
                                rentalInvoiceItemObj.REEndingDate = Convert.ToDateTime(reEndingDate.ToString("MM/dd/yyyy"));
                            }

                            XmlNode reSerialNode = rentalNode.SelectSingleNode("RESerial");
                            if (reSerialNode != null)
                            {
                                rentalInvoiceItemObj.RESerial = Convert.ToString(reSerialNode.InnerText);
                            }

                            XmlNode reVendorNameNode = rentalNode.SelectSingleNode("REVendorName");
                            if (reVendorNameNode != null)
                            {
                                rentalInvoiceItemObj.REVendorName = Convert.ToString(reVendorNameNode.InnerText);
                            }

                            XmlNode reModelNameNode = rentalNode.SelectSingleNode("REModelName");
                            if (reModelNameNode != null)
                            {
                                rentalInvoiceItemObj.ModelName = Convert.ToString(reModelNameNode.InnerText);
                            }

                            rentalInvoiceItemObj.StuffInitials = 0;

                            XmlNode depositAmountNode = rentalNode.SelectSingleNode("DepositAmount");
                            if (depositAmountNode != null)
                            {
                                rentalInvoiceItemObj.DepositAmount = Convert.ToDecimal(depositAmountNode.InnerText);
                            }

                            rentalInvoiceItemObj.REConditionWhenReturned = ""; 
                            #endregion

                            #region Rental Period
                            XmlNode rpStartingDateNode = rentalNode.SelectSingleNode("REStartingDate");
                            if (rpStartingDateNode != null)
                            {
                                DateTime rpStartingDate = DateTime.ParseExact(rpStartingDateNode.InnerText.Trim(), "dd/MM/yyyy", null);
                                rentalInvoiceItemObj.RPStartingDate = Convert.ToDateTime(rpStartingDate.ToString("MM/dd/yyyy"));
                            }

                            XmlNode rpEndingDateNode = rentalNode.SelectSingleNode("REEndingDate");
                            if (rpEndingDateNode != null)
                            {
                                DateTime rpEndingDate = DateTime.ParseExact(rpEndingDateNode.InnerText.Trim(), "dd/MM/yyyy", null);
                                rentalInvoiceItemObj.RPEndingDate = Convert.ToDateTime(rpEndingDate.ToString("MM/dd/yyyy"));
                            }

                            rentalInvoiceItemObj.RPDescription = "Description on mobile ";

                            XmlNode rpPreTaxAmountNode = rentalNode.SelectSingleNode("RPPreTaxAmount");
                            if (rpPreTaxAmountNode != null)
                            {
                                rentalInvoiceItemObj.RPPreTaxAmount = Convert.ToDecimal(rpPreTaxAmountNode.InnerText);
                            }

                            XmlNode rpSalesTaxNode = rentalNode.SelectSingleNode("RPSalesTax");
                            if (rpSalesTaxNode != null)
                            {
                                rentalInvoiceItemObj.RPSalesTax = Convert.ToDecimal(rpSalesTaxNode.InnerText);
                            }

                            XmlNode rpTotalNode = rentalNode.SelectSingleNode("RPTotal");
                            if (rpTotalNode != null)
                            {
                                rentalInvoiceItemObj.RPTotal = Convert.ToDecimal(rpTotalNode.InnerText);
                            } 
                            #endregion

                            new InvoiceServiceImpl().AddToCart(rentalInvoiceItemObj);
                            rentalInvoiceItemList.Add(rentalInvoiceItemObj);
                        }
                    }
                    invoiceObj.RentalInvoiceItems = rentalInvoiceItemList;
                }
            }
            catch (Exception ex)
            {

                throw;
            }
        }

        public void InsertMembership(XmlNode memberShipNode, Invoice invoiceObj)
        {
            try
            {
                if (memberShipNode != null && memberShipNode.ChildNodes.Count > 0)                
                {
                    MembershipInvoiceItem memberShipInvoiceItem = new MembershipInvoiceItem();
                    List<MembershipInvoiceItem> membershipInvoiceItemList = new List<MembershipInvoiceItem>();
                    memberShipInvoiceItem.InvoiceId = invoiceObj.Id;

                    memberShipInvoiceItem.Description = "WHRC Membership";

                    XmlNode memberShipIdNode = memberShipNode.SelectSingleNode("MembershipId");
                    if (memberShipIdNode != null)
                    {
                        memberShipInvoiceItem.MembershipId = Convert.ToInt64(memberShipIdNode.InnerText);
                    }

                    XmlNode dateJoinedNode = memberShipNode.SelectSingleNode("DateJoined");
                    if (dateJoinedNode != null)
                    {
                        DateTime dateJoined = DateTime.ParseExact(dateJoinedNode.InnerText.Trim(), "dd/MM/yyyy", null);
                        memberShipInvoiceItem.DateJoined = Convert.ToDateTime(dateJoined.ToString("MM/dd/yyyy"));
                    }

                    XmlNode feeNode = memberShipNode.SelectSingleNode("Fee");
                    if (feeNode != null)
                    {
                        memberShipInvoiceItem.Price = Convert.ToDecimal(feeNode.InnerText);
                    }

                    XmlNode dateValidTillNode = memberShipNode.SelectSingleNode("DateValidTill");
                    if (dateValidTillNode != null)
                    {
                        DateTime valitTillDate = DateTime.ParseExact(dateValidTillNode.InnerText.Trim(), "dd/MM/yyyy", null);
                        memberShipInvoiceItem.DateValidTill = Convert.ToDateTime(valitTillDate.ToString("MM/dd/yyyy"));
                    }
                    memberShipInvoiceItem.StaffInitial = 0;
                    new InvoiceServiceImpl().AddToCart(memberShipInvoiceItem);
                    membershipInvoiceItemList.Add(memberShipInvoiceItem);
                    invoiceObj.MembershipInvoiceItems = membershipInvoiceItemList;
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        //public void InsertInvoicePayment(Invoice invoice)
        //{
        //    InvoicePaymentDetail invoicePaymentDetail = new InvoicePaymentDetail();
        //    invoicePaymentDetail.InvoiceID = invoice.Id;
        //    invoicePaymentDetail.PaymentMode = PaymentType.Cash;
        //    invoicePaymentDetail.PaymentDate = DateTime.Now;
        //    invoicePaymentDetail.UserId = invoice.ClientId;
        //    invoicePaymentDetail.MedicalNumberOrNationalId = "";
        //    invoicePaymentDetail.TransactionID = "";
        //    invoicePaymentDetail.TransactionDetails = "";
        //    invoicePaymentDetail.PatientReceiptNumber = "";
        //    invoicePaymentDetail.Amount = invoice.TotalAmount;
        //    invoicePaymentDetail.PaymentStatus = PaymentStatus.Unpaid;

        //    invoicePaymentDetail.CheckNumber = "";
        //    new InvoicePaymentServiceImpl().Add(invoicePaymentDetail);
        //}

        public bool InsertInvoicePayments(XmlNode paymentNode,string mode,Invoice invoice)
        {
            bool result = true;

            XmlNode medicalNode = paymentNode.SelectSingleNode("Medical");
            XmlNode otherNode = paymentNode.SelectSingleNode("Other");
            if (mode == "On")
            {
                InsertMedicalPayment(medicalNode, invoice);
                if (InsertOtherPayment(otherNode, invoice))
                {
                    result = true;
                }
                else
                {
                    result = false;
                }
            }
            else if (mode == "Off")
            {
                if (InsertOtherPayment(otherNode, invoice))
                {
                    result = true;
                }
                else
                {
                    result = false;
                }
            }
            else if (mode == "SingleMedical")
            {
                InsertMedicalPayment(medicalNode, invoice);
            }

            return result;
        }

        public void InsertMedicalPayment(XmlNode medialPaymentNode, Invoice invoice)
        {
            InvoicePaymentDetail invoicePaymentDetail = new InvoicePaymentDetail();
            invoicePaymentDetail.InvoiceID = invoice.Id;
            invoicePaymentDetail.PaymentMode = PaymentType.Medical;
            invoicePaymentDetail.PaymentDate = DateTime.Now;
            invoicePaymentDetail.UserId = invoice.ClientId;

            XmlNode medicalIdNode = medialPaymentNode.SelectSingleNode("MedicalNumber");
            if (medicalIdNode != null)
            {
                invoicePaymentDetail.MedicalNumberOrNationalId = medicalIdNode.InnerText; ;
            }
            else
            {
                invoicePaymentDetail.MedicalNumberOrNationalId = "";
            }
            invoicePaymentDetail.TransactionID = "";
            invoicePaymentDetail.TransactionDetails = "";
            invoicePaymentDetail.PatientReceiptNumber = "";

            XmlNode medicalPaymentNode = medialPaymentNode.SelectSingleNode("MedicalAmount");
            if (medicalPaymentNode != null)
            {
                invoicePaymentDetail.Amount = Convert.ToDecimal(medicalPaymentNode.InnerText);
            }
            else
            {
                invoicePaymentDetail.Amount = 0;
            }

            //invoicePaymentDetail.Amount = invoice.TotalAmount;
            invoicePaymentDetail.PaymentStatus = PaymentStatus.Unpaid;

            invoicePaymentDetail.CheckNumber = "";
            new InvoicePaymentServiceImpl().Add(invoicePaymentDetail);
            new InvoiceServiceImpl().UpdateMedicalNumber(invoice.Id,invoicePaymentDetail.MedicalNumberOrNationalId);
        }
        
        public bool InsertOtherPayment(XmlNode otherNode, Invoice invoice)
        {
            bool paymentStatus = true;

            XmlNode paymentTypeNode = otherNode.SelectSingleNode("PaymentType");
            string paymentType = string.Empty;
            if (paymentTypeNode != null)
            {
                paymentType = paymentTypeNode.InnerText;
            }

            InvoicePaymentDetail invoicePaymentDetail = new InvoicePaymentDetail();
            invoicePaymentDetail.InvoiceID = invoice.Id;
            if (!string.IsNullOrEmpty(paymentType))
            {
                if (paymentType == "Cash")
                {
                    invoicePaymentDetail.PaymentMode = PaymentType.Cash;
                }
                else if (paymentType == "Check")
                {                   
                    invoicePaymentDetail.PaymentMode = PaymentType.Check;
                }
                else
                {
                    invoicePaymentDetail.PaymentMode = PaymentType.CreditCard;
                }
            }
            invoicePaymentDetail.PaymentDate = DateTime.Now;
            invoicePaymentDetail.UserId = invoice.ClientId;
            invoicePaymentDetail.MedicalNumberOrNationalId = "";
            invoicePaymentDetail.TransactionID = "";
            invoicePaymentDetail.TransactionDetails = "";
            invoicePaymentDetail.PatientReceiptNumber = "";

            invoicePaymentDetail.CheckNumber = "";

            XmlNode amountNode = otherNode.SelectSingleNode("Amount");
            if (amountNode != null)
            {
                invoicePaymentDetail.Amount = Convert.ToDecimal(amountNode.InnerText);
            }
            else
            {
                invoicePaymentDetail.Amount = 0;
            }
            invoicePaymentDetail.PaymentStatus = PaymentStatus.Unpaid;

           
            if (paymentType == "CreditCard")
            {
                string creditCardNumber = "";
                string expireDate = "";
                XmlNode creditCardNode = otherNode.SelectSingleNode("CreditCardNumber");
                if (creditCardNode != null)
                {
                    creditCardNumber = creditCardNode.InnerText;
                }
                XmlNode expireDateNode = otherNode.SelectSingleNode("ExpireDate");
                if (expireDateNode != null)
                {
                    expireDate = expireDateNode.InnerText;
                }
                bool result = InsertCreditCardAmount(invoice, Convert.ToString(invoicePaymentDetail.Amount), creditCardNumber, expireDate);
                if (result)
                {
                    new InvoicePaymentServiceImpl().Add(invoicePaymentDetail);
                }
                else
                {
                    DeleteInvoice(invoice.Id);
                    paymentStatus = false;
                }
            }
            else if (paymentType == "Check")
            {
                XmlNode checkNumberNode = otherNode.SelectSingleNode("CheckNumber");
                if (checkNumberNode != null)
                {
                    invoicePaymentDetail.CheckNumber = checkNumberNode.InnerText;
                }
                else
                {
                    invoicePaymentDetail.CheckNumber = "";
                }
                new InvoicePaymentServiceImpl().Add(invoicePaymentDetail);
                new InvoiceServiceImpl().UpdateCheckNumber(invoice.Id, invoicePaymentDetail.CheckNumber);
            }
            else
            {
                new InvoicePaymentServiceImpl().Add(invoicePaymentDetail);
            }

            return paymentStatus;
        }

        public bool InsertCreditCardAmount(Invoice invoice, string invoiceAmount, string creditCardNumber, string expirationDate)
        {
            bool result = false;
            try
            {
                User userObj = new UserServiceImpl().GetUserById(invoice.ClientId);

                CreditCardPaymentSetup creditCardPaymentSetup = null;
                List<CreditCardPaymentSetup> creditCardPaymentSetupList = new CreditCardPaymentSetupServiceImpl().GetAllCreditCardPaymentSetup();
                if (creditCardPaymentSetupList.Count > 0)
                {
                    creditCardPaymentSetup = creditCardPaymentSetupList[0];
                }

                PaymentGatewayType paymentGatewayType = PaymentGatewayType.OnSite;
                string marchentLogin = "";
                string marchentTransactionKey = "";


                if (creditCardPaymentSetup != null)
                {
                    marchentLogin = creditCardPaymentSetup.MarchentLoginID;
                    marchentTransactionKey = creditCardPaymentSetup.MarchentTransactionKey;
                    paymentGatewayType = creditCardPaymentSetup.PaymentGatewayType;
                }

                //String post_url = "https://secure.authorize.net/gateway/transact.dll";
                String post_url = "https://test.authorize.net/gateway/transact.dll";

                Dictionary<string, string> post_values = new Dictionary<string, string>();
                //the API Login ID and Transaction Key must be replaced with valid values
                post_values.Add("x_login", marchentLogin); //8V4Nf9AfH3L7
                post_values.Add("x_tran_key", marchentTransactionKey); //6256NqF3MXc9afqm
                post_values.Add("x_delim_data", "TRUE");
                post_values.Add("x_delim_char", "|");
                post_values.Add("x_relay_response", "FALSE");
                //post_values.Add("x_relay_response", "TRUE");
                //post_values.Add("x_relay_url", "http://173.11.76.139/WHRCAdmin/Default.aspx");
                //post_values.Add("x_receipt_link_url", "http://173.11.76.139/WHRCAdmin/Default.aspx");

                //post_values.Add("x_show_form", "PAYMENT_FORM");


                post_values.Add("x_type", "AUTH_CAPTURE");//these are for authorize.net see http://www.authorize.net/support/DirectPost_guide.pdf
                post_values.Add("x_method", "CC");
                post_values.Add("x_card_num", creditCardNumber);
                ////post_values.Add("x_card_num", "4111111111111111");
                post_values.Add("x_exp_date", expirationDate);
                //post_values.Add("x_exp_date", SecurityCode);

                post_values.Add("x_amount", invoiceAmount);
                post_values.Add("x_description", "Transaction description");
                post_values.Add("x_first_name", userObj.FirstName);
                post_values.Add("x_last_name", userObj.LastName);
                post_values.Add("x_address", userObj.Address1);
                post_values.Add("x_city", userObj.Ad1City);
                post_values.Add("x_state", userObj.Ad1State);
                post_values.Add("x_zip", userObj.Ad1Zip);

                // Additional fields can be added here as outlined in the AIM integration
                // guide at: http://developer.authorize.net

                // This section takes the input fields and converts them to the proper format
                // for an http post.  For example: "x_login=username&x_tran_key=a1B2c3D4"
                String post_string = "";

                foreach (KeyValuePair<string, string> post_value in post_values)
                {
                    post_string += post_value.Key + "=" + HttpUtility.UrlEncode(post_value.Value) + "&";//For example: "x_login=username&x_tran_key=a1B2c3D4"
                }
                post_string = post_string.TrimEnd('&');//remove ending chars  & sign from the string

                // create an HttpWebRequest object to communicate with Authorize.net
                HttpWebRequest objRequest = (HttpWebRequest)WebRequest.Create(post_url);
                objRequest.Method = "POST";
                objRequest.ContentLength = post_string.Length;
                objRequest.ContentType = "application/x-www-form-urlencoded";

                // post data is sent as a stream
                StreamWriter myWriter = null;
                myWriter = new StreamWriter(objRequest.GetRequestStream());
                myWriter.Write(post_string);
                myWriter.Close();

                String post_response;
                HttpWebResponse objResponse = (HttpWebResponse)objRequest.GetResponse();
                using (StreamReader responseStream = new StreamReader(objResponse.GetResponseStream()))
                {
                    post_response = responseStream.ReadToEnd();
                    responseStream.Close();
                }

                Array response_array = post_response.Split('|');
                string allResponse = "";
                string[] responseParts = new string[100];
                int i = 0;
                foreach (string value in response_array)
                {
                    allResponse += value + " ~";
                    responseParts[i] = value;
                    i++;
                }

                if (responseParts[0] == "1")
                {
                    int tranStatusID = Convert.ToInt32(responseParts[0]);
                    invoice.CreditCardTransactionStatus = (CreditCardTransactionStatus)tranStatusID;
                    invoice.CreditCardTransactionID = Convert.ToString(responseParts[6]);
                    invoice.CreditCardTransactionDetails = allResponse.Substring(0, allResponse.Length - 1);
                    invoice.PaymentDate = DateTime.Now;
                    new InvoiceServiceImpl().UpdateCreditCardPayment(invoice);//update invoice

                    result = true;
                }
                else
                {
                    result = false;
                }
            }
            catch (Exception ex)
            {
                return result;
            }
            return result;
        }

        private bool DeleteInvoice(long invoiceId)
        {
            bool result = false;
            MobileDataMapper mobileDataMapper = new MobileDataMapper();
            mobileDataMapper.DeleteRentalItemByInvoiceId(invoiceId);
            mobileDataMapper.DeleteProductByInvoiceId(invoiceId);
            mobileDataMapper.DeleteClassByInvoiceId(invoiceId);
            mobileDataMapper.DeleteMembershipByInvoiceId(invoiceId);
            mobileDataMapper.DeleteInvoicePaymentByInvoiceId(invoiceId);
            mobileDataMapper.DeleteInvoiceById(invoiceId);

            return result;
        }

        public PaymentType GetInvoicePaymentType(XmlNode paymentNode, string mode)
        {
            XmlNode medicalNode = paymentNode.SelectSingleNode("Medical");
            XmlNode otherNode = paymentNode.SelectSingleNode("Other");
            PaymentType paymentTypeEnum = PaymentType.Cash;

            if (mode == "On")
            {
                paymentTypeEnum = GetOtherPaymentType(otherNode);
            }
            else if (mode == "Off")
            {
              paymentTypeEnum  = GetOtherPaymentType(otherNode);
            }
            else if (mode == "SingleMedical")
            {
                paymentTypeEnum = PaymentType.Medical;
            }
            return paymentTypeEnum;
        }

        public PaymentType GetOtherPaymentType(XmlNode otherNode)
        {
            XmlNode paymentTypeNode = otherNode.SelectSingleNode("PaymentType");
            string paymentType = string.Empty;
            if (paymentTypeNode != null)
            {
                paymentType = paymentTypeNode.InnerText;
            }
            InvoicePaymentDetail invoicePaymentDetail = new InvoicePaymentDetail();

            if (!string.IsNullOrEmpty(paymentType))
            {
                if (paymentType == "Cash")
                {
                    invoicePaymentDetail.PaymentMode = PaymentType.Cash;
                }
                else if (paymentType == "Check")
                {
                    invoicePaymentDetail.PaymentMode = PaymentType.Check;
                }
                else
                {
                    invoicePaymentDetail.PaymentMode = PaymentType.CreditCard;
                }
            }
            return invoicePaymentDetail.PaymentMode;
        }
    }
}