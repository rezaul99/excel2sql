﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.Odbc;
using System.Collections;
using daoProject.Infrastructure;
using daoProject.Model;
using daoProject.Mapper;

namespace daoProject.Mapper
{
    internal class ShippingAddressDataMapper : SqlHelper
    {
        private static string _cmdInsert = @"INSERT INTO Whrc_ShippingAddress (ClientId,FullName,AddressLine1,AddressLine2,City,State,Zip,CountryId,PhoneNo,DefaultShippingAddess) VALUES (?,?,?,?,?,?,?,?,?,?); ";
        private static string _cmdUpdate = @"UPDATE Whrc_ShippingAddress SET ClientId=?,FullName=?,AddressLine1=?,AddressLine2=?,City=?,State=?,Zip=?,CountryId=?,PhoneNo=?,DefaultShippingAddess=? WHERE Id=? ";
        private static string _cmdUpdateDefaultShippingAddressByClientId = @"UPDATE Whrc_ShippingAddress SET DefaultShippingAddess=? WHERE Id=? and ClientId=? ";
        private static string _cmdUpdateNotDefaultShippingAddressRestAllByClientId = @"UPDATE Whrc_ShippingAddress SET DefaultShippingAddess=? WHERE Id != ? and ClientId=? ";
        private static string _cmdDelete = @"DELETE FROM Whrc_ShippingAddress WHERE Id=? ";
        private static string _cmdSelectAll = @"SELECT * FROM Whrc_ShippingAddress ";
        private static string _cmdSelectById = @"SELECT * FROM Whrc_ShippingAddress WHERE Id=? ";
        
        internal void Create(ShippingAddress shippingAddress)
        {
            long id = -1;
            try
            {
                OdbcCommand cmd = new OdbcCommand(_cmdInsert);

                cmd.Parameters.AddWithValue("@ClientId", shippingAddress.ClientId);
                cmd.Parameters.AddWithValue("@FullName", shippingAddress.FullName);
                cmd.Parameters.AddWithValue("@AddressLine1", shippingAddress.AddressLine1);
                cmd.Parameters.AddWithValue("@AddressLine2", shippingAddress.AddressLine2);
                cmd.Parameters.AddWithValue("@City", shippingAddress.City);
                cmd.Parameters.AddWithValue("@State", shippingAddress.State);
                cmd.Parameters.AddWithValue("@Zip", shippingAddress.Zip);
                cmd.Parameters.AddWithValue("@CountryId", shippingAddress.CountryId);
                cmd.Parameters.AddWithValue("@PhoneNo", shippingAddress.PhoneNo);
                cmd.Parameters.AddWithValue("@DefaultShippingAddess", shippingAddress.DefaultShippingAddess);
                //Execute the OdbcCommand
                id = Convert.ToInt64(ExecuteNonQueryAndScalar(cmd));

                shippingAddress.Id = id;
            }
            catch (Exception ex)
            {
                //Attempt to throw the exception for error handling
                throw ex;
            }
        }

        internal void Update(ShippingAddress shippingAddress)
        {
            try
            {
                OdbcCommand cmd = new OdbcCommand(_cmdUpdate);

                cmd.Parameters.AddWithValue("@ClientId", shippingAddress.ClientId);
                cmd.Parameters.AddWithValue("@FullName", shippingAddress.FullName);
                cmd.Parameters.AddWithValue("@AddressLine1", shippingAddress.AddressLine1);
                cmd.Parameters.AddWithValue("@AddressLine2", shippingAddress.AddressLine2);
                cmd.Parameters.AddWithValue("@City", shippingAddress.City);
                cmd.Parameters.AddWithValue("@State", shippingAddress.State);
                cmd.Parameters.AddWithValue("@Zip", shippingAddress.Zip);
                cmd.Parameters.AddWithValue("@CountryId", shippingAddress.CountryId);
                cmd.Parameters.AddWithValue("@PhoneNo", shippingAddress.PhoneNo);
                cmd.Parameters.AddWithValue("@DefaultShippingAddess", shippingAddress.DefaultShippingAddess);
                cmd.Parameters.AddWithValue("@Id", shippingAddress.Id);

                //Execute the OdbcCommand
                ExecuteNonQuery(cmd);
            }
            catch (Exception ex)
            {
                //Attempt to throw the exception for error handling
                throw ex;
            }
        }

        internal bool Delete(long id)
        {
            bool success = false;

            try
            {
                OdbcCommand cmd = new OdbcCommand(_cmdDelete);

                cmd.Parameters.AddWithValue("@Id", id);

                ExecuteNonQuery(cmd);

                success = true;
            }
            catch (Exception ex)
            {
                //Attempt to throw the exception for error handling
                throw ex;
            }

            return success;
        }

        private List<ShippingAddress> GetListFromDataSet(DataSet ds)
        {
            List<ShippingAddress> shippingAddressList = new List<ShippingAddress>();

            // This Loop reads all Data from DataSet                    
            for (int i = 0; i < ds.Tables["Data"].Rows.Count; i++)
            {
                ShippingAddress shippingAddress = new ShippingAddress();

                shippingAddress.Id = Convert.ToInt64(ds.Tables["Data"].Rows[i].ItemArray[0]);
                shippingAddress.ClientId = Convert.ToInt64(ds.Tables["Data"].Rows[i].ItemArray[1]);
                shippingAddress.FullName = Convert.ToString(ds.Tables["Data"].Rows[i].ItemArray[2]);
                shippingAddress.AddressLine1 = Convert.ToString(ds.Tables["Data"].Rows[i].ItemArray[3]);
                shippingAddress.AddressLine2 = Convert.ToString(ds.Tables["Data"].Rows[i].ItemArray[4]);
                shippingAddress.City = Convert.ToString(ds.Tables["Data"].Rows[i].ItemArray[5]);
                shippingAddress.State = Convert.ToString(ds.Tables["Data"].Rows[i].ItemArray[6]);
                shippingAddress.Zip = Convert.ToString(ds.Tables["Data"].Rows[i].ItemArray[7]);
                shippingAddress.CountryId = Convert.ToInt64(ds.Tables["Data"].Rows[i].ItemArray[8]);
                shippingAddress.PhoneNo = Convert.ToString(ds.Tables["Data"].Rows[i].ItemArray[9]);
               
                if (shippingAddress.AddressLine2 == "")
                {
                    shippingAddress.DetailDescription = shippingAddress.FullName + ", phone: " + shippingAddress.PhoneNo + ", " + shippingAddress.AddressLine1 + ", " + shippingAddress.City + ", State :" + shippingAddress.State + ", Zip : " + shippingAddress.Zip;
                }
                else
                {
                    shippingAddress.DetailDescription = shippingAddress.FullName + ", phone: " + shippingAddress.PhoneNo + ", " + shippingAddress.AddressLine1 + ", " + shippingAddress.AddressLine2 + ", " + shippingAddress.City + ", State :" + shippingAddress.State + ", Zip : " + shippingAddress.Zip;
                    
                }
                if (ds.Tables["Data"].Rows[i].ItemArray[10] != DBNull.Value)
                {
                    shippingAddress.DefaultShippingAddess = Convert.ToInt32(ds.Tables["Data"].Rows[i].ItemArray[10]);
                }
                else
                {
                    shippingAddress.DefaultShippingAddess = 0;
                }

                shippingAddressList.Add(shippingAddress);
            }

            return shippingAddressList;
        }

        internal List<ShippingAddress> GetAll()
        {
            try
            {
                //Create instance of sqlcommand
                OdbcCommand cmd = new OdbcCommand(_cmdSelectAll);

                DataSet ds = ExecuteQuery(cmd);

                return GetListFromDataSet(ds);
            }
            catch (Exception ex)
            {
                //Attempt to throw the exception for error handling
                throw ex;
            }
        }

        internal ShippingAddress GetById(long id)
        {
            try
            {
                OdbcCommand cmd = new OdbcCommand(_cmdSelectById);

                cmd.Parameters.AddWithValue("@Id", id);

                DataSet ds = ExecuteQuery(cmd);

                return GetListFromDataSet(ds)[0];
            }
            catch (Exception ex)
            {
                //Attempt to throw the exception for error handling
                throw ex;
            }
        }

        internal void UpdateDefaultShippingAddressByClientId(ShippingAddress shippingAddress)
        {
            try
            {
                OdbcCommand cmd = new OdbcCommand(_cmdUpdateDefaultShippingAddressByClientId);

                cmd.Parameters.AddWithValue("@DefaultShippingAddess", 1);
                cmd.Parameters.AddWithValue("@Id", shippingAddress.Id);
                cmd.Parameters.AddWithValue("@ClientId", shippingAddress.ClientId);

                //Execute the OdbcCommand
                ExecuteNonQuery(cmd);
            }
            catch (Exception ex)
            {
                //Attempt to throw the exception for error handling
                throw ex;
            }
        }

        internal void UpdateNotDefaultShippingAddressRestAllByClientId(ShippingAddress shippingAddress)
        {
            try
            {
                OdbcCommand cmd = new OdbcCommand(_cmdUpdateNotDefaultShippingAddressRestAllByClientId);

                cmd.Parameters.AddWithValue("@DefaultShippingAddess", 0);
                cmd.Parameters.AddWithValue("@Id", shippingAddress.Id);
                cmd.Parameters.AddWithValue("@ClientId", shippingAddress.ClientId);

                //Execute the OdbcCommand
                ExecuteNonQuery(cmd);
            }
            catch (Exception ex)
            {
                //Attempt to throw the exception for error handling
                throw ex;
            }
        }


    }
}
