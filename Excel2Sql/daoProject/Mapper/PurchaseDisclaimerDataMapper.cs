﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using daoProject.Model;
using System.Data;
using System.Data.Odbc;
using System.Collections;
using daoProject.Infrastructure;
using daoProject.Mapper;
using daoProject.Service;
namespace daoProject.Mapper
{
   public  class PurchaseDisclaimerDataMapper : SqlHelper
    {
       private static string _cmdGetAll = @"SELECT * FROM Whrc_PurchaseDisclaimer";
       private static string _cmdSelectById = @"SELECT * FROM Whrc_PurchaseDisclaimer Where Id=?";
       private static string _cmdInsert = @"INSERT INTO Whrc_PurchaseDisclaimer (PurchaseDisclaimerValue) VALUES (?); ";
       private static string _cmdUpdate = @"UPDATE Whrc_PurchaseDisclaimer SET PurchaseDisclaimerValue=? WHERE Id=?";

       public List<PurchaseDisclaimer> GetAll()
        {
            try
            {
                OdbcCommand cmd = new OdbcCommand(_cmdGetAll);
                DataSet ds = ExecuteQuery(cmd);
                return GetListFromDataSet(ds);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

       public PurchaseDisclaimer GetAllById(long Id)
        {
            try
            {
                OdbcCommand cmd = new OdbcCommand(_cmdSelectById);

                cmd.Parameters.AddWithValue("@Id", Id);

                DataSet ds = ExecuteQuery(cmd);

                return GetListFromDataSet(ds)[0];
            }
            catch (Exception ex)
            {
                //Attempt to throw the exception for error handling
                throw ex;
            }
        }

       private List<PurchaseDisclaimer> GetListFromDataSet(DataSet ds)
        {
            List<PurchaseDisclaimer> PurchaseDisclaimerObjList = new List<PurchaseDisclaimer>();

            // This Loop reads all Data from DataSet                    
            for (int i = 0; i < ds.Tables["Data"].Rows.Count; i++)
            {
                PurchaseDisclaimer PurchaseDisclaimerObj = new PurchaseDisclaimer();

                PurchaseDisclaimerObj.Id = Convert.ToInt64(ds.Tables["Data"].Rows[i].ItemArray[0]);
                PurchaseDisclaimerObj.PurchaseDisclaimerValue = Convert.ToString(ds.Tables["Data"].Rows[i].ItemArray[1]);

                PurchaseDisclaimerObjList.Add(PurchaseDisclaimerObj);
            }

            return PurchaseDisclaimerObjList;
        }

       public void Add(PurchaseDisclaimer PurchaseDisclaimerObj)
       {
           long id = -1;
           try
           {
               OdbcCommand cmd = new OdbcCommand(_cmdInsert);

               cmd.Parameters.AddWithValue("@PurchaseDisclaimerValue", PurchaseDisclaimerObj.PurchaseDisclaimerValue);

               id = Convert.ToInt64(ExecuteNonQueryAndScalar(cmd));

               PurchaseDisclaimerObj.Id = id;
           }
           catch (Exception ex)
           {
               throw ex;
           }

       }

       public void Add(PurchaseDisclaimer PurchaseDisclaimerObj, System.Data.Odbc.OdbcConnection conn, System.Data.Odbc.OdbcTransaction tx)
       {
           long id = -1;
           try
           {
               OdbcCommand cmd = new OdbcCommand(_cmdInsert);

               cmd.Parameters.AddWithValue("@PurchaseDisclaimerValue", PurchaseDisclaimerObj.PurchaseDisclaimerValue);

               id = Convert.ToInt64(ExecuteNonQueryAndScalar(cmd, conn, tx));

               PurchaseDisclaimerObj.Id = id;
           }
           catch (Exception ex)
           {
               throw ex;
           }

       }

       public void Update(PurchaseDisclaimer PurchaseDisclaimerObj)
        {
            try
            {
                OdbcCommand cmd = new OdbcCommand(_cmdUpdate);

                cmd.Parameters.AddWithValue("@PurchaseDisclaimerValue", PurchaseDisclaimerObj.PurchaseDisclaimerValue);

                cmd.Parameters.AddWithValue("@Id", PurchaseDisclaimerObj.Id);

                ExecuteNonQuery(cmd);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
