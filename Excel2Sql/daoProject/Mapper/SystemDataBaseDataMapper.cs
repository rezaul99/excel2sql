﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using daoProject.Model;
using System.Data;
using daoProject.Infrastructure;
using System.Data.Odbc;
namespace daoProject.Mapper
{
    internal class SystemDataBaseDataMapper : SqlHelper
    {

       public DataTable GetDataTable(string tableObjName)
       {
           DataTable table = new DataTable();
           try
           {

               string sql = "Select * from " + tableObjName;
               OdbcCommand cmd = new OdbcCommand(sql);

               DataSet ds = ExecuteQuery(cmd);
               table = (DataTable)ds.Tables[0];
           }
           catch (Exception ex)
           {
               throw ex;
           }
           return table;
       }

        public List<ReportClassObjects> GetTableListByDataBaseName(string databaseName)
        {
            try
            {
                string _cmdGetAll = @"Use " + databaseName + " ;SELECT  TABLE_CATALOG, TABLE_SCHEMA, TABLE_NAME, TABLE_TYPE " +
                                      "FROM INFORMATION_SCHEMA.TABLES " +
                                      "WHERE TABLE_TYPE = 'BASE TABLE' " +
                                      "ORDER BY TABLE_TYPE,TABLE_NAME";
                OdbcCommand cmd = new OdbcCommand(_cmdGetAll);
                DataSet ds = ExecuteQuery(cmd);
                return GetListFromDataSetByDataBaseName(ds);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private List<ReportClassObjects> GetListFromDataSetByDataBaseName(DataSet ds)
        {
            List<ReportClassObjects> TableObjList = new List<ReportClassObjects>();

            // This Loop reads all Data from DataSet                    
            for (int i = 0; i < ds.Tables["Data"].Rows.Count; i++)
            {
                ReportClassObjects TableObj = new ReportClassObjects();

                TableObj.Id = i;
                TableObj.Name = Convert.ToString(ds.Tables["Data"].Rows[i].ItemArray[2]);
                TableObj.DataBaseId = i;

                TableObjList.Add(TableObj);
            }

            return TableObjList;
        }

        public List<ReportAttributes> GetColumnListByTableName(string databaseName, string TableName)
        {
            try
            {
                string _cmdGetAll = @"Use " + databaseName + "; " +
 "SELECT    TABLE_NAME, COLUMN_NAME, DATA_TYPE, CHARACTER_MAXIMUM_LENGTH " +
            "FROM         INFORMATION_SCHEMA.COLUMNS " +
           "WHERE     TABLE_NAME = ? ";
                OdbcCommand cmd = new OdbcCommand(_cmdGetAll);
                cmd.Parameters.AddWithValue("@TABLE_NAME", TableName);
                DataSet ds = ExecuteQuery(cmd);
                return GetListFromDataSetByTableName(ds);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private List<ReportAttributes> GetListFromDataSetByTableName(DataSet ds)
        {
            List<ReportAttributes> ColumnObjList = new List<ReportAttributes>();

            // This Loop reads all Data from DataSet                    
            for (int i = 0; i < ds.Tables["Data"].Rows.Count; i++)
            {
                ReportAttributes ColumnObj = new ReportAttributes();

                ColumnObj.Id = i;
                ColumnObj.Name = Convert.ToString(ds.Tables["Data"].Rows[i].ItemArray[1]);
                ColumnObj.TableId = i;

                ColumnObjList.Add(ColumnObj);
            }

            return ColumnObjList;
        }

        public DataTable GenerateTableValues(List<ReportAttributes> columnList, string tableName, string databaseName) //use
        {
            try
            {
                string columns = "";
                string sql = @"Use " + databaseName + " ;Select ";
                foreach (ReportAttributes Columnitem in columnList)
                {
                    columns += Columnitem.Name + ", ";
                }
                sql += columns.Remove(columns.LastIndexOf(',')) + " from " + tableName;
                OdbcCommand cmd = new OdbcCommand(sql);

                DataSet ds = ExecuteQuery(cmd);
                return ds.Tables[0];
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
    }
}
