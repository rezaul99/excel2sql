﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using daoProject.Model;
using System.Data;
using daoProject.Infrastructure;
using System.Data.Odbc;

namespace daoProject.Mapper
{
    public class EmailAddressDataMapper : SqlHelper
    {
        private static string _cmdGetAll = @"SELECT * FROM Whrc_EmailAddress";
        private static string _cmdSelectById = @"SELECT * FROM Whrc_EmailAddress Where Id=?";
        private static string _cmdInsert = @"INSERT INTO Whrc_EmailAddress (MailAddress) VALUES (?); ";
        private static string _cmdUpdate = @"UPDATE Whrc_EmailAddress SET MailAddress=? WHERE Id=?";

        public List<EmailAddress> GetAll()
        {
            try
            {
                OdbcCommand cmd = new OdbcCommand(_cmdGetAll);
                DataSet ds = ExecuteQuery(cmd);
                return GetListFromDataSet(ds);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public EmailAddress GetById(long Id)
        {
            try
            {
                OdbcCommand cmd = new OdbcCommand(_cmdSelectById);

                cmd.Parameters.AddWithValue("@Id", Id);

                DataSet ds = ExecuteQuery(cmd);

                return GetListFromDataSet(ds)[0];
            }
            catch (Exception ex)
            {
                //Attempt to throw the exception for error handling
                throw ex;
            }
        }

        private List<EmailAddress> GetListFromDataSet(DataSet ds)
        {
            List<EmailAddress> EmailAddressObjList = new List<EmailAddress>();

            // This Loop reads all Data from DataSet                    
            for (int i = 0; i < ds.Tables["Data"].Rows.Count; i++)
            {
                EmailAddress EmailAddressObj = new EmailAddress();

                EmailAddressObj.Id = Convert.ToInt64(ds.Tables["Data"].Rows[i].ItemArray[0]);
                EmailAddressObj.MailAddress = Convert.ToString(ds.Tables["Data"].Rows[i].ItemArray[1]);

                EmailAddressObjList.Add(EmailAddressObj);
            }

            return EmailAddressObjList;
        }

        public void Add(EmailAddress EmailAddressObj)
        {
            long id = -1;
            try
            {
                OdbcCommand cmd = new OdbcCommand(_cmdInsert);

                cmd.Parameters.AddWithValue("@MailAddress", EmailAddressObj.MailAddress);

                id = Convert.ToInt64(ExecuteNonQueryAndScalar(cmd));

                EmailAddressObj.Id = id;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public void Add(EmailAddress EmailAddressObj, System.Data.Odbc.OdbcConnection conn, System.Data.Odbc.OdbcTransaction tx)
        {
            long id = -1;
            try
            {
                OdbcCommand cmd = new OdbcCommand(_cmdInsert);

                cmd.Parameters.AddWithValue("@MailAddress", EmailAddressObj.MailAddress);

                id = Convert.ToInt64(ExecuteNonQueryAndScalar(cmd, conn, tx));

                EmailAddressObj.Id = id;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public void Update(EmailAddress EmailAddressObj)
        {
            try
            {
                OdbcCommand cmd = new OdbcCommand(_cmdUpdate);

                cmd.Parameters.AddWithValue("@MailAddress", EmailAddressObj.MailAddress);

                cmd.Parameters.AddWithValue("@Id", EmailAddressObj.Id);

                ExecuteNonQuery(cmd);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
