﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.Odbc;
using System.Collections;
using daoProject.Infrastructure;
using daoProject.Model;
using daoProject.Mapper;

namespace daoProject.Mapper
{
    internal class InvoicePackageDataMapper : SqlHelper
    {
        private static string _cmdInsert = @"INSERT INTO Whrc_InvoicePackage (InvoiceId,MembershipId,Price,Description,DateJoined,DateValidTill,StaffInitial) VALUES (?,?,?,?,?,?,?); ";
        private static string _cmdUpdate = @"UPDATE Whrc_InvoicePackage SET InvoiceId=?,MembershipId=?,Price=?,Description=?,DateJoined=?,DateValidTill=?,StaffInitial=? WHERE Id=? ";
        private static string _cmdDelete = @"DELETE FROM Whrc_InvoicePackage WHERE Id=? ";
        private static string _cmdSelectAll = @"SELECT * FROM Whrc_InvoicePackage ";
        private static string _cmdSelectById = @"SELECT * FROM Whrc_InvoicePackage WHERE Id=? ";
        private static string _cmdSelectByInvoiceId = @"SELECT * FROM Whrc_InvoicePackage WHERE InvoiceId=? ";
        private static string _cmdDeleteByInvoiceId = @"DELETE FROM Whrc_InvoicePackage WHERE InvoiceId=? ";

        internal void Create(InvoicePackage InvoicePackage)
        {
            long id = -1;
            try
            {
                OdbcCommand cmd = new OdbcCommand(_cmdInsert);

                cmd.Parameters.AddWithValue("@InvoiceId", InvoicePackage.InvoiceID);
                cmd.Parameters.AddWithValue("@PackageID", InvoicePackage.PackageID);

                //Execute the OdbcCommand
                id = Convert.ToInt64(ExecuteNonQueryAndScalar(cmd));

                InvoicePackage.Id = id;
            }
            catch (Exception ex)
            {
                //Attempt to throw the exception for error handling
                throw ex;
            }
        }

        internal void Create(InvoicePackage InvoicePackage, System.Data.Odbc.OdbcConnection conn, System.Data.Odbc.OdbcTransaction tx)
        {
            long id = -1;
            try
            {
                OdbcCommand cmd = new OdbcCommand(_cmdInsert);

                cmd.Parameters.AddWithValue("@InvoiceId", InvoicePackage.InvoiceID);
                cmd.Parameters.AddWithValue("@PackageID", InvoicePackage.PackageID);

                //Execute the OdbcCommand
                id = Convert.ToInt64(ExecuteNonQueryAndScalar(cmd, conn, tx));

                InvoicePackage.Id = id;
            }
            catch (Exception ex)
            {
                //Attempt to throw the exception for error handling
                throw ex;
            }
        }

        internal void Update(InvoicePackage InvoicePackage)
        {
            try
            {
                OdbcCommand cmd = new OdbcCommand(_cmdUpdate);

                cmd.Parameters.AddWithValue("@InvoiceId", InvoicePackage.InvoiceID);
                cmd.Parameters.AddWithValue("@PackageID", InvoicePackage.PackageID);
                cmd.Parameters.AddWithValue("@Id", InvoicePackage.Id);

                //Execute the OdbcCommand
                ExecuteNonQuery(cmd);
            }
            catch (Exception ex)
            {
                //Attempt to throw the exception for error handling
                throw ex;
            }
        }

        internal bool Delete(long id)
        {
            bool success = false;

            try
            {
                OdbcCommand cmd = new OdbcCommand(_cmdDelete);

                cmd.Parameters.AddWithValue("@Id", id);

                ExecuteNonQuery(cmd);

                success = true;
            }
            catch (Exception ex)
            {
                //Attempt to throw the exception for error handling
                throw ex;
            }

            return success;
        }

        internal List<InvoicePackage> GetAll()
        {
            try
            {
                //Create instance of sqlcommand
                OdbcCommand cmd = new OdbcCommand(_cmdSelectAll);

                DataSet ds = ExecuteQuery(cmd);

                return GetListFromDataSet(ds);
            }
            catch (Exception ex)
            {
                //Attempt to throw the exception for error handling
                throw ex;
            }
        }

        internal InvoicePackage GetById(long id)
        {
            try
            {
                OdbcCommand cmd = new OdbcCommand(_cmdSelectById);

                cmd.Parameters.AddWithValue("@Id", id);

                DataSet ds = ExecuteQuery(cmd);

                return GetListFromDataSet(ds)[0];
            }
            catch (Exception ex)
            {
                //Attempt to throw the exception for error handling
                throw ex;
            }
        }

        internal List<InvoicePackage> GetListByInvoiceId(long invoiceId)
        {
            try
            {
                //Create instance of sqlcommand
                OdbcCommand cmd = new OdbcCommand(_cmdSelectByInvoiceId);

                cmd.Parameters.AddWithValue("@InvoiceId", invoiceId);

                DataSet ds = ExecuteQuery(cmd);

                return GetListFromDataSet(ds);
            }
            catch (Exception ex)
            {
                //Attempt to throw the exception for error handling
                throw ex;
            }
        }

        private List<InvoicePackage> GetListFromDataSet(DataSet ds)
        {
            List<InvoicePackage> invoicePackageList = new List<InvoicePackage>();

            // This Loop reads all Data from DataSet                    
            for (int i = 0; i < ds.Tables["Data"].Rows.Count; i++)
            {
                InvoicePackage invoicePackage = new InvoicePackage();

                invoicePackage.Id = Convert.ToInt64(ds.Tables["Data"].Rows[i].ItemArray[0]);
                invoicePackage.InvoiceID = Convert.ToInt64(ds.Tables["Data"].Rows[i].ItemArray[1]);
                invoicePackage.PackageID = Convert.ToInt64(ds.Tables["Data"].Rows[i].ItemArray[2]);
                invoicePackageList.Add(invoicePackage);
            }

            return invoicePackageList;
        }

        internal bool DeleteByInvoiceId(long invoiceId)
        {
            bool success = false;

            try
            {
                OdbcCommand cmd = new OdbcCommand(_cmdDeleteByInvoiceId);

                cmd.Parameters.AddWithValue("@InvoiceId", invoiceId);

                ExecuteNonQuery(cmd);

                success = true;
            }
            catch (Exception ex)
            {
                //Attempt to throw the exception for error handling
                throw ex;
            }

            return success;
        }
    }
}
