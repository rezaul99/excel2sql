﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.Odbc;
using System.Collections;
using daoProject.Infrastructure;
using daoProject.Model;
using daoProject.Mapper;

namespace daoProject.Mapper
{
    internal class WalkInDataMapper : SqlHelper
    {
        private static string _cmdSelectAllWalkInUsers = @"SELECT * FROM Whrc_WalkIn ";
        private static string _cmdSelectWalkInUserByEmail = @"SELECT * FROM Whrc_WalkIn WHERE Email=? ";
        private static string _cmdSelectWalkInUserById = @"SELECT * FROM Whrc_WalkIn WHERE Id=? ";
        private static string _cmdInsertWalkInUser = @"INSERT INTO Whrc_WalkIn (FirstName,MiddleName,LastName,Email,Address,Phone) VALUES (?,?,?,?,?,?); ";
       // private static string _cmdUpdateUser = @"UPDATE Whrc_Users SET UserRecordId=?, UserType=?, FirstName=?, LastName=?, Email=?, Password=?,MiddleName=?,Address1=?,AD1City=?,AD1State=?,AD1Zip=?,Address2=?,AD2City=?,AD2Zip=?,DOB=?,DueDate=?,Insurance=?,Hospital=?,HospitalPractice=?,Notes=?,AreaCode=?,Phone=?,PhoneType=?,PhoneNotes=?,Active=? WHERE Id=?";
        private static string _cmdSearchUserByInput = @"select * from Whrc_WalkIn where FirstName like ? or LastName like ? or Email like ?";
        private static string _cmdDeleteWalkInUserById = "Delete from Whrc_WalkIn where id=? ";


        internal void Create(WalkIn usermodel)
        {
            long id = -1;
            try
            {
                //This property create variable of OdbcCommand
                OdbcCommand comuser = new OdbcCommand();

                // Sql query for Insert Data into User
                comuser.CommandText = _cmdInsertWalkInUser;

                //Set value for parameter property
                comuser.Parameters.AddWithValue("@FirstName", usermodel.FirstName); 
                comuser.Parameters.AddWithValue("@MiddleName", usermodel.MiddleName);
                comuser.Parameters.AddWithValue("@LastName", usermodel.LastName);
                comuser.Parameters.AddWithValue("@Email", usermodel.Email);
                comuser.Parameters.AddWithValue("@Address", usermodel.Address);
                comuser.Parameters.AddWithValue("@Phone", usermodel.Phone);

                //OdbcCommand Type
                comuser.CommandType = CommandType.Text;

                //Execute the OdbcCommand
                id = Convert.ToInt64(ExecuteNonQueryAndScalar(comuser));
                usermodel.Id = id;
            }
            catch (Exception ex)
            {
                //Attempt to throw the exception for error handling
                throw ex;
            }
        }

        //internal bool Update(User userObj)
        //{
        //    bool result = false;
        //    try
        //    {
        //        //This property creates variable of OdbcCommand
        //        OdbcCommand userUpdateCommand = new OdbcCommand();

        //        //Sql query for Update Data of User
        //        userUpdateCommand.CommandText = _cmdUpdateUser;

        //        //Set value for parameter property
        //        if (userObj.UserRecordId == 0)
        //        {
        //            userUpdateCommand.Parameters.AddWithValue("@UserRecordId", DBNull.Value);
        //        }
        //        else
        //        {
        //            userUpdateCommand.Parameters.AddWithValue("@UserRecordId", userObj.UserRecordId);
        //        }
        //        userUpdateCommand.Parameters.AddWithValue("@UserType", userObj.UserType);
        //        userUpdateCommand.Parameters.AddWithValue("@FirstName", userObj.FirstName);
        //        userUpdateCommand.Parameters.AddWithValue("@LastName", userObj.LastName);
        //        userUpdateCommand.Parameters.AddWithValue("@Email", userObj.Email);
        //        userUpdateCommand.Parameters.AddWithValue("@Password", userObj.Password);
        //        userUpdateCommand.Parameters.AddWithValue("@MiddleName", userObj.MiddleName);
        //        userUpdateCommand.Parameters.AddWithValue("@Address1", userObj.Address1);
        //        userUpdateCommand.Parameters.AddWithValue("@AD1City", userObj.Ad1City);
        //        userUpdateCommand.Parameters.AddWithValue("@AD1State", userObj.Ad1State);
        //        userUpdateCommand.Parameters.AddWithValue("@AD1Zip", userObj.Ad1Zip);
        //        userUpdateCommand.Parameters.AddWithValue("@Address2", userObj.Address2);
        //        userUpdateCommand.Parameters.AddWithValue("@AD2City", userObj.Ad2City);
        //        userUpdateCommand.Parameters.AddWithValue("@AD2Zip", userObj.Ad2Zip);
        //        userUpdateCommand.Parameters.AddWithValue("@DOB", userObj.DateOfBirth);
        //        userUpdateCommand.Parameters.AddWithValue("@DueDate", userObj.DueDate);
        //        userUpdateCommand.Parameters.AddWithValue("@Insurance", userObj.Insurance);
        //        userUpdateCommand.Parameters.AddWithValue("@Hospital", userObj.Hospital);
        //        userUpdateCommand.Parameters.AddWithValue("@HospitalPractice", userObj.HospitalPractice);
        //        userUpdateCommand.Parameters.AddWithValue("@Notes", userObj.Notes);
        //        userUpdateCommand.Parameters.AddWithValue("@AreaCode", userObj.AreaCode);
        //        userUpdateCommand.Parameters.AddWithValue("@Phone", userObj.phone);
        //        userUpdateCommand.Parameters.AddWithValue("@PhoneType", userObj.phoneType);
        //        userUpdateCommand.Parameters.AddWithValue("@PhoneNotes", userObj.PhoneNotes);
        //        userUpdateCommand.Parameters.AddWithValue("@Active", userObj.Active);

        //        userUpdateCommand.Parameters.AddWithValue("@Id", userObj.Id);

        //        // Pass the parameter to ExecuteNonQuery from execute the sqlcommand
        //        ExecuteNonQuery(userUpdateCommand);
        //        result = true;
        //        return result;
        //    }
        //    catch (Exception ex)
        //    {
        //        //Attempt to throw the exception for error handling
        //        return result;
        //        throw ex;
        //    }
        //}

        internal List<WalkIn> GetAll()
        {
            try
            {
                //Create instance of sqlcommand
                OdbcCommand cmd = new OdbcCommand();

                // Set sqlquery stirng on sqlcommand text
                cmd.CommandText = _cmdSelectAllWalkInUsers;

                DataSet dsUser = ExecuteQuery(cmd);

                return GetUserListFromDataSet(dsUser);
            }
            catch (Exception ex)
            {
                //Attempt to throw the exception for error handling
                throw ex;
            }
        }

        //#region IDataMapper<User> Members
        internal WalkIn GetByEmailId(string email)//Bypass
        {
            try
            {
                //Create instance of sqlcommand
                WalkIn user = null;
                OdbcCommand cmd = new OdbcCommand();

                // Set sqlquery stirng on sqlcommand text
                cmd.CommandText = _cmdSelectWalkInUserByEmail;
                cmd.Parameters.AddWithValue("@Email", email);

                DataSet dsUser = ExecuteQuery(cmd);

                // return the HashTable with key ID
                List<WalkIn> userList = GetUserListFromDataSet(dsUser);
                if (userList.Count > 0)
                {
                    user = userList[0];
                }
                return user;
            }
            catch (Exception ex)
            {
                //Attempt to throw the exception for error handling
                throw ex;
            }
        }

        internal WalkIn GetUserById(long id)//Bypass
        {
            try
            {
                //Create instance of sqlcommand
                WalkIn user = null;
                OdbcCommand cmd = new OdbcCommand();

                // Set sqlquery stirng on sqlcommand text
                cmd.CommandText = _cmdSelectWalkInUserById;
                cmd.Parameters.AddWithValue("@Id", id);

                DataSet dsUser = ExecuteQuery(cmd);

                // return the HashTable with key ID
                List<WalkIn> userList = GetUserListFromDataSet(dsUser);
                if (userList.Count > 0)
                {
                    user = userList[0];
                }
                return user;
            }
            catch (Exception ex)
            {
                //Attempt to throw the exception for error handling
                throw ex;
            }
        }

        private List<WalkIn> GetUserListFromDataSet(DataSet dsUser)
        {
            List<WalkIn> walkInUserList = new List<WalkIn>();

            // This Loop reads all Data from DataSet                    
            for (int i = 0; i < dsUser.Tables["Data"].Rows.Count; i++)
            {
                WalkIn walkInUser = new WalkIn();

                walkInUser.Id = Convert.ToInt64(dsUser.Tables["Data"].Rows[i].ItemArray[0]);
                walkInUser.FirstName = Convert.ToString(dsUser.Tables["Data"].Rows[i].ItemArray[1]);
                walkInUser.MiddleName = Convert.ToString(dsUser.Tables["Data"].Rows[i].ItemArray[2]);
                walkInUser.LastName = Convert.ToString(dsUser.Tables["Data"].Rows[i].ItemArray[3]);
                walkInUser.Email = Convert.ToString(dsUser.Tables["Data"].Rows[i].ItemArray[4]);
                walkInUser.Address = Convert.ToString(dsUser.Tables["Data"].Rows[i].ItemArray[5]);
                walkInUser.Phone = Convert.ToString(dsUser.Tables["Data"].Rows[i].ItemArray[6]);
               
                //userUpdateCommand.Parameters.AddWithValue("@PhoneNotes", userObj.PhoneNotes);

                walkInUserList.Add(walkInUser);
            }
            return walkInUserList;
        }
        //#endregion Private Methods

        public List<WalkIn> SearchUserByInput(string searchString)
        {
            try
            {
                OdbcCommand cmd = new OdbcCommand();

                cmd.CommandText = _cmdSearchUserByInput;
                searchString = "%" + searchString + "%";
                cmd.Parameters.AddWithValue("@FirstName", searchString);
                cmd.Parameters.AddWithValue("@LastName", searchString);
                cmd.Parameters.AddWithValue("@Email", searchString);

                DataSet dsUser = ExecuteQuery(cmd);

                return GetUserListFromDataSet(dsUser);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
