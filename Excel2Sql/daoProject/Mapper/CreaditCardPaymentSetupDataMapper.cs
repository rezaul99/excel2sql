﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.Odbc;
using System.Collections;
using daoProject.Infrastructure;
using daoProject.Model;
using daoProject.Mapper;

namespace daoProject.Mapper
{

    internal class CreaditCardPaymentSetupDataMapper : SqlHelper
    {
        private static string _cmdInsert = @"INSERT INTO WHRC_CreditCardPaymentSetup (MarchentLoginID,MarchentTransactionKey,PaymentGateway,ImplementationMode,MarchentPassword,GatewayTypeId,IsActive) VALUES (?,?,?,?,?,?,?); ";
        private static string _cmdUpdate = @"UPDATE WHRC_CreditCardPaymentSetup SET MarchentLoginID=?,MarchentTransactionKey=?,PaymentGateway=?,ImplementationMode=?,MarchentPassword=?, GatewayTypeId=?, IsActive=? WHERE Id=? ";
        private static string _cmdUpdateActiveStatusforOthers = @"UPDATE WHRC_CreditCardPaymentSetup SET  IsActive=0 WHERE Id != ? ";
        private static string _cmdDelete = @"DELETE FROM WHRC_CreditCardPaymentSetup WHERE Id=? ";
        private static string _cmdSelectAll = @"SELECT * FROM WHRC_CreditCardPaymentSetup ";
        private static string _cmdSelectById = @"SELECT * FROM WHRC_CreditCardPaymentSetup WHERE Id=? ";

        internal void Create(CreditCardPaymentSetup creditCardPaymentSetup)
        {
            long id = -1;
            try
            {
                OdbcCommand cmd = new OdbcCommand(_cmdInsert);

                cmd.Parameters.AddWithValue("@MarchentLoginID", creditCardPaymentSetup.MarchentLoginID);
                cmd.Parameters.AddWithValue("@MarchentTransactionKey", creditCardPaymentSetup.MarchentTransactionKey);
                cmd.Parameters.AddWithValue("@PaymentGateway", creditCardPaymentSetup.PaymentGatewayType);
                cmd.Parameters.AddWithValue("@ImplementationMode", creditCardPaymentSetup.ImplementationMode);//MarchentPassword
                cmd.Parameters.AddWithValue("@MarchentPassword", creditCardPaymentSetup.MarchentPassword);
                cmd.Parameters.AddWithValue("@GatewayTypeId", creditCardPaymentSetup.GatewayID);
                cmd.Parameters.AddWithValue("@IsActive", creditCardPaymentSetup.isActive);
                //Execute the OdbcCommand
                id = Convert.ToInt64(ExecuteNonQueryAndScalar(cmd));

                creditCardPaymentSetup.Id = id;
            }
            catch (Exception ex)
            {
                //Attempt to throw the exception for error handling
                throw ex;
            }
        }

        internal void Update(CreditCardPaymentSetup creditCardPaymentSetup)
        {
            try
            {
                OdbcCommand cmd = new OdbcCommand(_cmdUpdate);

                cmd.Parameters.AddWithValue("@MarchentLoginID", creditCardPaymentSetup.MarchentLoginID);
                cmd.Parameters.AddWithValue("@MarchentTransactionKey", creditCardPaymentSetup.MarchentTransactionKey);
                cmd.Parameters.AddWithValue("@PaymentGateway", creditCardPaymentSetup.PaymentGatewayType);
                cmd.Parameters.AddWithValue("@ImplementationMode", creditCardPaymentSetup.ImplementationMode);
                cmd.Parameters.AddWithValue("@MarchentPassword", creditCardPaymentSetup.MarchentPassword);
                 cmd.Parameters.AddWithValue("@GatewayTypeId", creditCardPaymentSetup.GatewayID);
                 cmd.Parameters.AddWithValue("@isActive", creditCardPaymentSetup.isActive);
                cmd.Parameters.AddWithValue("@Id", creditCardPaymentSetup.Id);
                 
                //Execute the OdbcCommand
                ExecuteNonQuery(cmd);
            }
            catch (Exception ex)
            {
                //Attempt to throw the exception for error handling
                throw ex;
            }
        }
        internal void UpdateActiveStatusforOthers(CreditCardPaymentSetup creditCardPaymentSetup)
        {
            try
            {
                OdbcCommand cmd = new OdbcCommand(_cmdUpdateActiveStatusforOthers);

                cmd.Parameters.AddWithValue("@Id", creditCardPaymentSetup.Id);

                //Execute the OdbcCommand
                ExecuteNonQuery(cmd);
            }
            catch (Exception ex)
            {
                //Attempt to throw the exception for error handling
                throw ex;
            }
        }
        internal bool Delete(long id)
        {
            bool success = false;

            try
            {
                OdbcCommand cmd = new OdbcCommand(_cmdDelete);

                cmd.Parameters.AddWithValue("@Id", id);

                ExecuteNonQuery(cmd);

                success = true;
            }
            catch (Exception ex)
            {
                //Attempt to throw the exception for error handling
                throw ex;
            }

            return success;
        }

        private List<CreditCardPaymentSetup> GetListFromDataSet(DataSet ds)
        {
            List<CreditCardPaymentSetup> creditCardPaymentSetupList = new List<CreditCardPaymentSetup>();

            // This Loop reads all Data from DataSet                    
            for (int i = 0; i < ds.Tables["Data"].Rows.Count; i++)
            {
                CreditCardPaymentSetup creditCardPaymentSetup = new CreditCardPaymentSetup();

                creditCardPaymentSetup.Id = Convert.ToInt64(ds.Tables["Data"].Rows[i].ItemArray[0]);
                creditCardPaymentSetup.MarchentLoginID = Convert.ToString(ds.Tables["Data"].Rows[i].ItemArray[1]);
                creditCardPaymentSetup.MarchentTransactionKey = Convert.ToString(ds.Tables["Data"].Rows[i].ItemArray[2]);
                creditCardPaymentSetup.PaymentGatewayType = (PaymentGatewayType)Convert.ToInt32(ds.Tables["Data"].Rows[i].ItemArray[3]);
                if (creditCardPaymentSetup.PaymentGatewayType == PaymentGatewayType.OnSite)
                {
                    creditCardPaymentSetup.PaymentGatewayTypeStr = "On Site";
                }
                else
                {
                    creditCardPaymentSetup.PaymentGatewayTypeStr = "By AuthorizeDotNet";
                }
                if (ds.Tables["Data"].Rows[i].ItemArray[4] != null)
                {
                    creditCardPaymentSetup.ImplementationMode = Convert.ToString(ds.Tables["Data"].Rows[i].ItemArray[4]);
                }
                else
                {
                    creditCardPaymentSetup.ImplementationMode = "Test";
                }
                if (ds.Tables["Data"].Rows[i].ItemArray[5] != null)
                {
                    creditCardPaymentSetup.MarchentPassword= Convert.ToString(ds.Tables["Data"].Rows[i].ItemArray[5]);
                }
                else
                {
                    creditCardPaymentSetup.MarchentPassword = "";
                }
                if (ds.Tables["Data"].Rows[i].ItemArray[6] != null)
                {
                    creditCardPaymentSetup.GatewayID = Convert.ToInt32(ds.Tables["Data"].Rows[i].ItemArray[6]);
                }
                else
                {
                    creditCardPaymentSetup.GatewayID = 0;
                }
                if (ds.Tables["Data"].Rows[i].ItemArray[7] != null)
                {
                    creditCardPaymentSetup.isActive = Convert.ToInt32(ds.Tables["Data"].Rows[i].ItemArray[7]);
                }
                else
                {
                    creditCardPaymentSetup.isActive = 0;
                }
                creditCardPaymentSetupList.Add(creditCardPaymentSetup);
            }

            return creditCardPaymentSetupList;
        }

        internal List<CreditCardPaymentSetup> GetAll()
        {
            try
            {
                //Create instance of sqlcommand
                OdbcCommand cmd = new OdbcCommand(_cmdSelectAll);

                DataSet ds = ExecuteQuery(cmd);

                return GetListFromDataSet(ds);
            }
            catch (Exception ex)
            {
                //Attempt to throw the exception for error handling
                throw ex;
            }
        }

        internal CreditCardPaymentSetup GetById(long id)
        {
            try
            {
                OdbcCommand cmd = new OdbcCommand(_cmdSelectById);

                cmd.Parameters.AddWithValue("@Id", id);

                DataSet ds = ExecuteQuery(cmd);

                return GetListFromDataSet(ds)[0];
            }
            catch (Exception ex)
            {
                //Attempt to throw the exception for error handling
                throw ex;
            }
        }
    }

}
