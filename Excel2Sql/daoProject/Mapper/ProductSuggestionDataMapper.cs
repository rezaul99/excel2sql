﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using daoProject.Infrastructure;
using daoProject.Model;
using System.Data.Odbc;
using System.Data;
using daoProject.Service;

namespace daoProject.Mapper
{
    class ProductSuggestionDataMapper : SqlHelper
    {
        private static string _cmdInsert = @"INSERT INTO Whrc_Package (PackageName,PackageDescription,PackageType,DiscountAmount,Active,CreateDate,UpdateDate) VALUES (?,?,?,?,?,?,?); ";
        private static string _cmdUpdate = @"UPDATE Whrc_Package SET PackageName=?,PackageDescription=?,PackageType=?,DiscountAmount=?,Active=?,CreateDate=?,UpdateDate=? WHERE Id=? ";
        private static string _cmdDelete = @"DELETE FROM Whrc_Package WHERE Id=? ";
        private static string _cmdSelectAll = @"SELECT * FROM Whrc_Package WHERE PackageType=?";
        private static string _cmdSelectById = @"SELECT * FROM Whrc_Package WHERE Id=? ";
        private static string _cmdSelectByClientId = @"SELECT * FROM Whrc_Invoice WHERE ClientId=? ";

        private static string _cmdGetInvoiceListByOrderStatus = @"SELECT * FROM Whrc_Invoice WHERE OrderStatus=? ";
        private static string _cmdGetInvoiceListForClientByOrderStatus = @"SELECT * FROM Whrc_Invoice WHERE ClientId=? And OrderStatus=? ";
        private static string _cmdGetInvoiceListByClientName = @"Select Whrc_Invoice.* From Whrc_Invoice,Whrc_Users Where Whrc_Invoice.ClientID=Whrc_Users.ID And (Whrc_Users.FirstName like ? Or Whrc_Users.LastName like ?)";
        private static string _cmdApplyDiscount = @"UPDATE Whrc_Invoice SET Discount=?,DiscountTypeId=?,DiscountSubTypeId=? WHERE Id=? ";
        private static string _cmdMakePayment = @"UPDATE Whrc_Invoice SET PaidAmount=?,PaymentMode=?,PaymentDate=?,PaymentNotes=? WHERE Id=? ";

        private static string _cmdUpdateTotalRefundAmount = @"UPDATE Whrc_Invoice SET TotalRefundAmount=? WHERE Id=? ";
        private static string _cmdGetInvoiceByStaffIdForCart = @"SELECT * FROM Whrc_Invoice WHERE InvoiceType=? And  OrderStatus=? And StaffInitial=?";
        private static string _cmdUpdateInvoiceClientID = @"UPDATE Whrc_Invoice SET ClientId=? WHERE Id=? ";

        private static string _cmdGetInvoiceByClientIDAndOrderType = @"SELECT * FROM Whrc_Invoice WHERE InvoiceType=? And  OrderStatus=? And ClientID=?";
        private static string _cmdGetAll = @"Select * FROM Whrc_Package";
        private static string _cmdGetPackagebyRecordId = @"select * from Whrc_Package where id in(select packageid from Whrc_PackageItem where recordid = ?) And Active = ? ";

        internal void Create(PackageWHRC packageWhrc)
        {
            long id = -1;
            try
            {
                OdbcCommand cmd = new OdbcCommand(_cmdInsert);

                cmd.Parameters.AddWithValue("@PackageName", packageWhrc.PackageName);
                cmd.Parameters.AddWithValue("@PackageDescription", packageWhrc.PackageDescription);
                cmd.Parameters.AddWithValue("@PackageType", packageWhrc.PackageType);
                cmd.Parameters.AddWithValue("@DiscountAmount", packageWhrc.DiscountAmount);
                cmd.Parameters.AddWithValue("@Active", packageWhrc.Active);
                cmd.Parameters.AddWithValue("@CreateDate", packageWhrc.CreateDate);
                cmd.Parameters.AddWithValue("@UpdateDate", packageWhrc.UpdateDate);

                //Execute the OdbcCommand
                id = Convert.ToInt64(ExecuteNonQueryAndScalar(cmd));

                packageWhrc.ID = id;
            }
            catch (Exception ex)
            {
                //Attempt to throw the exception for error handling
                throw ex;
            }
        }

        internal void Create(PackageWHRC packageWhrc, System.Data.Odbc.OdbcConnection conn, System.Data.Odbc.OdbcTransaction tx)
        {
            long id = -1;
            try
            {
                OdbcCommand cmd = new OdbcCommand(_cmdInsert);

                cmd.Parameters.AddWithValue("@PackageName", packageWhrc.PackageName);
                cmd.Parameters.AddWithValue("@PackageDescription", packageWhrc.PackageDescription);
                cmd.Parameters.AddWithValue("@PackageType", packageWhrc.PackageType);
                cmd.Parameters.AddWithValue("@DiscountAmount", packageWhrc.DiscountAmount);
                cmd.Parameters.AddWithValue("@Active", packageWhrc.Active);
                cmd.Parameters.AddWithValue("@CreateDate", packageWhrc.CreateDate);
                cmd.Parameters.AddWithValue("@UpdateDate", packageWhrc.UpdateDate);

                //Execute the OdbcCommand
                id = Convert.ToInt64(ExecuteNonQueryAndScalar(cmd, conn, tx));

                packageWhrc.ID = id;
            }
            catch (Exception ex)
            {
                //Attempt to throw the exception for error handling
                throw ex;
            }
        }

        internal void Update(PackageWHRC packageWhrc, System.Data.Odbc.OdbcConnection conn, System.Data.Odbc.OdbcTransaction tx)
        {
            try
            {
                OdbcCommand cmd = new OdbcCommand(_cmdUpdate);

                cmd.Parameters.AddWithValue("@PackageName", packageWhrc.PackageName);
                cmd.Parameters.AddWithValue("@PackageDescription", packageWhrc.PackageDescription);
                cmd.Parameters.AddWithValue("@PackageType", packageWhrc.PackageType);
                cmd.Parameters.AddWithValue("@DiscountAmount", packageWhrc.DiscountAmount);
                cmd.Parameters.AddWithValue("@Active", packageWhrc.Active);
                cmd.Parameters.AddWithValue("@CreateDate", packageWhrc.CreateDate);
                cmd.Parameters.AddWithValue("@UpdateDate", packageWhrc.UpdateDate);
                cmd.Parameters.AddWithValue("@Id", packageWhrc.ID);

                //Execute the OdbcCommand
                ExecuteNonQuery(cmd, conn, tx);
            }
            catch (Exception ex)
            {
                //Attempt to throw the exception for error handling
                throw ex;
            }
        }

        internal bool Delete(long id)
        {
            bool success = false;
            try
            {
                OdbcCommand cmd = new OdbcCommand(_cmdDelete);

                cmd.Parameters.AddWithValue("@Id", id);

                ExecuteNonQuery(cmd);

                success = true;
            }
            catch (Exception ex)
            {
                //Attempt to throw the exception for error handling
                throw ex;
            }

            return success;
        }

        private List<PackageWHRC> GetListFromDataSet(DataSet ds)
        {
            List<PackageWHRC> PackageWHRCList = new List<PackageWHRC>();

            // This Loop reads all Data from DataSet                    
            for (int i = 0; i < ds.Tables["Data"].Rows.Count; i++)
            {
                PackageWHRC packageWHRC = new PackageWHRC();

                packageWHRC.ID = Convert.ToInt64(ds.Tables["Data"].Rows[i].ItemArray[0]);
                packageWHRC.PackageName = Convert.ToString(ds.Tables["Data"].Rows[i].ItemArray[1]);
                packageWHRC.PackageDescription = Convert.ToString(ds.Tables["Data"].Rows[i].ItemArray[2]);
                int packageType = Convert.ToInt32(ds.Tables["Data"].Rows[i].ItemArray[3]);
                if (packageType == 0)
                {
                    packageWHRC.PackageType = PackageType.Product;
                }
                else if (packageType == 1)
                {
                    packageWHRC.PackageType = PackageType.Class;
                }
                packageWHRC.DiscountAmount = Convert.ToDecimal(ds.Tables["Data"].Rows[i].ItemArray[4]);
                packageWHRC.Active = Convert.ToBoolean(ds.Tables["Data"].Rows[i].ItemArray[5]);

                if (ds.Tables["Data"].Rows[i].ItemArray[6] != DBNull.Value)
                {
                    packageWHRC.CreateDate = Convert.ToDateTime(ds.Tables["Data"].Rows[i].ItemArray[6]);
                }
                else
                {
                    packageWHRC.CreateDate = Convert.ToDateTime("1/1/1900");
                }

                if (ds.Tables["Data"].Rows[i].ItemArray[7] != DBNull.Value)
                {
                    packageWHRC.UpdateDate = Convert.ToDateTime(ds.Tables["Data"].Rows[i].ItemArray[7]);
                }
                else
                {
                    packageWHRC.UpdateDate = Convert.ToDateTime("1/1/1900");
                }

                packageWHRC.PackageItemsWHRC = new PackageItemWHRCServiceImpl().GetByPackageId(packageWHRC.ID);

                PackageWHRCList.Add(packageWHRC);
            }

            return PackageWHRCList;
        }

        internal List<PackageWHRC> GetAll()
        {
            try
            {
                OdbcCommand cmd = new OdbcCommand(_cmdGetAll);
                DataSet ds = ExecuteQuery(cmd);
                return GetListFromDataSet(ds);
            }
            catch (Exception ex)
            {
                //Attempt to throw the exception for error handling
                throw ex;
            }
        }

        internal List<PackageWHRC> GetAll(PackageType packageType)
        {
            try
            {
                //Create instance of sqlcommand
                OdbcCommand cmd = new OdbcCommand(_cmdSelectAll);
                cmd.Parameters.AddWithValue("@PackageType", packageType);
                DataSet ds = ExecuteQuery(cmd);

                return GetListFromDataSet(ds);
            }
            catch (Exception ex)
            {
                //Attempt to throw the exception for error handling
                throw ex;
            }
        }

        internal PackageWHRC GetById(long id)
        {
            try
            {
                OdbcCommand cmd = new OdbcCommand(_cmdSelectById);

                cmd.Parameters.AddWithValue("@Id", id);
                DataSet ds = ExecuteQuery(cmd);
                return GetListFromDataSet(ds)[0];
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        internal List<PackageWHRC> GetPackageById(long id)
        {
            try
            {
                OdbcCommand cmd = new OdbcCommand(_cmdGetPackagebyRecordId);

                cmd.Parameters.AddWithValue("@recordid", id);
                cmd.Parameters.AddWithValue("@Active", "1");
                DataSet ds = ExecuteQuery(cmd);
                return GetListFromDataSet(ds);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
