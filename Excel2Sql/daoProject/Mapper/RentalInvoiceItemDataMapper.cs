﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.Odbc;
using System.Collections;
using daoProject.Infrastructure;
using daoProject.Model;
using daoProject.Mapper;

namespace daoProject.Mapper
{
    internal class RentalInvoiceItemDataMapper : SqlHelper
    {

        private static string _cmdInsert = @"INSERT INTO Whrc_RentalInvoiceItem (InvoiceId,RentalEquipmentID,AgreementNo,REStartingDate,RESerial,REVendorName,ModelName,REEndingDate,REConditionWhenReturned,StuffInitials,RPStartingDate,RPEndingDate,RPDescription,RPPreTaxAmount,RPSalesTax,RPTotal,DepositAmount, IsItemReturned, ReturnDate, IsRentAmountRefundable, RefundAmount, RentalItemStatus, IsRefundProcessed,RefundedDate,OverdueCharge,IsExtensionRequestByClient, DateExtensionRequestByClient, ExtensionDays, ExtensionPreTaxAmount, ExtensionSalesTax, ExtensionTotalAmount, IsExtensionApprovedByAdmin, DateExtensionApprovedByAdmin, RegularFee) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?); ";
        private static string _cmdUpdate = @"UPDATE Whrc_RentalInvoiceItem SET InvoiceId=?,RentalEquipmentID=?,AgreementNo=?,REStartingDate=?,RESerial=?,REVendorName=?,ModelName=?,REEndingDate=?,REConditionWhenReturned=?,StuffInitials=?,RPStartingDate=?,RPEndingDate=?,RPDescription=?,RPPreTaxAmount=?,RPSalesTax=?,RPTotal=?,DepositAmount=?, IsItemReturned=?, ReturnDate=?, IsRentAmountRefundable=?, RefundAmount=?, RentalItemStatus=?,IsRefundProcessed=?,RefundedDate=?,OverdueCharge=?,IsExtensionRequestByClient=?, DateExtensionRequestByClient=?, ExtensionDays=?, ExtensionPreTaxAmount=?, ExtensionSalesTax=?, ExtensionTotalAmount=?, IsExtensionApprovedByAdmin=?, DateExtensionApprovedByAdmin=?, RegularFee=? WHERE Id=? ";
        private static string _cmdDelete = @"DELETE FROM Whrc_RentalInvoiceItem WHERE Id=? ";
        private static string _cmdSelectAll = @"SELECT * FROM Whrc_RentalInvoiceItem ";
        private static string _cmdSelectById = @"SELECT * FROM Whrc_RentalInvoiceItem WHERE Id=? ";
        private static string _cmdSelectByInvoiceId = @"SELECT * FROM Whrc_RentalInvoiceItem WHERE InvoiceId=? ";
        private static string _cmdDeleteByInvoiceId = @"DELETE FROM Whrc_RentalInvoiceItem WHERE InvoiceId=? ";
        private static string _cmdgetItemAmountPriceByID = @"SELECT RPTotal FROM Whrc_RentalInvoiceItem WHERE Id=? ";
        private static string _cmdgetItemAmountDepositPriceByID = @"SELECT DepositAmount FROM Whrc_RentalInvoiceItem WHERE Id=? ";
        private static string _cmdDeleteByRentalId = @"DELETE FROM Whrc_RentalInvoiceItem WHERE Id=? ";
        private static string _cmdReturnItem = @"UPDATE Whrc_RentalInvoiceItem SET IsItemReturned=?,ReturnDate=?,IsRentAmountRefundable=?,RefundAmount=?,RentalItemStatus=?,OverdueCharge=?  WHERE Id=? ";
        private static string _cmdUpdateRefundProcessed = @"UPDATE Whrc_RentalInvoiceItem SET IsRefundProcessed=?,RefundedDate=?,RefundAmount=? WHERE Id=? ";
        private static string _cmdUpdateRegularfee = @"UPDATE Whrc_RentalInvoiceItem SET RegularFee=? WHERE Id=? ";
        private static string _cmdUpdateRentalInvoiceId = @"UPDATE Whrc_RentalInvoiceItem SET InvoiceId=? WHERE Id=? ";
        private static string _cmdUpdateRentalExtensionRequest = @"UPDATE Whrc_RentalInvoiceItem SET IsExtensionRequestByClient=?, DateExtensionRequestByClient=?, ExtensionDays=?, ExtensionPreTaxAmount=?, ExtensionSalesTax=?, ExtensionTotalAmount=?, IsExtensionApprovedByAdmin=?, DateExtensionApprovedByAdmin=? WHERE Id=? ";
        private static string _cmdUpdateRentalItemStatus = @"UPDATE Whrc_RentalInvoiceItem SET RentalItemStatus=?  WHERE Id=? ";
        
        internal void Create(RentalInvoiceItem rentalInvoiceItem)
        {
            long id = -1;
            try
            {
                OdbcCommand cmd = new OdbcCommand(_cmdInsert);

                cmd.Parameters.AddWithValue("@InvoiceId", rentalInvoiceItem.InvoiceId);
                cmd.Parameters.AddWithValue("@RentalEquipmentID", rentalInvoiceItem.RentalEquipmentID);
                cmd.Parameters.AddWithValue("@AgreementNo", rentalInvoiceItem.AgreementNo);
                cmd.Parameters.AddWithValue("@REStartingDate", rentalInvoiceItem.REStartingDate);
                cmd.Parameters.AddWithValue("@RESerial", rentalInvoiceItem.RESerial);
                cmd.Parameters.AddWithValue("@REVendorName", rentalInvoiceItem.REVendorName);
                cmd.Parameters.AddWithValue("@ModelName", rentalInvoiceItem.ModelName);
                cmd.Parameters.AddWithValue("@REEndingDate", rentalInvoiceItem.REEndingDate);
                cmd.Parameters.AddWithValue("@REConditionWhenReturned", rentalInvoiceItem.REConditionWhenReturned);
                cmd.Parameters.AddWithValue("@StuffInitials", rentalInvoiceItem.StuffInitials);
                cmd.Parameters.AddWithValue("@RPStartingDate", rentalInvoiceItem.RPStartingDate);
                cmd.Parameters.AddWithValue("@RPEndingDate", rentalInvoiceItem.RPEndingDate);
                cmd.Parameters.AddWithValue("@RPDescription", rentalInvoiceItem.RPDescription);
                cmd.Parameters.AddWithValue("@RPPreTaxAmount", rentalInvoiceItem.RPPreTaxAmount);
                cmd.Parameters.AddWithValue("@RPSalesTax", rentalInvoiceItem.RPSalesTax);
                cmd.Parameters.AddWithValue("@RPTotal", rentalInvoiceItem.RPTotal);
                cmd.Parameters.AddWithValue("@DepositAmount", rentalInvoiceItem.DepositAmount);

                cmd.Parameters.AddWithValue("@IsItemReturned", rentalInvoiceItem.IsItemReturned);
                cmd.Parameters.AddWithValue("@ReturnDate", rentalInvoiceItem.ReturnDate);
                cmd.Parameters.AddWithValue("@IsRentAmountRefundable", rentalInvoiceItem.IsRentAmountRefundable);
                cmd.Parameters.AddWithValue("@RefundAmount", rentalInvoiceItem.RefundAmount);
                cmd.Parameters.AddWithValue("@RentalItemStatus", rentalInvoiceItem.RentalItemStatus);
                cmd.Parameters.AddWithValue("@IsRefundProcessed", rentalInvoiceItem.IsRefundProcessed);
                cmd.Parameters.AddWithValue("@RefundedDate", rentalInvoiceItem.RefundedDate);
                cmd.Parameters.AddWithValue("@OverdueCharge", rentalInvoiceItem.OverdueCharge);

                cmd.Parameters.AddWithValue("@IsExtensionRequestByClient", rentalInvoiceItem.IsExtensionRequestByClient);
                cmd.Parameters.AddWithValue("@DateExtensionRequestByClient", rentalInvoiceItem.DateExtensionRequestByClient);
                cmd.Parameters.AddWithValue("@ExtensionDays", rentalInvoiceItem.ExtensionDays);
                cmd.Parameters.AddWithValue("@ExtensionPreTaxAmount", rentalInvoiceItem.ExtensionPreTaxAmount);
                cmd.Parameters.AddWithValue("@ExtensionSalesTax", rentalInvoiceItem.ExtensionSalesTax);
                cmd.Parameters.AddWithValue("@ExtensionTotalAmount", rentalInvoiceItem.ExtensionTotalAmount);
                cmd.Parameters.AddWithValue("@IsExtensionApprovedByAdmin", rentalInvoiceItem.IsExtensionApprovedByAdmin);
                cmd.Parameters.AddWithValue("@DateExtensionApprovedByAdmin", rentalInvoiceItem.DateExtensionApprovedByAdmin);//
                cmd.Parameters.AddWithValue("@RegularFee", rentalInvoiceItem.RegularFee);

                //Execute the OdbcCommand
                id = Convert.ToInt64(ExecuteNonQueryAndScalar(cmd));

                rentalInvoiceItem.Id = id;
            }
            catch (Exception ex)
            {
                //Attempt to throw the exception for error handling
                throw ex;
            }
        }

        internal void Create(RentalInvoiceItem rentalInvoiceItem, System.Data.Odbc.OdbcConnection conn, System.Data.Odbc.OdbcTransaction tx)
        {
            long id = -1;
            try
            {
                OdbcCommand cmd = new OdbcCommand(_cmdInsert);

                cmd.Parameters.AddWithValue("@InvoiceId", rentalInvoiceItem.InvoiceId);
                cmd.Parameters.AddWithValue("@RentalEquipmentID", rentalInvoiceItem.RentalEquipmentID);
                cmd.Parameters.AddWithValue("@AgreementNo", rentalInvoiceItem.AgreementNo);
                cmd.Parameters.AddWithValue("@REStartingDate", rentalInvoiceItem.REStartingDate);
                cmd.Parameters.AddWithValue("@RESerial", rentalInvoiceItem.RESerial);
                cmd.Parameters.AddWithValue("@REVendorName", rentalInvoiceItem.REVendorName);
                cmd.Parameters.AddWithValue("@ModelName", rentalInvoiceItem.ModelName);
                cmd.Parameters.AddWithValue("@REEndingDate", rentalInvoiceItem.REEndingDate);
                cmd.Parameters.AddWithValue("@REConditionWhenReturned", rentalInvoiceItem.REConditionWhenReturned);
                cmd.Parameters.AddWithValue("@StuffInitials", rentalInvoiceItem.StuffInitials);
                cmd.Parameters.AddWithValue("@RPStartingDate", rentalInvoiceItem.RPStartingDate);
                cmd.Parameters.AddWithValue("@RPEndingDate", rentalInvoiceItem.RPEndingDate);
                cmd.Parameters.AddWithValue("@RPDescription", rentalInvoiceItem.RPDescription);
                cmd.Parameters.AddWithValue("@RPPreTaxAmount", rentalInvoiceItem.RPPreTaxAmount);
                cmd.Parameters.AddWithValue("@RPSalesTax", rentalInvoiceItem.RPSalesTax);
                cmd.Parameters.AddWithValue("@RPTotal", rentalInvoiceItem.RPTotal);
                cmd.Parameters.AddWithValue("@DepositAmount", rentalInvoiceItem.DepositAmount);

                cmd.Parameters.AddWithValue("@IsItemReturned", rentalInvoiceItem.IsItemReturned);
                cmd.Parameters.AddWithValue("@ReturnDate", rentalInvoiceItem.ReturnDate);
                cmd.Parameters.AddWithValue("@IsRentAmountRefundable", rentalInvoiceItem.IsRentAmountRefundable);
                cmd.Parameters.AddWithValue("@RefundAmount", rentalInvoiceItem.RefundAmount);
                cmd.Parameters.AddWithValue("@RentalItemStatus", rentalInvoiceItem.RentalItemStatus);
                cmd.Parameters.AddWithValue("@IsRefundProcessed", rentalInvoiceItem.IsRefundProcessed);
                cmd.Parameters.AddWithValue("@RefundedDate", rentalInvoiceItem.RefundedDate);
                cmd.Parameters.AddWithValue("@OverdueCharge", rentalInvoiceItem.OverdueCharge);

                cmd.Parameters.AddWithValue("@IsExtensionRequestByClient", rentalInvoiceItem.IsExtensionRequestByClient);
                cmd.Parameters.AddWithValue("@DateExtensionRequestByClient", rentalInvoiceItem.DateExtensionRequestByClient);
                cmd.Parameters.AddWithValue("@ExtensionDays", rentalInvoiceItem.ExtensionDays);
                cmd.Parameters.AddWithValue("@ExtensionPreTaxAmount", rentalInvoiceItem.ExtensionPreTaxAmount);
                cmd.Parameters.AddWithValue("@ExtensionSalesTax", rentalInvoiceItem.ExtensionSalesTax);
                cmd.Parameters.AddWithValue("@ExtensionTotalAmount", rentalInvoiceItem.ExtensionTotalAmount);
                cmd.Parameters.AddWithValue("@IsExtensionApprovedByAdmin", rentalInvoiceItem.IsExtensionApprovedByAdmin);
                cmd.Parameters.AddWithValue("@DateExtensionApprovedByAdmin", rentalInvoiceItem.DateExtensionApprovedByAdmin);
                cmd.Parameters.AddWithValue("@RegularFee", rentalInvoiceItem.RegularFee);
                //Execute the OdbcCommand
                id = Convert.ToInt64(ExecuteNonQueryAndScalar(cmd,conn,tx));

                rentalInvoiceItem.Id = id;
            }
            catch (Exception ex)
            {
                //Attempt to throw the exception for error handling
                throw ex;
            }
        }

        internal void Update(RentalInvoiceItem rentalInvoiceItem)
        {
            try
            {
                OdbcCommand cmd = new OdbcCommand(_cmdUpdate);

                cmd.Parameters.AddWithValue("@InvoiceId", rentalInvoiceItem.InvoiceId);
                cmd.Parameters.AddWithValue("@RentalEquipmentID", rentalInvoiceItem.RentalEquipmentID);
                cmd.Parameters.AddWithValue("@AgreementNo", rentalInvoiceItem.AgreementNo);
                cmd.Parameters.AddWithValue("@REStartingDate", rentalInvoiceItem.REStartingDate);
                cmd.Parameters.AddWithValue("@RESerial", rentalInvoiceItem.RESerial);
                cmd.Parameters.AddWithValue("@REVendorName", rentalInvoiceItem.REVendorName);
                cmd.Parameters.AddWithValue("@ModelName", rentalInvoiceItem.ModelName);
                cmd.Parameters.AddWithValue("@REEndingDate", rentalInvoiceItem.REEndingDate);
                cmd.Parameters.AddWithValue("@REConditionWhenReturned", rentalInvoiceItem.REConditionWhenReturned);
                cmd.Parameters.AddWithValue("@StuffInitials", rentalInvoiceItem.StuffInitials);
                cmd.Parameters.AddWithValue("@RPStartingDate", rentalInvoiceItem.RPStartingDate);
                cmd.Parameters.AddWithValue("@RPEndingDate", rentalInvoiceItem.RPEndingDate);
                cmd.Parameters.AddWithValue("@RPDescription", rentalInvoiceItem.RPDescription);
                cmd.Parameters.AddWithValue("@RPPreTaxAmount", rentalInvoiceItem.RPPreTaxAmount);
                cmd.Parameters.AddWithValue("@RPSalesTax", rentalInvoiceItem.RPSalesTax);
                cmd.Parameters.AddWithValue("@RPTotal", rentalInvoiceItem.RPTotal);
                cmd.Parameters.AddWithValue("@DepositAmount", rentalInvoiceItem.DepositAmount);

                cmd.Parameters.AddWithValue("@IsItemReturned", rentalInvoiceItem.IsItemReturned);
                cmd.Parameters.AddWithValue("@ReturnDate", rentalInvoiceItem.ReturnDate);
                cmd.Parameters.AddWithValue("@IsRentAmountRefundable", rentalInvoiceItem.IsRentAmountRefundable);
                cmd.Parameters.AddWithValue("@RefundAmount", rentalInvoiceItem.RefundAmount);
                cmd.Parameters.AddWithValue("@RentalItemStatus", rentalInvoiceItem.RentalItemStatus);
                cmd.Parameters.AddWithValue("@IsRefundProcessed", rentalInvoiceItem.IsRefundProcessed);
                cmd.Parameters.AddWithValue("@RefundedDate", rentalInvoiceItem.RefundedDate);
                cmd.Parameters.AddWithValue("@OverdueCharge", rentalInvoiceItem.OverdueCharge);

                cmd.Parameters.AddWithValue("@IsExtensionRequestByClient", rentalInvoiceItem.IsExtensionRequestByClient);
                cmd.Parameters.AddWithValue("@DateExtensionRequestByClient", rentalInvoiceItem.DateExtensionRequestByClient);
                cmd.Parameters.AddWithValue("@ExtensionDays", rentalInvoiceItem.ExtensionDays);
                cmd.Parameters.AddWithValue("@ExtensionPreTaxAmount", rentalInvoiceItem.ExtensionPreTaxAmount);
                cmd.Parameters.AddWithValue("@ExtensionSalesTax", rentalInvoiceItem.ExtensionSalesTax);
                cmd.Parameters.AddWithValue("@ExtensionTotalAmount", rentalInvoiceItem.ExtensionTotalAmount);
                cmd.Parameters.AddWithValue("@IsExtensionApprovedByAdmin", rentalInvoiceItem.IsExtensionApprovedByAdmin);
                cmd.Parameters.AddWithValue("@DateExtensionApprovedByAdmin", rentalInvoiceItem.DateExtensionApprovedByAdmin);
                cmd.Parameters.AddWithValue("@RegularFee", rentalInvoiceItem.RegularFee);
                cmd.Parameters.AddWithValue("@Id", rentalInvoiceItem.Id);

                //Execute the OdbcCommand
                ExecuteNonQuery(cmd);
            }
            catch (Exception ex)
            {
                //Attempt to throw the exception for error handling
                throw ex;
            }
        }

        internal bool Delete(long id)
        {
            bool success = false;

            try
            {
                OdbcCommand cmd = new OdbcCommand(_cmdDelete);

                cmd.Parameters.AddWithValue("@Id", id);

                ExecuteNonQuery(cmd);

                success = true;
            }
            catch (Exception ex)
            {
                //Attempt to throw the exception for error handling
                throw ex;
            }

            return success;
        }

        public decimal getItemAmountPrice(long Id)
        {
            decimal totalAmountPaid = 0;

            try
            {
                OdbcCommand cmd = new OdbcCommand(_cmdgetItemAmountPriceByID);
                cmd.Parameters.AddWithValue("@Id", Id);
                DataSet ds = ExecuteQuery(cmd);

                if (ds != null)
                {
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        if (ds.Tables[0].Rows[0].ItemArray[0] != DBNull.Value)
                        {
                            totalAmountPaid = Convert.ToDecimal(ds.Tables[0].Rows[0].ItemArray[0]);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return totalAmountPaid;
        }//

        public decimal getItemDepositAmountPrice(long Id)
        {
            decimal totalAmountPaid = 0;

            try
            {
                OdbcCommand cmd = new OdbcCommand(_cmdgetItemAmountDepositPriceByID);
                cmd.Parameters.AddWithValue("@Id", Id);
                DataSet ds = ExecuteQuery(cmd);

                if (ds != null)
                {
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        if (ds.Tables[0].Rows[0].ItemArray[0] != DBNull.Value)
                        {
                            totalAmountPaid = Convert.ToDecimal(ds.Tables[0].Rows[0].ItemArray[0]);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return totalAmountPaid;
        }

        private List<RentalInvoiceItem> GetListFromDataSet(DataSet ds)
        {
            List<RentalInvoiceItem> rentalInvoiceItemList = new List<RentalInvoiceItem>();

            // This Loop reads all Data from DataSet                    
            for (int i = 0; i < ds.Tables["Data"].Rows.Count; i++)
            {
                RentalInvoiceItem rentalInvoiceItem = new RentalInvoiceItem();

                rentalInvoiceItem.Id = Convert.ToInt64(ds.Tables["Data"].Rows[i].ItemArray[0]);
                rentalInvoiceItem.InvoiceId = Convert.ToInt64(ds.Tables["Data"].Rows[i].ItemArray[1]);
                rentalInvoiceItem.RentalEquipmentID = Convert.ToInt64(ds.Tables["Data"].Rows[i].ItemArray[2]);
                rentalInvoiceItem.AgreementNo = Convert.ToString(ds.Tables["Data"].Rows[i].ItemArray[3]);
                rentalInvoiceItem.REStartingDate = Convert.ToDateTime(ds.Tables["Data"].Rows[i].ItemArray[4]);
                rentalInvoiceItem.RESerial = Convert.ToString(ds.Tables["Data"].Rows[i].ItemArray[5]);
                rentalInvoiceItem.REVendorName = Convert.ToString(ds.Tables["Data"].Rows[i].ItemArray[6]);
                rentalInvoiceItem.ModelName = Convert.ToString(ds.Tables["Data"].Rows[i].ItemArray[7]);
                rentalInvoiceItem.REEndingDate = Convert.ToDateTime(ds.Tables["Data"].Rows[i].ItemArray[8]);
                rentalInvoiceItem.REConditionWhenReturned = Convert.ToString(ds.Tables["Data"].Rows[i].ItemArray[9]);
                rentalInvoiceItem.StuffInitials = Convert.ToInt64(ds.Tables["Data"].Rows[i].ItemArray[10]);
                rentalInvoiceItem.RPStartingDate = Convert.ToDateTime(ds.Tables["Data"].Rows[i].ItemArray[11]);
                rentalInvoiceItem.RPEndingDate = Convert.ToDateTime(ds.Tables["Data"].Rows[i].ItemArray[12]);
                rentalInvoiceItem.RPDescription = Convert.ToString(ds.Tables["Data"].Rows[i].ItemArray[13]);
                rentalInvoiceItem.RPPreTaxAmount = Convert.ToDecimal(ds.Tables["Data"].Rows[i].ItemArray[14]);
                rentalInvoiceItem.RPSalesTax = Convert.ToDecimal(ds.Tables["Data"].Rows[i].ItemArray[15]);
                rentalInvoiceItem.RPTotal = Convert.ToDecimal(ds.Tables["Data"].Rows[i].ItemArray[16]);
                string depositAmount = Convert.ToString(ds.Tables["Data"].Rows[i].ItemArray[17]);
                if (!string.IsNullOrEmpty(depositAmount))
                {
                    rentalInvoiceItem.DepositAmount = Convert.ToDecimal(depositAmount);
                }
                else
                {
                    rentalInvoiceItem.DepositAmount = Convert.ToDecimal(50.00);
                }


                if (ds.Tables["Data"].Rows[i].ItemArray[18] != DBNull.Value)
                {
                    rentalInvoiceItem.IsItemReturned = Convert.ToBoolean(ds.Tables["Data"].Rows[i].ItemArray[18]);
                }
                else
                {
                    rentalInvoiceItem.IsItemReturned = false;
                }

                if (ds.Tables["Data"].Rows[i].ItemArray[19] != DBNull.Value)
                {
                    rentalInvoiceItem.ReturnDate = Convert.ToDateTime(Convert.ToString(ds.Tables["Data"].Rows[i].ItemArray[19]));
                }
                else
                {
                    rentalInvoiceItem.ReturnDate = Convert.ToDateTime("1/1/1900");
                }

                if (ds.Tables["Data"].Rows[i].ItemArray[20] != DBNull.Value)
                {
                    rentalInvoiceItem.IsRentAmountRefundable = Convert.ToBoolean(ds.Tables["Data"].Rows[i].ItemArray[20]);
                }
                else
                {
                    rentalInvoiceItem.IsRentAmountRefundable = false;
                }
                if (ds.Tables["Data"].Rows[i].ItemArray[21] != DBNull.Value)
                {
                    rentalInvoiceItem.RefundAmount = Convert.ToDecimal(ds.Tables["Data"].Rows[i].ItemArray[21]);
                }
                else
                {
                    rentalInvoiceItem.RefundAmount = 0;
                }

                if (ds.Tables["Data"].Rows[i].ItemArray[22] != DBNull.Value)
                {
                    rentalInvoiceItem.RentalItemStatus = (ReturnStatus)Convert.ToInt32(ds.Tables["Data"].Rows[i].ItemArray[22]);
                }
                else
                {
                    rentalInvoiceItem.RentalItemStatus = ReturnStatus.NotReturned;
                }

                if (ds.Tables["Data"].Rows[i].ItemArray[23] != DBNull.Value)
                {
                    rentalInvoiceItem.IsRefundProcessed = Convert.ToBoolean(ds.Tables["Data"].Rows[i].ItemArray[23]);
                }
                else
                {
                    rentalInvoiceItem.IsRefundProcessed = false;
                }

                if (ds.Tables["Data"].Rows[i].ItemArray[24] != DBNull.Value)
                {
                    rentalInvoiceItem.RefundedDate = Convert.ToDateTime(Convert.ToString(ds.Tables["Data"].Rows[i].ItemArray[24]));
                }
                else
                {
                    rentalInvoiceItem.RefundedDate = Convert.ToDateTime("1/1/1900");
                }

                if (ds.Tables["Data"].Rows[i].ItemArray[25] != DBNull.Value)
                {
                    rentalInvoiceItem.OverdueCharge = Convert.ToDecimal(ds.Tables["Data"].Rows[i].ItemArray[25]);
                }
                else
                {
                    rentalInvoiceItem.OverdueCharge =0;
                }

                if (ds.Tables["Data"].Rows[i].ItemArray[26] != DBNull.Value)
                {
                    rentalInvoiceItem.IsExtensionRequestByClient = Convert.ToBoolean(ds.Tables["Data"].Rows[i].ItemArray[26]);
                }
                else
                {
                    rentalInvoiceItem.IsExtensionRequestByClient = false;
                }

                if (ds.Tables["Data"].Rows[i].ItemArray[27] != DBNull.Value)
                {
                    rentalInvoiceItem.DateExtensionRequestByClient = Convert.ToDateTime(Convert.ToString(ds.Tables["Data"].Rows[i].ItemArray[27]));
                }
                else
                {
                    rentalInvoiceItem.DateExtensionRequestByClient = Convert.ToDateTime("1/1/1900");
                }
                if (ds.Tables["Data"].Rows[i].ItemArray[28] != DBNull.Value)
                {
                    rentalInvoiceItem.ExtensionDays = Convert.ToInt32(Convert.ToString(ds.Tables["Data"].Rows[i].ItemArray[28]));
                }
                else
                {
                    rentalInvoiceItem.ExtensionDays = 0;
                }
                if (ds.Tables["Data"].Rows[i].ItemArray[29] != DBNull.Value)
                {
                    rentalInvoiceItem.ExtensionPreTaxAmount = Convert.ToDecimal(Convert.ToString(ds.Tables["Data"].Rows[i].ItemArray[29]));
                }
                else
                {
                    rentalInvoiceItem.ExtensionPreTaxAmount = 0;
                }
                if (ds.Tables["Data"].Rows[i].ItemArray[30] != DBNull.Value)
                {
                    rentalInvoiceItem.ExtensionSalesTax = Convert.ToDecimal(Convert.ToString(ds.Tables["Data"].Rows[i].ItemArray[30]));
                }
                else
                {
                    rentalInvoiceItem.ExtensionSalesTax = 0;
                }
                if (ds.Tables["Data"].Rows[i].ItemArray[31] != DBNull.Value)
                {
                    rentalInvoiceItem.ExtensionTotalAmount = Convert.ToDecimal(Convert.ToString(ds.Tables["Data"].Rows[i].ItemArray[31]));
                }
                else
                {
                    rentalInvoiceItem.ExtensionTotalAmount = 0;
                }
                if (ds.Tables["Data"].Rows[i].ItemArray[32] != DBNull.Value)
                {
                    rentalInvoiceItem.IsExtensionApprovedByAdmin= Convert.ToBoolean(ds.Tables["Data"].Rows[i].ItemArray[32]);
                }
                else
                {
                    rentalInvoiceItem.IsExtensionApprovedByAdmin = false;
                }

                if (ds.Tables["Data"].Rows[i].ItemArray[33] != DBNull.Value)
                {
                    rentalInvoiceItem.DateExtensionApprovedByAdmin = Convert.ToDateTime(Convert.ToString(ds.Tables["Data"].Rows[i].ItemArray[33]));
                }
                else
                {
                    rentalInvoiceItem.DateExtensionApprovedByAdmin = Convert.ToDateTime("1/1/1900");
                }
                if (ds.Tables["Data"].Rows[i].ItemArray[34] != DBNull.Value)
                {
                    rentalInvoiceItem.RegularFee = Convert.ToDecimal(Convert.ToString(ds.Tables["Data"].Rows[i].ItemArray[34]));
                }
                else
                {
                    rentalInvoiceItem.RegularFee = 0;
                }

                rentalInvoiceItemList.Add(rentalInvoiceItem);
            }

            return rentalInvoiceItemList;
        }

        internal List<RentalInvoiceItem> GetAll()
        {
            try
            {
                //Create instance of sqlcommand
                OdbcCommand cmd = new OdbcCommand(_cmdSelectAll);

                DataSet ds = ExecuteQuery(cmd);

                return GetListFromDataSet(ds);
            }
            catch (Exception ex)
            {
                //Attempt to throw the exception for error handling
                throw ex;
            }
        }

        internal RentalInvoiceItem GetById(long id)
        {
            try
            {
                OdbcCommand cmd = new OdbcCommand(_cmdSelectById);

                cmd.Parameters.AddWithValue("@Id", id);

                DataSet ds = ExecuteQuery(cmd);

                return GetListFromDataSet(ds)[0];
            }
            catch (Exception ex)
            {
                //Attempt to throw the exception for error handling
                throw ex;
            }
        }

        internal List<RentalInvoiceItem> GetListByInvoiceId(long invoiceId)
        {
            try
            {
                //Create instance of sqlcommand
                OdbcCommand cmd = new OdbcCommand(_cmdSelectByInvoiceId);

                cmd.Parameters.AddWithValue("@InvoiceId", invoiceId);

                DataSet ds = ExecuteQuery(cmd);

                return GetListFromDataSet(ds);
            }
            catch (Exception ex)
            {
                //Attempt to throw the exception for error handling
                throw ex;
            }
        }

        internal bool DeleteByInvoiceId(long invoiceId)
        {
            bool success = false;

            try
            {
                OdbcCommand cmd = new OdbcCommand(_cmdDeleteByInvoiceId);

                cmd.Parameters.AddWithValue("@InvoiceId", invoiceId);

                ExecuteNonQuery(cmd);

                success = true;
            }
            catch (Exception ex)
            {
                //Attempt to throw the exception for error handling
                throw ex;
            }

            return success;
        }

        internal void DeleteByRentalid(long RentalId)
        {
           // bool success = false;

            try
            {
                OdbcCommand cmd = new OdbcCommand(_cmdDeleteByRentalId);

                cmd.Parameters.AddWithValue("@Id", RentalId);

                ExecuteNonQuery(cmd);

               // success = true;
            }
            catch (Exception ex)
            {
                //Attempt to throw the exception for error handling
                throw ex;
            }

            //return success;
        }
       // UpdateRentalItemStatus
        internal void UpdateRentalItemStatus(RentalInvoiceItem rentalInvoiceItem)
        {
            try
            {
                OdbcCommand cmd = new OdbcCommand(_cmdUpdateRentalItemStatus);
                cmd.Parameters.AddWithValue("@RentalItemStatus", rentalInvoiceItem.RentalItemStatus);

                cmd.Parameters.AddWithValue("@Id", rentalInvoiceItem.Id);
                ExecuteNonQuery(cmd);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        internal void UpdateRentalInvoiceId(RentalInvoiceItem item, long newInvoiceId)
        {
            try
            {
                OdbcCommand cmd = new OdbcCommand(_cmdUpdateRentalInvoiceId);
                cmd.Parameters.AddWithValue("@InvoiceId", newInvoiceId);

                cmd.Parameters.AddWithValue("@Id", item.Id);
                ExecuteNonQuery(cmd);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        internal void ReturnItem(RentalInvoiceItem rentalInvoiceItem)
        {
            try
            {
                OdbcCommand cmd = new OdbcCommand(_cmdReturnItem);


                cmd.Parameters.AddWithValue("@IsItemReturned", rentalInvoiceItem.IsItemReturned);
                cmd.Parameters.AddWithValue("@ReturnDate", rentalInvoiceItem.ReturnDate);
                cmd.Parameters.AddWithValue("@IsRentAmountRefundable", rentalInvoiceItem.IsRentAmountRefundable);
                cmd.Parameters.AddWithValue("@RefundAmount", rentalInvoiceItem.RefundAmount);
                cmd.Parameters.AddWithValue("@RentalItemStatus", rentalInvoiceItem.RentalItemStatus);
                cmd.Parameters.AddWithValue("@OverdueCharge", rentalInvoiceItem.OverdueCharge);

                cmd.Parameters.AddWithValue("@Id", rentalInvoiceItem.Id);

                ExecuteNonQuery(cmd);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        internal void UpdateRefundProcessed(RentalInvoiceItem rentalInvoiceItem)
        {
            try
            {
                OdbcCommand cmd = new OdbcCommand(_cmdUpdateRefundProcessed);
                cmd.Parameters.AddWithValue("@IsRefundProcessed", rentalInvoiceItem.IsRefundProcessed);
                cmd.Parameters.AddWithValue("@RefundedDate", rentalInvoiceItem.RefundedDate);
                cmd.Parameters.AddWithValue("@RefundAmount", rentalInvoiceItem.RefundAmount);

                cmd.Parameters.AddWithValue("@Id", rentalInvoiceItem.Id);
                ExecuteNonQuery(cmd);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        internal void UpdateRegularfee(RentalInvoiceItem item)
        {
            try
            {
                OdbcCommand cmd = new OdbcCommand(_cmdUpdateRegularfee);
                cmd.Parameters.AddWithValue("@RegularFee", item.RegularFee);
                cmd.Parameters.AddWithValue("@Id", item.Id);
                ExecuteNonQuery(cmd);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        internal void UpdateRentalExtensionRequest(RentalInvoiceItem rentalInvoiceItem)
        {
            try
            {
                OdbcCommand cmd = new OdbcCommand(_cmdUpdateRentalExtensionRequest);
                cmd.Parameters.AddWithValue("@IsExtensionRequestByClient", rentalInvoiceItem.IsExtensionRequestByClient);
                cmd.Parameters.AddWithValue("@DateExtensionRequestByClient", rentalInvoiceItem.DateExtensionRequestByClient);
                cmd.Parameters.AddWithValue("@ExtensionDays", rentalInvoiceItem.ExtensionDays);
                cmd.Parameters.AddWithValue("@ExtensionPreTaxAmount", rentalInvoiceItem.ExtensionPreTaxAmount);
                cmd.Parameters.AddWithValue("@ExtensionSalesTax", rentalInvoiceItem.ExtensionSalesTax);
                cmd.Parameters.AddWithValue("@ExtensionTotalAmount", rentalInvoiceItem.ExtensionTotalAmount);
                cmd.Parameters.AddWithValue("@IsExtensionApprovedByAdmin", rentalInvoiceItem.IsExtensionApprovedByAdmin);
                cmd.Parameters.AddWithValue("@DateExtensionApprovedByAdmin", rentalInvoiceItem.DateExtensionApprovedByAdmin);

                cmd.Parameters.AddWithValue("@Id", rentalInvoiceItem.Id);
                ExecuteNonQuery(cmd);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
