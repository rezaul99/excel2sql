﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.Odbc;
using System.Collections;
using daoProject.Infrastructure;
using daoProject.Model;
using daoProject.Mapper;

namespace daoProject.Mapper
{
    internal class PackageDataMapper : SqlHelper
    {
        private static string _cmdInsert = @"INSERT INTO Whrc_ClientDeposit (ClientId,Amount,Comment,ModifiedBy,ModifiedDate) VALUES (?,?,?,?,?); ";
        private static string _cmdUpdate = @"UPDATE Whrc_ClientDeposit SET ClientId=?,Amount=?,Comment=?,ModifiedBy=?,ModifiedDate=? WHERE Id=? ";
        private static string _cmdDelete = @"DELETE FROM Whrc_ClientDeposit WHERE Id=? ";
        private static string _cmdSelectAll = @"SELECT * FROM Whrc_ClientDeposit ";
        private static string _cmdSelectById = @"SELECT * FROM Whrc_ClientDeposit WHERE Id=? ";
        
        internal void Create(ClientDeposit clientDeposit)
        {
            long id = -1;
            try
            {
                OdbcCommand cmd = new OdbcCommand(_cmdInsert);

                cmd.Parameters.AddWithValue("@ClientId", clientDeposit.ClientId);
                cmd.Parameters.AddWithValue("@Amount", clientDeposit.Amount);
                cmd.Parameters.AddWithValue("@Comment", clientDeposit.Comment);
                cmd.Parameters.AddWithValue("@ModifiedBy", clientDeposit.ModifiedBy);
                cmd.Parameters.AddWithValue("@ModifiedDate", clientDeposit.ModifiedDate);

                //Execute the OdbcCommand
                id = Convert.ToInt64(ExecuteNonQueryAndScalar(cmd));

                clientDeposit.Id = id;
            }
            catch (Exception ex)
            {
                //Attempt to throw the exception for error handling
                throw ex;
            }
        }

        internal void Update(ClientDeposit clientDeposit)
        {
            try
            {
                OdbcCommand cmd = new OdbcCommand(_cmdUpdate);

                cmd.Parameters.AddWithValue("@ClientId", clientDeposit.ClientId);
                cmd.Parameters.AddWithValue("@Amount", clientDeposit.Amount);
                cmd.Parameters.AddWithValue("@Comment", clientDeposit.Comment);
                cmd.Parameters.AddWithValue("@ModifiedBy", clientDeposit.ModifiedBy);
                cmd.Parameters.AddWithValue("@ModifiedDate", clientDeposit.ModifiedDate);
                cmd.Parameters.AddWithValue("@Id", clientDeposit.Id);

                //Execute the OdbcCommand
                ExecuteNonQuery(cmd);
            }
            catch (Exception ex)
            {
                //Attempt to throw the exception for error handling
                throw ex;
            }
        }

        internal bool Delete(long id)
        {
            bool success = false;

            try
            {
                OdbcCommand cmd = new OdbcCommand(_cmdDelete);

                cmd.Parameters.AddWithValue("@Id", id);

                ExecuteNonQuery(cmd);

                success = true;
            }
            catch (Exception ex)
            {
                //Attempt to throw the exception for error handling
                throw ex;
            }

            return success;
        }

        private List<ClientDeposit> GetListFromDataSet(DataSet ds)
        {
            List<ClientDeposit> clientDepositList = new List<ClientDeposit>();

            // This Loop reads all Data from DataSet                    
            for (int i = 0; i < ds.Tables["Data"].Rows.Count; i++)
            {
                ClientDeposit clientDeposit = new ClientDeposit();

                clientDeposit.Id = Convert.ToInt64(ds.Tables["Data"].Rows[i].ItemArray[0]);
                clientDeposit.ClientId = Convert.ToInt64(ds.Tables["Data"].Rows[i].ItemArray[1]);
                clientDeposit.Amount = Convert.ToDecimal(ds.Tables["Data"].Rows[i].ItemArray[2]);
                clientDeposit.Comment = Convert.ToString(ds.Tables["Data"].Rows[i].ItemArray[3]);
                clientDeposit.ModifiedBy = Convert.ToInt64(ds.Tables["Data"].Rows[i].ItemArray[4]);
                clientDeposit.ModifiedDate = Convert.ToDateTime(ds.Tables["Data"].Rows[i].ItemArray[5]);

                clientDepositList.Add(clientDeposit);
            }

            return clientDepositList;
        }

        internal List<ClientDeposit> GetAll()
        {
            try
            {
                //Create instance of sqlcommand
                OdbcCommand cmd = new OdbcCommand(_cmdSelectAll);

                DataSet ds = ExecuteQuery(cmd);

                return GetListFromDataSet(ds);
            }
            catch (Exception ex)
            {
                //Attempt to throw the exception for error handling
                throw ex;
            }
        }

        internal ClientDeposit GetById(long id)
        {
            try
            {
                OdbcCommand cmd = new OdbcCommand(_cmdSelectById);

                cmd.Parameters.AddWithValue("@Id", id);

                DataSet ds = ExecuteQuery(cmd);

                return GetListFromDataSet(ds)[0];
            }
            catch (Exception ex)
            {
                //Attempt to throw the exception for error handling
                throw ex;
            }
        }
    }
}
