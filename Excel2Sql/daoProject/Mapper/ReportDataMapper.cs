﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.Odbc;
using System.Collections;
using daoProject.Infrastructure;
using daoProject.Model;
using daoProject.Mapper;
using daoProject.Service;

namespace daoProject.Mapper
{
    internal class ReportDataMapper : SqlHelper
    {
        internal DataTable GetAllSignupClassesByDateRange(DateTime startDate, DateTime endDate)
        {
            try
            {
                string sql = @"select * 
                            from Whrc_ClassInvoiceItem
                                inner join Whrc_Invoice on Whrc_Invoice.Id = Whrc_ClassInvoiceItem.InvoiceId
                            where Whrc_Invoice.OrderStatus NOT IN (0) and (SignupDate between ? and ?)";

                OdbcCommand cmd = new OdbcCommand(sql);

                cmd.Parameters.AddWithValue("@startDate", startDate);
                cmd.Parameters.AddWithValue("@endDate", endDate);

                DataSet ds = ExecuteQuery(cmd);

                return ds.Tables[0];
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        internal DataTable GetTotalSold(DateTime startDate, DateTime endDate)
        {
            try
            {
                string sql = @"Select productID, count(*) as totalsold  
                            From WHRC_ProductInvoiceItem, Whrc_Invoice
                            where WHRC_ProductInvoiceItem.InvoiceId = Whrc_Invoice.Id and Whrc_Invoice.PaymentDate between ? and ?
                            Group by productID";

                OdbcCommand cmd = new OdbcCommand(sql);

                cmd.Parameters.AddWithValue("@startDate", startDate);
                cmd.Parameters.AddWithValue("@endDate", endDate);

                DataSet ds = ExecuteQuery(cmd);

                return ds.Tables[0];
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

//        internal DataTable GetTotalWaitingList(DateTime startDate, DateTime endDate)
//        {
//            try
//            {
//                string sql = @"Select SectionId, Count(*) as TotalWaiting 
//                            From WHRC_ClassInvoiceItem 
//                            where Signupdate between ? and ?
//                            And IsInwaitingList=? Group by SectionId";

//                OdbcCommand cmd = new OdbcCommand(sql);

//                cmd.Parameters.AddWithValue("@startDate", startDate);
//                cmd.Parameters.AddWithValue("@endDate", endDate);
//                cmd.Parameters.AddWithValue("@isInwaitingList", true);

//                DataSet ds = ExecuteQuery(cmd);

//                return ds.Tables[0];
//            }
//            catch (Exception ex)
//            {
//                throw ex;
//            }
//        }
        internal DataTable GetTotalWaitingList(long sectionId)
        {
            try
            {
                string sql = @"Select SectionId, Count(*) as TotalWaiting 
                            From WHRC_ClassInvoiceItem 
                            where IsInwaitingList=? and SectionId=? Group by SectionId";

                OdbcCommand cmd = new OdbcCommand(sql);

                cmd.Parameters.AddWithValue("@isInwaitingList", true);
                cmd.Parameters.AddWithValue("@sectionId", sectionId);

                DataSet ds = ExecuteQuery(cmd);

                return ds.Tables[0];
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        internal DataTable GetClassAttendanceBySectionId(long sectionId)
        {
            try
            {
                string sql = @"Select WHRC_ClassInvoiceItem.SignupDate,WHRC_ClassInvoiceItem.IsInWaitingList,
                            WHRC_ClassInvoiceItem.NumberAttending,WHRC_ClassInvoiceItem.IsClassCanceled,WHRC_ClassInvoiceItem.CancelDate, 
                            WHRC_ClassInvoiceItem.IsClassCanceledByClient,WHRC_Users.FirstName, WHRC_Users.LastName 
                            From WHRC_ClassInvoiceItem,WHRC_Invoice,WHRC_Users
                            Where WHRC_Invoice.ID = WHRC_ClassInvoiceItem.InvoiceID
                            And WHRC_Invoice.ClientID = WHRC_Users.ID
                            And Whrc_Invoice.OrderStatus NOT IN (0)
                            and WHRC_ClassInvoiceItem.SectionId=?";

                OdbcCommand cmd = new OdbcCommand(sql);

                cmd.Parameters.AddWithValue("@sectionId", sectionId);

                DataSet ds = ExecuteQuery(cmd);

                return ds.Tables[0];
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        
        internal DataTable GetClassAttendanceSignUPBySectionId(long sectionId)
        {
            try
            {
                string sql = @"Select  Whrc_Users.Id,Whrc_Users.FirstName,Whrc_Users.MiddleName,Whrc_Users.LastName,Whrc_Users.Address1,Whrc_Users.AD1City,
Whrc_Users.AD1State,Whrc_Users.AD2Zip,Whrc_Users.Phone,Whrc_Users.Email,Whrc_Users.DOB,Whrc_Users.DueDate,
Whrc_Users.Insurance,
Whrc_ClassInvoiceItem.SignupDate,Whrc_ClassInvoiceItem.NumberAttending,Whrc_ClassInvoiceItem.IsInWaitingList 
from Whrc_ClassInvoiceItem,Whrc_Users,WHRC_Invoice
 Where WHRC_Invoice.ID = WHRC_ClassInvoiceItem.InvoiceID
                            And WHRC_Invoice.ClientID = WHRC_Users.ID
                            And Whrc_Invoice.OrderStatus NOT IN (0)
                            and WHRC_ClassInvoiceItem.SectionId=?";
                OdbcCommand cmd = new OdbcCommand(sql);

                cmd.Parameters.AddWithValue("@sectionId", sectionId);

                DataSet ds = ExecuteQuery(cmd);

                return ds.Tables[0];


            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        internal DataTable GetClassFillRateBySectionId(long sectionId)
        {
            try
            {
//                string sql = @"SELECT SectionId, NoOfAttendee*100/(NoOfAttendee+NoOfNotAttendee) AS FillRate
//                            FROM
//                            (Select t1.SectionId, NoOfAttendee, NoOfNotAttendee = CASE WHEN NoOfNotAttendee is NULL THEN 0 ELSE NoOfNotAttendee END
//                            From
//                            (Select WHRC_ClassinvoiceItem.SectionId, SUM(NumberAttending) AS NoOfAttendee
//                            From Whrc_Invoice, WHRC_ClassInvoiceItem
//                            Where Whrc_Invoice.Id = WHRC_ClassinvoiceItem.InvoiceId
//	                            and Whrc_Invoice.OrderStatus NOT IN (0) and WHRC_ClassinvoiceItem.IsInWaitingList=?
//	                            and WHRC_ClassinvoiceItem.IsClassCanceled=? and WHRC_ClassInvoiceItem.SectionId=?
//                            group by WHRC_ClassinvoiceItem.SectionId) AS t1
//                            JOIN
//                            (Select WHRC_ClassinvoiceItem.SectionId, SUM(NumberAttending) AS NoOfNotAttendee
//                            From Whrc_Invoice, WHRC_ClassInvoiceItem
//                            Where Whrc_Invoice.Id = WHRC_ClassinvoiceItem.InvoiceId
//	                            and Whrc_Invoice.OrderStatus NOT IN (0) and WHRC_ClassinvoiceItem.IsInWaitingList=?
//	                            and WHRC_ClassinvoiceItem.IsClassCanceled=? and WHRC_ClassInvoiceItem.SectionId=?
//                            group by WHRC_ClassinvoiceItem.SectionId) AS t2
//                            ON t1.SectionId = t2.SectionId) AS t";

                string sql = @"SELECT WHRC_ClassinvoiceItem.SectionId, SUM(NumberAttending) AS FillRate
                            From Whrc_Invoice, WHRC_ClassInvoiceItem
                            Where Whrc_Invoice.Id = WHRC_ClassinvoiceItem.InvoiceId
	                            and Whrc_Invoice.OrderStatus NOT IN (0) and WHRC_ClassinvoiceItem.IsInWaitingList=?
	                            and WHRC_ClassinvoiceItem.IsClassCanceled=? and WHRC_ClassInvoiceItem.SectionId=?
                            group by WHRC_ClassinvoiceItem.SectionId";

                OdbcCommand cmd = new OdbcCommand(sql);
               
                cmd.Parameters.AddWithValue("@isInWaitingList1", false);
                cmd.Parameters.AddWithValue("@isClassCanceled1", false);
                cmd.Parameters.AddWithValue("@sectionId1", sectionId);
                //cmd.Parameters.AddWithValue("@isInWaitingList2", false);
                //cmd.Parameters.AddWithValue("@isClassCanceled2", false);
                //cmd.Parameters.AddWithValue("@isClassCanceled2", true);
                //cmd.Parameters.AddWithValue("@sectionId2", sectionId);

                DataSet ds = ExecuteQuery(cmd);

                return ds.Tables[0];
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        internal DataTable GetAllRentalOverdueEquipments()
        {
            try
            {
                string sql = @"Select FirstName, LastName, PhoneType, Phone, AreaCode, RentalEquipmentID, RPStartingDate, RPEndingDate  
                            from WHRC_users,WHRC_Invoice, WHRC_RentalInvoiceItem
                            Where WHRC_users.id = WHRC_Invoice.ClientID
                            And WHRC_Invoice.id=WHRC_RentalInvoiceItem.InvoiceID and Whrc_Invoice.OrderStatus NOT IN (0)
                            And WHRC_RentalInvoiceItem.IsItemReturned=?";

                OdbcCommand cmd = new OdbcCommand(sql);

                cmd.Parameters.AddWithValue("@isItemReturned", false);

                DataSet ds = ExecuteQuery(cmd);

                return ds.Tables[0];
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        internal DataTable GetRentalEquipments()
        {
            try
            {
                string sql = @"select RentalEquipmentID, RentalItemStatus, LastName, ReturnDate
                            from Whrc_RentalInvoiceItem, Whrc_Invoice, Whrc_Users
                            where Whrc_RentalInvoiceItem.InvoiceId=Whrc_Invoice.Id
	                            and Whrc_Invoice.ClientId=Whrc_Users.Id and Whrc_Invoice.OrderStatus NOT IN (0)";

                OdbcCommand cmd = new OdbcCommand(sql);

                DataSet ds = ExecuteQuery(cmd);

                return ds.Tables[0];
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        internal DataTable GetClientFeeOwedDataTable()
        {
            try
            {
                //string sql = @"Select Distinct LastName, FirstName, AreaCode, Phone
                           // From WHRC_Invoice, Whrc_Users
                           // Where Whrc_Invoice.ClientId=Whrc_Users.Id and Whrc_Invoice.OrderStatus NOT IN (0)";
//                string sql = @"Select Distinct LastName, FirstName, AreaCode, Phone,WHRC_Invoice.PurchaseDate as Date, Amount as AmountOwed
//                            From WHRC_Invoice, Whrc_Users, WHRC_InvoicePayment
//                            Where Whrc_Invoice.ClientId=Whrc_Users.Id and WHRC_InvoicePayment.UserId=Whrc_Users.Id and Whrc_Invoice.OrderStatus NOT IN (0) and WHRC_InvoicePayment.PaymentStatus NOT IN (0)";

                string sql = @"(Select  LastName, FirstName, AreaCode, Phone,WHRC_Invoice.PurchaseDate as Date,Amount as AmountOwed From WHRC_Invoice, Whrc_Users, WHRC_InvoicePayment Where WHRC_InvoicePayment.InvoiceId=Whrc_Invoice.Id 
and Whrc_Invoice.ClientId=Whrc_Users.Id and WHRC_InvoicePayment.UserId=Whrc_Users.Id 
and Whrc_Invoice.ClientId = WHRC_InvoicePayment.UserId and Whrc_Invoice.OrderStatus  IN (1)) Union
(Select LastName, FirstName, AreaCode, Phone,WHRC_Invoice.PurchaseDate as Date,Price as AmountOwed From WHRC_Invoice, Whrc_Users, WHRC_InvoicePayment,Whrc_ClassInvoiceItem
Where WHRC_InvoicePayment.InvoiceId=Whrc_Invoice.Id and Whrc_Invoice.ClientId=Whrc_Users.Id 
and WHRC_InvoicePayment.UserId=Whrc_Users.Id and Whrc_Invoice.ClientId = WHRC_InvoicePayment.UserId and
Whrc_ClassInvoiceItem.InvoiceId=Whrc_Invoice.Id and Whrc_ClassInvoiceItem.InvoiceId=WHRC_InvoicePayment.InvoiceId
and Whrc_ClassInvoiceItem.ClassInvoiceItemState=2)";

//                string sql = @"(Select Distinct LastName, FirstName, AreaCode, Phone,WHRC_Invoice.PurchaseDate as Date, Amount as AmountOwed
//From WHRC_Invoice, Whrc_Users, WHRC_InvoicePayment
//Where Whrc_Invoice.ClientId=Whrc_Users.Id and WHRC_InvoicePayment.UserId=Whrc_Users.Id and WHRC_InvoicePayment.InvoiceId=Whrc_Invoice.Id
//and Whrc_Invoice.OrderStatus NOT IN (0) and WHRC_InvoicePayment.PaymentStatus NOT IN (0) and WHRC_InvoicePayment.Amount Not In (0)) 
//";
                OdbcCommand cmd = new OdbcCommand(sql);
               // cmd.Parameters.AddWithValue("@Whrc_ClassInvoiceItem.Id", classItemId);
                DataSet ds = ExecuteQuery(cmd);

                return ds.Tables[0];
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        internal DataTable GetRevenueTaxReceivedForRental(DateTime startDate, DateTime endDate)
        {
            try
            {
                string sql = @"Select MAX(Whrc_Invoice.PaymentDate) as PaymentDate, SUM(RPSalesTax) as RPSalesTax
                            From WHRC_Invoice, WHRC_RentalinvoiceItem
                            Where WHRC_Invoice.ID = WHRC_RentalinvoiceItem.InvoiceID and Whrc_Invoice.OrderStatus NOT IN (0)
	                            and Whrc_Invoice.PaymentDate between ? and ?
                            Group by WHRC_Invoice.Id";

                OdbcCommand cmd = new OdbcCommand(sql);

                cmd.Parameters.AddWithValue("@startDate", startDate);
                cmd.Parameters.AddWithValue("@endDate", endDate);

                DataSet ds = ExecuteQuery(cmd);

                return ds.Tables[0];
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        internal DataTable GetRevenueTaxReceivedForProduct(DateTime startDate, DateTime endDate)
        {
            try
            {
                string sql = @"Select MAX(Whrc_Invoice.PaymentDate) as PaymentDate, SUM(TaxAmount) as TaxAmount
                            From WHRC_Invoice, Whrc_ProductInvoiceItem
                            Where WHRC_Invoice.ID = Whrc_ProductInvoiceItem.InvoiceID and Whrc_Invoice.OrderStatus NOT IN (0)
	                            and Whrc_Invoice.PaymentDate between ? and ?
                            Group by WHRC_Invoice.Id";

                OdbcCommand cmd = new OdbcCommand(sql);

                cmd.Parameters.AddWithValue("@startDate", startDate);
                cmd.Parameters.AddWithValue("@endDate", endDate);

                DataSet ds = ExecuteQuery(cmd);

                return ds.Tables[0];
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        internal DataTable GetRevenueDeposit(DateTime startDate, DateTime endDate)
        {
            try
            {
                string sql = @"Select MAX(Whrc_Invoice.PaymentDate) as PaymentDate, SUM(DepositAmount) as DepositAmount
                            From WHRC_Invoice, WHRC_RentalinvoiceItem
                            Where WHRC_Invoice.ID = WHRC_RentalinvoiceItem.InvoiceID and Whrc_Invoice.OrderStatus NOT IN (0)
	                            and Whrc_Invoice.PaymentDate between ? and ?
                            Group by WHRC_Invoice.Id";

                OdbcCommand cmd = new OdbcCommand(sql);

                cmd.Parameters.AddWithValue("@startDate", startDate);
                cmd.Parameters.AddWithValue("@endDate", endDate);

                DataSet ds = ExecuteQuery(cmd);

                return ds.Tables[0];
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        internal DataTable GetRevenueBreakdownSummaryClass(DateTime startDate, DateTime endDate)
        {
            try
            {
                string sql = @"Select MAX(Whrc_Invoice.PaymentDate) as PaymentDate, SUM(Price) as Class
                            From WHRC_Invoice, Whrc_ClassInvoiceItem
                            Where WHRC_Invoice.ID = Whrc_ClassInvoiceItem.InvoiceID and Whrc_Invoice.OrderStatus NOT IN (0)
                                and Whrc_Invoice.PaymentDate between ? and ?
                            Group by WHRC_Invoice.Id";

                OdbcCommand cmd = new OdbcCommand(sql);

                cmd.Parameters.AddWithValue("@startDate", startDate);
                cmd.Parameters.AddWithValue("@endDate", endDate);

                DataSet ds = ExecuteQuery(cmd);

                return ds.Tables[0];
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        internal DataTable GetRevenueBreakdownSummaryMembership(DateTime startDate, DateTime endDate)
        {
            try
            {
                string sql = @"Select MAX(Whrc_Invoice.PaymentDate) as PaymentDate, SUM(Price) as Membership
                            From WHRC_Invoice, Whrc_MembershipInvoiceItem
                            Where WHRC_Invoice.ID = Whrc_MembershipInvoiceItem.InvoiceID and Whrc_Invoice.OrderStatus NOT IN (0)
                                and Whrc_Invoice.PaymentDate between ? and ?
                            Group by WHRC_Invoice.Id";

                OdbcCommand cmd = new OdbcCommand(sql);

                cmd.Parameters.AddWithValue("@startDate", startDate);
                cmd.Parameters.AddWithValue("@endDate", endDate);

                DataSet ds = ExecuteQuery(cmd);

                return ds.Tables[0];
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        internal DataTable GetRevenueBreakdownSummaryProduct(DateTime startDate, DateTime endDate)
        {
            try
            {
                string sql = @"Select MAX(Whrc_Invoice.PaymentDate) as PaymentDate, SUM(Price) as Purchase
                            From WHRC_Invoice, Whrc_ProductInvoiceItem
                            Where WHRC_Invoice.ID = Whrc_ProductInvoiceItem.InvoiceID and Whrc_Invoice.OrderStatus NOT IN (0)
                                and Whrc_Invoice.PaymentDate between ? and ?
                            Group by WHRC_Invoice.Id";

                OdbcCommand cmd = new OdbcCommand(sql);

                cmd.Parameters.AddWithValue("@startDate", startDate);
                cmd.Parameters.AddWithValue("@endDate", endDate);

                DataSet ds = ExecuteQuery(cmd);

                return ds.Tables[0];
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        internal DataTable GetRevenueBreakdownSummaryRental(DateTime startDate, DateTime endDate)
        {
            try
            {
                string sql = @"Select MAX(Whrc_Invoice.PaymentDate) as PaymentDate, SUM(DepositAmount) as Deposit,
                                SUM(WHRC_RentalinvoiceItem.RPTotal) as RentalFee
                            From WHRC_Invoice, WHRC_RentalinvoiceItem
                            Where WHRC_Invoice.ID = WHRC_RentalinvoiceItem.InvoiceID and Whrc_Invoice.OrderStatus NOT IN (0)
                                and Whrc_Invoice.PaymentDate between ? and ?
                            Group by WHRC_Invoice.Id";

                OdbcCommand cmd = new OdbcCommand(sql);

                cmd.Parameters.AddWithValue("@startDate", startDate);
                cmd.Parameters.AddWithValue("@endDate", endDate);

                DataSet ds = ExecuteQuery(cmd);

                return ds.Tables[0];
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        internal DataTable GetRevenueBreakdownClassDataTable(DateTime startDate, DateTime endDate)
        {
            try
            {
                string sql = @"Select Whrc_Invoice.PaymentDate, WHRC_ClassInvoiceItem.SectionId, WHRC_ClassInvoiceItem.Price
                            From WHRC_ClassInvoiceItem
                            join Whrc_Invoice
                            on WHRC_ClassInvoiceItem.InvoiceId = Whrc_Invoice.Id
                            where Whrc_Invoice.OrderStatus NOT IN (0) and Whrc_Invoice.PaymentDate between ? and ?
                            order by Whrc_Invoice.PaymentDate";

                OdbcCommand cmd = new OdbcCommand(sql);

                cmd.Parameters.AddWithValue("@startDate", startDate);
                cmd.Parameters.AddWithValue("@endDate", endDate);

                DataSet ds = ExecuteQuery(cmd);

                return ds.Tables[0];
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        internal DataTable GetRevenueBreakdownProductItemDataTable()
        {
            try
            {
                string sql = @"Select ProductId, PaymentDate, Price
                            From WHRC_Invoice, Whrc_ProductInvoiceItem
                            Where Whrc_Invoice.Id=Whrc_ProductInvoiceItem.InvoiceId and Whrc_Invoice.OrderStatus NOT IN (0)
                            Order by PaymentDate, ProductId ";

                OdbcCommand cmd = new OdbcCommand(sql);

                DataSet ds = ExecuteQuery(cmd);

                return ds.Tables[0];
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        internal DataTable GetRevenueMOMMobileSummaryClass(DateTime startDate, DateTime endDate)
        {
            try
            {
                string sql = @"Select MAX(Whrc_Invoice.PaymentDate) as PaymentDate, SUM(Price) as Class
                            From WHRC_Invoice, Whrc_ClassInvoiceItem
                            Where WHRC_Invoice.ID = Whrc_ClassInvoiceItem.InvoiceID and Whrc_Invoice.OrderStatus NOT IN (0) and Whrc_Invoice.InvoiceType=2
                                and Whrc_Invoice.PaymentDate between ? and ?
                            Group by WHRC_Invoice.Id";

                OdbcCommand cmd = new OdbcCommand(sql);

                cmd.Parameters.AddWithValue("@startDate", startDate);
                cmd.Parameters.AddWithValue("@endDate", endDate);

                DataSet ds = ExecuteQuery(cmd);

                return ds.Tables[0];
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        internal DataTable GetRevenueMOMMobileSummaryMembership(DateTime startDate, DateTime endDate)
        {
            try
            {
                string sql = @"Select MAX(Whrc_Invoice.PaymentDate) as PaymentDate, SUM(Price) as Membership
                            From WHRC_Invoice, Whrc_MembershipInvoiceItem
                            Where WHRC_Invoice.ID = Whrc_MembershipInvoiceItem.InvoiceID and Whrc_Invoice.OrderStatus NOT IN (0) and Whrc_Invoice.InvoiceType=2
                                and Whrc_Invoice.PaymentDate between ? and ?
                            Group by WHRC_Invoice.Id";

                OdbcCommand cmd = new OdbcCommand(sql);

                cmd.Parameters.AddWithValue("@startDate", startDate);
                cmd.Parameters.AddWithValue("@endDate", endDate);

                DataSet ds = ExecuteQuery(cmd);

                return ds.Tables[0];
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        internal DataTable GetRevenueMOMMobileSummaryProduct(DateTime startDate, DateTime endDate)
        {
            try
            {
                string sql = @"Select MAX(Whrc_Invoice.PaymentDate) as PaymentDate, SUM(Price) as Purchase
                            From WHRC_Invoice, Whrc_ProductInvoiceItem
                            Where WHRC_Invoice.ID = Whrc_ProductInvoiceItem.InvoiceID and Whrc_Invoice.OrderStatus NOT IN (0) and Whrc_Invoice.InvoiceType=2
                                and Whrc_Invoice.PaymentDate between ? and ?
                            Group by WHRC_Invoice.Id";

                OdbcCommand cmd = new OdbcCommand(sql);

                cmd.Parameters.AddWithValue("@startDate", startDate);
                cmd.Parameters.AddWithValue("@endDate", endDate);

                DataSet ds = ExecuteQuery(cmd);

                return ds.Tables[0];
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        internal DataTable GetRevenueMOMMobileSummaryRental(DateTime startDate, DateTime endDate)
        {
            try
            {
                string sql = @"Select MAX(Whrc_Invoice.PaymentDate) as PaymentDate, SUM(DepositAmount) as Deposit,
                                SUM(WHRC_RentalinvoiceItem.RPTotal) as RentalFee
                            From WHRC_Invoice, WHRC_RentalinvoiceItem
                            Where WHRC_Invoice.ID = WHRC_RentalinvoiceItem.InvoiceID and Whrc_Invoice.OrderStatus NOT IN (0) and Whrc_Invoice.InvoiceType=2
                                and Whrc_Invoice.PaymentDate between ? and ?
                            Group by WHRC_Invoice.Id";

                OdbcCommand cmd = new OdbcCommand(sql);

                cmd.Parameters.AddWithValue("@startDate", startDate);
                cmd.Parameters.AddWithValue("@endDate", endDate);

                DataSet ds = ExecuteQuery(cmd);

                return ds.Tables[0];
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        internal DataTable GetRevenueMOMMobileProductItemDataTable()
        {
            try
            {
                string sql = @"Select ProductId, PaymentDate, Price
                            From WHRC_Invoice, Whrc_ProductInvoiceItem
                            Where Whrc_Invoice.Id=Whrc_ProductInvoiceItem.InvoiceId and Whrc_Invoice.OrderStatus NOT IN (0) and Whrc_Invoice.InvoiceType=2
                            Order by PaymentDate, ProductId ";

                OdbcCommand cmd = new OdbcCommand(sql);

                DataSet ds = ExecuteQuery(cmd);

                return ds.Tables[0];
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        internal DataTable GetRevenueMOMMobileRentalDataTable(DateTime startDate, DateTime endDate)
        {
            try
            {
                string sql = @"Select Whrc_Invoice.PaymentDate, Whrc_RentalInvoiceItem.RentalEquipmentID, Whrc_RentalInvoiceItem.RPTotal AS Price
                            From Whrc_RentalInvoiceItem
                            join Whrc_Invoice
                            on Whrc_RentalInvoiceItem.InvoiceId = Whrc_Invoice.Id
                            where Whrc_Invoice.OrderStatus NOT IN (0) and Whrc_Invoice.InvoiceType=2 and Whrc_Invoice.PaymentDate between ? and ?
                            order by Whrc_Invoice.PaymentDate";

                OdbcCommand cmd = new OdbcCommand(sql);

                cmd.Parameters.AddWithValue("@startDate", startDate);
                cmd.Parameters.AddWithValue("@endDate", endDate);

                DataSet ds = ExecuteQuery(cmd);

                return ds.Tables[0];
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        internal DataTable GetRevenueBreakdownClientCreditClass(DateTime startDate, DateTime endDate)
        {
            try
            {
                string sql = @"Select MAX(Whrc_Invoice.PaymentDate) as PaymentDate, SUM(Price) as Class
                            From WHRC_Invoice, Whrc_ClassInvoiceItem
                            Where WHRC_Invoice.ID = Whrc_ClassInvoiceItem.InvoiceID and Whrc_Invoice.OrderStatus NOT IN (0)
                                and Whrc_Invoice.PaymentDate between '1/1/2000' and '5/5/2050' and Whrc_ClassInvoiceItem.IsClassCanceled=?
                            Group by WHRC_Invoice.Id";

                OdbcCommand cmd = new OdbcCommand(sql);

                cmd.Parameters.AddWithValue("@startDate", startDate);
                cmd.Parameters.AddWithValue("@endDate", endDate);
                cmd.Parameters.AddWithValue("@isClassCanceled", true);

                DataSet ds = ExecuteQuery(cmd);

                return ds.Tables[0];
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        internal DataTable GetRevenueBreakdownClientCreditProduct(DateTime startDate, DateTime endDate)
        {
            try
            {
                string sql = @"Select MAX(Whrc_Invoice.PaymentDate) as PaymentDate, SUM(Price) as Purchase
                            From WHRC_Invoice, Whrc_ProductInvoiceItem
                            Where WHRC_Invoice.ID = Whrc_ProductInvoiceItem.InvoiceID and Whrc_Invoice.OrderStatus NOT IN (0)
                                and Whrc_Invoice.PaymentDate between ? and ? and Whrc_ProductInvoiceItem.IsItemReturned=?
                            Group by WHRC_Invoice.Id";

                OdbcCommand cmd = new OdbcCommand(sql);

                cmd.Parameters.AddWithValue("@startDate", startDate);
                cmd.Parameters.AddWithValue("@endDate", endDate);
                cmd.Parameters.AddWithValue("@isItemReturned", true);

                DataSet ds = ExecuteQuery(cmd);

                return ds.Tables[0];
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        internal DataTable GetRevenueBreakdownClientCreditRental(DateTime startDate, DateTime endDate)
        {
            try
            {
                string sql = @"Select MAX(Whrc_Invoice.PaymentDate) as PaymentDate, SUM(DepositAmount) as Deposit
                            From WHRC_Invoice, WHRC_RentalinvoiceItem
                            Where WHRC_Invoice.ID = WHRC_RentalinvoiceItem.InvoiceID and Whrc_Invoice.OrderStatus NOT IN (0)
                                and Whrc_Invoice.PaymentDate between ? and ? and WHRC_RentalinvoiceItem.IsItemReturned=?
                            Group by WHRC_Invoice.Id";

                OdbcCommand cmd = new OdbcCommand(sql);

                cmd.Parameters.AddWithValue("@startDate", startDate);
                cmd.Parameters.AddWithValue("@endDate", endDate);
                cmd.Parameters.AddWithValue("@isItemReturned", true);

                DataSet ds = ExecuteQuery(cmd);

                return ds.Tables[0];
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        internal DataTable GetRevenueBreakdownClientCreditDiscount(DateTime startDate, DateTime endDate)
        {
            try
            {
                string sql = @"Select PaymentDate, Discount
                            From WHRC_Invoice
                            Where Whrc_Invoice.OrderStatus NOT IN (0)
                                and Whrc_Invoice.PaymentDate between ? and ?";

                OdbcCommand cmd = new OdbcCommand(sql);

                cmd.Parameters.AddWithValue("@startDate", startDate);
                cmd.Parameters.AddWithValue("@endDate", endDate);

                DataSet ds = ExecuteQuery(cmd);

                return ds.Tables[0];
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        internal DataTable GetRevenueBreakdownClientCreditPackageDiscount(DateTime startDate, DateTime endDate)
        {
            try
            {
                string sql = @"Select PaymentDate, PackageDiscount = CASE WHEN PackageDiscount IS NULL THEN 0.00 ELSE PackageDiscount END
                            From WHRC_Invoice
                            Where Whrc_Invoice.OrderStatus NOT IN (0)
                                and Whrc_Invoice.PaymentDate between ? and ?";

                OdbcCommand cmd = new OdbcCommand(sql);

                cmd.Parameters.AddWithValue("@startDate", startDate);
                cmd.Parameters.AddWithValue("@endDate", endDate);

                DataSet ds = ExecuteQuery(cmd);

                return ds.Tables[0];
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        internal List<ClassInvoiceItem> GetAllClassSignips(DateTime startDate, DateTime endDate)
        {
            try
            {
                string sql = @"SELECT Whrc_ClassInvoiceItem.* 
                            FROM Whrc_ClassInvoiceItem, Whrc_Invoice
                            WHERE Whrc_ClassInvoiceItem.InvoiceId=Whrc_Invoice.Id 
                                and Whrc_Invoice.OrderStatus NOT IN (0)
                                and Whrc_Invoice.PaymentDate between ? and ?
                                and Whrc_ClassInvoiceItem.IsClassCanceled=?";

                OdbcCommand cmd = new OdbcCommand(sql);

                cmd.Parameters.AddWithValue("@startDate", startDate);
                cmd.Parameters.AddWithValue("@endDate", endDate);
                cmd.Parameters.AddWithValue("@isClassCanceled", false);

                DataSet ds = ExecuteQuery(cmd);

                return new ClassInvoiceItemDataMapper().GetListFromDataSet(ds);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        internal List<Invoice> GetInvoiceMedicalSignups(DateTime startDate, DateTime endDate)
        {
            try
            {
                string sql = @"Select * From WHRC_invoice Where ID in(
                             SELECT Whrc_ClassInvoiceItem.invoiceID 
                            FROM Whrc_ClassInvoiceItem, Whrc_Invoice
                            WHERE Whrc_ClassInvoiceItem.InvoiceId=Whrc_Invoice.Id 
                            and Whrc_Invoice.OrderStatus NOT IN (0)
                            and Whrc_Invoice.PaymentDate between ? and ?
                            and Whrc_ClassInvoiceItem.IsClassCanceled=?) And paymentMode=3";

                OdbcCommand cmd = new OdbcCommand(sql);

                cmd.Parameters.AddWithValue("@startDate", startDate);
                cmd.Parameters.AddWithValue("@endDate", endDate);
                cmd.Parameters.AddWithValue("@isClassCanceled", false);

                DataSet ds = ExecuteQuery(cmd);

                return new InvoiceDataMapper().GetListFromDataSet(ds);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        internal DataTable GetClassAttendingsGroupBySectionId()
        {
            try
            {
                string sql = @"SELECT SectionID, Sum(NumberAttending)as TotalAttending
                            FROM Whrc_ClassInvoiceItem, Whrc_Invoice
                            WHERE Whrc_ClassInvoiceItem.InvoiceId=Whrc_Invoice.Id 
                                and Whrc_Invoice.OrderStatus NOT IN (0)
                                and Whrc_ClassInvoiceItem.IsClassCanceled=?
                            Group By SectionID";

                OdbcCommand cmd = new OdbcCommand(sql);

                cmd.Parameters.AddWithValue("@isClassCanceled", false);

                DataSet ds = ExecuteQuery(cmd);

                return ds.Tables[0];
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        internal bool GetMedicalSignupExistsByInvoiceId(long invoiceId)
        {
            try
            {
                string sql = @"SELECT Id
                            FROM WHRC_InvoicePayment
                            WHERE InvoiceId=? and PaymentMode=3";

                OdbcCommand cmd = new OdbcCommand(sql);

                cmd.Parameters.AddWithValue("@invoiceId", invoiceId);

                DataSet ds = ExecuteQuery(cmd);

                DataTable dt = ds.Tables[0];

                return (dt.Rows.Count > 0);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
