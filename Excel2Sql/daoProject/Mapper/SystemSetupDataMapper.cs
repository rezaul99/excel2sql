﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.Odbc;
using System.Collections;
using daoProject.Infrastructure;
using daoProject.Model;
using daoProject.Mapper;

namespace daoProject.Mapper
{
    internal class SystemSetupDataMapper : SqlHelper
    {
        private static string _cmdInsert = @"INSERT INTO Whrc_SystemSetup (Id,SalesTaxRate,MembershipFee,EquipmentDeposit,HospitalDiscountName,HospitalDiscountAmount) VALUES (?,?,?,?,?,?); ";
        private static string _cmdUpdate = @"UPDATE Whrc_SystemSetup SET SalesTaxRate=?,MembershipFee=?,EquipmentDeposit=?,HospitalDiscountName=?,HospitalDiscountAmount=? WHERE Id=? ";
        private static string _cmdDelete = @"DELETE FROM Whrc_SystemSetup WHERE Id=? ";
        private static string _cmdSelectAll = @"SELECT * FROM Whrc_SystemSetup ";
        private static string _cmdSelectById = @"SELECT * FROM Whrc_SystemSetup WHERE Id=? ";
        
        internal void Create(SystemSetup systemSetup)
        {
            try
            {
                OdbcCommand cmd = new OdbcCommand(_cmdInsert);

                cmd.Parameters.AddWithValue("@Id", systemSetup.Id);
                cmd.Parameters.AddWithValue("@SalesTaxRate", systemSetup.SalesTaxRate);
                cmd.Parameters.AddWithValue("@MembershipFee", systemSetup.MembershipFee);
                cmd.Parameters.AddWithValue("@EquipmentDeposit", systemSetup.EquipmentDeposit);
                cmd.Parameters.AddWithValue("@HospitalDiscountName", systemSetup.HospitalDiscountName);
                cmd.Parameters.AddWithValue("@HospitalDiscountAmount", systemSetup.HospitalDiscountAmount);

                //Execute the OdbcCommand
                ExecuteNonQuery(cmd);
            }
            catch (Exception ex)
            {
                //Attempt to throw the exception for error handling
                throw ex;
            }
        }

        internal void Update(SystemSetup systemSetup)
        {
            try
            {
                OdbcCommand cmd = new OdbcCommand(_cmdUpdate);

                cmd.Parameters.AddWithValue("@SalesTaxRate", systemSetup.SalesTaxRate);
                cmd.Parameters.AddWithValue("@MembershipFee", systemSetup.MembershipFee);
                cmd.Parameters.AddWithValue("@EquipmentDeposit", systemSetup.EquipmentDeposit);
                cmd.Parameters.AddWithValue("@HospitalDiscountName", systemSetup.HospitalDiscountName);
                cmd.Parameters.AddWithValue("@HospitalDiscountAmount", systemSetup.HospitalDiscountAmount);
                cmd.Parameters.AddWithValue("@Id", systemSetup.Id);

                //Execute the OdbcCommand
                ExecuteNonQuery(cmd);
            }
            catch (Exception ex)
            {
                //Attempt to throw the exception for error handling
                throw ex;
            }
        }

        internal bool Delete(long id)
        {
            bool success = false;

            try
            {
                OdbcCommand cmd = new OdbcCommand(_cmdDelete);

                cmd.Parameters.AddWithValue("@Id", id);

                ExecuteNonQuery(cmd);

                success = true;
            }
            catch (Exception ex)
            {
                //Attempt to throw the exception for error handling
                throw ex;
            }

            return success;
        }

        private List<SystemSetup> GetListFromDataSet(DataSet ds)
        {
            List<SystemSetup> systemSetupList = new List<SystemSetup>();

            // This Loop reads all Data from DataSet                    
            for (int i = 0; i < ds.Tables["Data"].Rows.Count; i++)
            {
                SystemSetup systemSetup = new SystemSetup();

                systemSetup.Id = Convert.ToInt64(ds.Tables["Data"].Rows[i].ItemArray[0]);
                systemSetup.SalesTaxRate = Convert.ToDecimal(ds.Tables["Data"].Rows[i].ItemArray[1]);
                systemSetup.MembershipFee = Convert.ToDecimal(ds.Tables["Data"].Rows[i].ItemArray[2]);
                systemSetup.EquipmentDeposit = Convert.ToDecimal(ds.Tables["Data"].Rows[i].ItemArray[3]);
                systemSetup.HospitalDiscountName = Convert.ToString(ds.Tables["Data"].Rows[i].ItemArray[4]);
                systemSetup.HospitalDiscountAmount = Convert.ToDecimal(ds.Tables["Data"].Rows[i].ItemArray[5]);

                systemSetupList.Add(systemSetup);
            }

            return systemSetupList;
        }

        internal List<SystemSetup> GetAll()
        {
            try
            {
                //Create instance of sqlcommand
                OdbcCommand cmd = new OdbcCommand(_cmdSelectAll);

                DataSet ds = ExecuteQuery(cmd);

                return GetListFromDataSet(ds);
            }
            catch (Exception ex)
            {
                //Attempt to throw the exception for error handling
                throw ex;
            }
        }

        internal SystemSetup GetById(long id)
        {
            try
            {
                OdbcCommand cmd = new OdbcCommand(_cmdSelectById);

                cmd.Parameters.AddWithValue("@Id", id);

                DataSet ds = ExecuteQuery(cmd);

                return GetListFromDataSet(ds)[0];
            }
            catch (Exception ex)
            {
                //Attempt to throw the exception for error handling
                throw ex;
            }
        }
    }
}
