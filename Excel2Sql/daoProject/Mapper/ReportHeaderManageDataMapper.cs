﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using daoProject.Model;
using System.Data;
using System.Data.Odbc;
using System.Collections;
using daoProject.Infrastructure;
using daoProject.Model;
using daoProject.Mapper;
namespace daoProject.Mapper
{
   public  class ReportHeaderManageDataMapper : SqlHelper
    {
       private static string _cmdGetAll = @"SELECT * FROM Whrc_ReportHeaderManage";
       private static string _cmdSelectById = @"SELECT * FROM Whrc_ReportHeaderManage Where Id=?";
       private static string _cmdInsert = @"INSERT INTO Whrc_ReportHeaderManage (ReportHeaderValue) VALUES (?); ";
       private static string _cmdUpdate = @"UPDATE Whrc_ReportHeaderManage SET ReportHeaderValue=? WHERE Id=?";

       public List<ReportHeaderManager> GetAll()
       {
           try
            {
                OdbcCommand cmd = new OdbcCommand(_cmdGetAll);
                DataSet ds = ExecuteQuery(cmd);
                return GetListFromDataSet(ds);
            }
            catch (Exception ex)
            {
                throw ex;
            }
       }

       public ReportHeaderManager GetAllById(long Id)
       {
           try
           {
               OdbcCommand cmd = new OdbcCommand(_cmdSelectById);

               cmd.Parameters.AddWithValue("@Id", Id);

               DataSet ds = ExecuteQuery(cmd);

               return GetListFromDataSet(ds)[0];
           }
           catch (Exception ex)
           {
               //Attempt to throw the exception for error handling
               throw ex;
           }
       }

       private List<ReportHeaderManager> GetListFromDataSet(DataSet ds)
       {
           List<ReportHeaderManager> RdlcGenerateClassObjList = new List<ReportHeaderManager>();

           // This Loop reads all Data from DataSet                    
           for (int i = 0; i < ds.Tables["Data"].Rows.Count; i++)
           {
               ReportHeaderManager RdlcGenerateClassObj = new ReportHeaderManager();

               RdlcGenerateClassObj.Id = Convert.ToInt64(ds.Tables["Data"].Rows[i].ItemArray[0]);
               RdlcGenerateClassObj.ReportHeaderValue= Convert.ToString(ds.Tables["Data"].Rows[i].ItemArray[1]);
             
               RdlcGenerateClassObjList.Add(RdlcGenerateClassObj);
           }

           return RdlcGenerateClassObjList;
       }

       public void Add(ReportHeaderManager RdlcGenerateObj)
       {
           long id = -1;
           try
           {
               OdbcCommand cmd = new OdbcCommand(_cmdInsert);

               cmd.Parameters.AddWithValue("@ReportHeaderValue", RdlcGenerateObj.ReportHeaderValue);

               id = Convert.ToInt64(ExecuteNonQueryAndScalar(cmd));

               RdlcGenerateObj.Id = id;
           }
           catch (Exception ex)
           {
               throw ex;
           }

       }

       public void Add(ReportHeaderManager RdlcGenerateObj, System.Data.Odbc.OdbcConnection conn, System.Data.Odbc.OdbcTransaction tx)
       {
           long id = -1;
           try
           {
               OdbcCommand cmd = new OdbcCommand(_cmdInsert);

               cmd.Parameters.AddWithValue("@ReportHeaderValue", RdlcGenerateObj.ReportHeaderValue);

               id = Convert.ToInt64(ExecuteNonQueryAndScalar(cmd, conn, tx));

               RdlcGenerateObj.Id = id;
           }
           catch (Exception ex)
           {
               throw ex;
           }

       }

       public void Update(ReportHeaderManager RdlcGenerateObjd)
       {
           try
           {
               OdbcCommand cmd = new OdbcCommand(_cmdUpdate);

               cmd.Parameters.AddWithValue("@ReportHeaderValue", RdlcGenerateObjd.ReportHeaderValue);

               cmd.Parameters.AddWithValue("@Id", RdlcGenerateObjd.Id);

               ExecuteNonQuery(cmd);
           }
           catch (Exception ex)
           {
               throw ex;
           }
       }
    }
}
