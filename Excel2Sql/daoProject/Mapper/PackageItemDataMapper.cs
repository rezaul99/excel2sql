﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Odbc;
using System.Collections;
using daoProject.Infrastructure;
using daoProject.Model;
using daoProject.Mapper;
using daoProject.Service;
using System.Data;


namespace daoProject.Mapper
{
    public class PackageItemDataMapper : SqlHelper
    {
        private static string _cmdInsert = @"INSERT INTO Whrc_PackageItem (PackageId,RecordId,Quantity) VALUES (?,?,?); ";
        private static string _cmdUpdate = @"UPDATE Whrc_Invoice SET ClientId=?,InvoiceType=?,ShippingAddressId=?,PurchaseDate=?,TotalAmount=?,Discount=?,NetPayable=?,PaymentMode=?,PaidAmount=?, PaymentDate=?,AddedBy=?,AddedDate=?,ModifiedBy=?,ModifiedDate=?,OrderStatus=?,ClientRequestedDiscountMsg=?,DiscountTypeId=?,DiscountSubTypeId=?, TotalRefundAmount=?,StaffInitial=?,PaymentNotes=? WHERE Id=? ";
        private static string _cmdDelete = @"DELETE FROM Whrc_Invoice WHERE Id=? ";
        private static string _cmdDeleteByPackageId = @"DELETE FROM Whrc_PackageItem WHERE PackageId=? ";
        private static string _cmdSelectAll = @"SELECT * FROM Whrc_PackageItem ";
        private static string _cmdSelectById = @"SELECT * FROM Whrc_PackageItem WHERE Id=? ";
        private static string _cmdSelectByPackageId = @"SELECT * FROM Whrc_PackageItem WHERE PackageId=? ";
       // private static string _cmdSelectPackageByRecordId = @"Select Whrc_PackageItem.id,Whrc_PackageItem.PackageId, Whrc_PackageItem.RecordID,Whrc_PackageItem.Quantity from Whrc_Package, Whrc_PackageItem Where Whrc_Package.id = Whrc_PackageItem.PackageId And Active = ? And recordid = ?";
        //@"select * from Whrc_Package where id in(select packageid from Whrc_PackageItem where recordid = ?) And Active = ? ";
        private static string _cmdSelectPackageByRecordId = @"select Whrc_PackageItem.id,Whrc_PackageItem.PackageId, Whrc_PackageItem.RecordID,Whrc_PackageItem.Quantity from Whrc_PackageItem  where Whrc_PackageItem.PackageId in (select id from Whrc_Package where Active = ?) And Whrc_PackageItem.RecordID = ? ";

        private static string _cmdGetPackagebyRecordIdToDelete = @"select * from  Whrc_PackageItem   where Whrc_PackageItem.RecordID = ? ";
        internal void Create(PackageItemWHRC packageItemWHRC, System.Data.Odbc.OdbcConnection conn, System.Data.Odbc.OdbcTransaction tx)
        {
            long id = -1;
            try
            {
                OdbcCommand cmd = new OdbcCommand(_cmdInsert);

                cmd.Parameters.AddWithValue("@PackageId", packageItemWHRC.PackageId);
                cmd.Parameters.AddWithValue("@RecordId", packageItemWHRC.RecordId);
                cmd.Parameters.AddWithValue("@Quantity", packageItemWHRC.Quantity);

                //Execute the OdbcCommand
                id = Convert.ToInt64(ExecuteNonQueryAndScalar(cmd, conn, tx));

                packageItemWHRC.ID = id;
            }
            catch (Exception ex)
            {
                //Attempt to throw the exception for error handling
                throw ex;
            }
        }
       
        internal void Update(Invoice invoice, System.Data.Odbc.OdbcConnection conn, System.Data.Odbc.OdbcTransaction tx)
        {
            try
            {
                OdbcCommand cmd = new OdbcCommand(_cmdUpdate);

                cmd.Parameters.AddWithValue("@ClientId", invoice.ClientId);
                cmd.Parameters.AddWithValue("@InvoiceType", invoice.InvoiceType);
                cmd.Parameters.AddWithValue("@ShippingAddressId", invoice.ShippingAddressId);
                cmd.Parameters.AddWithValue("@PurchaseDate", invoice.PurchaseDate);
                cmd.Parameters.AddWithValue("@TotalAmount", invoice.TotalAmount);
                cmd.Parameters.AddWithValue("@Discount", invoice.Discount);
                cmd.Parameters.AddWithValue("@NetPayable", invoice.NetPayable);
                cmd.Parameters.AddWithValue("@PaymentMode", invoice.PaymentMode);
                cmd.Parameters.AddWithValue("@PaidAmount", invoice.PaidAmount);
                cmd.Parameters.AddWithValue("@PaymentDate", invoice.PaymentDate);
                cmd.Parameters.AddWithValue("@AddedBy", invoice.AddedBy);
                cmd.Parameters.AddWithValue("@AddedDate", invoice.AddedDate);
                cmd.Parameters.AddWithValue("@ModifiedBy", invoice.ModifiedBy);
                cmd.Parameters.AddWithValue("@ModifiedDate", invoice.ModifiedDate);
                cmd.Parameters.AddWithValue("@OrderStatus", invoice.OrderStatus);
                cmd.Parameters.AddWithValue("@ClientRequestedDiscountMsg", invoice.ClientRequestedDiscountMsg);
                cmd.Parameters.AddWithValue("@DiscountTypeId", invoice.DiscountTypeId);
                cmd.Parameters.AddWithValue("@DiscountSubTypeId", invoice.DiscountSubTypeId);
                cmd.Parameters.AddWithValue("@TotalRefundAmount", invoice.TotalRefundAmount);
                cmd.Parameters.AddWithValue("@StaffInitial", invoice.StaffInitial);
                cmd.Parameters.AddWithValue("@PaymentNotes", invoice.PaymentNotes);
                cmd.Parameters.AddWithValue("@Id", invoice.Id);

                //Execute the OdbcCommand
                ExecuteNonQuery(cmd, conn, tx);
            }
            catch (Exception ex)
            {
                //Attempt to throw the exception for error handling
                throw ex;
            }
        }

        internal bool Delete(long id)
        {
            bool success = false;

            try
            {
                OdbcCommand cmd = new OdbcCommand(_cmdDelete);

                cmd.Parameters.AddWithValue("@Id", id);

                ExecuteNonQuery(cmd);

                success = true;
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return success;
        }

        internal bool DeleteByPackageId(long id)
        {
            bool success = false;

            try
            {
                OdbcCommand cmd = new OdbcCommand(_cmdDeleteByPackageId);

                cmd.Parameters.AddWithValue("@PackageId", id);

                ExecuteNonQuery(cmd);

                success = true;
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return success;
        }

        private List<PackageItemWHRC> GetListFromDataSet(DataSet ds)
        {
            List<PackageItemWHRC> PackageItemWHRCList = new List<PackageItemWHRC>();

            for (int i = 0; i < ds.Tables["Data"].Rows.Count; i++)
            {
                PackageItemWHRC packageItemWHRC = new PackageItemWHRC();

                packageItemWHRC.ID = Convert.ToInt64(ds.Tables["Data"].Rows[i].ItemArray[0]);
                packageItemWHRC.PackageId = Convert.ToInt64(ds.Tables["Data"].Rows[i].ItemArray[1]);
                packageItemWHRC.RecordId = Convert.ToInt64(ds.Tables["Data"].Rows[i].ItemArray[2]);
                packageItemWHRC.Quantity = Convert.ToInt32(ds.Tables["Data"].Rows[i].ItemArray[3]);

                PackageItemWHRCList.Add(packageItemWHRC);
            }
            return PackageItemWHRCList;
        }

        //private List<long> GetPackageListFromDataSet(DataSet ds)
        //{
        //    List<long> packageIdList = new List<long>();

        //    for (int i = 0; i < ds.Tables["Data"].Rows.Count; i++)
        //    {             
        //        long packageID = Convert.ToInt64(ds.Tables["Data"].Rows[i].ItemArray[0]);

        //        packageIdList.Add(packageID);
        //    }
        //    return packageIdList;
        //}


        internal List<PackageItemWHRC> GetAll()
        {
            try
            {
                OdbcCommand cmd = new OdbcCommand(_cmdSelectAll);
                DataSet ds = ExecuteQuery(cmd);
                return GetListFromDataSet(ds);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        internal PackageItemWHRC GetById(long id)
        {
            try
            {
                OdbcCommand cmd = new OdbcCommand(_cmdSelectById);

                cmd.Parameters.AddWithValue("@Id", id);
                DataSet ds = ExecuteQuery(cmd);
                return GetListFromDataSet(ds)[0];
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<PackageItemWHRC> GetAllByPackageId(long id)
        {
            try
            {
                OdbcCommand cmd = new OdbcCommand(_cmdSelectByPackageId);
                cmd.Parameters.AddWithValue("@Id", id);
                DataSet ds = ExecuteQuery(cmd);
                return GetListFromDataSet(ds);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<PackageItemWHRC> GetPackageIdByRecordId(long id)
        {
            try
            {
                OdbcCommand cmd = new OdbcCommand(_cmdSelectPackageByRecordId);

                cmd.Parameters.AddWithValue("@Active", "1");
                cmd.Parameters.AddWithValue("@recordid", id);
               
               
                DataSet ds = ExecuteQuery(cmd);
                return GetListFromDataSet(ds);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        internal List<PackageItemWHRC> GetPackageByIdToDelete(long id)
        {
            try
            {
                OdbcCommand cmd = new OdbcCommand(_cmdGetPackagebyRecordIdToDelete);

                cmd.Parameters.AddWithValue("@recordid", id);
                DataSet ds = ExecuteQuery(cmd);
                return GetListFromDataSet(ds);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

       
    }
}
