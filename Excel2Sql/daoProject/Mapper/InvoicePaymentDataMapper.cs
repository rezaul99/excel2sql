﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.Odbc;
using System.Collections;
using daoProject.Infrastructure;
using daoProject.Model;
using daoProject.Mapper;

namespace daoProject.Mapper
{
    public class InvoicePaymentDataMapper : SqlHelper
    {
        private static string _cmdInsert = @"INSERT INTO WHRC_InvoicePayment (InvoiceId,UserId,PaymentMode,Amount,PaymentDate,TransactionStatus,TransactionID, TransactionDetails,MedicalNumberOrNationalId,PatientReceiptNumber,PaymentStatus,CheckNumber) VALUES (?,?,?,?,?,?,?,?,?,?,?,?); ";
        private static string _cmdUpdate = @"UPDATE WHRC_InvoicePayment SET InvoiceId=?,UserId=?,PaymentMode=?,Amount=?,PaymentDate=?,TransactionStatus=?,TransactionID=?, TransactionDetails=?,MedicalNumberOrNationalId=?,PatientReceiptNumber=?,PaymentStatus=?,CheckNumber=? WHERE Id=? ";
        private static string _cmdUpdateTotalAmountByInvoiceID = @"UPDATE WHRC_InvoicePayment SET Amount=? WHERE InvoiceId=? ";
        private static string _cmdUpdateUpdateAmount = @"UPDATE WHRC_InvoicePayment SET Amount=? WHERE Id=? ";
        private static string _cmdUpdateTotalAmountForMediCalMethod = @"UPDATE WHRC_InvoicePayment SET Amount=? WHERE Id=? ";
        private static string _cmdUpdateCheckNumber = @"UPDATE WHRC_InvoicePayment SET CheckNumber=? WHERE InvoiceId=? ";
        private static string _cmdUpdatePaymentStatus = @"UPDATE WHRC_InvoicePayment SET PaymentStatus=? WHERE InvoiceId=? ";
        private static string _cmdDelete = @"DELETE FROM WHRC_InvoicePayment WHERE Id=? ";
        private static string _cmdSelectAll = @"SELECT * FROM WHRC_InvoicePayment ";
        private static string _cmdSelectById = @"SELECT * FROM WHRC_InvoicePayment WHERE Id=? ";
        private static string _cmdSelectByInvoiceId = @"SELECT * FROM WHRC_InvoicePayment WHERE InvoiceId=? ";
        private static string _cmdDeleteByInvoiceId = @"DELETE FROM WHRC_InvoicePayment WHERE InvoiceId=? ";
        private static string _cmdGetTotalPaymentAmountByInvoiceID = @"SELECT Sum(Amount) FROM WHRC_InvoicePayment WHERE InvoiceId=? and PaymentStatus = 0";
        private static string _cmdGetTotalPaymentAmountOnlyByInvoiceID = @"SELECT Sum(Amount) FROM WHRC_InvoicePayment WHERE InvoiceId=?";

        private static string _cmdIsPatientReceiptExist = @"Select Count(*) From WHRC_InvoicePayment where PatientReceiptNumber=?";

        internal void Create(InvoicePaymentDetail invoicePayment)
        {
            long id = -1;
            try
            {
                OdbcCommand cmd = new OdbcCommand(_cmdInsert);

                cmd.Parameters.AddWithValue("@InvoiceId", invoicePayment.InvoiceID);
                cmd.Parameters.AddWithValue("@UserId", invoicePayment.UserId);
                cmd.Parameters.AddWithValue("@PaymentMode", invoicePayment.PaymentMode);
                cmd.Parameters.AddWithValue("@Amount", invoicePayment.Amount);
                cmd.Parameters.AddWithValue("@PaymentDate", invoicePayment.PaymentDate);
                cmd.Parameters.AddWithValue("@TransactionStatus", invoicePayment.TransactionStatus);
                cmd.Parameters.AddWithValue("@TransactionID", invoicePayment.TransactionID);
                cmd.Parameters.AddWithValue("@TransactionDetails", invoicePayment.TransactionDetails);
                cmd.Parameters.AddWithValue("@MedicalNumberOrNationalId", invoicePayment.MedicalNumberOrNationalId);
                cmd.Parameters.AddWithValue("@PatientReceiptNumber", invoicePayment.PatientReceiptNumber);
                cmd.Parameters.AddWithValue("@PaymentStatus", invoicePayment.PaymentStatus);
                cmd.Parameters.AddWithValue("@CheckNumber", invoicePayment.CheckNumber);

                //Execute the OdbcCommand
                id = Convert.ToInt64(ExecuteNonQueryAndScalar(cmd));

                invoicePayment.Id = id;
            }
            catch (Exception ex)
            {
                //Attempt to throw the exception for error handling
                throw ex;
            }
        }

        internal void Create(InvoicePaymentDetail invoicePayment, System.Data.Odbc.OdbcConnection conn, System.Data.Odbc.OdbcTransaction tx)
        {
            long id = -1;
            try
            {
                OdbcCommand cmd = new OdbcCommand(_cmdInsert);

                cmd.Parameters.AddWithValue("@InvoiceId", invoicePayment.InvoiceID);
                cmd.Parameters.AddWithValue("@UserId", invoicePayment.UserId);
                cmd.Parameters.AddWithValue("@PaymentMode", invoicePayment.PaymentMode);
                cmd.Parameters.AddWithValue("@Amount", invoicePayment.Amount);
                cmd.Parameters.AddWithValue("@PaymentDate", invoicePayment.PaymentDate);
                cmd.Parameters.AddWithValue("@TransactionStatus", invoicePayment.TransactionStatus);
                cmd.Parameters.AddWithValue("@TransactionID", invoicePayment.TransactionID);
                cmd.Parameters.AddWithValue("@TransactionDetails", invoicePayment.TransactionDetails);
                cmd.Parameters.AddWithValue("@MedicalNumberOrNationalId", invoicePayment.MedicalNumberOrNationalId);
                cmd.Parameters.AddWithValue("@PatientReceiptNumber", invoicePayment.PatientReceiptNumber);
                cmd.Parameters.AddWithValue("@CheckNumber", invoicePayment.CheckNumber);

                //Execute the OdbcCommand
                id = Convert.ToInt64(ExecuteNonQueryAndScalar(cmd, conn, tx));

                invoicePayment.Id = id;
            }
            catch (Exception ex)
            {
                //Attempt to throw the exception for error handling
                throw ex;
            }
        }

        internal void Update(InvoicePaymentDetail invoicePayment)
        {
            try
            {
                OdbcCommand cmd = new OdbcCommand(_cmdUpdate);

                cmd.Parameters.AddWithValue("@InvoiceId", invoicePayment.InvoiceID);
                cmd.Parameters.AddWithValue("@UserId", invoicePayment.UserId);
                cmd.Parameters.AddWithValue("@PaymentMode", invoicePayment.PaymentMode);
                cmd.Parameters.AddWithValue("@Amount", invoicePayment.Amount);
                cmd.Parameters.AddWithValue("@PaymentDate", invoicePayment.PaymentDate);
                cmd.Parameters.AddWithValue("@TransactionStatus", invoicePayment.TransactionStatus);
                cmd.Parameters.AddWithValue("@TransactionID", invoicePayment.TransactionID);
                cmd.Parameters.AddWithValue("@TransactionDetails", invoicePayment.TransactionDetails);
                cmd.Parameters.AddWithValue("@MedicalNumberOrNationalId", invoicePayment.MedicalNumberOrNationalId);
                cmd.Parameters.AddWithValue("@PatientReceiptNumber", invoicePayment.PatientReceiptNumber);
                cmd.Parameters.AddWithValue("@PaymentStatus", invoicePayment.PaymentStatus);
                cmd.Parameters.AddWithValue("@CheckNumber", invoicePayment.CheckNumber);
                cmd.Parameters.AddWithValue("@Id", invoicePayment.Id);

                //Execute the OdbcCommand
                ExecuteNonQuery(cmd);
            }
            catch (Exception ex)
            {
                //Attempt to throw the exception for error handling
                throw ex;
            }
        }
        internal void updateTotalAmount(decimal totalPriceAmount, long invoiceId)
        {
            try
            {
                OdbcCommand cmd = new OdbcCommand(_cmdUpdateTotalAmountByInvoiceID);

                cmd.Parameters.AddWithValue("@Amount", totalPriceAmount);
                cmd.Parameters.AddWithValue("@InvoiceId", invoiceId);
               
                //Execute the OdbcCommand
                ExecuteNonQuery(cmd);
            }
            catch (Exception ex)
            {
                //Attempt to throw the exception for error handling
                throw ex;
            }
        }
        internal void UpdateAmount(InvoicePaymentDetail invoicePaymentDetail)
        {
            try
            {
                OdbcCommand cmd = new OdbcCommand(_cmdUpdateUpdateAmount);

                cmd.Parameters.AddWithValue("@Amount", invoicePaymentDetail.Amount);
                cmd.Parameters.AddWithValue("@Id", invoicePaymentDetail.Id);

                //Execute the OdbcCommand
                ExecuteNonQuery(cmd);
            }
            catch (Exception ex)
            {
                //Attempt to throw the exception for error handling
                throw ex;
            }
        }
        internal void UpdatePaymentStatus(int statusNumber, long invoiceId)
        {
            try
            {
                OdbcCommand cmd = new OdbcCommand(_cmdUpdatePaymentStatus);

                cmd.Parameters.AddWithValue("@PaymentStatus", statusNumber);
                cmd.Parameters.AddWithValue("@InvoiceId", invoiceId);

                //Execute the OdbcCommand
                ExecuteNonQuery(cmd);
            }
            catch (Exception ex)
            {
                //Attempt to throw the exception for error handling
                throw ex;
            }
        }
        internal void UpdateTotalAmountForMediCalMethod(decimal updateAmountInvoicePayment,long invoicePaymentId)
        {
            try
            {
                OdbcCommand cmd = new OdbcCommand(_cmdUpdateTotalAmountForMediCalMethod);

                cmd.Parameters.AddWithValue("@Amount", updateAmountInvoicePayment);
                cmd.Parameters.AddWithValue("@Id", invoicePaymentId);
               
                //Execute the OdbcCommand
                ExecuteNonQuery(cmd);
            }
            catch (Exception ex)
            {
                //Attempt to throw the exception for error handling
                throw ex;
            }
        }
        internal void UpdateCheckNumber(long invoiceID, string checkNumber)
        {
            try
            {
                OdbcCommand cmd = new OdbcCommand(_cmdUpdateCheckNumber);


                cmd.Parameters.AddWithValue("@CheckNumber", checkNumber);
                cmd.Parameters.AddWithValue("@InvoiceId", invoiceID);

                //Execute the OdbcCommand
                ExecuteNonQuery(cmd);
            }
            catch (Exception ex)
            {
                //Attempt to throw the exception for error handling
                throw ex;
            }
        }
        internal bool Delete(long id)
        {
            bool success = false;

            try
            {
                OdbcCommand cmd = new OdbcCommand(_cmdDelete);

                cmd.Parameters.AddWithValue("@Id", id);

                ExecuteNonQuery(cmd);

                success = true;
            }
            catch (Exception ex)
            {
                //Attempt to throw the exception for error handling
                throw ex;
            }

            return success;
        }

        internal List<InvoicePaymentDetail> GetAll()
        {
            try
            {
                //Create instance of sqlcommand
                OdbcCommand cmd = new OdbcCommand(_cmdSelectAll);

                DataSet ds = ExecuteQuery(cmd);

                return GetListFromDataSet(ds);
            }
            catch (Exception ex)
            {
                //Attempt to throw the exception for error handling
                throw ex;
            }
        }

        internal InvoicePaymentDetail GetById(long id)
        {
            try
            {
                OdbcCommand cmd = new OdbcCommand(_cmdSelectById);

                cmd.Parameters.AddWithValue("@Id", id);

                DataSet ds = ExecuteQuery(cmd);

                return GetListFromDataSet(ds)[0];
            }
            catch (Exception ex)
            {
                //Attempt to throw the exception for error handling
                throw ex;
            }
        }

        internal List<InvoicePaymentDetail> GetListByInvoiceId(long invoiceId)
        {
            try
            {
                //Create instance of sqlcommand
                OdbcCommand cmd = new OdbcCommand(_cmdSelectByInvoiceId);

                cmd.Parameters.AddWithValue("@InvoiceId", invoiceId);

                DataSet ds = ExecuteQuery(cmd);

                return GetListFromDataSet(ds);
            }
            catch (Exception ex)
            {
                //Attempt to throw the exception for error handling
                throw ex;
            }
        }

        private List<InvoicePaymentDetail> GetListFromDataSet(DataSet ds)
        {
            List<InvoicePaymentDetail> invoicePaymentList = new List<InvoicePaymentDetail>();

            // This Loop reads all Data from DataSet                    
            for (int i = 0; i < ds.Tables["Data"].Rows.Count; i++)
            {
                InvoicePaymentDetail invoicePayment = new InvoicePaymentDetail();

                invoicePayment.Id = Convert.ToInt64(ds.Tables["Data"].Rows[i].ItemArray[0]);
                invoicePayment.InvoiceID = Convert.ToInt64(ds.Tables["Data"].Rows[i].ItemArray[1]);
                invoicePayment.UserId = Convert.ToInt64(ds.Tables["Data"].Rows[i].ItemArray[2]);
                invoicePayment.PaymentMode = (PaymentType) Convert.ToInt32(ds.Tables["Data"].Rows[i].ItemArray[3]);
                invoicePayment.Amount = Convert.ToDecimal(ds.Tables["Data"].Rows[i].ItemArray[4]);
                invoicePayment.PaymentDate = Convert.ToDateTime(ds.Tables["Data"].Rows[i].ItemArray[5]);
                invoicePayment.TransactionStatus = (CreditCardTransactionStatus) Convert.ToInt32(ds.Tables["Data"].Rows[i].ItemArray[6]);
                invoicePayment.TransactionID = Convert.ToString(ds.Tables["Data"].Rows[i].ItemArray[7]);
                invoicePayment.TransactionDetails = Convert.ToString(ds.Tables["Data"].Rows[i].ItemArray[8]);
                if (ds.Tables["Data"].Rows[i].ItemArray[9] != DBNull.Value)
                {
                    invoicePayment.MedicalNumberOrNationalId = Convert.ToString(ds.Tables["Data"].Rows[i].ItemArray[9]);
                }
                else
                {
                    invoicePayment.MedicalNumberOrNationalId = "";
                }
                if (ds.Tables["Data"].Rows[i].ItemArray[10] != DBNull.Value)
                {
                    invoicePayment.PatientReceiptNumber = Convert.ToString(ds.Tables["Data"].Rows[i].ItemArray[10]);
                }
                else
                {
                    invoicePayment.PatientReceiptNumber = "";
                }
                string paymentStatus = Convert.ToString(ds.Tables["Data"].Rows[i].ItemArray[11]);
                if (!string.IsNullOrEmpty(paymentStatus))
                {
                    invoicePayment.PaymentStatus = (PaymentStatus)Convert.ToInt32(ds.Tables["Data"].Rows[i].ItemArray[11]);
                }
                else
                {
                    invoicePayment.PaymentStatus = PaymentStatus.Unpaid;
                }
                if (ds.Tables["Data"].Rows[i].ItemArray[12] != DBNull.Value)
                {
                    invoicePayment.CheckNumber = Convert.ToString(ds.Tables["Data"].Rows[i].ItemArray[12]);
                }
                else
                {
                    invoicePayment.CheckNumber = "";
                }
                invoicePaymentList.Add(invoicePayment);
            }

            return invoicePaymentList;
        }

        internal bool DeleteByInvoiceId(long invoiceId)
        {
            bool success = false;

            try
            {
                OdbcCommand cmd = new OdbcCommand(_cmdDeleteByInvoiceId);

                cmd.Parameters.AddWithValue("@InvoiceId", invoiceId);

                ExecuteNonQuery(cmd);

                success = true;
            }
            catch (Exception ex)
            {
                //Attempt to throw the exception for error handling
                throw ex;
            }

            return success;
        }

        internal decimal GetTotalPaymentAmountByInvoiceID(long invoiceId)
        {
            decimal totalAmountPaid = 0;

            try
            {
                OdbcCommand cmd = new OdbcCommand(_cmdGetTotalPaymentAmountByInvoiceID);
                cmd.Parameters.AddWithValue("@InvoiceId", invoiceId);
                DataSet ds = ExecuteQuery(cmd);

                if(ds !=null)
                {
                    if(ds.Tables[0].Rows.Count>0)
                    {
                        if (ds.Tables[0].Rows[0].ItemArray[0] != DBNull.Value)
                        {
                            totalAmountPaid = Convert.ToDecimal(ds.Tables[0].Rows[0].ItemArray[0]);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return totalAmountPaid;
        }
        internal decimal GetTotalPaymentAmountOnlyByInvoiceID(long invoiceId)
        {
            decimal totalAmountPaid = 0;

            try
            {
                OdbcCommand cmd = new OdbcCommand(_cmdGetTotalPaymentAmountOnlyByInvoiceID);
                cmd.Parameters.AddWithValue("@InvoiceId", invoiceId);
                DataSet ds = ExecuteQuery(cmd);

                if (ds != null)
                {
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        if (ds.Tables[0].Rows[0].ItemArray[0] != DBNull.Value)
                        {
                            totalAmountPaid = Convert.ToDecimal(ds.Tables[0].Rows[0].ItemArray[0]);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return totalAmountPaid;
        }

        internal bool IsPatientReceiptExist(string patientReceiptNumber)
        {
            bool isExist = false;

            try
            {
                OdbcCommand cmd = new OdbcCommand(_cmdIsPatientReceiptExist);
                cmd.Parameters.AddWithValue("@PatientReceiptNumber", patientReceiptNumber);
                DataSet ds = ExecuteQuery(cmd);

                if (ds != null)
                {
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        if (ds.Tables[0].Rows[0].ItemArray[0] != DBNull.Value)
                        {
                            int count = Convert.ToInt32(ds.Tables[0].Rows[0].ItemArray[0]);
                            if (count > 0)
                            {
                                isExist = true;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return isExist;
        }
    }
}
