﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.Odbc;
using System.Collections;
using daoProject.Infrastructure;
using daoProject.Model;
using daoProject.Mapper;
using daoProject.Service;

namespace daoProject.Mapper
{
    internal class InvoiceDataMapper : SqlHelper
    {
        private static string _cmdInsert = @"INSERT INTO Whrc_Invoice (ClientId,InvoiceType,ShippingAddressId,PurchaseDate,TotalAmount,Discount,NetPayable,PaymentMode,PaidAmount, PaymentDate,AddedBy,AddedDate,ModifiedBy,ModifiedDate,OrderStatus,ClientRequestedDiscountMsg,DiscountTypeId, DiscountSubTypeId, TotalRefundAmount,StaffInitial,PaymentNotes,PackageDiscount, CreditCardTransactionStatus, CreditCardTransactionID, CreditCardTransactionDetails, MedicalNumberOrNationalId,CheckNumber) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?); ";
        private static string _cmdUpdate = @"UPDATE Whrc_Invoice SET ClientId=?,InvoiceType=?,ShippingAddressId=?,PurchaseDate=?,TotalAmount=?,Discount=?,NetPayable=?,PaymentMode=?,PaidAmount=?, PaymentDate=?,AddedBy=?,AddedDate=?,ModifiedBy=?,ModifiedDate=?,OrderStatus=?,ClientRequestedDiscountMsg=?,DiscountTypeId=?,DiscountSubTypeId=?, TotalRefundAmount=?,StaffInitial=?,PaymentNotes=?, PackageDiscount=?,CreditCardTransactionStatus=?, CreditCardTransactionID=?, CreditCardTransactionDetails=?,MedicalNumberOrNationalId=?,CheckNumber=? WHERE Id=? ";
        private static string _cmdUpdatePaidAmount = @"UPDATE Whrc_Invoice SET PaidAmount=? WHERE Id=? ";
        private static string _cmdUpdateCheckNumber = @"UPDATE Whrc_Invoice SET CheckNumber=? WHERE Id=? ";
        private static string _cmdUpdateMedicalNumber = @"UPDATE Whrc_Invoice SET MedicalNumberOrNationalId=? WHERE Id=? ";
        private static string _cmdUpdateStaffInitialOrderProcessByAdmin = @"UPDATE Whrc_Invoice SET StaffInitial=? WHERE Id=? ";
        private static string _cmdUpdateTotalAmount = @"UPDATE Whrc_Invoice SET TotalAmount=? WHERE Id=? ";
        private static string _cmdDelete = @"DELETE FROM Whrc_Invoice WHERE Id=? ";
        private static string _cmdSelectAll = @"SELECT * FROM Whrc_Invoice ";
        private static string _cmdSelectById = @"SELECT * FROM Whrc_Invoice WHERE Id=? ";
        private static string _cmdSelectByClientId = @"SELECT * FROM Whrc_Invoice WHERE ClientId=? ";

        private static string _cmdGetInvoiceListByOrderStatus = @"SELECT * FROM Whrc_Invoice WHERE OrderStatus=? ";
        private static string _cmdGetInvoiceListForClientByOrderStatus = @"SELECT * FROM Whrc_Invoice WHERE ClientId=? And OrderStatus=? ";
        private static string _cmdGetInvoiceListByClientName = @"Select Whrc_Invoice.* From Whrc_Invoice,Whrc_Users Where Whrc_Invoice.ClientID=Whrc_Users.ID And (Whrc_Users.FirstName like ? Or Whrc_Users.LastName like ?)";

        private static string _cmdGetInvoiceListNeedReprocessingByClientName = @"(Select * From WHRC_Invoice Where ClientID In(Select ID From Whrc_Users Where Whrc_Users.FirstName like ? Or Whrc_Users.LastName like ?) And ID in(Select Distinct(InvoiceID) From WHRC_ClassInvoiceItem Where (IsClassCanceled=1 OR IsClassCanceledByClient=1 Or ClassInvoiceItemState=2) And (IsRefundProcessed=0)) Or (Orderstatus=1 Or Orderstatus=4)) Union (Select * From WHRC_Invoice Where ClientID In(Select ID From Whrc_Users Where Whrc_Users.FirstName like ? Or Whrc_Users.LastName like ?) And Id in (Select Distinct(InvoiceID) From WHRC_RentalInvoiceItem where  IsExtensionRequestbyClient=1 And IsExtensionApprovedByAdmin=0))";

        private static string _cmdApplyDiscount = @"UPDATE Whrc_Invoice SET Discount=?,DiscountTypeId=?,DiscountSubTypeId=?,PackageDiscount=? WHERE Id=? ";
        private static string _cmdMakePayment = @"UPDATE Whrc_Invoice SET PaidAmount=?,PaymentMode=?,PaymentDate=?,PaymentNotes=?,Orderstatus=?, PackageDiscount=? WHERE Id=? ";

        private static string _cmdUpdateTotalRefundAmount = @"UPDATE Whrc_Invoice SET TotalRefundAmount=? WHERE Id=? ";
        private static string _cmdGetInvoiceByStaffIdForCart = @"SELECT * FROM Whrc_Invoice WHERE InvoiceType=? And  OrderStatus=? And StaffInitial=?";
        private static string _cmdUpdateInvoiceClientID = @"UPDATE Whrc_Invoice SET ClientId=? WHERE Id=? ";
       private static string _cmdGetInvoiceByClientIDAndOrderType = @"SELECT * FROM Whrc_Invoice WHERE InvoiceType=? And  OrderStatus=? And ClientID=?";
       //private static string _cmdGetInvoiceByClientIDAndOrderType = @"SELECT * FROM Whrc_Invoice WHERE OrderStatus=? And ClientID=?";
        
        //private static string _cmdGetInvoiceListForProcess = @"Select * From WHRC_Invoice Where ID in(Select Distinct(InvoiceID) From WHRC_ClassInvoiceItem Where (IsClassCanceled=1 OR IsClassCanceledByClient=1 Or ClassInvoiceItemState=2) And (IsRefundProcessed=0)) Or (Orderstatus=1 Or Orderstatus=4)";
       private static string _cmdGetInvoiceListForProcess = @"(Select * From WHRC_Invoice Where ID in(Select Distinct(InvoiceID) From WHRC_ClassInvoiceItem Where (IsClassCanceled=1 OR IsClassCanceledByClient=1 Or ClassInvoiceItemState=2) And (IsRefundProcessed=0)) Or (Orderstatus=1 Or Orderstatus=4)) Union (Select * From WHRC_Invoice Where Id in (Select Distinct(InvoiceID) From WHRC_RentalInvoiceItem where  IsExtensionRequestbyClient=1 And IsExtensionApprovedByAdmin=0 And IsItemReturned = 0))";
     //  private static string _cmdGetInvoiceListForProcess = @"(Select * From WHRC_Invoice Where ID in(Select Distinct(InvoiceID) From WHRC_ClassInvoiceItem Where (IsClassCanceled=0 And (IsClassCanceledByClient=0 Or ClassInvoiceItemState=3)) And (IsRefundProcessed=0)) And (Orderstatus=1)) Union (Select * From WHRC_Invoice Where Id in (Select Distinct(InvoiceID) From WHRC_RentalInvoiceItem where  IsExtensionRequestbyClient=1 And IsExtensionApprovedByAdmin=0))";
       private static string _cmdGetInvoiceListForReProcess = @"(Select * From WHRC_Invoice Where ID in(Select Distinct(InvoiceID) From WHRC_ClassInvoiceItem Where (IsClassCanceled=1 OR (IsClassCanceledByClient=1 Or ClassInvoiceItemState=2)) And (IsRefundProcessed=0)) And (Orderstatus=1)) Union (Select * From WHRC_Invoice Where Id in (Select Distinct(InvoiceID) From WHRC_RentalInvoiceItem where  IsExtensionRequestbyClient=1 And IsExtensionApprovedByAdmin=0))";
        private static string _cmdUpdateCreditCardPayment = @"UPDATE Whrc_Invoice SET CreditCardTransactionStatus=?, CreditCardTransactionID=?, CreditCardTransactionDetails=?,PaymentDate=? WHERE Id=? ";
        private static string _cmdGetClassMedicalPaymentCount = @"Select Count(*) From WHRC_ClassInvoiceItem Where IsClassCanceled=0 And InvoiceID In (Select Distinct(WHRC_Invoice.Id) From WHRC_Invoice,WHRC_InvoicePayment Where WHRC_Invoice.Id=WHRC_InvoicePayment.InvoiceID And WHRC_Invoice.ClientID=? And WHRC_Invoice.OrderStatus!=? And WHRC_InvoicePayment.PaymentMode=?)";
        

        internal void Create(Invoice invoice)
        {
            long id = -1;
            try
            {
                OdbcCommand cmd = new OdbcCommand(_cmdInsert);

                cmd.Parameters.AddWithValue("@ClientId", invoice.ClientId);
                cmd.Parameters.AddWithValue("@InvoiceType", invoice.InvoiceType);
                cmd.Parameters.AddWithValue("@ShippingAddressId", invoice.ShippingAddressId);
                cmd.Parameters.AddWithValue("@PurchaseDate", invoice.PurchaseDate);
                cmd.Parameters.AddWithValue("@TotalAmount", invoice.TotalAmount);
                cmd.Parameters.AddWithValue("@Discount", invoice.Discount);
                cmd.Parameters.AddWithValue("@NetPayable", invoice.NetPayable);
                cmd.Parameters.AddWithValue("@PaymentMode", invoice.PaymentMode);
                cmd.Parameters.AddWithValue("@PaidAmount", invoice.PaidAmount);
                cmd.Parameters.AddWithValue("@PaymentDate", invoice.PaymentDate);
                cmd.Parameters.AddWithValue("@AddedBy", invoice.AddedBy);
                cmd.Parameters.AddWithValue("@AddedDate", invoice.AddedDate);
                cmd.Parameters.AddWithValue("@ModifiedBy", invoice.ModifiedBy);
                cmd.Parameters.AddWithValue("@ModifiedDate", invoice.ModifiedDate);
                cmd.Parameters.AddWithValue("@OrderStatus", invoice.OrderStatus);
                cmd.Parameters.AddWithValue("@ClientRequestedDiscountMsg", invoice.ClientRequestedDiscountMsg);
                cmd.Parameters.AddWithValue("@DiscountTypeId", invoice.DiscountTypeId);
                cmd.Parameters.AddWithValue("@DiscountSubTypeId", invoice.DiscountSubTypeId);
                cmd.Parameters.AddWithValue("@PaymentNotes", invoice.PaymentNotes);
                cmd.Parameters.AddWithValue("@PackageDiscount", invoice.PackageDiscount);

                cmd.Parameters.AddWithValue("@CreditCardTransactionStatus", invoice.CreditCardTransactionStatus);
                cmd.Parameters.AddWithValue("@CreditCardTransactionID", invoice.CreditCardTransactionID);
                cmd.Parameters.AddWithValue("@CreditCardTransactionDetails", invoice.CreditCardTransactionDetails);
                cmd.Parameters.AddWithValue("@MedicalNumberOrNationalId", invoice.MedicalNumberOrNationalId);
                cmd.Parameters.AddWithValue("@CheckNumber", invoice.CheckNumber);

                //Execute the OdbcCommand
                id = Convert.ToInt64(ExecuteNonQueryAndScalar(cmd));

                invoice.Id = id;
            }
            catch (Exception ex)
            {
                //Attempt to throw the exception for error handling
                throw ex;
            }
        }

        internal void Create(Invoice invoice, System.Data.Odbc.OdbcConnection conn, System.Data.Odbc.OdbcTransaction tx)
        {
            long id = -1;
            try
            {
                OdbcCommand cmd = new OdbcCommand(_cmdInsert);

                cmd.Parameters.AddWithValue("@ClientId", invoice.ClientId);
                cmd.Parameters.AddWithValue("@InvoiceType", invoice.InvoiceType);
                cmd.Parameters.AddWithValue("@ShippingAddressId", invoice.ShippingAddressId);
                cmd.Parameters.AddWithValue("@PurchaseDate", invoice.PurchaseDate);
                cmd.Parameters.AddWithValue("@TotalAmount", invoice.TotalAmount);
                cmd.Parameters.AddWithValue("@Discount", invoice.Discount);
                cmd.Parameters.AddWithValue("@NetPayable", invoice.NetPayable);
                cmd.Parameters.AddWithValue("@PaymentMode", invoice.PaymentMode);
                cmd.Parameters.AddWithValue("@PaidAmount", invoice.PaidAmount);
                cmd.Parameters.AddWithValue("@PaymentDate", invoice.PaymentDate);
                cmd.Parameters.AddWithValue("@AddedBy", invoice.AddedBy);
                cmd.Parameters.AddWithValue("@AddedDate", invoice.AddedDate);
                cmd.Parameters.AddWithValue("@ModifiedBy", invoice.ModifiedBy);
                cmd.Parameters.AddWithValue("@ModifiedDate", invoice.ModifiedDate);
                cmd.Parameters.AddWithValue("@OrderStatus", invoice.OrderStatus);
                cmd.Parameters.AddWithValue("@ClientRequestedDiscountMsg", invoice.ClientRequestedDiscountMsg);
                cmd.Parameters.AddWithValue("@DiscountTypeId", invoice.DiscountTypeId);
                cmd.Parameters.AddWithValue("@DiscountSubTypeId", invoice.DiscountSubTypeId);
                cmd.Parameters.AddWithValue("@TotalRefundAmount", invoice.TotalRefundAmount);
                cmd.Parameters.AddWithValue("@StaffInitial", invoice.StaffInitial);
                cmd.Parameters.AddWithValue("@PaymentNotes", invoice.PaymentNotes);
                cmd.Parameters.AddWithValue("@PackageDiscount", invoice.PackageDiscount);
                cmd.Parameters.AddWithValue("@CreditCardTransactionStatus", invoice.CreditCardTransactionStatus);
                cmd.Parameters.AddWithValue("@CreditCardTransactionID", invoice.CreditCardTransactionID);
                cmd.Parameters.AddWithValue("@CreditCardTransactionDetails", invoice.CreditCardTransactionDetails);
                cmd.Parameters.AddWithValue("@MedicalNumberOrNationalId", (invoice.MedicalNumberOrNationalId == null ? "": invoice.MedicalNumberOrNationalId));
                cmd.Parameters.AddWithValue("@CheckNumber", (invoice.CheckNumber == null ? "" : invoice.CheckNumber));
                //Execute the OdbcCommand
                id = Convert.ToInt64(ExecuteNonQueryAndScalar(cmd,conn,tx));

                invoice.Id = id;
            }
            catch (Exception ex)
            {
                //Attempt to throw the exception for error handling
                throw ex;
            }
        }

        internal void Update(Invoice invoice)
        {
            try
            {
                OdbcCommand cmd = new OdbcCommand(_cmdUpdate);

                cmd.Parameters.AddWithValue("@ClientId", invoice.ClientId);
                cmd.Parameters.AddWithValue("@InvoiceType", invoice.InvoiceType);
                cmd.Parameters.AddWithValue("@ShippingAddressId", invoice.ShippingAddressId);
                cmd.Parameters.AddWithValue("@PurchaseDate", invoice.PurchaseDate);
                cmd.Parameters.AddWithValue("@TotalAmount", invoice.TotalAmount);
                cmd.Parameters.AddWithValue("@Discount", invoice.Discount);
                cmd.Parameters.AddWithValue("@NetPayable", invoice.NetPayable);
                cmd.Parameters.AddWithValue("@PaymentMode", invoice.PaymentMode);
                cmd.Parameters.AddWithValue("@PaidAmount", invoice.PaidAmount);
                cmd.Parameters.AddWithValue("@PaymentDate", invoice.PaymentDate);
                cmd.Parameters.AddWithValue("@AddedBy", invoice.AddedBy);
                cmd.Parameters.AddWithValue("@AddedDate", invoice.AddedDate);
                cmd.Parameters.AddWithValue("@ModifiedBy", invoice.ModifiedBy);
                cmd.Parameters.AddWithValue("@ModifiedDate", invoice.ModifiedDate);
                cmd.Parameters.AddWithValue("@OrderStatus", invoice.OrderStatus);
                cmd.Parameters.AddWithValue("@ClientRequestedDiscountMsg", invoice.ClientRequestedDiscountMsg);
                cmd.Parameters.AddWithValue("@DiscountTypeId", invoice.DiscountTypeId);
                cmd.Parameters.AddWithValue("@DiscountSubTypeId", invoice.DiscountSubTypeId);
                cmd.Parameters.AddWithValue("@TotalRefundAmount", invoice.TotalRefundAmount);
                cmd.Parameters.AddWithValue("@StaffInitial", invoice.StaffInitial);
                cmd.Parameters.AddWithValue("@PaymentNotes", invoice.PaymentNotes);
                cmd.Parameters.AddWithValue("@PackageDiscount", invoice.PackageDiscount);
                cmd.Parameters.AddWithValue("@CreditCardTransactionStatus", invoice.CreditCardTransactionStatus);
                cmd.Parameters.AddWithValue("@CreditCardTransactionID", invoice.CreditCardTransactionID);
                cmd.Parameters.AddWithValue("@CreditCardTransactionDetails", invoice.CreditCardTransactionDetails);
                cmd.Parameters.AddWithValue("@MedicalNumberOrNationalId", invoice.MedicalNumberOrNationalId);
                cmd.Parameters.AddWithValue("@CheckNumber", invoice.CheckNumber);
                cmd.Parameters.AddWithValue("@Id", invoice.Id);

                //Execute the OdbcCommand
                ExecuteNonQuery(cmd);
            }
            catch (Exception ex)
            {
                //Attempt to throw the exception for error handling
                throw ex;
            }
        }
        internal void UpdatePaidAmount(Invoice invoice)
        {
            try
            {
                OdbcCommand cmd = new OdbcCommand(_cmdUpdatePaidAmount);

               
                cmd.Parameters.AddWithValue("@PaidAmount", invoice.PaidAmount);
                cmd.Parameters.AddWithValue("@Id", invoice.Id);

                //Execute the OdbcCommand
                ExecuteNonQuery(cmd);
            }
            catch (Exception ex)
            {
                //Attempt to throw the exception for error handling
                throw ex;
            }
        }
        internal void UpdateCheckNumber(long invoiceID, string checkNumber)
        {
            try
            {
                OdbcCommand cmd = new OdbcCommand(_cmdUpdateCheckNumber);


                cmd.Parameters.AddWithValue("@CheckNumber", checkNumber);
                cmd.Parameters.AddWithValue("@Id", invoiceID);

                //Execute the OdbcCommand
                ExecuteNonQuery(cmd);
            }
            catch (Exception ex)
            {
                //Attempt to throw the exception for error handling
                throw ex;
            }
        }
        internal void UpdateMedicalNumber(long invoiceID, string medialNumber)
        {
            try
            {
                OdbcCommand cmd = new OdbcCommand(_cmdUpdateMedicalNumber);
                cmd.Parameters.AddWithValue("@MedicalNumberOrNationalId", medialNumber);
                cmd.Parameters.AddWithValue("@Id", invoiceID);
                //Execute the OdbcCommand
                ExecuteNonQuery(cmd);
            }
            catch (Exception ex)
            {
                //Attempt to throw the exception for error handling
                throw ex;
            }
        }
        internal void Update(Invoice invoice, System.Data.Odbc.OdbcConnection conn, System.Data.Odbc.OdbcTransaction tx)
        {
            try
            {
                OdbcCommand cmd = new OdbcCommand(_cmdUpdate);

                cmd.Parameters.AddWithValue("@ClientId", invoice.ClientId);
                cmd.Parameters.AddWithValue("@InvoiceType", invoice.InvoiceType);
                cmd.Parameters.AddWithValue("@ShippingAddressId", invoice.ShippingAddressId);
                cmd.Parameters.AddWithValue("@PurchaseDate", invoice.PurchaseDate);
                cmd.Parameters.AddWithValue("@TotalAmount", invoice.TotalAmount);
                cmd.Parameters.AddWithValue("@Discount", invoice.Discount);
                cmd.Parameters.AddWithValue("@NetPayable", invoice.NetPayable);
                cmd.Parameters.AddWithValue("@PaymentMode", invoice.PaymentMode);
                cmd.Parameters.AddWithValue("@PaidAmount", invoice.PaidAmount);
                cmd.Parameters.AddWithValue("@PaymentDate", invoice.PaymentDate);
                cmd.Parameters.AddWithValue("@AddedBy", invoice.AddedBy);
                cmd.Parameters.AddWithValue("@AddedDate", invoice.AddedDate);
                cmd.Parameters.AddWithValue("@ModifiedBy", invoice.ModifiedBy);
                cmd.Parameters.AddWithValue("@ModifiedDate", invoice.ModifiedDate);
                cmd.Parameters.AddWithValue("@OrderStatus", invoice.OrderStatus);
                cmd.Parameters.AddWithValue("@ClientRequestedDiscountMsg", invoice.ClientRequestedDiscountMsg);
                cmd.Parameters.AddWithValue("@DiscountTypeId", invoice.DiscountTypeId);
                cmd.Parameters.AddWithValue("@DiscountSubTypeId", invoice.DiscountSubTypeId);
                cmd.Parameters.AddWithValue("@TotalRefundAmount", invoice.TotalRefundAmount);
                cmd.Parameters.AddWithValue("@StaffInitial", invoice.StaffInitial);
                cmd.Parameters.AddWithValue("@PaymentNotes", invoice.PaymentNotes);
                cmd.Parameters.AddWithValue("@PackageDiscount", invoice.PackageDiscount);
                cmd.Parameters.AddWithValue("@CreditCardTransactionStatus", invoice.CreditCardTransactionStatus);
                cmd.Parameters.AddWithValue("@CreditCardTransactionID", invoice.CreditCardTransactionID);
                cmd.Parameters.AddWithValue("@CreditCardTransactionDetails", invoice.CreditCardTransactionDetails);
                cmd.Parameters.AddWithValue("@MedicalNumberOrNationalId", invoice.MedicalNumberOrNationalId);
                cmd.Parameters.AddWithValue("@CheckNumber", invoice.CheckNumber);
                cmd.Parameters.AddWithValue("@Id", invoice.Id);

                //Execute the OdbcCommand
                ExecuteNonQuery(cmd, conn, tx);
            }
            catch (Exception ex)
            {
                //Attempt to throw the exception for error handling
                throw ex;
            }
        }
        internal void UpdateStaffInitialOrderProcessByAdmin(User userObj, Invoice invoiceObj)
        {
            try
            {
                OdbcCommand cmd = new OdbcCommand(_cmdUpdateStaffInitialOrderProcessByAdmin);
                cmd.Parameters.AddWithValue("@StaffInitial", userObj.Id);
                cmd.Parameters.AddWithValue("@Id", invoiceObj.Id);
                ExecuteNonQuery(cmd);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        internal void UpdateTotalAmount(decimal updateAmountInvoice, long invoiceId)
        {
            try
            {
                OdbcCommand cmd = new OdbcCommand(_cmdUpdateTotalAmount);
                cmd.Parameters.AddWithValue("@TotalAmount", updateAmountInvoice);
                cmd.Parameters.AddWithValue("@Id", invoiceId);
                ExecuteNonQuery(cmd);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public void UpdateAmount(Invoice invoice)
        {
            try
            {
                OdbcCommand cmd = new OdbcCommand(_cmdUpdateTotalAmount);
                cmd.Parameters.AddWithValue("@TotalAmount", invoice.TotalAmount);
                cmd.Parameters.AddWithValue("@Id", invoice.Id);
                ExecuteNonQuery(cmd);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        internal bool Delete(long id)
        {
            bool success = false;

            try
            {
                OdbcCommand cmd = new OdbcCommand(_cmdDelete);

                cmd.Parameters.AddWithValue("@Id", id);

                ExecuteNonQuery(cmd);

                success = true;
            }
            catch (Exception ex)
            {
                //Attempt to throw the exception for error handling
                throw ex;
            }

            return success;
        }

        public List<Invoice> GetListFromDataSet(DataSet ds)
        {
            List<Invoice> invoiceList = new List<Invoice>();

            // This Loop reads all Data from DataSet                    
            for (int i = 0; i < ds.Tables["Data"].Rows.Count; i++)
            {
                Invoice invoice = new Invoice();

                invoice.Id = Convert.ToInt64(ds.Tables["Data"].Rows[i].ItemArray[0]);
                invoice.ClientId = Convert.ToInt64(ds.Tables["Data"].Rows[i].ItemArray[1]);
                invoice.InvoiceType = (InvoiceType)Convert.ToInt32(ds.Tables["Data"].Rows[i].ItemArray[2]);
                invoice.ShippingAddressId = Convert.ToInt64(ds.Tables["Data"].Rows[i].ItemArray[3]);
                invoice.PurchaseDate = Convert.ToDateTime(ds.Tables["Data"].Rows[i].ItemArray[4]);
                invoice.TotalAmount = Convert.ToDecimal(ds.Tables["Data"].Rows[i].ItemArray[5]);
                invoice.Discount = Convert.ToDecimal(ds.Tables["Data"].Rows[i].ItemArray[6]);
                invoice.NetPayable = Convert.ToDecimal(ds.Tables["Data"].Rows[i].ItemArray[7]);
                invoice.PaymentMode = (PaymentType)Convert.ToInt32(ds.Tables["Data"].Rows[i].ItemArray[8]);
                invoice.PaidAmount = Convert.ToDecimal(ds.Tables["Data"].Rows[i].ItemArray[9]);

                if (ds.Tables["Data"].Rows[i].ItemArray[10] != DBNull.Value)
                {
                    invoice.PaymentDate = Convert.ToDateTime(ds.Tables["Data"].Rows[i].ItemArray[10]);
                }
                else
                {
                    invoice.PaymentDate = Convert.ToDateTime("1/1/1900");
                }

                invoice.AddedBy = Convert.ToInt64(ds.Tables["Data"].Rows[i].ItemArray[11]);
                invoice.AddedDate = Convert.ToDateTime(ds.Tables["Data"].Rows[i].ItemArray[12]);
                invoice.ModifiedBy = Convert.ToInt64(ds.Tables["Data"].Rows[i].ItemArray[13]);
                invoice.ModifiedDate = Convert.ToDateTime(ds.Tables["Data"].Rows[i].ItemArray[14]);
                invoice.OrderStatus = (OrderState)Convert.ToInt32(ds.Tables["Data"].Rows[i].ItemArray[15]);
                invoice.ClientRequestedDiscountMsg = Convert.ToString(ds.Tables["Data"].Rows[i].ItemArray[16]);
                if (ds.Tables["Data"].Rows[i].ItemArray[17] != DBNull.Value)
                {
                    invoice.DiscountTypeId = Convert.ToInt64(ds.Tables["Data"].Rows[i].ItemArray[17]);
                }
                else
                {
                    invoice.DiscountTypeId = 0;
                }
                if (ds.Tables["Data"].Rows[i].ItemArray[18] != DBNull.Value)
                {
                    invoice.DiscountSubTypeId = Convert.ToInt64(ds.Tables["Data"].Rows[i].ItemArray[18]);
                }
                else
                {
                    invoice.DiscountSubTypeId = 0;
                }
                if (ds.Tables["Data"].Rows[i].ItemArray[19] != DBNull.Value)
                {
                    invoice.TotalRefundAmount = Convert.ToDecimal(ds.Tables["Data"].Rows[i].ItemArray[19]);
                }
                else
                {
                    invoice.TotalRefundAmount = 0;
                }
                if (ds.Tables["Data"].Rows[i].ItemArray[20] != DBNull.Value)
                {
                    invoice.StaffInitial = Convert.ToInt64(ds.Tables["Data"].Rows[i].ItemArray[20]);
                }
                else
                {
                    invoice.StaffInitial = 0;
                }
                if (ds.Tables["Data"].Rows[i].ItemArray[21] != DBNull.Value)
                {
                    invoice.PaymentNotes = Convert.ToString(ds.Tables["Data"].Rows[i].ItemArray[21]);
                }
                else
                {
                    invoice.PaymentNotes = "";
                }
                if (ds.Tables["Data"].Rows[i].ItemArray[22] != DBNull.Value)
                {
                    invoice.PackageDiscount = Convert.ToDecimal(ds.Tables["Data"].Rows[i].ItemArray[22]);
                }
                else
                {
                    invoice.PackageDiscount = 0;
                }

                if (ds.Tables["Data"].Rows[i].ItemArray[23] != DBNull.Value)
                {
                    invoice.CreditCardTransactionStatus =(CreditCardTransactionStatus )Convert.ToInt32(ds.Tables["Data"].Rows[i].ItemArray[23]);
                }
                else
                {
                    invoice.CreditCardTransactionStatus = CreditCardTransactionStatus.NA;
                }
                if (ds.Tables["Data"].Rows[i].ItemArray[24] != DBNull.Value)
                {
                    invoice.CreditCardTransactionID = Convert.ToString(ds.Tables["Data"].Rows[i].ItemArray[24]);
                }
                else
                {
                    invoice.CreditCardTransactionID ="";
                }
                if (ds.Tables["Data"].Rows[i].ItemArray[25] != DBNull.Value)
                {
                    invoice.CreditCardTransactionDetails = Convert.ToString(ds.Tables["Data"].Rows[i].ItemArray[25]);
                }
                else
                {
                    invoice.CreditCardTransactionDetails = "";
                }

                if (ds.Tables["Data"].Rows[i].ItemArray[26] != DBNull.Value)
                {
                    invoice.MedicalNumberOrNationalId = Convert.ToString(ds.Tables["Data"].Rows[i].ItemArray[26]);
                }
                else
                {
                    invoice.MedicalNumberOrNationalId = "";
                }
                if (ds.Tables["Data"].Rows[i].ItemArray[27] != DBNull.Value)
                {
                    invoice.CheckNumber = Convert.ToString(ds.Tables["Data"].Rows[i].ItemArray[27]);
                }
                else
                {
                    invoice.CheckNumber = "";
                }


                invoice.ClassInvoiceItems = new ClassInvoiceItemServiceImpl().GetListByInvoiceId(invoice.Id);
                invoice.ProductInvoiceItems = new ProductInvoiceItemServiceImpl().GetListByInvoiceId(invoice.Id);
                invoice.RentalInvoiceItems = new RentalInvoiceItemServiceImpl().GetListByInvoiceId(invoice.Id);
                invoice.MembershipInvoiceItems = new MembershipInvoiceItemServiceImpl().GetListByInvoiceId(invoice.Id);
                invoice.InvoicePackages = new InvoicePackageServiceImpl().GetListByInvoiceId(invoice.Id);

                decimal totalPrice = 0;
                foreach (ClassInvoiceItem item in invoice.ClassInvoiceItems)
                {
                    if (!item.IsInWaitingList)
                    {
                        totalPrice += item.Price;
                    }
                }
                foreach (ProductInvoiceItem item in invoice.ProductInvoiceItems)
                {
                    totalPrice += item.Price;
                }
                foreach (RentalInvoiceItem item in invoice.RentalInvoiceItems)
                {
                    totalPrice += item.RPTotal +item.DepositAmount;
                }
                foreach (MembershipInvoiceItem item in invoice.MembershipInvoiceItems)
                {
                    totalPrice += item.Price;
                }
                invoice.TotalAmount = totalPrice;
                invoice.NetPayable = invoice.TotalAmount - invoice.Discount-invoice.PackageDiscount;


                invoiceList.Add(invoice);
            }

            return invoiceList;
        }

        internal List<Invoice> GetAll()
        {
            try
            {
                //Create instance of sqlcommand
                OdbcCommand cmd = new OdbcCommand(_cmdSelectAll);

                DataSet ds = ExecuteQuery(cmd);

                return GetListFromDataSet(ds);
            }
            catch (Exception ex)
            {
                //Attempt to throw the exception for error handling
                throw ex;
            }
        }

        internal Invoice GetById(long id)
        {
            try
            {
                OdbcCommand cmd = new OdbcCommand(_cmdSelectById);

                cmd.Parameters.AddWithValue("@Id", id);

                DataSet ds = ExecuteQuery(cmd);

                if (ds.Tables[0].Rows.Count > 0)
                {
                    return GetListFromDataSet(ds)[0];
                }
                else
                {
                    return null;
                }
            }
            catch (Exception ex)
            {
                //Attempt to throw the exception for error handling
                throw ex;
            }
        }

        internal List<Invoice> GetListByClientId(long clientId)
        {
            try
            {
                //Create instance of sqlcommand
                OdbcCommand cmd = new OdbcCommand(_cmdSelectByClientId);

                cmd.Parameters.AddWithValue("@ClientId", clientId);

                DataSet ds = ExecuteQuery(cmd);

                return GetListFromDataSet(ds);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        internal List<Invoice> GetInvoiceByStaffIdForCart(long staffId, OrderState orderState)
        {
            try
            {
                OdbcCommand cmd = new OdbcCommand(_cmdGetInvoiceByStaffIdForCart);
                cmd.Parameters.AddWithValue("@InvoiceType", InvoiceType.WalkIn);
                cmd.Parameters.AddWithValue("@OrderStatus", orderState);
                cmd.Parameters.AddWithValue("@StaffInitial", staffId);

                DataSet ds = ExecuteQuery(cmd);

                return GetListFromDataSet(ds);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        internal List<Invoice> GetInvoiceByClientIDAndOrderType(long clientID, OrderState orderState, InvoiceType invoiceType)
        {
            try
            {
                OdbcCommand cmd = new OdbcCommand(_cmdGetInvoiceByClientIDAndOrderType);

                cmd.Parameters.AddWithValue("@InvoiceType", invoiceType);
                cmd.Parameters.AddWithValue("@OrderStatus", orderState);
                cmd.Parameters.AddWithValue("@ClientID", clientID);

                DataSet ds = ExecuteQuery(cmd);

                return GetListFromDataSet(ds);
            }
            catch (Exception ex)
            {
                //Attempt to throw the exception for error handling
                throw ex;
            }
        }
       

        internal List<Invoice> GetInvoiceListByOrderStatus(OrderState orderState)
        {
            try
            {
                //Create instance of sqlcommand
                OdbcCommand cmd = new OdbcCommand(_cmdGetInvoiceListByOrderStatus);
                cmd.Parameters.AddWithValue("@OrderStatus", Convert.ToInt32(orderState));

                DataSet ds = ExecuteQuery(cmd);

                return GetListFromDataSet(ds);
            }
            catch (Exception ex)
            {
                //Attempt to throw the exception for error handling
                throw ex;
            }
        }

        internal List<Invoice> GetInvoiceListForProcess()
        {
            try
            {
                OdbcCommand cmd = new OdbcCommand(_cmdGetInvoiceListForProcess);
                DataSet ds = ExecuteQuery(cmd);
                return GetListFromDataSet(ds);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        internal List<Invoice>  GetInvoiceListForReProcess()
        {
            try
            {
                OdbcCommand cmd = new OdbcCommand(_cmdGetInvoiceListForReProcess);
                DataSet ds = ExecuteQuery(cmd);
                return GetListFromDataSet(ds);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        internal List<Invoice> GetInvoiceListByClientName(string  clientName)
        {
            try
            {
                OdbcCommand cmd = new OdbcCommand(_cmdGetInvoiceListByClientName);
                clientName = "%" + clientName + "%";
                cmd.Parameters.AddWithValue("@FirstName", clientName);
                cmd.Parameters.AddWithValue("@LastName", clientName);

                DataSet ds = ExecuteQuery(cmd);
                return GetListFromDataSet(ds);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        internal List<Invoice> GetInvoiceListNeedReprocessingByClientName(string clientName)
        {
            try
            {
                OdbcCommand cmd = new OdbcCommand(_cmdGetInvoiceListNeedReprocessingByClientName);
                clientName = "%" + clientName + "%";
                cmd.Parameters.AddWithValue("@FirstName", clientName);
                cmd.Parameters.AddWithValue("@LastName", clientName);
                cmd.Parameters.AddWithValue("@FirstName", clientName);
                cmd.Parameters.AddWithValue("@LastName", clientName);

                DataSet ds = ExecuteQuery(cmd);
                return GetListFromDataSet(ds);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        internal List<Invoice> GetInvoiceListByOrderProcessStatus(bool isProcessed)
        {
            try
            {
                string cmdGetInvoiceListByOrderProcessStatus = "";
                if (isProcessed)
                {
                   // cmdGetInvoiceListByOrderProcessStatus = @"Select * From Whrc_Invoice Where PaidAmount >0 or OrderStatus=2";
                    cmdGetInvoiceListByOrderProcessStatus = @"Select * From Whrc_Invoice Where  OrderStatus=2";
                }
                else
                {
                   // cmdGetInvoiceListByOrderProcessStatus = @"Select * From Whrc_Invoice Where PaidAmount =0 and OrderStatus=1";
                    cmdGetInvoiceListByOrderProcessStatus = @"Select * From Whrc_Invoice Where  OrderStatus=1";
                }

                OdbcCommand cmd = new OdbcCommand(cmdGetInvoiceListByOrderProcessStatus);

                DataSet ds = ExecuteQuery(cmd);
                return GetListFromDataSet(ds);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        internal List<Invoice> GetInvoiceListByOrderProcessStatusAndClientName(bool isProcessed, string clientName)
        {
            try
            {
                clientName = "'%" + clientName + "%'";
                string cmdStrClientPart = @"Select Whrc_Invoice.* From Whrc_Invoice,Whrc_Users Where Whrc_Invoice.ClientID=Whrc_Users.ID And (Whrc_Users.FirstName like " + clientName + " Or Whrc_Users.LastName like " + clientName + ")";
                string cmdOrderProcessStatusPart = "";
                if (isProcessed)
                {
                    cmdOrderProcessStatusPart = " And PaidAmount >0";
                }
                else
                {
                    cmdOrderProcessStatusPart = " And PaidAmount =0";
                }
                cmdStrClientPart += cmdOrderProcessStatusPart;

                OdbcCommand cmd = new OdbcCommand(cmdStrClientPart);

                DataSet ds = ExecuteQuery(cmd);
                return GetListFromDataSet(ds);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        

        internal List<Invoice> GetInvoiceListForClientByOrderStatus(long clientID, OrderState orderState)
        {
            try
            {
                //Create instance of sqlcommand
                OdbcCommand cmd = new OdbcCommand(_cmdGetInvoiceListForClientByOrderStatus);
                cmd.Parameters.AddWithValue("@ClientId", clientID);
                cmd.Parameters.AddWithValue("@OrderStatus", Convert.ToInt32(orderState));

                DataSet ds = ExecuteQuery(cmd);

                return GetListFromDataSet(ds);
            }
            catch (Exception ex)
            {
                //Attempt to throw the exception for error handling
                throw ex;
            }
        }

        internal void ApplyDiscount(Invoice invoice)
        {
            try
            {
                OdbcCommand cmd = new OdbcCommand(_cmdApplyDiscount);

                cmd.Parameters.AddWithValue("@Discount", invoice.Discount);
                cmd.Parameters.AddWithValue("@DiscountTypeId", invoice.DiscountTypeId);
                cmd.Parameters.AddWithValue("@DiscountSubTypeId", invoice.DiscountSubTypeId);
                cmd.Parameters.AddWithValue("@PackageDiscount", invoice.PackageDiscount);
            
                cmd.Parameters.AddWithValue("@Id", invoice.Id);
                ExecuteNonQuery(cmd);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        internal void MakePayment(Invoice invoice)
        {
            try
            {
                OdbcCommand cmd = new OdbcCommand(_cmdMakePayment);

                cmd.Parameters.AddWithValue("@PaidAmount", invoice.PaidAmount);
                cmd.Parameters.AddWithValue("@PaymentMode", Convert.ToInt32(invoice.PaymentMode));
                cmd.Parameters.AddWithValue("@PaymentDate", invoice.PaymentDate);
                cmd.Parameters.AddWithValue("@PaymentNotes", invoice.PaymentNotes);
                cmd.Parameters.AddWithValue("@Orderstatus", invoice.OrderStatus);
                cmd.Parameters.AddWithValue("@PackageDiscount", invoice.PackageDiscount);

                cmd.Parameters.AddWithValue("@Id", invoice.Id);
                ExecuteNonQuery(cmd);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        internal void UpdateTotalRefundAmount(Invoice invoice)
        {
            try
            {
                OdbcCommand cmd = new OdbcCommand(_cmdUpdateTotalRefundAmount);

                cmd.Parameters.AddWithValue("@TotalRefundAmount", invoice.TotalRefundAmount);

                cmd.Parameters.AddWithValue("@Id", invoice.Id);
                ExecuteNonQuery(cmd);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        internal void UpdateTotalRefundAmount(decimal totalRefundedAmount, long invoiceID)
        {
            try
            {
                OdbcCommand cmd = new OdbcCommand(_cmdUpdateTotalRefundAmount);

                cmd.Parameters.AddWithValue("@TotalRefundAmount", totalRefundedAmount);
                cmd.Parameters.AddWithValue("@Id", invoiceID);

                ExecuteNonQuery(cmd);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        internal void UpdateInvoiceClientID(long invoiceID, long clientID)
        {
            try
            {
                OdbcCommand cmd = new OdbcCommand(_cmdUpdateInvoiceClientID);

                cmd.Parameters.AddWithValue("@ClientId", clientID);
                cmd.Parameters.AddWithValue("@Id", invoiceID);

                ExecuteNonQuery(cmd);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        internal void UpdateCreditCardPayment(Invoice invoice)
        {
            try
            {
                OdbcCommand cmd = new OdbcCommand(_cmdUpdateCreditCardPayment);

                cmd.Parameters.AddWithValue("@CreditCardTransactionStatus", invoice.CreditCardTransactionStatus);
                cmd.Parameters.AddWithValue("@CreditCardTransactionID", invoice.CreditCardTransactionID);
                cmd.Parameters.AddWithValue("@CreditCardTransactionDetails", invoice.CreditCardTransactionDetails);
                cmd.Parameters.AddWithValue("@PaymentDate", invoice.PaymentDate);
                cmd.Parameters.AddWithValue("@Id", invoice.Id);

                ExecuteNonQuery(cmd);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        internal long GetClassMedicalPaymentCount(long clientID)
        {
            try
            {
                OrderState orderState = OrderState.InCart;
                PaymentType paymentType = PaymentType.Medical;


                long classMedicalPaymentCount = 0;
                OdbcCommand cmd = new OdbcCommand(_cmdGetClassMedicalPaymentCount);

                cmd.Parameters.AddWithValue("@ClientID", clientID);
                cmd.Parameters.AddWithValue("@OrderStatus", Convert.ToInt32(orderState));
                cmd.Parameters.AddWithValue("@PaymentMode", Convert.ToInt32(paymentType));

                DataSet ds = ExecuteQuery(cmd);
                if (ds != null)
                {
                    if (ds.Tables[0].Rows[0][0] != DBNull.Value)
                    {
                        classMedicalPaymentCount = Convert.ToInt64(ds.Tables[0].Rows[0][0]);
                    }
                }

                return classMedicalPaymentCount;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
