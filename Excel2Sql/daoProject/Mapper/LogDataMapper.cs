﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.Odbc;
using System.Collections;
using daoProject.Infrastructure;
using daoProject.Model;
using daoProject.Mapper;

namespace daoProject.Mapper
{
    public class LogDataMapper : SqlHelper
    {
        private static string _cmdInsert = @"INSERT INTO WHRC_LogData (UserID,ObjectName,FieldName,ControlValue,ActionDate,ActionName,RecordId) VALUES (?,?,?,?,?,?,?); ";
        private static string _cmdUpdate = @"UPDATE WHRC_LogData SET UserID=?,ObjectName=?,FieldName=?,ControlValue=?,ActionDate=?,ActionName=?,RecordId=? WHERE Id=? ";
        private static string _cmdDelete = @"DELETE FROM WHRC_LogData WHERE Id=? ";
        private static string _cmdSelectAll = @"SELECT * FROM WHRC_LogData ";
        private static string _cmdSelectById = @"SELECT * FROM WHRC_LogData WHERE Id=? ";
        private static string _cmdSelectByObjectName = @"SELECT * FROM WHRC_LogData WHERE ObjectName=? ORDER BY RecordId ";
        private static string _cmdDeleteByObjectName = @"DELETE FROM WHRC_LogData WHERE ObjectName=? ";
        private static string _cmdDeleteLogOlderThanThisDate = @"DELETE FROM WHRC_LogData WHERE ActionDate<? ";

        internal void Create(LogData logData)
        {
            long id = -1;
            try
            {
                OdbcCommand cmd = new OdbcCommand(_cmdInsert);

                cmd.Parameters.AddWithValue("@InvoiceId", logData.UserID);
                cmd.Parameters.AddWithValue("@UserId", logData.ObjectName);
                cmd.Parameters.AddWithValue("@PaymentMode", logData.FieldName);
                cmd.Parameters.AddWithValue("@Amount", logData.ControlValue);
                cmd.Parameters.AddWithValue("@PaymentDate", logData.ActionDate);
                cmd.Parameters.AddWithValue("@TransactionStatus", logData.ActionName);
                cmd.Parameters.AddWithValue("@RecordId", logData.RecordId);

                //Execute the OdbcCommand
                id = Convert.ToInt64(ExecuteNonQueryAndScalar(cmd));

                logData.Id = id;
            }
            catch (Exception ex)
            {
                //Attempt to throw the exception for error handling
                throw ex;
            }
        }

        internal void Create(LogData logData, System.Data.Odbc.OdbcConnection conn, System.Data.Odbc.OdbcTransaction tx)
        {
            long id = -1;
            try
            {
                OdbcCommand cmd = new OdbcCommand(_cmdInsert);
                cmd.Parameters.AddWithValue("@InvoiceId", logData.UserID);
                cmd.Parameters.AddWithValue("@UserId", logData.ObjectName);
                cmd.Parameters.AddWithValue("@PaymentMode", logData.FieldName);
                cmd.Parameters.AddWithValue("@Amount", logData.ControlValue);
                cmd.Parameters.AddWithValue("@PaymentDate", logData.ActionDate);
                cmd.Parameters.AddWithValue("@TransactionStatus", logData.ActionName);
                cmd.Parameters.AddWithValue("@RecordId", logData.RecordId);

                //Execute the OdbcCommand
                id = Convert.ToInt64(ExecuteNonQueryAndScalar(cmd, conn, tx));

                logData.Id = id;
            }
            catch (Exception ex)
            {
                //Attempt to throw the exception for error handling
                throw ex;
            }
        }

        internal void Update(LogData logData)
        {
            try
            {
                OdbcCommand cmd = new OdbcCommand(_cmdUpdate);

                cmd.Parameters.AddWithValue("@InvoiceId", logData.UserID);
                cmd.Parameters.AddWithValue("@UserId", logData.ObjectName);
                cmd.Parameters.AddWithValue("@PaymentMode", logData.FieldName);
                cmd.Parameters.AddWithValue("@Amount", logData.ControlValue);
                cmd.Parameters.AddWithValue("@PaymentDate", logData.ActionDate);
                cmd.Parameters.AddWithValue("@TransactionStatus", logData.ActionName);
                cmd.Parameters.AddWithValue("@RecordId", logData.RecordId);
                cmd.Parameters.AddWithValue("@Id", logData.Id);

                //Execute the OdbcCommand
                ExecuteNonQuery(cmd);
            }
            catch (Exception ex)
            {
                //Attempt to throw the exception for error handling
                throw ex;
            }
        }

        internal bool Delete(long id)
        {
            bool success = false;

            try
            {
                OdbcCommand cmd = new OdbcCommand(_cmdDelete);

                cmd.Parameters.AddWithValue("@Id", id);

                ExecuteNonQuery(cmd);

                success = true;
            }
            catch (Exception ex)
            {
                //Attempt to throw the exception for error handling
                throw ex;
            }

            return success;
        }

        internal List<LogData> GetAll()
        {
            try
            {
                //Create instance of sqlcommand
                OdbcCommand cmd = new OdbcCommand(_cmdSelectAll);

                DataSet ds = ExecuteQuery(cmd);

                return GetListFromDataSet(ds);
            }
            catch (Exception ex)
            {
                //Attempt to throw the exception for error handling
                throw ex;
            }
        }

        internal LogData GetById(long id)
        {
            try
            {
                OdbcCommand cmd = new OdbcCommand(_cmdSelectById);
                
                cmd.Parameters.AddWithValue("@Id", id);

                DataSet ds = ExecuteQuery(cmd);

                return GetListFromDataSet(ds)[0];
            }
            catch (Exception ex)
            {
                //Attempt to throw the exception for error handling
                throw ex;
            }
        }

        internal List<LogData> GetListByObjectName(string  objectName)
        {
            try
            {
                //Create instance of sqlcommand
                OdbcCommand cmd = new OdbcCommand(_cmdSelectByObjectName);

                cmd.Parameters.AddWithValue("@ObjectName", objectName);

                DataSet ds = ExecuteQuery(cmd);

                return GetListFromDataSet(ds);
            }
            catch (Exception ex)
            {
                //Attempt to throw the exception for error handling
                throw ex;
            }
        }

        private List<LogData> GetListFromDataSet(DataSet ds)
        {
            List<LogData> logDataList = new List<LogData>();

            // This Loop reads all Data from DataSet                    
            for (int i = 0; i < ds.Tables["Data"].Rows.Count; i++)
            {
                LogData logData = new LogData();

                logData.Id = Convert.ToInt64(ds.Tables["Data"].Rows[i].ItemArray[0]);
                logData.UserID = Convert.ToInt64(ds.Tables["Data"].Rows[i].ItemArray[1]);
                logData.ObjectName = Convert.ToString(ds.Tables["Data"].Rows[i].ItemArray[2]);
                logData.FieldName = Convert.ToString(ds.Tables["Data"].Rows[i].ItemArray[3]);
                logData.ControlValue = Convert.ToString(ds.Tables["Data"].Rows[i].ItemArray[4]);
                logData.ActionDate = Convert.ToDateTime(ds.Tables["Data"].Rows[i].ItemArray[5]);
                logData.ActionName = (UserAction)Convert.ToInt32(ds.Tables["Data"].Rows[i].ItemArray[6]);
                logData.RecordId =Convert.ToInt64(ds.Tables["Data"].Rows[i].ItemArray[7]);

                logDataList.Add(logData);
            }
            return logDataList;
        }

        internal bool DeleteByObjectName(string objectName)
        {
            bool success = false;

            try
            {
                OdbcCommand cmd = new OdbcCommand(_cmdDeleteByObjectName);

                cmd.Parameters.AddWithValue("@ObjectName", objectName);

                ExecuteNonQuery(cmd);

                success = true;
            }
            catch (Exception ex)
            {
                //Attempt to throw the exception for error handling
                throw ex;
            }

            return success;
        }

        internal void DeleteLogOlderThanThisDate(DateTime date)
        {
            try
            {
                OdbcCommand cmd = new OdbcCommand(_cmdDeleteLogOlderThanThisDate);

                cmd.Parameters.AddWithValue("@date", date);

                ExecuteNonQuery(cmd);
            }
            catch (Exception ex)
            {
                //Attempt to throw the exception for error handling
                throw ex;
            }
        }
    }
}
