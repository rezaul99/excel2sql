﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.Odbc;
using System.Collections;
using daoProject.Infrastructure;
using daoProject.Model;
using daoProject.Mapper;

namespace daoProject.Mapper
{
    internal class ClassInvoiceItemDataMapper : SqlHelper
    {
        private static string _cmdInsert = @"INSERT INTO Whrc_ClassInvoiceItem (InvoiceId,SectionId,SignupDate,NumberAttending,Price,Description, IsClassCanceled, CancelDate, RefundAmount,IsRefundProcessed,RefundedDate,IsEnrollmentAsVisitor, IsInWaitingList, IsClassCanceledByClient, DateClassCanceledByClient,ClassInvoiceItemState) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?, ?, ?,?); ";
        private static string _cmdUpdate = @"UPDATE Whrc_ClassInvoiceItem SET InvoiceId=?,SectionId=?,SignupDate=?,NumberAttending=?,Price=?,Description=?,IsClassCanceled=?, CancelDate=?, RefundAmount=?,IsRefundProcessed=?,RefundedDate=?,IsEnrollmentAsVisitor=?,IsInWaitingList=?, IsClassCanceledByClient=?,DateClassCanceledByClient=?,ClassInvoiceItemState=? WHERE Id=? ";
        private static string _cmdDelete = @"DELETE FROM Whrc_ClassInvoiceItem WHERE Id=? ";
        private static string _cmdSelectAll = @"SELECT * FROM Whrc_ClassInvoiceItem ";
        private static string _cmdSelectById = @"SELECT * FROM Whrc_ClassInvoiceItem WHERE Id=? ";
        private static string _cmdSelectByInvoiceId = @"SELECT * FROM Whrc_ClassInvoiceItem WHERE InvoiceId=? ";
        private static string _cmdgetItemAmountPriceByID = @"SELECT Price FROM Whrc_ClassInvoiceItem WHERE Id=? ";
        private static string _cmdDeleteByInvoiceId = @"DELETE FROM Whrc_ClassInvoiceItem WHERE InvoiceId=? ";

        private static string _cmdCancelClass = @"UPDATE Whrc_ClassInvoiceItem SET IsClassCanceled=?,CancelDate=?,RefundAmount=? WHERE Id=? ";
        private static string _cmdUpdateRefundProcessed = @"UPDATE Whrc_ClassInvoiceItem SET IsRefundProcessed=?,RefundedDate=?,RefundAmount=?  WHERE Id=? ";

        private static string _cmdGetNumberOfAttendingForUser = @"SELECT NumberAttending FROM Whrc_ClassInvoiceItem WHERE InvoiceId = ? And SectionId=? ";
       private static string _cmdCalculateTotalSignUPInCart = @"SELECT Sum(NumberAttending) FROM Whrc_ClassInvoiceItem WHERE SectionId=? And IsClassCanceled=0 And ClassInvoiceItemState=3";
       private static string _cmdTotalWaitingListBySectionId = @"SELECT Sum(NumberAttending) FROM Whrc_ClassInvoiceItem WHERE SectionId=? And IsInWaitingList = 1 And IsClassCanceled=0";
       private static string _cmdTotalSignUPBySectionId = @"SELECT Sum(NumberAttending) FROM Whrc_ClassInvoiceItem WHERE SectionId=? And IsInWaitingList = 0 And IsClassCanceled=0 And ClassInvoiceItemState NOT IN (0)";
        private static string _cmdCalculateTotalSignUP = @"SELECT Sum(NumberAttending) FROM Whrc_ClassInvoiceItem WHERE SectionId=? And IsClassCanceled=0";
        //private static string _cmdGetListByDateRange = @"SELECT * FROM Whrc_ClassInvoiceItem WHERE SignupDate between ? and ? ";
        //private static string _cmdGetListByDateRangeAndSectionId = @"SELECT * FROM Whrc_ClassInvoiceItem WHERE (SignupDate between ? and ?) AND SectionId=? ";
        //private static string _cmdGetListByDateRangeAndSectionId = @"SELECT * FROM Whrc_ClassInvoiceItem WHERE SectionId=? ";
        private static string _cmdUpdateEnrollmentWaitingStatus = @"UPDATE Whrc_ClassInvoiceItem SET IsInWaitingList=?, ClassInvoiceItemState=?  WHERE Id=? ";
        private static string _cmdGetClassListByUserId = @"Select * from Whrc_ClassInvoiceItem where WHRC_ClassInvoiceItem.InvoiceID IN 
(select Whrc_Invoice.Id from Whrc_Invoice,WHRC_Users Where  WHRC_Invoice.ID = WHRC_ClassInvoiceItem.InvoiceID
                            And WHRC_Invoice.ClientID = WHRC_Users.ID
                            And Whrc_Invoice.OrderStatus NOT IN (0)
And WHRC_Invoice.ClientID = ?)";
        private static string _cmdGetBySectionId = @"SELECT * FROM Whrc_ClassInvoiceItem WHERE SectionId=? ";
        private static string _cmdGetEnrollmentAndWaitingListBySectionID = @"Select FirstName, LastName, Phone, email,Address1, SignupDate,IsInWaitingList,WHRC_ClassInvoiceItem.id as ClassInvoiceItemID, IsClassCanceledByClient,IsClassCanceled From WHRC_ClassInvoiceItem,WHRC_Invoice,WHRC_Users  where WHRC_Invoice.ID = WHRC_ClassInvoiceItem.InvoiceID And WHRC_Invoice.ClientID=WHRC_Users.id  And SectionID=?";

        private static string _cmdUpdateEnrollmentCancelByClient = @"UPDATE Whrc_ClassInvoiceItem SET IsClassCanceledByClient=?, DateClassCanceledByClient=?  WHERE Id=? ";

        private static string _cmdUpdateSection = @"UPDATE Whrc_ClassInvoiceItem SET SectionId=?,SignupDate=?,NumberAttending=?,Price=?,IsEnrollmentAsVisitor=?,IsInWaitingList=?,ClassInvoiceItemState=?  WHERE Id=? ";
        private static string _cmdUpdateItemStatus = @"UPDATE Whrc_ClassInvoiceItem SET ClassInvoiceItemState=?  WHERE Id=? ";
        private static string _cmdUpdateClassInvoiceId = @"UPDATE Whrc_ClassInvoiceItem SET InvoiceId=? WHERE Id=? ";

        internal void Create(ClassInvoiceItem classInvoiceItem)
        {
            long id = -1;
            try
            {
                OdbcCommand cmd = new OdbcCommand(_cmdInsert);

                OdbcParameter odbcParamObj = new OdbcParameter("@InvoiceId", classInvoiceItem.InvoiceId);
               
                cmd.Parameters.Add(odbcParamObj);
                cmd.Parameters.AddWithValue("@SectionId", classInvoiceItem.SectionId);
                cmd.Parameters.AddWithValue("@SignupDate", classInvoiceItem.SignupDate);
                cmd.Parameters.AddWithValue("@NumberAttending", classInvoiceItem.NumberAttending);
                cmd.Parameters.AddWithValue("@Price", classInvoiceItem.Price);
                cmd.Parameters.AddWithValue("@Description", classInvoiceItem.Description);

                cmd.Parameters.AddWithValue("@IsClassCanceled", classInvoiceItem.IsClassCanceled);
                cmd.Parameters.AddWithValue("@CancelDate", classInvoiceItem.CancelDate);
                cmd.Parameters.AddWithValue("@RefundAmount", classInvoiceItem.RefundAmount);
                cmd.Parameters.AddWithValue("@IsRefundProcessed", classInvoiceItem.IsRefundProcessed);
                cmd.Parameters.AddWithValue("@RefundedDate", classInvoiceItem.RefundedDate);
                cmd.Parameters.AddWithValue("@IsEnrollmentAsVisitor", classInvoiceItem.IsEnrollmentAsVisitor);
                cmd.Parameters.AddWithValue("@IsInWaitingList", classInvoiceItem.IsInWaitingList);
                cmd.Parameters.AddWithValue("@IsClassCanceledByClient", classInvoiceItem.IsClassCanceledByClient);
                cmd.Parameters.AddWithValue("@DateClassCanceledByClient", classInvoiceItem.DateClassCanceledByClient);
                cmd.Parameters.AddWithValue("@ClassInvoiceItemState", classInvoiceItem.ClassInvoiceItemState);

                //Execute the OdbcCommand
                id = Convert.ToInt64(ExecuteNonQueryAndScalar(cmd));

                classInvoiceItem.Id = id;
            }
            catch (Exception ex)
            {
                //Attempt to throw the exception for error handling
                throw ex;
            }
        }

        internal void Create(ClassInvoiceItem classInvoiceItem, System.Data.Odbc.OdbcConnection conn, System.Data.Odbc.OdbcTransaction tx)
        {
            long id = -1;
            try
            {
                OdbcCommand cmd = new OdbcCommand(_cmdInsert);
                
                cmd.Parameters.AddWithValue("@InvoiceId", classInvoiceItem.InvoiceId);
                cmd.Parameters.AddWithValue("@SectionId", classInvoiceItem.SectionId);
                cmd.Parameters.AddWithValue("@SignupDate", classInvoiceItem.SignupDate);
                cmd.Parameters.AddWithValue("@NumberAttending", classInvoiceItem.NumberAttending);
                cmd.Parameters.AddWithValue("@Price", classInvoiceItem.Price);
                cmd.Parameters.AddWithValue("@Description", classInvoiceItem.Description);

                cmd.Parameters.AddWithValue("@IsClassCanceled", classInvoiceItem.IsClassCanceled);
                cmd.Parameters.AddWithValue("@CancelDate", classInvoiceItem.CancelDate);
                cmd.Parameters.AddWithValue("@RefundAmount", classInvoiceItem.RefundAmount);
                cmd.Parameters.AddWithValue("@IsRefundProcessed", classInvoiceItem.IsRefundProcessed);
                cmd.Parameters.AddWithValue("@RefundedDate", classInvoiceItem.RefundedDate);
                cmd.Parameters.AddWithValue("@IsEnrollmentAsVisitor", classInvoiceItem.IsEnrollmentAsVisitor);
                cmd.Parameters.AddWithValue("@IsInWaitingList", classInvoiceItem.IsInWaitingList);

                cmd.Parameters.AddWithValue("@IsClassCanceledByClient", classInvoiceItem.IsClassCanceledByClient);
                cmd.Parameters.AddWithValue("@DateClassCanceledByClient", classInvoiceItem.DateClassCanceledByClient);
                cmd.Parameters.AddWithValue("@ClassInvoiceItemState", classInvoiceItem.ClassInvoiceItemState);

                //Execute the OdbcCommand
                id = Convert.ToInt64(ExecuteNonQueryAndScalar(cmd,conn,tx));

                classInvoiceItem.Id = id;
            }
            catch (Exception ex)
            {
                //Attempt to throw the exception for error handling
                throw ex;
            }
        }

        internal void Update(ClassInvoiceItem classInvoiceItem)
        {
            try
            {
                OdbcCommand cmd = new OdbcCommand(_cmdUpdate);

                cmd.Parameters.AddWithValue("@InvoiceId", classInvoiceItem.InvoiceId);
                cmd.Parameters.AddWithValue("@SectionId", classInvoiceItem.SectionId);
                cmd.Parameters.AddWithValue("@SignupDate", classInvoiceItem.SignupDate);
                cmd.Parameters.AddWithValue("@NumberAttending", classInvoiceItem.NumberAttending);
                cmd.Parameters.AddWithValue("@Price", classInvoiceItem.Price);
                cmd.Parameters.AddWithValue("@Description", classInvoiceItem.Description);

                cmd.Parameters.AddWithValue("@IsClassCanceled", classInvoiceItem.IsClassCanceled);
                cmd.Parameters.AddWithValue("@CancelDate", classInvoiceItem.CancelDate);
                cmd.Parameters.AddWithValue("@RefundAmount", classInvoiceItem.RefundAmount);
                cmd.Parameters.AddWithValue("@IsRefundProcessed", classInvoiceItem.IsRefundProcessed);
                cmd.Parameters.AddWithValue("@RefundedDate", classInvoiceItem.RefundedDate);
                cmd.Parameters.AddWithValue("@IsEnrollmentAsVisitor", classInvoiceItem.IsEnrollmentAsVisitor);
                cmd.Parameters.AddWithValue("@IsInWaitingList", classInvoiceItem.IsInWaitingList);

                cmd.Parameters.AddWithValue("@IsClassCanceledByClient", classInvoiceItem.IsClassCanceledByClient);
                cmd.Parameters.AddWithValue("@DateClassCanceledByClient", classInvoiceItem.DateClassCanceledByClient);
                cmd.Parameters.AddWithValue("@ClassInvoiceItemState", classInvoiceItem.ClassInvoiceItemState);

                cmd.Parameters.AddWithValue("@Id", classInvoiceItem.Id);

                //Execute the OdbcCommand
                ExecuteNonQuery(cmd);
            }
            catch (Exception ex)
            {
                //Attempt to throw the exception for error handling
                throw ex;
            }
        }

        internal void UpdateSection(ClassInvoiceItem classInvoiceItem)
        {
            try
            {
                OdbcCommand cmd = new OdbcCommand(_cmdUpdateSection);

                cmd.Parameters.AddWithValue("@SectionId", classInvoiceItem.SectionId);
                cmd.Parameters.AddWithValue("@SignupDate", classInvoiceItem.SignupDate);
                cmd.Parameters.AddWithValue("@NumberAttending", classInvoiceItem.NumberAttending);
                cmd.Parameters.AddWithValue("@Price", classInvoiceItem.Price);
                cmd.Parameters.AddWithValue("@IsEnrollmentAsVisitor", classInvoiceItem.IsEnrollmentAsVisitor);
                cmd.Parameters.AddWithValue("@IsInWaitingList", classInvoiceItem.IsInWaitingList);
                cmd.Parameters.AddWithValue("@ClassInvoiceItemState", classInvoiceItem.ClassInvoiceItemState);

                cmd.Parameters.AddWithValue("@Id", classInvoiceItem.Id);

                //Execute the OdbcCommand
                ExecuteNonQuery(cmd);
            }
            catch (Exception ex)
            {
                //Attempt to throw the exception for error handling
                throw ex;
            }
        }

        internal void Delete(long id)
        {
            bool success = false;

            try
            {
                OdbcCommand cmd = new OdbcCommand(_cmdDelete);

                cmd.Parameters.AddWithValue("@Id", id);

                ExecuteNonQuery(cmd);

                //success = true;
            }
            catch (Exception ex)
            {
                //Attempt to throw the exception for error handling
                throw ex;
            }

           // return success;
        }
        public decimal getItemAmountPrice(long Id)
        {
            decimal totalAmountPaid = 0;

            try
            {
                OdbcCommand cmd = new OdbcCommand(_cmdgetItemAmountPriceByID);
                cmd.Parameters.AddWithValue("@Id", Id);
                DataSet ds = ExecuteQuery(cmd);

                if (ds != null)
                {
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        if (ds.Tables[0].Rows[0].ItemArray[0] != DBNull.Value)
                        {
                            totalAmountPaid = Convert.ToDecimal(ds.Tables[0].Rows[0].ItemArray[0]);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return totalAmountPaid;
        }
        //
        public int  GetNumberOfAttendingForUser(long InvoiceID,long SectionID)
        {
            int  numberOfAttending = 0;

            try
            {
                OdbcCommand cmd = new OdbcCommand(_cmdGetNumberOfAttendingForUser);
                cmd.Parameters.AddWithValue("@InvoiceId", InvoiceID);
                cmd.Parameters.AddWithValue("@SectionId", SectionID);
                DataSet ds = ExecuteQuery(cmd);

                if (ds != null)
                {
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        if (ds.Tables[0].Rows[0].ItemArray[0] != DBNull.Value)
                        {
                            numberOfAttending = Convert.ToInt32(ds.Tables[0].Rows[0].ItemArray[0]);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return numberOfAttending;
        }
        public List<ClassInvoiceItem> GetListFromDataSet(DataSet ds)
        {
            List<ClassInvoiceItem> classInvoiceItemList = new List<ClassInvoiceItem>();

            // This Loop reads all Data from DataSet                    
            for (int i = 0; i < ds.Tables["Data"].Rows.Count; i++)
            {
                ClassInvoiceItem classInvoiceItem = new ClassInvoiceItem();

                classInvoiceItem.Id = Convert.ToInt64(ds.Tables["Data"].Rows[i].ItemArray[0]);
                classInvoiceItem.InvoiceId = Convert.ToInt64(ds.Tables["Data"].Rows[i].ItemArray[1]);
                classInvoiceItem.SectionId = Convert.ToInt64(ds.Tables["Data"].Rows[i].ItemArray[2]);
                classInvoiceItem.SignupDate = Convert.ToDateTime(ds.Tables["Data"].Rows[i].ItemArray[3]);
                classInvoiceItem.NumberAttending = Convert.ToInt32(ds.Tables["Data"].Rows[i].ItemArray[4]);
                classInvoiceItem.Price = Convert.ToDecimal(ds.Tables["Data"].Rows[i].ItemArray[5]);
                classInvoiceItem.Description = Convert.ToString(ds.Tables["Data"].Rows[i].ItemArray[6]);

                if (ds.Tables["Data"].Rows[i].ItemArray[7] != DBNull.Value)
                {
                    classInvoiceItem.IsClassCanceled = Convert.ToBoolean(ds.Tables["Data"].Rows[i].ItemArray[7]);
                }
                else
                {
                    classInvoiceItem.IsClassCanceled = false;
                }

                if (ds.Tables["Data"].Rows[i].ItemArray[8] != DBNull.Value)
                {
                    classInvoiceItem.CancelDate = Convert.ToDateTime(Convert.ToString(ds.Tables["Data"].Rows[i].ItemArray[8]));
                }
                else
                {
                    classInvoiceItem.CancelDate = Convert.ToDateTime("1/1/1900");
                }

                if (ds.Tables["Data"].Rows[i].ItemArray[9] != DBNull.Value)
                {
                    classInvoiceItem.RefundAmount = Convert.ToDecimal(ds.Tables["Data"].Rows[i].ItemArray[9]);
                }
                else
                {
                    classInvoiceItem.RefundAmount = 0;
                }

                if (ds.Tables["Data"].Rows[i].ItemArray[10] != DBNull.Value)
                {
                    classInvoiceItem.IsRefundProcessed = Convert.ToBoolean(ds.Tables["Data"].Rows[i].ItemArray[10]);
                }
                else
                {
                    classInvoiceItem.IsRefundProcessed = false;
                }

                if (ds.Tables["Data"].Rows[i].ItemArray[11] != DBNull.Value)
                {
                    classInvoiceItem.RefundedDate = Convert.ToDateTime(Convert.ToString(ds.Tables["Data"].Rows[i].ItemArray[11]));
                }
                else
                {
                    classInvoiceItem.RefundedDate = Convert.ToDateTime("1/1/1900");
                }
                if (ds.Tables["Data"].Rows[i].ItemArray[12] != DBNull.Value)
                {
                    classInvoiceItem.IsEnrollmentAsVisitor = Convert.ToBoolean(ds.Tables["Data"].Rows[i].ItemArray[12]);
                }
                else
                {
                    classInvoiceItem.IsEnrollmentAsVisitor = false;
                }
                if (ds.Tables["Data"].Rows[i].ItemArray[13] != DBNull.Value)
                {
                    classInvoiceItem.IsInWaitingList = Convert.ToBoolean(ds.Tables["Data"].Rows[i].ItemArray[13]);
                }
                else
                {
                    classInvoiceItem.IsInWaitingList = false;
                }

                if (ds.Tables["Data"].Rows[i].ItemArray[14] != DBNull.Value)
                {
                    classInvoiceItem.IsClassCanceledByClient = Convert.ToBoolean(ds.Tables["Data"].Rows[i].ItemArray[14]);
                }
                else
                {
                    classInvoiceItem.IsClassCanceledByClient = false;
                }

                if (ds.Tables["Data"].Rows[i].ItemArray[15] != DBNull.Value)
                {
                    classInvoiceItem.DateClassCanceledByClient = Convert.ToDateTime(Convert.ToString(ds.Tables["Data"].Rows[i].ItemArray[15]));
                }
                else
                {
                    classInvoiceItem.DateClassCanceledByClient = Convert.ToDateTime("1/1/1900");
                }

                if (ds.Tables["Data"].Rows[i].ItemArray[16] != DBNull.Value)
                {
                    classInvoiceItem.ClassInvoiceItemState = (ClassInvoiceItemState)Convert.ToInt32(Convert.ToString(ds.Tables["Data"].Rows[i].ItemArray[16]));
                }
                else
                {
                    classInvoiceItem.ClassInvoiceItemState = ClassInvoiceItemState.NA;
                }

                classInvoiceItemList.Add(classInvoiceItem);
            }

            return classInvoiceItemList;
        }

        internal List<ClassInvoiceItem> GetAll()
        {
            try
            {
                //Create instance of sqlcommand
                OdbcCommand cmd = new OdbcCommand(_cmdSelectAll);

                DataSet ds = ExecuteQuery(cmd);

                return GetListFromDataSet(ds);
            }
            catch (Exception ex)
            {
                //Attempt to throw the exception for error handling
                throw ex;
            }
        }

        internal ClassInvoiceItem GetById(long id)
        {
            try
            {
                OdbcCommand cmd = new OdbcCommand(_cmdSelectById);

                cmd.Parameters.AddWithValue("@Id", id);

                DataSet ds = ExecuteQuery(cmd);

                List<ClassInvoiceItem> ClassInvoiceItemList=GetListFromDataSet(ds);
                if (ClassInvoiceItemList.Count > 0)
                {
                    return ClassInvoiceItemList[0];
                }
                else
                {
                    return null;
                }
               
            }
            catch (Exception ex)
            {
                //Attempt to throw the exception for error handling
                throw ex;
            }
        }

        internal ClassInvoiceItem GetBySectionId(long sectionID)
        {
            try
            {
                OdbcCommand cmd = new OdbcCommand(_cmdGetBySectionId);
                cmd.Parameters.AddWithValue("@SectionID", sectionID);
                DataSet ds = ExecuteQuery(cmd);

                return GetListFromDataSet(ds)[0];
            }
            catch (Exception ex)
            {
                //Attempt to throw the exception for error handling
                throw ex;
            }
        }
        internal List<ClassInvoiceItem> GetClassListByUserId(long UserID)
        {
            try
            {
                OdbcCommand cmd = new OdbcCommand(_cmdGetClassListByUserId);
                cmd.Parameters.AddWithValue("@ClientID", UserID);
                DataSet ds = ExecuteQuery(cmd);

                return GetListFromDataSet(ds);
            }
            catch (Exception ex)
            {
                //Attempt to throw the exception for error handling
                throw ex;
            }
        }
        internal List<ClassInvoiceItem> GetAllBySectionId(long sectionID)
        {
            try
            {
                OdbcCommand cmd = new OdbcCommand(_cmdGetBySectionId);
                cmd.Parameters.AddWithValue("@SectionID", sectionID);
                DataSet ds = ExecuteQuery(cmd);

                return GetListFromDataSet(ds);
            }
            catch (Exception ex)
            {
                //Attempt to throw the exception for error handling
                throw ex;
            }
        }

       

        internal List<ClassEnrollmentWaitingHelper> GetEnrollmentAndWaitingListBySectionID(long sectionID)
        {
            try
            {
                OdbcCommand cmd = new OdbcCommand(_cmdGetEnrollmentAndWaitingListBySectionID);
                cmd.Parameters.AddWithValue("@SectionID", sectionID);
                DataSet ds = ExecuteQuery(cmd);

                List<ClassEnrollmentWaitingHelper> classEnrollmentWaitingHelperList = new List<ClassEnrollmentWaitingHelper>();
                ClassEnrollmentWaitingHelper classEnrollmentWaitingHelper = null;
                for (int i = 0; i < ds.Tables["Data"].Rows.Count; i++)
                {
                    classEnrollmentWaitingHelper  = new ClassEnrollmentWaitingHelper();

                    classEnrollmentWaitingHelper.SectionID = sectionID;
                    classEnrollmentWaitingHelper.FirstName = Convert.ToString(ds.Tables["Data"].Rows[i].ItemArray[0]);
                    classEnrollmentWaitingHelper.LastName = Convert.ToString(ds.Tables["Data"].Rows[i].ItemArray[1]);
                    classEnrollmentWaitingHelper.Phone = Convert.ToString(ds.Tables["Data"].Rows[i].ItemArray[2]);
                    classEnrollmentWaitingHelper.Email = Convert.ToString(ds.Tables["Data"].Rows[i].ItemArray[3]);
                    classEnrollmentWaitingHelper.Address = Convert.ToString(ds.Tables["Data"].Rows[i].ItemArray[4]);
                    classEnrollmentWaitingHelper.SignupDate = Convert.ToDateTime(ds.Tables["Data"].Rows[i].ItemArray[5]);
                    classEnrollmentWaitingHelper.SignupDateStr = classEnrollmentWaitingHelper.SignupDate.ToString("MM/dd/yyy");
                    if (ds.Tables["Data"].Rows[i].ItemArray[6] != DBNull.Value)
                    {
                        classEnrollmentWaitingHelper.IsInWaitingList = Convert.ToBoolean(ds.Tables["Data"].Rows[i].ItemArray[6]);
                    }
                    else
                    {
                        classEnrollmentWaitingHelper.IsInWaitingList = false;
                    }
                    classEnrollmentWaitingHelper.ClassInvoiceItemID = Convert.ToInt64(ds.Tables["Data"].Rows[i].ItemArray[7]);

                    if (ds.Tables["Data"].Rows[i].ItemArray[8] != DBNull.Value)
                    {
                        classEnrollmentWaitingHelper.IsClassCanceledByClient = Convert.ToBoolean(ds.Tables["Data"].Rows[i].ItemArray[8]);
                        if (classEnrollmentWaitingHelper.IsClassCanceledByClient)
                        {
                            classEnrollmentWaitingHelper.ClassCanceledByClientMsg = "Request for signup cancellation";
                        }
                        else
                        {
                            classEnrollmentWaitingHelper.ClassCanceledByClientMsg = "";
                        }
                    }
                    else
                    {
                        classEnrollmentWaitingHelper.IsClassCanceledByClient = false;
                        classEnrollmentWaitingHelper.ClassCanceledByClientMsg = "";
                    }
                    if (ds.Tables["Data"].Rows[i].ItemArray[9] != DBNull.Value)
                    {
                        classEnrollmentWaitingHelper.IsClassCanceled = Convert.ToBoolean(ds.Tables["Data"].Rows[i].ItemArray[9]);
                        if (classEnrollmentWaitingHelper.IsClassCanceled)
                        {
                            classEnrollmentWaitingHelper.ClassCanceledByClientMsg = "Signup cancelled";
                        }
                    }
                    else
                    {
                        classEnrollmentWaitingHelper.IsClassCanceled = false;
                    }

                    classEnrollmentWaitingHelperList.Add(classEnrollmentWaitingHelper);
                }

                return classEnrollmentWaitingHelperList;
            }
            catch (Exception ex)
            {
                //Attempt to throw the exception for error handling
                throw ex;
            }
        }

        internal int GetTotalSignUPBySectionId(long sectionId)
        {
            try
            {
                int TotalWaitingList = 0;
                OdbcCommand cmd = new OdbcCommand(_cmdTotalSignUPBySectionId);
                cmd.Parameters.AddWithValue("@SectionId", sectionId);
                DataSet ds = ExecuteQuery(cmd);
                if (ds != null)
                {
                    if (ds.Tables[0] != null)
                    {
                        if (ds.Tables[0].Rows.Count > 0)
                        {
                            if (ds.Tables[0].Rows[0][0] != DBNull.Value)
                            {
                                TotalWaitingList = Convert.ToInt32(ds.Tables[0].Rows[0][0]);
                            }
                        }
                    }
                }
                return TotalWaitingList;
            }
            catch (Exception ex)
            {
                //Attempt to throw the exception for error handling
                throw ex;
            }
        }

        internal int GetTotalWaitingListBySectionId(long sectionId)
        {
            try
            {
                int TotalWaitingLis = 0;
                OdbcCommand cmd = new OdbcCommand(_cmdTotalWaitingListBySectionId);
                cmd.Parameters.AddWithValue("@SectionId", sectionId);
                DataSet ds = ExecuteQuery(cmd);
                if (ds != null)
                {
                    if (ds.Tables[0] != null)
                    {
                        if (ds.Tables[0].Rows.Count > 0)
                        {
                            if (ds.Tables[0].Rows[0][0] != DBNull.Value)
                            {
                                TotalWaitingLis = Convert.ToInt32(ds.Tables[0].Rows[0][0]);
                            }
                        }
                    }
                }
                return TotalWaitingLis;
            }
            catch (Exception ex)
            {
                //Attempt to throw the exception for error handling
                throw ex;
            }
        }

        internal List<ClassInvoiceItem> GetListByInvoiceId(long invoiceId)
        {
            try
            {
                //Create instance of sqlcommand
                OdbcCommand cmd = new OdbcCommand(_cmdSelectByInvoiceId);

                cmd.Parameters.AddWithValue("@InvoiceId", invoiceId);

                DataSet ds = ExecuteQuery(cmd);

                return GetListFromDataSet(ds);
            }
            catch (Exception ex)
            {
                //Attempt to throw the exception for error handling
                throw ex;
            }
        }

        //internal List<ClassInvoiceItem> GetListByDateRange(DateTime fromDate, DateTime toDate)
        //{
        //    try
        //    {
        //        //Create instance of sqlcommand
        //        OdbcCommand cmd = new OdbcCommand(_cmdGetListByDateRange);

        //        cmd.Parameters.AddWithValue("@fromDate", fromDate);
        //        cmd.Parameters.AddWithValue("@toDate", toDate);

        //        DataSet ds = ExecuteQuery(cmd);

        //        return GetListFromDataSet(ds);
        //    }
        //    catch (Exception ex)
        //    {
        //        //Attempt to throw the exception for error handling
        //        throw ex;
        //    }
        //}

        //internal List<ClassInvoiceItem> GetListByDateRangeAndSectionId(DateTime fromDate, DateTime toDate, long sectionId)
        //{
        //    try
        //    {
        //        //Create instance of sqlcommand
        //        OdbcCommand cmd = new OdbcCommand(_cmdGetListByDateRangeAndSectionId);

        //        cmd.Parameters.AddWithValue("@fromDate", fromDate);
        //        cmd.Parameters.AddWithValue("@toDate", toDate);
        //        cmd.Parameters.AddWithValue("@sectionId", sectionId);

        //        DataSet ds = ExecuteQuery(cmd);

        //        return GetListFromDataSet(ds);
        //    }
        //    catch (Exception ex)
        //    {
        //        //Attempt to throw the exception for error handling
        //        throw ex;
        //    }
        //}
        //internal List<ClassInvoiceItem> GetListByDateRangeAndSectionId(long sectionId)
        //{
        //    try
        //    {
        //        //Create instance of sqlcommand
        //        OdbcCommand cmd = new OdbcCommand(_cmdGetListByDateRangeAndSectionId);

        //        cmd.Parameters.AddWithValue("@sectionId", sectionId);

        //        DataSet ds = ExecuteQuery(cmd);

        //        return GetListFromDataSet(ds);
        //    }
        //    catch (Exception ex)
        //    {
        //        //Attempt to throw the exception for error handling
        //        throw ex;
        //    }
        //}

        internal bool DeleteByInvoiceId(long invoiceId)
        {
            bool success = false;

            try
            {
                OdbcCommand cmd = new OdbcCommand(_cmdDeleteByInvoiceId);

                cmd.Parameters.AddWithValue("@InvoiceId", invoiceId);

                ExecuteNonQuery(cmd);

                success = true;
            }
            catch (Exception ex)
            {
                //Attempt to throw the exception for error handling
                throw ex;
            }

            return success;
        }

        internal void CancelClass(ClassInvoiceItem classInvoiceItem)
        {
            try
            {
                OdbcCommand cmd = new OdbcCommand(_cmdCancelClass);

                cmd.Parameters.AddWithValue("@IsClassCanceled", classInvoiceItem.IsClassCanceled);
                cmd.Parameters.AddWithValue("@CancelDate", classInvoiceItem.CancelDate);
                cmd.Parameters.AddWithValue("@RefundAmount", classInvoiceItem.RefundAmount);
         
                cmd.Parameters.AddWithValue("@Id", classInvoiceItem.Id);

                ExecuteNonQuery(cmd);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        internal void UpdateRefundProcessed(ClassInvoiceItem classInvoiceItem)
        {
            try
            {
                OdbcCommand cmd = new OdbcCommand(_cmdUpdateRefundProcessed);
                cmd.Parameters.AddWithValue("@IsRefundProcessed", classInvoiceItem.IsRefundProcessed);
                cmd.Parameters.AddWithValue("@RefundedDate", classInvoiceItem.RefundedDate);
                cmd.Parameters.AddWithValue("@RefundAmount", classInvoiceItem.RefundAmount);

                cmd.Parameters.AddWithValue("@Id", classInvoiceItem.Id);
                ExecuteNonQuery(cmd);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        internal int CalculateTotalSignUP(long sectionID)
        {
            try
            {
                int totalSignUP = 0;

                //Create instance of sqlcommand
                OdbcCommand cmd = new OdbcCommand(_cmdCalculateTotalSignUP);

                cmd.Parameters.AddWithValue("@SectionId", sectionID);


                DataSet ds = ExecuteQuery(cmd);

                if (ds != null)
                {
                    if (ds.Tables[0] != null)
                    {
                        if (ds.Tables[0].Rows.Count > 0)
                        {
                            if (ds.Tables[0].Rows[0][0] != DBNull.Value)
                            {
                                totalSignUP = Convert.ToInt32(ds.Tables[0].Rows[0][0]);
                            }
                        }
                    }
                }
               

                return totalSignUP;
            }
            catch (Exception ex)
            {
                //Attempt to throw the exception for error handling
                throw ex;
            }
        }

        internal int CalculateTotalSignUPInCart(long sectionID)
        {
            try
            {
                int totalSignUP = 0;

               // Create instance of sqlcommand
                OdbcCommand cmd = new OdbcCommand(_cmdCalculateTotalSignUPInCart);

               // int i = 1;// ClassInvoiceItemState.Processed;
                cmd.Parameters.AddWithValue("@SectionId", sectionID);
                // cmd.Parameters.AddWithValue("@ClassInvoiceItemState",i);


                DataSet ds = ExecuteQuery(cmd);

                if (ds != null)
                {
                    if (ds.Tables[0] != null)
                    {
                        if (ds.Tables[0].Rows.Count > 0)
                        {
                            if (ds.Tables[0].Rows[0][0] != DBNull.Value)
                            {
                                totalSignUP = Convert.ToInt32(ds.Tables[0].Rows[0][0]);
                            }
                        }
                    }
                }


                return totalSignUP;
            }
            catch (Exception ex)
            {
                //Attempt to throw the exception for error handling
                throw ex;
            }
        }

        internal void UpdateEnrollmentWaitingStatus(ClassInvoiceItem classInvoiceItem)
        {
            try
            {
                OdbcCommand cmd = new OdbcCommand(_cmdUpdateEnrollmentWaitingStatus);
                cmd.Parameters.AddWithValue("@IsInWaitingList", classInvoiceItem.IsInWaitingList);
                cmd.Parameters.AddWithValue("@ClassInvoiceItemState", classInvoiceItem.ClassInvoiceItemState);

                cmd.Parameters.AddWithValue("@Id", classInvoiceItem.Id);
                ExecuteNonQuery(cmd);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        internal void UpdateEnrollmentCancelByClient(long classInvoiceItemID, bool isSignupCanceled, DateTime cancelDate)
        {
            try
            {
                OdbcCommand cmd = new OdbcCommand(_cmdUpdateEnrollmentCancelByClient);
                cmd.Parameters.AddWithValue("@IsClassCanceledByClient", isSignupCanceled);
               // cmd.Parameters.AddWithValue("@ClassInvoiceItemState", classState);
                cmd.Parameters.AddWithValue("@DateClassCanceledByClient", cancelDate);

                cmd.Parameters.AddWithValue("@Id", classInvoiceItemID);
                ExecuteNonQuery(cmd);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        internal void UpdateItemStatus(ClassInvoiceItem classInvoiceItem)
        {
            try
            {
                OdbcCommand cmd = new OdbcCommand(_cmdUpdateItemStatus);
                cmd.Parameters.AddWithValue("@ClassInvoiceItemState", classInvoiceItem.ClassInvoiceItemState);

                cmd.Parameters.AddWithValue("@Id", classInvoiceItem.Id);
                ExecuteNonQuery(cmd);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        internal void UpdateClassInvoiceId(ClassInvoiceItem item, long newInvoiceId)
        {
            try
            {
                OdbcCommand cmd = new OdbcCommand(_cmdUpdateClassInvoiceId);
                cmd.Parameters.AddWithValue("@InvoiceId", newInvoiceId);

                cmd.Parameters.AddWithValue("@Id", item.Id);
                ExecuteNonQuery(cmd);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

    }
}
