﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using daoProject.Model;
using daoProject.Infrastructure;
using System.Data.Odbc;
using System.Data;

namespace daoProject.Mapper
{
   public class PaymentGatewayDataMapper : SqlHelper
    {
        private static string _cmdSelectAll = @"SELECT * FROM Whrc_PaymentGateway ";
        internal List<PaymentGatewayDetail> GetAll()
        {
            try
            {
                //Create instance of sqlcommand
                OdbcCommand cmd = new OdbcCommand(_cmdSelectAll);

                DataSet ds = ExecuteQuery(cmd);

                return GetListFromDataSet(ds);
            }
            catch (Exception ex)
            {
                //Attempt to throw the exception for error handling
                throw ex;
            }
        }

        public List<PaymentGatewayDetail> GetListFromDataSet(DataSet ds)
        {
            List<PaymentGatewayDetail> paymentGatewayList = new List<PaymentGatewayDetail>();

            // This Loop reads all Data from DataSet                    
            for (int i = 0; i < ds.Tables["Data"].Rows.Count; i++)
            {
                PaymentGatewayDetail paymentGateway = new PaymentGatewayDetail();

                paymentGateway.Id = Convert.ToInt64(ds.Tables["Data"].Rows[i].ItemArray[0]);
                paymentGateway.GatewayName = Convert.ToString(ds.Tables["Data"].Rows[i].ItemArray[1]);


                paymentGatewayList.Add(paymentGateway);
            }

            return paymentGatewayList;
        }
    }
}
