﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.Odbc;
using System.Collections;
using daoProject.Infrastructure;
using daoProject.Model;
using daoProject.Mapper;

namespace daoProject.Mapper
{
    internal class ProductInvoiceItemDataMapper : SqlHelper
    {
        private static string _cmdInsert = @"INSERT INTO Whrc_ProductInvoiceItem (InvoiceId,ProductId,Quantity,UnitPrice,Price,SubTotal,TaxAmount,IsItemReturned, ReturnDate, RefundAmount, IsRefundProcessed,RefundedDate,ShippingAddressId) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?); ";
        private static string _cmdUpdate = @"UPDATE Whrc_ProductInvoiceItem SET InvoiceId=?,ProductId=?,Quantity=?,UnitPrice=?,Price=?,SubTotal=?,TaxAmount=?,IsItemReturned=?,ReturnDate=?,RefundAmount=?,IsRefundProcessed=?,RefundedDate=?,ShippingAddressId=? WHERE Id=? ";
        private static string _cmdDelete = @"DELETE FROM Whrc_ProductInvoiceItem WHERE Id=? ";
        private static string _cmdSelectAll = @"SELECT * FROM Whrc_ProductInvoiceItem ";
        private static string _cmdSelectById = @"SELECT * FROM Whrc_ProductInvoiceItem WHERE Id=? ";
        private static string _cmdSelectByInvoiceId = @"SELECT * FROM Whrc_ProductInvoiceItem WHERE InvoiceId=? ";
        private static string _cmdDeleteByInvoiceId = @"DELETE FROM Whrc_ProductInvoiceItem WHERE InvoiceId=? ";
        private static string _cmdgetItemAmountPriceByID = @"SELECT Price FROM Whrc_ProductInvoiceItem WHERE Id=? ";
        private static string _cmdReturnItem = @"UPDATE Whrc_ProductInvoiceItem SET IsItemReturned=?,ReturnDate=?,RefundAmount=? WHERE Id=? ";
        private static string _cmdUpdateRefundProcessed = @"UPDATE Whrc_ProductInvoiceItem SET IsRefundProcessed=?,RefundedDate=?,RefundAmount=? WHERE Id=? ";
        private static string _cmdUpdateShipmentAddress = @"UPDATE Whrc_ProductInvoiceItem SET ShippingAddressId=? WHERE Id=? ";
        private static string _cmdUpdateProductInvoiceId =  @"UPDATE Whrc_ProductInvoiceItem SET InvoiceId=? WHERE Id=? ";
        private static string _cmdUpdateProductQuantity = @"UPDATE Whrc_ProductInvoiceItem SET Quantity=? WHERE Id=? ";
        internal void Create(ProductInvoiceItem productInvoiceItem)
        {
            long id = -1;
            try
            {
                OdbcCommand cmd = new OdbcCommand(_cmdInsert);

                cmd.Parameters.AddWithValue("@InvoiceId", productInvoiceItem.InvoiceId);
                cmd.Parameters.AddWithValue("@ProductId", productInvoiceItem.ProductId);
                cmd.Parameters.AddWithValue("@Quantity", productInvoiceItem.Quantity);
                cmd.Parameters.AddWithValue("@UnitPrice", productInvoiceItem.UnitPrice);
                cmd.Parameters.AddWithValue("@Price", productInvoiceItem.Price);
                cmd.Parameters.AddWithValue("@SubTotal", productInvoiceItem.SubTotal);
                cmd.Parameters.AddWithValue("@TaxAmount", productInvoiceItem.TaxAmount);

                cmd.Parameters.AddWithValue("@IsItemReturned", productInvoiceItem.IsItemReturned);
                cmd.Parameters.AddWithValue("@ReturnDate", productInvoiceItem.ReturnDate);
                cmd.Parameters.AddWithValue("@RefundAmount", productInvoiceItem.RefundAmount);
                cmd.Parameters.AddWithValue("@IsRefundProcessed", productInvoiceItem.IsRefundProcessed);
                cmd.Parameters.AddWithValue("@RefundedDate", productInvoiceItem.RefundedDate);
                cmd.Parameters.AddWithValue("@ShippingAddressId", productInvoiceItem.ShippingAddressId);

                //Execute the OdbcCommand
                id = Convert.ToInt64(ExecuteNonQueryAndScalar(cmd));

                productInvoiceItem.Id = id;
            }
            catch (Exception ex)
            {
                //Attempt to throw the exception for error handling
                throw ex;
            }
        }

        internal void Create(ProductInvoiceItem productInvoiceItem, System.Data.Odbc.OdbcConnection conn, System.Data.Odbc.OdbcTransaction tx)
        {
            long id = -1;
            try
            {
                OdbcCommand cmd = new OdbcCommand(_cmdInsert);

                cmd.Parameters.AddWithValue("@InvoiceId", productInvoiceItem.InvoiceId);
                cmd.Parameters.AddWithValue("@ProductId", productInvoiceItem.ProductId);
                cmd.Parameters.AddWithValue("@Quantity", productInvoiceItem.Quantity);
                cmd.Parameters.AddWithValue("@UnitPrice", productInvoiceItem.UnitPrice);
                cmd.Parameters.AddWithValue("@Price", productInvoiceItem.Price);
                cmd.Parameters.AddWithValue("@SubTotal", productInvoiceItem.SubTotal);
                cmd.Parameters.AddWithValue("@TaxAmount", productInvoiceItem.TaxAmount);

                cmd.Parameters.AddWithValue("@IsItemReturned", productInvoiceItem.IsItemReturned);
                cmd.Parameters.AddWithValue("@ReturnDate", productInvoiceItem.ReturnDate);
                cmd.Parameters.AddWithValue("@RefundAmount", productInvoiceItem.RefundAmount);
                cmd.Parameters.AddWithValue("@IsRefundProcessed", productInvoiceItem.IsRefundProcessed);
                cmd.Parameters.AddWithValue("@RefundedDate", productInvoiceItem.RefundedDate);
                cmd.Parameters.AddWithValue("@ShippingAddressId", productInvoiceItem.ShippingAddressId);

                //Execute the OdbcCommand
                id = Convert.ToInt64(ExecuteNonQueryAndScalar(cmd,conn,tx));

                productInvoiceItem.Id = id;
            }
            catch (Exception ex)
            {
                //Attempt to throw the exception for error handling
                throw ex;
            }
        }

        internal void Update(ProductInvoiceItem productInvoiceItem)
        {
            try
            {
                OdbcCommand cmd = new OdbcCommand(_cmdUpdate);

                cmd.Parameters.AddWithValue("@InvoiceId", productInvoiceItem.InvoiceId);
                cmd.Parameters.AddWithValue("@ProductId", productInvoiceItem.ProductId);
                cmd.Parameters.AddWithValue("@Quantity", productInvoiceItem.Quantity);
                cmd.Parameters.AddWithValue("@UnitPrice", productInvoiceItem.UnitPrice);
                cmd.Parameters.AddWithValue("@Price", productInvoiceItem.Price);
                cmd.Parameters.AddWithValue("@SubTotal", productInvoiceItem.SubTotal);
                cmd.Parameters.AddWithValue("@TaxAmount", productInvoiceItem.TaxAmount);

                cmd.Parameters.AddWithValue("@IsItemReturned", productInvoiceItem.IsItemReturned);
                cmd.Parameters.AddWithValue("@ReturnDate", productInvoiceItem.ReturnDate);
                cmd.Parameters.AddWithValue("@RefundAmount", productInvoiceItem.RefundAmount);
                cmd.Parameters.AddWithValue("@IsRefundProcessed", productInvoiceItem.IsRefundProcessed);
                cmd.Parameters.AddWithValue("@RefundedDate", productInvoiceItem.RefundedDate);
                cmd.Parameters.AddWithValue("@ShippingAddressId", productInvoiceItem.ShippingAddressId);

                cmd.Parameters.AddWithValue("@Id", productInvoiceItem.Id);

                //Execute the OdbcCommand
                ExecuteNonQuery(cmd);
            }
            catch (Exception ex)
            {
                //Attempt to throw the exception for error handling
                throw ex;
            }
        }

        internal bool Delete(long id)
        {
            bool success = false;

            try
            {
                OdbcCommand cmd = new OdbcCommand(_cmdDelete);

                cmd.Parameters.AddWithValue("@Id", id);

                ExecuteNonQuery(cmd);

                success = true;
            }
            catch (Exception ex)
            {
                //Attempt to throw the exception for error handling
                throw ex;
            }

            return success;
        }

        public decimal getItemAmountPrice(long Id)
        {
            decimal totalAmountPaid = 0;

            try
            {
                OdbcCommand cmd = new OdbcCommand(_cmdgetItemAmountPriceByID);
                cmd.Parameters.AddWithValue("@Id", Id);
                DataSet ds = ExecuteQuery(cmd);

                if (ds != null)
                {
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        if (ds.Tables[0].Rows[0].ItemArray[0] != DBNull.Value)
                        {
                            totalAmountPaid = Convert.ToDecimal(ds.Tables[0].Rows[0].ItemArray[0]);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return totalAmountPaid;
        }

        private List<ProductInvoiceItem> GetListFromDataSet(DataSet ds)
        {
            List<ProductInvoiceItem> productInvoiceItemList = new List<ProductInvoiceItem>();

            // This Loop reads all Data from DataSet                    
            for (int i = 0; i < ds.Tables["Data"].Rows.Count; i++)
            {
                ProductInvoiceItem productInvoiceItem = new ProductInvoiceItem();

                productInvoiceItem.Id = Convert.ToInt64(ds.Tables["Data"].Rows[i].ItemArray[0]);
                productInvoiceItem.InvoiceId = Convert.ToInt64(ds.Tables["Data"].Rows[i].ItemArray[1]);
                productInvoiceItem.ProductId = Convert.ToInt64(ds.Tables["Data"].Rows[i].ItemArray[2]);
                productInvoiceItem.Quantity = Convert.ToInt32(ds.Tables["Data"].Rows[i].ItemArray[3]);
                productInvoiceItem.UnitPrice = Convert.ToDecimal(ds.Tables["Data"].Rows[i].ItemArray[4]);
                productInvoiceItem.Price = Convert.ToDecimal(ds.Tables["Data"].Rows[i].ItemArray[5]);
                productInvoiceItem.SubTotal = Convert.ToInt64(ds.Tables["Data"].Rows[i].ItemArray[6]);
                productInvoiceItem.TaxAmount = Convert.ToInt64(ds.Tables["Data"].Rows[i].ItemArray[7]);

                if (ds.Tables["Data"].Rows[i].ItemArray[8] != DBNull.Value)
                {
                    productInvoiceItem.IsItemReturned = Convert.ToBoolean(ds.Tables["Data"].Rows[i].ItemArray[8]);
                }
                else
                {
                    productInvoiceItem.IsItemReturned = false;
                }

                if (ds.Tables["Data"].Rows[i].ItemArray[9] != DBNull.Value)
                {
                    productInvoiceItem.ReturnDate =Convert.ToDateTime(Convert.ToString(ds.Tables["Data"].Rows[i].ItemArray[9]));
                }
                else
                {
                    productInvoiceItem.ReturnDate = Convert.ToDateTime("1/1/1900");
                }

                if (ds.Tables["Data"].Rows[i].ItemArray[10] != DBNull.Value)
                {
                    productInvoiceItem.RefundAmount = Convert.ToDecimal(ds.Tables["Data"].Rows[i].ItemArray[10]);
                }
                else
                {
                    productInvoiceItem.RefundAmount = 0;
                }

                if (ds.Tables["Data"].Rows[i].ItemArray[11] != DBNull.Value)
                {
                    productInvoiceItem.IsRefundProcessed = Convert.ToBoolean(ds.Tables["Data"].Rows[i].ItemArray[11]);
                }
                else
                {
                    productInvoiceItem.IsRefundProcessed = false;
                }
                if (ds.Tables["Data"].Rows[i].ItemArray[12] != DBNull.Value)
                {
                    productInvoiceItem.RefundedDate = Convert.ToDateTime(Convert.ToString(ds.Tables["Data"].Rows[i].ItemArray[12]));
                }
                else
                {
                    productInvoiceItem.RefundedDate = Convert.ToDateTime("1/1/1900");
                }
                if (ds.Tables["Data"].Rows[i].ItemArray[13] != DBNull.Value)
                {
                    productInvoiceItem.ShippingAddressId = Convert.ToInt64(ds.Tables["Data"].Rows[i].ItemArray[13]);
                }
                else
                {
                    productInvoiceItem.ShippingAddressId = 0;
                }


                productInvoiceItemList.Add(productInvoiceItem);
            }

            return productInvoiceItemList;
        }

        internal List<ProductInvoiceItem> GetAll()
        {
            try
            {
                //Create instance of sqlcommand
                OdbcCommand cmd = new OdbcCommand(_cmdSelectAll);

                DataSet ds = ExecuteQuery(cmd);

                return GetListFromDataSet(ds);
            }
            catch (Exception ex)
            {
                //Attempt to throw the exception for error handling
                throw ex;
            }
        }

        internal DataTable GetDataTable()
        {
            DataTable table = new DataTable();
            try
            {
                //Create instance of sqlcommand
                OdbcCommand cmd = new OdbcCommand(_cmdSelectAll);

                DataSet ds = ExecuteQuery(cmd);

                table = ds.Tables[0];
            }
            catch (Exception ex)
            {
                //Attempt to throw the exception for error handling
                throw ex;
            }
            return table;

        }

        internal ProductInvoiceItem GetById(long id)
        {
            try
            {
                OdbcCommand cmd = new OdbcCommand(_cmdSelectById);

                cmd.Parameters.AddWithValue("@Id", id);

                DataSet ds = ExecuteQuery(cmd);

                return GetListFromDataSet(ds)[0];
            }
            catch (Exception ex)
            {
                //Attempt to throw the exception for error handling
                throw ex;
            }
        }

        internal List<ProductInvoiceItem> GetListByInvoiceId(long invoiceId)
        {
            try
            {
                //Create instance of sqlcommand
                OdbcCommand cmd = new OdbcCommand(_cmdSelectByInvoiceId);

                cmd.Parameters.AddWithValue("@InvoiceId", invoiceId);

                DataSet ds = ExecuteQuery(cmd);

                return GetListFromDataSet(ds);
            }
            catch (Exception ex)
            {
                //Attempt to throw the exception for error handling
                throw ex;
            }
        }

        internal bool DeleteByInvoiceId(long invoiceId)
        {
            bool success = false;

            try
            {
                OdbcCommand cmd = new OdbcCommand(_cmdDeleteByInvoiceId);

                cmd.Parameters.AddWithValue("@InvoiceId", invoiceId);

                ExecuteNonQuery(cmd);

                success = true;
            }
            catch (Exception ex)
            {
                //Attempt to throw the exception for error handling
                throw ex;
            }

            return success;
        }

        internal void ReturnItem(ProductInvoiceItem productInvoiceItem)
        {
            try
            {
                OdbcCommand cmd = new OdbcCommand(_cmdReturnItem);

                cmd.Parameters.AddWithValue("@IsItemReturned", productInvoiceItem.IsItemReturned);
                cmd.Parameters.AddWithValue("@ReturnDate", productInvoiceItem.ReturnDate);
                cmd.Parameters.AddWithValue("@RefundAmount", productInvoiceItem.RefundAmount);

                cmd.Parameters.AddWithValue("@Id", productInvoiceItem.Id);

                ExecuteNonQuery(cmd);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        internal void UpdateRefundProcessed(ProductInvoiceItem productInvoiceItem)
        {
            try
            {
                OdbcCommand cmd = new OdbcCommand(_cmdUpdateRefundProcessed);
                cmd.Parameters.AddWithValue("@IsRefundProcessed", productInvoiceItem.IsRefundProcessed);
                cmd.Parameters.AddWithValue("@RefundedDate", productInvoiceItem.RefundedDate);
                cmd.Parameters.AddWithValue("@RefundAmount", productInvoiceItem.RefundAmount);

                cmd.Parameters.AddWithValue("@Id", productInvoiceItem.Id);
                ExecuteNonQuery(cmd);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        internal void UpdateShipmentAddress(ProductInvoiceItem productInvoiceItem)
        {
            try
            {
                OdbcCommand cmd = new OdbcCommand(_cmdUpdateShipmentAddress);
                cmd.Parameters.AddWithValue("@ShippingAddressId", productInvoiceItem.ShippingAddressId);

                cmd.Parameters.AddWithValue("@Id", productInvoiceItem.Id);
                ExecuteNonQuery(cmd);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        internal void UpdateProductInvoiceId(ProductInvoiceItem item, long newInvoiceId)
        {
            try
            {
                OdbcCommand cmd = new OdbcCommand(_cmdUpdateProductInvoiceId);
                cmd.Parameters.AddWithValue("@InvoiceId", newInvoiceId);

                cmd.Parameters.AddWithValue("@Id", item.Id);
                ExecuteNonQuery(cmd);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        internal void UpdateProductQuantity(ProductInvoiceItem productInvoiceItem, int newProductQuantity)
        {
            try
            {
                OdbcCommand cmd = new OdbcCommand(_cmdUpdateProductQuantity);
                cmd.Parameters.AddWithValue("@Quantity", newProductQuantity);

                cmd.Parameters.AddWithValue("@Id", productInvoiceItem.Id);
                ExecuteNonQuery(cmd);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
