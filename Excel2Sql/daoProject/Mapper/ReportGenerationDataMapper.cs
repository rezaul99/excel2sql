﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using daoProject.Model;
using daoProject.Infrastructure;
using System.Data;
using ReportLibrary.Model;
using System.Data.Odbc;
namespace daoProject.Mapper
{
    internal class ReportGenerationDataMapper : SqlHelper
    {
        #region ReportRdlcTbl
        private static string _cmdInsertReport = @"INSERT INTO Whrc_ReportDetails (ReportTitle,Description,ReportFileName,ReportFilePath,ReportTableName) VALUES (?,?,?,?,?); ";//use
        private static string _cmdGetAll = @"SELECT * FROM Whrc_ReportDetails ";//use
        private static string _cmdSelectById = @"SELECT * FROM Whrc_ReportDetails WHERE ReportId=? ";//use

        public void AddReportInformaton(ReportDetails RdlcGenerateObj) //use
        {
            long id = -1;
            try
            {
                OdbcCommand cmd = new OdbcCommand(_cmdInsertReport);

                cmd.Parameters.AddWithValue("@ReportTitle", RdlcGenerateObj.ReportTitle);
                cmd.Parameters.AddWithValue("@Description", RdlcGenerateObj.Description);
                cmd.Parameters.AddWithValue("@ReportFileName", RdlcGenerateObj.ReportFileName);
                cmd.Parameters.AddWithValue("@ReportFilePath", RdlcGenerateObj.ReportFilePath);
                cmd.Parameters.AddWithValue("@ReportTableName", RdlcGenerateObj.ReportTableName);
                id = Convert.ToInt64(ExecuteNonQueryAndScalar(cmd));

                RdlcGenerateObj.ReportId = id;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public void AddReportInformaton(ReportDetails RdlcGenerateObj, System.Data.Odbc.OdbcConnection conn, System.Data.Odbc.OdbcTransaction tx) //use
        {
            long id = -1;
            try
            {
                OdbcCommand cmd = new OdbcCommand(_cmdInsertReport);

                cmd.Parameters.AddWithValue("@ReportTitle", RdlcGenerateObj.ReportTitle);
                cmd.Parameters.AddWithValue("@Description", RdlcGenerateObj.Description);
                cmd.Parameters.AddWithValue("@ReportFileName", RdlcGenerateObj.ReportFileName);
                cmd.Parameters.AddWithValue("@ReportFilePath", RdlcGenerateObj.ReportFilePath);
                cmd.Parameters.AddWithValue("@ReportTableName", RdlcGenerateObj.ReportTableName);
                id = Convert.ToInt64(ExecuteNonQueryAndScalar(cmd, conn, tx));

                RdlcGenerateObj.ReportId = id;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public List<ReportDetails> GetAllReports() //use
        {
            try
            {
                OdbcCommand cmd = new OdbcCommand(_cmdGetAll);
                DataSet ds = ExecuteQuery(cmd);
                return GetReportListFromDataSet(ds);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public ReportDetails GetReportById(long RdlcId)  //use
        {
            try
            {
                OdbcCommand cmd = new OdbcCommand(_cmdSelectById);

                cmd.Parameters.AddWithValue("@Id", RdlcId);

                DataSet ds = ExecuteQuery(cmd);

                return GetReportListFromDataSet(ds)[0];
            }
            catch (Exception ex)
            {
                //Attempt to throw the exception for error handling
                throw ex;
            }
        }

        private List<ReportDetails> GetReportListFromDataSet(DataSet ds) //use
        {
            List<ReportDetails> RdlcGenerateClassObjList = new List<ReportDetails>();

            // This Loop reads all Data from DataSet                    
            for (int i = 0; i < ds.Tables["Data"].Rows.Count; i++)
            {
                ReportDetails RdlcGenerateClassObj = new ReportDetails();

                RdlcGenerateClassObj.ReportId = Convert.ToInt64(ds.Tables["Data"].Rows[i].ItemArray[0]);
                RdlcGenerateClassObj.ReportTitle = Convert.ToString(ds.Tables["Data"].Rows[i].ItemArray[1]);
                RdlcGenerateClassObj.Description = Convert.ToString(ds.Tables["Data"].Rows[i].ItemArray[2]);
                RdlcGenerateClassObj.ReportFileName = Convert.ToString(ds.Tables["Data"].Rows[i].ItemArray[3]);

                RdlcGenerateClassObj.ReportFilePath = Convert.ToString(ds.Tables["Data"].Rows[i].ItemArray[4]);
                RdlcGenerateClassObj.ReportTableName = Convert.ToString(ds.Tables["Data"].Rows[i].ItemArray[5]);

                RdlcGenerateClassObjList.Add(RdlcGenerateClassObj);
            }

            return RdlcGenerateClassObjList;
        }
        #endregion

        #region Table
        private static string _cmdInsertTable = @"INSERT INTO Whrc_ReportObjects (Name,DataBaseId) VALUES (?,?); "; //use
        private static string _cmdGetAllByDataBaseID = @"SELECT * FROM Whrc_ReportObjects Where DataBaseId=?";//use

        public void AddTable(ReportClassObjects TableInfoObj)  //use
        {
            long id = -1;
            try
            {
                OdbcCommand cmd = new OdbcCommand(_cmdInsertTable);

                cmd.Parameters.AddWithValue("@Name", TableInfoObj.Name);
                cmd.Parameters.AddWithValue("@DataBaseId", TableInfoObj.DataBaseId);

                id = Convert.ToInt64(ExecuteNonQueryAndScalar(cmd));

                TableInfoObj.Id = id;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public void AddTable(ReportClassObjects TableInfoObj, System.Data.Odbc.OdbcConnection conn, System.Data.Odbc.OdbcTransaction tx)  //use
        {
            long id = -1;
            try
            {
                OdbcCommand cmd = new OdbcCommand(_cmdInsertTable);

                cmd.Parameters.AddWithValue("@Name", TableInfoObj.Name);
                cmd.Parameters.AddWithValue("@DataBaseId", TableInfoObj.DataBaseId);

                id = Convert.ToInt64(ExecuteNonQueryAndScalar(cmd, conn, tx));

                TableInfoObj.Id = id;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public List<ReportClassObjects> GetTableListByDataBaseID(long dataBaseId)//use
        {
            try
            {
                OdbcCommand cmd = new OdbcCommand(_cmdGetAllByDataBaseID);
                cmd.Parameters.AddWithValue("@DataBaseId", dataBaseId);
                DataSet ds = ExecuteQuery(cmd);
                return GetTableListFromDataSet(ds);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        private List<ReportClassObjects> GetTableListFromDataSet(DataSet ds) //use
        {
            List<ReportClassObjects> TableInfoObjList = new List<ReportClassObjects>();

            // This Loop reads all Data from DataSet                    
            for (int i = 0; i < ds.Tables["Data"].Rows.Count; i++)
            {
                ReportClassObjects TableInfoObj = new ReportClassObjects();

                TableInfoObj.Id = Convert.ToInt64(ds.Tables["Data"].Rows[i].ItemArray[0]);
                TableInfoObj.Name = Convert.ToString(ds.Tables["Data"].Rows[i].ItemArray[1]);
                TableInfoObj.DataBaseId = Convert.ToInt64(ds.Tables["Data"].Rows[i].ItemArray[2]);

                TableInfoObjList.Add(TableInfoObj);
            }

            return TableInfoObjList;
        }
        #endregion

        #region Column
        private static string _cmdInsertColumn = @"INSERT INTO Whrc_ReportAttributes (Name,TableId) VALUES (?,?); "; //use
        private static string _cmdGetColumnListByTableId = @"SELECT * FROM Whrc_ReportAttributes WHERE TableId=?"; //use

        public List<ReportAttributes> GetColumnListByTableId(long tableId) //use
        {
            try
            {
                OdbcCommand cmd = new OdbcCommand(_cmdGetColumnListByTableId);
                cmd.Parameters.AddWithValue("@TableId", tableId);
                DataSet ds = ExecuteQuery(cmd);
                return GetColumnListFromDataSet(ds);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private List<ReportAttributes> GetColumnListFromDataSet(DataSet ds) //use
        {
            List<ReportAttributes> ColumnObjList = new List<ReportAttributes>();
            // This Loop reads all Data from DataSet                    
            for (int i = 0; i < ds.Tables["Data"].Rows.Count; i++)
            {
                ReportAttributes ColumnObj = new ReportAttributes();

                ColumnObj.Id = Convert.ToInt64(ds.Tables["Data"].Rows[i].ItemArray[0]);
                ColumnObj.Name = Convert.ToString(ds.Tables["Data"].Rows[i].ItemArray[1]);
                ColumnObj.TableId = Convert.ToInt64(ds.Tables["Data"].Rows[i].ItemArray[2]);

                ColumnObjList.Add(ColumnObj);
            }

            return ColumnObjList;
        }

        public void AddColumn(ReportAttributes ColumnObj) //use
        {
            long id = -1;
            try
            {
                OdbcCommand cmd = new OdbcCommand(_cmdInsertColumn);

                cmd.Parameters.AddWithValue("@Name", ColumnObj.Name);
                cmd.Parameters.AddWithValue("@TableId", ColumnObj.TableId);

                id = Convert.ToInt64(ExecuteNonQueryAndScalar(cmd));

                ColumnObj.Id = id;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public void AddColumn(ReportAttributes ColumnObj, System.Data.Odbc.OdbcConnection conn, System.Data.Odbc.OdbcTransaction tx) //use
        {
            long id = -1;
            try
            {
                OdbcCommand cmd = new OdbcCommand(_cmdInsertColumn);

                cmd.Parameters.AddWithValue("@Name", ColumnObj.Name);
                cmd.Parameters.AddWithValue("@TableId", ColumnObj.TableId);

                id = Convert.ToInt64(ExecuteNonQueryAndScalar(cmd, conn, tx));

                ColumnObj.Id = id;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
        #endregion

        #region object relation

        private static string _cmdInsertReportObjectRelation = @"INSERT INTO Whrc_ReportObjectRelation (Table1Id,ColumnName1,Table2Id,ColumnName2) VALUES (?,?,?,?); "; //use

        private static string _cmdGetReportObjectRelationByObjectsId = @"SELECT * FROM Whrc_ReportObjectRelation Where (Table1Id=? or Table1Id=?) and (Table2Id=? or Table2Id=?)";//use

        private static string _cmdGetAllRelationList = @"SELECT * FROM Whrc_ReportObjectRelation";

        internal void AddReportObjectRelation(ReportObjectRelation reportObjectRelation)
        {
            long id = -1;
            try
            {
                OdbcCommand cmd = new OdbcCommand(_cmdInsertReportObjectRelation);

                cmd.Parameters.AddWithValue("@Table1Id", reportObjectRelation.Table1Id);
                cmd.Parameters.AddWithValue("@ColumnName1", reportObjectRelation.ColumnName1);
                cmd.Parameters.AddWithValue("@Table2Id", reportObjectRelation.Table2Id);
                cmd.Parameters.AddWithValue("@ColumnName2", reportObjectRelation.ColumnName2);
                id = Convert.ToInt64(ExecuteNonQueryAndScalar(cmd));

                reportObjectRelation.Id = id;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        internal void AddReportObjectRelation(ReportObjectRelation reportObjectRelation, System.Data.Odbc.OdbcConnection conn, System.Data.Odbc.OdbcTransaction tx) //use
        {
            long id = -1;
            try
            {
                OdbcCommand cmd = new OdbcCommand(_cmdInsertReportObjectRelation);

                cmd.Parameters.AddWithValue("@Table1Id", reportObjectRelation.Table1Id);
                cmd.Parameters.AddWithValue("@ColumnName1", reportObjectRelation.ColumnName1);
                cmd.Parameters.AddWithValue("@Table2Id", reportObjectRelation.Table2Id);
                cmd.Parameters.AddWithValue("@ColumnName2", reportObjectRelation.ColumnName2);
                id = Convert.ToInt64(ExecuteNonQueryAndScalar(cmd, conn, tx));

                reportObjectRelation.Id = id;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public List<ReportObjectRelation> GetAllReportObjectRelationList()
        {
            try
            {
                OdbcCommand cmd = new OdbcCommand(_cmdGetAllRelationList);
                DataSet ds = ExecuteQuery(cmd);
                return GetReportObjectRelationFromDataSet(ds);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        internal ReportObjectRelation GetReportObjectRelationByObjectsId(string Table1Name, string Table2Name)  //use
        {
            try
            {
                string[] tables = Table1Name.Split('~');

                List<ReportObjectRelation> getList = null;

                foreach (string tbl in tables)
                {
                    OdbcCommand cmd = new OdbcCommand(_cmdGetReportObjectRelationByObjectsId);

                    cmd.Parameters.AddWithValue("@Table1Id", tbl);
                    cmd.Parameters.AddWithValue("@Table1Id", Table2Name);
                    cmd.Parameters.AddWithValue("@Table2Id", tbl);
                    cmd.Parameters.AddWithValue("@Table2Id", Table2Name);

                    DataSet ds = ExecuteQuery(cmd);
                    getList = GetReportObjectRelationFromDataSet(ds);

                    if (getList.Count > 0) break;
                }

                if (getList.Count > 0)
                {
                    return getList[0];
                }
                else
                {
                    ReportObjectRelation reportObjectRelation = null;
                    return reportObjectRelation;
                }
               // return GetReportObjectRelationFromDataSet(ds)[0];
            }
            catch (Exception ex)
            {
                //Attempt to throw the exception for error handling
                throw ex;
            }
        }
      
        private List<ReportObjectRelation> GetReportObjectRelationFromDataSet(DataSet ds) //use
        {
            List<ReportObjectRelation> reportObjectRelationList = new List<ReportObjectRelation>();

            // This Loop reads all Data from DataSet                    
            for (int i = 0; i < ds.Tables["Data"].Rows.Count; i++)
            {
                ReportObjectRelation reportObjectRelation = new ReportObjectRelation();

                reportObjectRelation.Id = Convert.ToInt64(ds.Tables["Data"].Rows[i].ItemArray[0]);
                reportObjectRelation.Table1Id = Convert.ToString(ds.Tables["Data"].Rows[i].ItemArray[1]);
                reportObjectRelation.ColumnName1 = Convert.ToString(ds.Tables["Data"].Rows[i].ItemArray[2]);
               reportObjectRelation.ColumnPK = Convert.ToString(ds.Tables["Data"].Rows[i].ItemArray[3]);
                reportObjectRelation.Table2Id = Convert.ToString(ds.Tables["Data"].Rows[i].ItemArray[4]);
                reportObjectRelation.ColumnName2 = Convert.ToString(ds.Tables["Data"].Rows[i].ItemArray[5]);
               reportObjectRelation.ColumnFK = Convert.ToString(ds.Tables["Data"].Rows[i].ItemArray[6]);
                reportObjectRelationList.Add(reportObjectRelation);
            }

            return reportObjectRelationList;
        }
        #endregion
    }
}
