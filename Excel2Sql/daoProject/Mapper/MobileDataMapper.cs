﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Odbc;
using System.Collections;
using daoProject.Infrastructure;
using daoProject.Model;
using daoProject.Mapper;
using daoProject.Service;
using System.Data;

namespace daoProject.Mapper
{
    public class MobileDataMapper : SqlHelper
    {
        private static string _cmdRentalInvoiceItemById = @"DELETE FROM WHRC_RentalInvoiceItem WHERE InvoiceId=? ";
        private static string _cmdProductInvoiceItemById = @"DELETE FROM WHRC_ProductInvoiceItem WHERE InvoiceId=? ";
        private static string _cmdClassInvoiceItemById = @"DELETE FROM WHRC_ClassInvoiceItem WHERE InvoiceId=? ";
        private static string _cmdMembershipInvoiceItemById = @"DELETE FROM WHRC_MembershipInvoiceItem WHERE InvoiceId=? ";
        private static string _cmdInvoicePaymentItemById = @"DELETE FROM WHRC_InvoicePayment WHERE InvoiceId=? ";
        private static string _cmdInvoiceItemById = @"DELETE FROM WHRC_Invoice WHERE Id=? ";

        internal bool DeleteRentalItemByInvoiceId(long invoiceId)
        {
            bool success = false;

            try
            {
                OdbcCommand cmd = new OdbcCommand(_cmdRentalInvoiceItemById);

                cmd.Parameters.AddWithValue("@InvoiceId", invoiceId);

                ExecuteNonQuery(cmd);

                success = true;
            }
            catch (Exception ex)
            {
                //Attempt to throw the exception for error handling
                throw ex;
            }

            return success;
        }

        internal bool DeleteProductByInvoiceId(long invoiceId)
        {
            bool success = false;

            try
            {
                OdbcCommand cmd = new OdbcCommand(_cmdProductInvoiceItemById);

                cmd.Parameters.AddWithValue("@InvoiceId", invoiceId);

                ExecuteNonQuery(cmd);

                success = true;
            }
            catch (Exception ex)
            {
                //Attempt to throw the exception for error handling
                throw ex;
            }

            return success;
        }

        internal bool DeleteClassByInvoiceId(long invoiceId)
        {
            bool success = false;

            try
            {
                OdbcCommand cmd = new OdbcCommand(_cmdClassInvoiceItemById);

                cmd.Parameters.AddWithValue("@InvoiceId", invoiceId);

                ExecuteNonQuery(cmd);

                success = true;
            }
            catch (Exception ex)
            {
                //Attempt to throw the exception for error handling
                throw ex;
            }

            return success;
        }

        internal bool DeleteMembershipByInvoiceId(long invoiceId)
        {
            bool success = false;

            try
            {
                OdbcCommand cmd = new OdbcCommand(_cmdMembershipInvoiceItemById);

                cmd.Parameters.AddWithValue("@InvoiceId", invoiceId);

                ExecuteNonQuery(cmd);

                success = true;
            }
            catch (Exception ex)
            {
                //Attempt to throw the exception for error handling
                throw ex;
            }

            return success;
        }

        internal bool DeleteInvoicePaymentByInvoiceId(long invoiceId)
        {
            bool success = false;

            try
            {
                OdbcCommand cmd = new OdbcCommand(_cmdInvoicePaymentItemById);

                cmd.Parameters.AddWithValue("@InvoiceId", invoiceId);

                ExecuteNonQuery(cmd);

                success = true;
            }
            catch (Exception ex)
            {
                //Attempt to throw the exception for error handling
                throw ex;
            }

            return success;
        }

        internal bool DeleteInvoiceById(long invoiceId)
        {
            bool success = false;

            try
            {
                OdbcCommand cmd = new OdbcCommand(_cmdInvoiceItemById);

                cmd.Parameters.AddWithValue("@InvoiceId", invoiceId);

                ExecuteNonQuery(cmd);

                success = true;
            }
            catch (Exception ex)
            {
                //Attempt to throw the exception for error handling
                throw ex;
            }

            return success;
        }
    }
}
