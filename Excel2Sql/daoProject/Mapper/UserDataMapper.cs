﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.Odbc;
using System.Collections;
using daoProject.Infrastructure;
using daoProject.Model;
using daoProject.Mapper;

namespace daoProject.Mapper
{
    internal class UserDataMapper : SqlHelper
    {
        private static string _cmdSelectAllUsers = @"SELECT * FROM Whrc_Users ";
        private static string _cmdSelectAllAdminUsers = @"select * from Whrc_Users where UserType= 0 or UserType= 2";
        private static string _cmdSelectAllVisitors = @"select * from Whrc_Users where UserType= 3";
        private static string _cmdSelectUserByEmail = @"SELECT * FROM Whrc_Users WHERE Email=?";
        private static string _cmdSelectUserById = @"SELECT * FROM Whrc_Users WHERE Id=? ";
        private static string _cmdSelectUserByUserRecordId = @"SELECT * FROM Whrc_Users WHERE UserRecordId=? ";
        private static string _cmdSelectMaxRecordId = @"SELECT max(UserRecordId) FROM Whrc_Users";
        private static string _cmdInsertUser = @"INSERT INTO Whrc_Users (UserRecordId,UserType,FirstName, LastName,Email,Password,MiddleName,Address1,AD1City,AD1State,AD1Zip,Address2,AD2City,AD2State,AD2Zip,DOB,DueDate,Insurance,Hospital,HospitalPractice,Notes,AreaCode,Phone,PhoneType,PhoneNotes,Active,AreaCode2,Phone2,PhoneType2,PhoneNotes2,UserStatus,MedicalRecordNo,OBProviderName,ReferringPhysician,IsReceiveEmailNotification) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,'1',?,?,?,?,?,?,?,?,?); ";
        private static string _cmdUpdateUser = @"UPDATE Whrc_Users SET UserRecordId=?, UserType=?, FirstName=?, LastName=?, Email=?, Password=?,MiddleName=?,Address1=?,AD1City=?,AD1State=?,AD1Zip=?,Address2=?,AD2City=?,AD2State=?,AD2Zip=?,DOB=?,DueDate=?,Insurance=?,Hospital=?,HospitalPractice=?,Notes=?,AreaCode=?,Phone=?,PhoneType=?,PhoneNotes=?,Active=?,AreaCode2=?,Phone2=?,PhoneType2=?,PhoneNotes2=?,UserStatus=?,MedicalRecordNo=?,OBProviderName=?,ReferringPhysician=?,IsReceiveEmailNotification=? WHERE Id=?";
        private static string _cmdUpdateUserFromMobile = @"UPDATE Whrc_Users SET UserRecordId=?, FirstName=?, LastName=?, Address1=?, AD1City=?, AD1State=?, AD1Zip=?, Phone=?, PhoneType=? WHERE Id=?";
        private static string _cmdSearchUserByInput = @"select * from Whrc_Users where FirstName like ? or LastName like ? or Email like ? and UserType='1'";
        private static string _cmdSearchVisitorByInput = @"select * from Whrc_Users where FirstName like ? or LastName like ? or Email like ? and UserType='3'";
        private static string _cmdDeleteUserById = "Delete from Whrc_Users where id=? ";
        private static string _cmdUpdateUserStatus = @"UPDATE Whrc_Users SET UserStatus=? WHERE Id=?";
        private static string _cmdInsertUserTest = @"INSERT INTO userTest (FirstName) VALUES (?); ";
        
        internal void Create(User usermodel)
        {
            long id = -1;
            try
            {
                //This property create variable of OdbcCommand
                OdbcCommand comuser = new OdbcCommand(_cmdInsertUser);

                // Sql query for Insert Data into User
                //comuser.CommandText = _cmdInsertUser;
                //OdbcParameter odParam = new OdbcParameter();
                //comuser.Parameters.Add(odParam);

                //Set value for parameter property
                comuser.Parameters.AddWithValue("@UserRecordId", usermodel.UserRecordId);
                comuser.Parameters.AddWithValue("@UserType", usermodel.UserType);
                comuser.Parameters.AddWithValue("@FirstName", usermodel.FirstName);
                comuser.Parameters.AddWithValue("@LastName", usermodel.LastName);
                comuser.Parameters.AddWithValue("@Email", usermodel.Email);
                comuser.Parameters.AddWithValue("@Password", Convert.ToString(usermodel.Password));
                comuser.Parameters.AddWithValue("@MiddleName", usermodel.MiddleName);
                comuser.Parameters.AddWithValue("@Address1", usermodel.Address1);
                comuser.Parameters.AddWithValue("@AD1City", usermodel.Ad1City);
                comuser.Parameters.AddWithValue("@AD1State", usermodel.Ad1State);
                comuser.Parameters.AddWithValue("@AD1Zip", usermodel.Ad1Zip);
                comuser.Parameters.AddWithValue("@Address2", usermodel.Address2);
                comuser.Parameters.AddWithValue("@AD2City", usermodel.Ad2City);
                comuser.Parameters.AddWithValue("@AD2State", usermodel.Ad2State);
                comuser.Parameters.AddWithValue("@AD2Zip", usermodel.Ad2Zip);
                comuser.Parameters.AddWithValue("@DOB", usermodel.DateOfBirth);
                comuser.Parameters.AddWithValue("@DueDate", usermodel.DueDate);
                comuser.Parameters.AddWithValue("@Insurance", usermodel.Insurance);
                comuser.Parameters.AddWithValue("@Hospital", usermodel.Hospital);
                comuser.Parameters.AddWithValue("@HospitalPractice", usermodel.HospitalPractice);
                comuser.Parameters.AddWithValue("@Notes", usermodel.Notes);
                comuser.Parameters.AddWithValue("@AreaCode", usermodel.AreaCode);
                comuser.Parameters.AddWithValue("@Phone", usermodel.phone);
                comuser.Parameters.AddWithValue("@PhoneType", usermodel.phoneType);
                comuser.Parameters.AddWithValue("@PhoneNotes", usermodel.PhoneNotes);
                //comuser.Parameters.AddWithValue("@Active", usermodel.Active);
                comuser.Parameters.AddWithValue("@AreaCode2", usermodel.AreaCode2 == null  ? "": usermodel.AreaCode2);
                comuser.Parameters.AddWithValue("@Phone2", usermodel.phone2 == null ? "" : usermodel.phone2);
                comuser.Parameters.AddWithValue("@PhoneType2", usermodel.phoneType2 == null ? "" : usermodel.phoneType2);
                comuser.Parameters.AddWithValue("@PhoneNotes2", usermodel.PhoneNotes2 == null ? "" : usermodel.PhoneNotes2);
                comuser.Parameters.AddWithValue("@UserStatus", usermodel.UserStatus);
                comuser.Parameters.AddWithValue("@MedicalRecordNo", usermodel.MedicalRecordNo);
                comuser.Parameters.AddWithValue("@OBProviderName", usermodel.OBProviderName);
                comuser.Parameters.AddWithValue("@ReferringPhysician", usermodel.ReferringPhysician);
                comuser.Parameters.AddWithValue("@IsReceiveEmailNotification", usermodel.ReceiveEmailNotification);
                

                //OdbcCommand Type
                comuser.CommandType = CommandType.Text;

                //Execute the OdbcCommand
                id = Convert.ToInt64(ExecuteNonQueryAndScalar(comuser));
                usermodel.Id = id;
            }
            catch (Exception ex)
            {
                //Attempt to throw the exception for error handling
                throw ex;
            }
        }

        internal bool UpdateFromMobile(User userObj)
        {
            bool result = false;
            try
            {
                //This property creates variable of OdbcCommand
                OdbcCommand userUpdateCommand = new OdbcCommand();

                //Sql query for Update Data of User
                userUpdateCommand.CommandText = _cmdUpdateUserFromMobile;

                //Set value for parameter property
                if (userObj.UserRecordId == 0)
                {
                    userUpdateCommand.Parameters.AddWithValue("@UserRecordId", DBNull.Value);
                }
                else
                {
                    userUpdateCommand.Parameters.AddWithValue("@UserRecordId", userObj.UserRecordId);
                }
                
                userUpdateCommand.Parameters.AddWithValue("@FirstName", userObj.FirstName);
                userUpdateCommand.Parameters.AddWithValue("@LastName", userObj.LastName);                             
                userUpdateCommand.Parameters.AddWithValue("@Address1", userObj.Address1);
                userUpdateCommand.Parameters.AddWithValue("@AD1City", userObj.Ad1City);
                userUpdateCommand.Parameters.AddWithValue("@AD1State", userObj.Ad1State);
                userUpdateCommand.Parameters.AddWithValue("@AD1Zip", userObj.Ad1Zip);
                userUpdateCommand.Parameters.AddWithValue("@Phone", userObj.phone);
                userUpdateCommand.Parameters.AddWithValue("@PhoneType", userObj.phoneType);

                userUpdateCommand.Parameters.AddWithValue("@Id", userObj.Id);

                // Pass the parameter to ExecuteNonQuery from execute the sqlcommand
                ExecuteNonQuery(userUpdateCommand);
                result = true;
                return result;
            }
            catch (Exception ex)
            {
                //Attempt to throw the exception for error handling
                return result;
                throw ex;
            }
        }
        internal bool Update(User userObj)
        {
            bool result = false;
            try
            {
                //This property creates variable of OdbcCommand
                OdbcCommand userUpdateCommand = new OdbcCommand();

                //Sql query for Update Data of User
                userUpdateCommand.CommandText = _cmdUpdateUser;

                //Set value for parameter property
                if (userObj.UserRecordId == 0)
                {
                    userUpdateCommand.Parameters.AddWithValue("@UserRecordId", DBNull.Value);
                }
                else
                {
                    userUpdateCommand.Parameters.AddWithValue("@UserRecordId", userObj.UserRecordId);
                }
                userUpdateCommand.Parameters.AddWithValue("@UserType", userObj.UserType);
                userUpdateCommand.Parameters.AddWithValue("@FirstName", userObj.FirstName);
                userUpdateCommand.Parameters.AddWithValue("@LastName", userObj.LastName);
                userUpdateCommand.Parameters.AddWithValue("@Email", userObj.Email);
                userUpdateCommand.Parameters.AddWithValue("@Password", userObj.Password);
                userUpdateCommand.Parameters.AddWithValue("@MiddleName", userObj.MiddleName);
                userUpdateCommand.Parameters.AddWithValue("@Address1", userObj.Address1);
                userUpdateCommand.Parameters.AddWithValue("@AD1City", userObj.Ad1City);
                userUpdateCommand.Parameters.AddWithValue("@AD1State", userObj.Ad1State);
                userUpdateCommand.Parameters.AddWithValue("@AD1Zip", userObj.Ad1Zip);
                userUpdateCommand.Parameters.AddWithValue("@Address2", userObj.Address2);
                userUpdateCommand.Parameters.AddWithValue("@AD2City", userObj.Ad2City);
                userUpdateCommand.Parameters.AddWithValue("@AD2State", userObj.Ad2State);
                userUpdateCommand.Parameters.AddWithValue("@AD2Zip", userObj.Ad2Zip);
                userUpdateCommand.Parameters.AddWithValue("@DOB", userObj.DateOfBirth);
                userUpdateCommand.Parameters.AddWithValue("@DueDate", userObj.DueDate);
                userUpdateCommand.Parameters.AddWithValue("@Insurance", userObj.Insurance);
                userUpdateCommand.Parameters.AddWithValue("@Hospital", userObj.Hospital);
                userUpdateCommand.Parameters.AddWithValue("@HospitalPractice", userObj.HospitalPractice);
                userUpdateCommand.Parameters.AddWithValue("@Notes", userObj.Notes);
                userUpdateCommand.Parameters.AddWithValue("@AreaCode", userObj.AreaCode);
                userUpdateCommand.Parameters.AddWithValue("@Phone", userObj.phone);
                userUpdateCommand.Parameters.AddWithValue("@PhoneType", userObj.phoneType);
                userUpdateCommand.Parameters.AddWithValue("@PhoneNotes", userObj.PhoneNotes);
                userUpdateCommand.Parameters.AddWithValue("@Active", userObj.Active);
                userUpdateCommand.Parameters.AddWithValue("@AreaCode2", userObj.AreaCode2);
                userUpdateCommand.Parameters.AddWithValue("@Phone2", userObj.phone2);
                userUpdateCommand.Parameters.AddWithValue("@PhoneType2", userObj.phoneType2);
                userUpdateCommand.Parameters.AddWithValue("@PhoneNotes2", userObj.PhoneNotes2);
                userUpdateCommand.Parameters.AddWithValue("@UserStatus", userObj.UserStatus);
                userUpdateCommand.Parameters.AddWithValue("@MedicalRecordNo", userObj.MedicalRecordNo);
                userUpdateCommand.Parameters.AddWithValue("@OBProviderName", userObj.OBProviderName);
                userUpdateCommand.Parameters.AddWithValue("@ReferringPhysician", userObj.ReferringPhysician);
                userUpdateCommand.Parameters.AddWithValue("@IsReceiveEmailNotification", userObj.ReceiveEmailNotification);



                userUpdateCommand.Parameters.AddWithValue("@Id", userObj.Id);

                // Pass the parameter to ExecuteNonQuery from execute the sqlcommand
                ExecuteNonQuery(userUpdateCommand);
                result = true;
                return result;
            }
            catch (Exception ex)
            {
                //Attempt to throw the exception for error handling
                return result;
                throw ex;
            }
        }

        internal List<User> GetAll()
        {
            try
            {
                //Create instance of sqlcommand
                OdbcCommand cmd = new OdbcCommand();

                // Set sqlquery stirng on sqlcommand text
                cmd.CommandText = _cmdSelectAllUsers;

                DataSet dsUser = ExecuteQuery(cmd);

                return GetUserListFromDataSet(dsUser);
            }
            catch (Exception ex)
            {
                //Attempt to throw the exception for error handling
                throw ex;
            }
        }

        //#region IDataMapper<User> Members
        internal User GetByEmailId(string  email)//Bypass
        {
            try
            {
                //Create instance of sqlcommand
                User user = null;
                OdbcCommand cmd = new OdbcCommand();

                // Set sqlquery stirng on sqlcommand text
                cmd.CommandText = _cmdSelectUserByEmail;
                cmd.Parameters.AddWithValue("@Email", email);

                DataSet dsUser = ExecuteQuery(cmd);

                // return the HashTable with key ID
                List<User> userList = GetUserListFromDataSet(dsUser);
                if (userList.Count > 0)
                {
                    user = userList[0];
                }
                return user;
            }
            catch (Exception ex)
            {
                //Attempt to throw the exception for error handling
                throw ex;
            }
        }

        internal User GetUserByUserRecordId(long recordId)//Bypass
        {
            try
            {
                //Create instance of sqlcommand
                User user = null;
                OdbcCommand cmd = new OdbcCommand();

                // Set sqlquery stirng on sqlcommand text
                //cmd.CommandText = _cmdSelectUserById;
                cmd.CommandText = _cmdSelectUserByUserRecordId;
                cmd.Parameters.AddWithValue("@Id", recordId);

                DataSet dsUser = ExecuteQuery(cmd);

                // return the HashTable with key ID
                List<User> userList = GetUserListFromDataSet(dsUser);
                if (userList.Count > 0)
                {
                    user = userList[0];
                }
                return user;
            }
            catch (Exception ex)
            {
                //Attempt to throw the exception for error handling
                throw ex;
            }
        }
        internal User GetUserById(long id)//Bypass
        {
            try
            {
                //Create instance of sqlcommand
                User user = null;
                OdbcCommand cmd = new OdbcCommand();

                // Set sqlquery stirng on sqlcommand text
                cmd.CommandText = _cmdSelectUserById;                
                cmd.Parameters.AddWithValue("@Id", id);

                DataSet dsUser = ExecuteQuery(cmd);

                // return the HashTable with key ID
                List<User> userList = GetUserListFromDataSet(dsUser);
                if (userList.Count > 0)
                {
                    user = userList[0];
                }
                return user;
            }
            catch (Exception ex)
            {
                //Attempt to throw the exception for error handling
                throw ex;
            }
        }

        private List<User> GetUserListFromDataSet(DataSet dsUser)
        {
            List<User> userList = new List<User>();

            // This Loop reads all Data from DataSet                    
            for (int i = 0; i < dsUser.Tables["Data"].Rows.Count; i++)
            {
                User user = new User();

                user.Id = Convert.ToInt64(dsUser.Tables["Data"].Rows[i].ItemArray[0]);
                if (dsUser.Tables["Data"].Rows[i].ItemArray[1] != null)

                    user.UserRecordId = dsUser.Tables["Data"].Rows[i].ItemArray[1] != DBNull.Value ? Convert.ToInt64(dsUser.Tables["Data"].Rows[i].ItemArray[1]) : 0;
                
                int userType = Convert.ToInt32(dsUser.Tables["Data"].Rows[i].ItemArray[2]);
                if (userType == 0)
                {
                    user.UserType = UserType.Administrator;
                }
                else if (userType == 1)
                {
                    user.UserType = UserType.Member;
                }
                else if (userType == 2)
                {
                    user.UserType = UserType.SuperAdmin;
                }
                else if (userType == 3)
                {
                    user.UserType = UserType.Visitor;
                }
                user.FirstName = Convert.ToString(dsUser.Tables["Data"].Rows[i].ItemArray[3]);
                user.LastName = Convert.ToString(dsUser.Tables["Data"].Rows[i].ItemArray[4]);
                user.Email = Convert.ToString(dsUser.Tables["Data"].Rows[i].ItemArray[5]);
                user.Password = Convert.ToString(dsUser.Tables["Data"].Rows[i].ItemArray[6]);
                user.MiddleName = Convert.ToString(dsUser.Tables["Data"].Rows[i].ItemArray[7]);
                user.Address1 = Convert.ToString(dsUser.Tables["Data"].Rows[i].ItemArray[8]);
                user.Ad1City = Convert.ToString(dsUser.Tables["Data"].Rows[i].ItemArray[9]);
                user.Ad1State = Convert.ToString(dsUser.Tables["Data"].Rows[i].ItemArray[10]);
                user.Ad1Zip = Convert.ToString(dsUser.Tables["Data"].Rows[i].ItemArray[11]);
                user.Address2 = Convert.ToString(dsUser.Tables["Data"].Rows[i].ItemArray[12]);
                user.Ad2City = Convert.ToString(dsUser.Tables["Data"].Rows[i].ItemArray[13]);
                user.Ad2State = Convert.ToString(dsUser.Tables["Data"].Rows[i].ItemArray[14]);
                user.Ad2Zip = Convert.ToString(dsUser.Tables["Data"].Rows[i].ItemArray[15]);
                string dob = Convert.ToString(dsUser.Tables["Data"].Rows[i].ItemArray[16]);
                
                if (!string.IsNullOrEmpty(dob))
                {
                    user.DateOfBirth = Convert.ToDateTime(dob);
                }
                else
                {
                    user.DateOfBirth = Convert.ToDateTime("1/1/1900");
                }
                
                string dueDate = Convert.ToString(dsUser.Tables["Data"].Rows[i].ItemArray[17]);
                if (!string.IsNullOrEmpty(dueDate))
                {
                    user.DueDate = Convert.ToDateTime(dueDate);
                }
                else
                {
                    user.DueDate = Convert.ToDateTime("1/1/1900");
                }
                user.Insurance = Convert.ToString(dsUser.Tables["Data"].Rows[i].ItemArray[18]);
                user.Hospital = Convert.ToString(dsUser.Tables["Data"].Rows[i].ItemArray[19]);
                user.HospitalPractice = Convert.ToString(dsUser.Tables["Data"].Rows[i].ItemArray[20]);
                user.Notes = Convert.ToString(dsUser.Tables["Data"].Rows[i].ItemArray[21]);
                user.AreaCode = Convert.ToString(dsUser.Tables["Data"].Rows[i].ItemArray[22]);
                user.phone = Convert.ToString(dsUser.Tables["Data"].Rows[i].ItemArray[23]);
                user.phoneType = Convert.ToString(dsUser.Tables["Data"].Rows[i].ItemArray[24]);
                user.PhoneNotes = Convert.ToString(dsUser.Tables["Data"].Rows[i].ItemArray[25]);

                string active = Convert.ToString(dsUser.Tables["Data"].Rows[i].ItemArray[26]);
                if (!string.IsNullOrEmpty(active))
                {
                    user.Active = Convert.ToInt32(active);
                }
                else
                {
                    user.Active = 1;// only for now.. only for tomorrows deployment
                    // have to change to 0 after deployment 17/04/2012
                }

                user.AreaCode2 = Convert.ToString(dsUser.Tables["Data"].Rows[i].ItemArray[27]);
                user.phone2 = Convert.ToString(dsUser.Tables["Data"].Rows[i].ItemArray[28]);
                user.phoneType2 = Convert.ToString(dsUser.Tables["Data"].Rows[i].ItemArray[29]);
                user.PhoneNotes2 = Convert.ToString(dsUser.Tables["Data"].Rows[i].ItemArray[30]);
                string userStatus = Convert.ToString(dsUser.Tables["Data"].Rows[i].ItemArray[31]);
                if (!string.IsNullOrEmpty(userStatus))
                {
                    int userStatusEnum = Convert.ToInt32(userStatus);
                    if (userStatusEnum == 0)
                    {
                        user.UserStatus = UserStatus.Active;
                    }
                    else if (userStatusEnum == 1)
                    {
                        user.UserStatus = UserStatus.Inactive;
                    }
                    else
                    {
                        user.UserStatus = UserStatus.Active;
                    }
                }
                else
                {
                    user.UserStatus = UserStatus.Active;
                }
                user.MedicalRecordNo= Convert.ToString(dsUser.Tables["Data"].Rows[i].ItemArray[32]);
                user.OBProviderName = Convert.ToString(dsUser.Tables["Data"].Rows[i].ItemArray[33]);
                user.ReferringPhysician = Convert.ToString(dsUser.Tables["Data"].Rows[i].ItemArray[34]);
                string emailNotificationStatus = Convert.ToString(dsUser.Tables["Data"].Rows[i].ItemArray[35]);
                if (!string.IsNullOrEmpty(emailNotificationStatus))
                {
                    int emailNotificationStatusInt = Convert.ToInt32(emailNotificationStatus);
                    if (emailNotificationStatusInt == 0)
                    {
                        user.ReceiveEmailNotification = IsReceiveEmailNotification.Yes;
                    }
                    else if (emailNotificationStatusInt == 1)
                    {
                        user.ReceiveEmailNotification = IsReceiveEmailNotification.No;
                    }
                    else
                    {
                        user.ReceiveEmailNotification = IsReceiveEmailNotification.No;
                    }
                }
                else
                {
                    user.ReceiveEmailNotification = IsReceiveEmailNotification.No;
                }
                userList.Add(user);
            }
            return userList;
        }
        //#endregion Private Methods

        public List<User> GetAllAdminAccounts()
        {
            try
            {
                //Create instance of sqlcommand
                OdbcCommand cmd = new OdbcCommand();

                // Set sqlquery stirng on sqlcommand text
                cmd.CommandText = _cmdSelectAllAdminUsers;

                DataSet dsUser = ExecuteQuery(cmd);

                return GetUserListFromDataSet(dsUser);
            }
            catch (Exception ex)
            {
                //Attempt to throw the exception for error handling
                throw ex;
            }
        }

        public long GetMaxRecordId()
        {            
          
            DataSet dsTools = new DataSet();
            long recordId =0;
            try
            {
                //Create instance of sqlcommand
                OdbcCommand cmd = new OdbcCommand();

                // Set sqlquery stirng on sqlcommand text
                cmd.CommandText = _cmdSelectMaxRecordId;
                // Pass the parameter to ExecuteQuery from execute the sqlcommand
                dsTools = ExecuteQuery(cmd);

                if (dsTools.Tables["Data"].Rows.Count == 0)
                {
                    recordId = -1;
                }
                else
                {
                    recordId = Convert.ToInt64(dsTools.Tables["Data"].Rows[0].ItemArray[0]);
                }
                // return the HashTable key Id
              
            }
            catch (Exception ex)
            { 
                
            }
            return recordId;
        }

        public List<User> SearchUserByInput(string searchString)
        {
            try
            {
                OdbcCommand cmd = new OdbcCommand();

                searchString = "%" + searchString + "%";
                cmd.CommandText = _cmdSearchUserByInput;
                cmd.Parameters.AddWithValue("@FirstName", searchString);
                cmd.Parameters.AddWithValue("@LastName", searchString);
                cmd.Parameters.AddWithValue("@Email", searchString);

                DataSet dsUser = ExecuteQuery(cmd);

                return GetUserListFromDataSet(dsUser);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void DeleteUserById(long userId)
        {
            try
            {
                //This property create variable of OdbcCommand
                OdbcCommand comDataTypeDelete = new OdbcCommand();

                // Delete row from ToolsDataStore table by Id
                comDataTypeDelete.CommandText = _cmdDeleteUserById;

                //Set value for the parameter
                comDataTypeDelete.Parameters.AddWithValue("@id", userId);

                //OdbcCommand Type
                comDataTypeDelete.CommandType = CommandType.Text;

                // Pass the parameter to ExecuteNonQuery from execute the sqlcommand
                ExecuteNonQuery(comDataTypeDelete);

            }

            catch (Exception ex)
            {

                //Attempt to throw the exception for error handiling
                throw ex;
            }
        }

        #region Visitor region
        public List<User> GetAllVisitors()
        {
            try
            {
                //Create instance of sqlcommand
                OdbcCommand cmd = new OdbcCommand();

                // Set sqlquery stirng on sqlcommand text
                cmd.CommandText = _cmdSelectAllVisitors;

                DataSet dsUser = ExecuteQuery(cmd);

                return GetUserListFromDataSet(dsUser);
            }
            catch (Exception ex)
            {
                //Attempt to throw the exception for error handling
                throw ex;
            }
        }
        public List<User> SearchVisitorByInput(string searchString)
        {
            try
            {
                OdbcCommand cmd = new OdbcCommand();

                cmd.CommandText = _cmdSearchVisitorByInput;
                searchString = "%" + searchString + "%";
                cmd.Parameters.AddWithValue("@FirstName", searchString);
                cmd.Parameters.AddWithValue("@LastName", searchString);
                cmd.Parameters.AddWithValue("@Email", searchString);

                DataSet dsUser = ExecuteQuery(cmd);

                return GetUserListFromDataSet(dsUser);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion


        internal bool UpdateUserStatus(User userObj)
        {
            bool result = false;
            try
            {
                //This property creates variable of OdbcCommand
                OdbcCommand userUpdateCommand = new OdbcCommand();

                //Sql query for Update Data of User
                userUpdateCommand.CommandText = _cmdUpdateUserStatus;

                //Set value for parameter property

                userUpdateCommand.Parameters.AddWithValue("@UserStatus", userObj.UserStatus);
  

                userUpdateCommand.Parameters.AddWithValue("@Id", userObj.Id);

                // Pass the parameter to ExecuteNonQuery from execute the sqlcommand
                ExecuteNonQuery(userUpdateCommand);
                result = true;
                return result;
            }
            catch (Exception ex)
            {
                //Attempt to throw the exception for error handling
                return result;
                throw ex;
            }
        }

    }
}
