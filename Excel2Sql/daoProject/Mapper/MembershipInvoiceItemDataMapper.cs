﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.Odbc;
using System.Collections;
using daoProject.Infrastructure;
using daoProject.Model;
using daoProject.Mapper;

namespace daoProject.Mapper
{
    internal class MembershipInvoiceItemDataMapper : SqlHelper
    {
        private static string _cmdInsert = @"INSERT INTO Whrc_MembershipInvoiceItem (InvoiceId,MembershipId,Price,Description,DateJoined,DateValidTill,StaffInitial) VALUES (?,?,?,?,?,?,?); ";
        private static string _cmdUpdate = @"UPDATE Whrc_MembershipInvoiceItem SET InvoiceId=?,MembershipId=?,Price=?,Description=?,DateJoined=?,DateValidTill=?,StaffInitial=? WHERE Id=? ";
        private static string _cmdUpdateMemberInvoiceId = @"UPDATE Whrc_MembershipInvoiceItem SET InvoiceId=? WHERE Id=? ";
        private static string _cmdDelete = @"DELETE FROM Whrc_MembershipInvoiceItem WHERE Id=? ";
        private static string _cmdSelectAll = @"SELECT * FROM Whrc_MembershipInvoiceItem ";
        private static string _cmdSelectById = @"SELECT * FROM Whrc_MembershipInvoiceItem WHERE Id=? ";
        private static string _cmdSelectByInvoiceId = @"SELECT * FROM Whrc_MembershipInvoiceItem WHERE InvoiceId=? ";
        private static string _cmdDeleteByInvoiceId = @"DELETE FROM Whrc_MembershipInvoiceItem WHERE InvoiceId=? ";
        private static string _cmdSelectByUserId = @"SELECT Whrc_MembershipInvoiceItem.Id, Whrc_MembershipInvoiceItem.InvoiceId, Whrc_MembershipInvoiceItem.MembershipId,Whrc_MembershipInvoiceItem.Price,Whrc_MembershipInvoiceItem.Description,Whrc_MembershipInvoiceItem.DateJoined,Whrc_MembershipInvoiceItem.DateValidTill, Whrc_MembershipInvoiceItem.StaffInitial FROM Whrc_Invoice INNER JOIN Whrc_MembershipInvoiceItem ON Whrc_Invoice.Id = Whrc_MembershipInvoiceItem.InvoiceId WHERE Whrc_Invoice.ClientId=?";
        private static string _cmdgetItemAmountPriceByID = @"SELECT Price FROM Whrc_MembershipInvoiceItem WHERE Id=? ";
        private static string _cmdGetMembershipByLastValidDate = @"Select Whrc_MembershipInvoiceItem.*,FirstName,LastName  From Whrc_MembershipInvoiceItem,Whrc_Invoice,Whrc_Users Where Whrc_MembershipInvoiceItem.InvoiceID=Whrc_Invoice.ID And Whrc_Invoice.ClientID=Whrc_Users.ID And DateValidTill<?";
        
        
        internal void Create(MembershipInvoiceItem membershipInvoiceItem)
        {
            long id = -1;
            try
            {
                OdbcCommand cmd = new OdbcCommand(_cmdInsert);

                cmd.Parameters.AddWithValue("@InvoiceId", membershipInvoiceItem.InvoiceId);
                cmd.Parameters.AddWithValue("@MembershipId", membershipInvoiceItem.MembershipId);
                cmd.Parameters.AddWithValue("@Price", membershipInvoiceItem.Price);
                cmd.Parameters.AddWithValue("@Description", membershipInvoiceItem.Description);
                cmd.Parameters.AddWithValue("@DateJoined", membershipInvoiceItem.DateJoined);
                cmd.Parameters.AddWithValue("@DateValidTill", membershipInvoiceItem.DateValidTill);
                cmd.Parameters.AddWithValue("@StaffInitial", membershipInvoiceItem.StaffInitial);
                

                //Execute the OdbcCommand
                id = Convert.ToInt64(ExecuteNonQueryAndScalar(cmd));

                membershipInvoiceItem.Id = id;
            }
            catch (Exception ex)
            {
                //Attempt to throw the exception for error handling
                throw ex;
            }
        }

        internal void Create(MembershipInvoiceItem membershipInvoiceItem, System.Data.Odbc.OdbcConnection conn, System.Data.Odbc.OdbcTransaction tx)
        {
            long id = -1;
            try
            {
                OdbcCommand cmd = new OdbcCommand(_cmdInsert);

                cmd.Parameters.AddWithValue("@InvoiceId", membershipInvoiceItem.InvoiceId);
                cmd.Parameters.AddWithValue("@MembershipId", membershipInvoiceItem.MembershipId);
                cmd.Parameters.AddWithValue("@Price", membershipInvoiceItem.Price);
                cmd.Parameters.AddWithValue("@Description", membershipInvoiceItem.Description);
                cmd.Parameters.AddWithValue("@DateJoined", membershipInvoiceItem.DateJoined);
                cmd.Parameters.AddWithValue("@DateValidTill", membershipInvoiceItem.DateValidTill);
                cmd.Parameters.AddWithValue("@StaffInitial", membershipInvoiceItem.StaffInitial);

                //Execute the OdbcCommand
                id = Convert.ToInt64(ExecuteNonQueryAndScalar(cmd,conn,tx));

                membershipInvoiceItem.Id = id;
            }
            catch (Exception ex)
            {
                //Attempt to throw the exception for error handling
                throw ex;
            }
        }

        internal void Update(MembershipInvoiceItem membershipInvoiceItem)
        {
            try
            {
                OdbcCommand cmd = new OdbcCommand(_cmdUpdate);

                cmd.Parameters.AddWithValue("@InvoiceId", membershipInvoiceItem.InvoiceId);
                cmd.Parameters.AddWithValue("@MembershipId", membershipInvoiceItem.MembershipId);
                cmd.Parameters.AddWithValue("@Price", membershipInvoiceItem.Price);
                cmd.Parameters.AddWithValue("@Description", membershipInvoiceItem.Description);
                cmd.Parameters.AddWithValue("@DateJoined", membershipInvoiceItem.DateJoined);
                cmd.Parameters.AddWithValue("@DateValidTill", membershipInvoiceItem.DateValidTill);
                cmd.Parameters.AddWithValue("@StaffInitial", membershipInvoiceItem.StaffInitial);
                cmd.Parameters.AddWithValue("@Id", membershipInvoiceItem.Id);

                //Execute the OdbcCommand
                ExecuteNonQuery(cmd);
            }
            catch (Exception ex)
            {
                //Attempt to throw the exception for error handling
                throw ex;
            }
        }
        internal void UpdateMemberInvoiceId(MembershipInvoiceItem item, long newInvoiceId)
        {
            try
            {
                OdbcCommand cmd = new OdbcCommand(_cmdUpdateMemberInvoiceId);
                cmd.Parameters.AddWithValue("@InvoiceId", newInvoiceId);

                cmd.Parameters.AddWithValue("@Id", item.Id);
                ExecuteNonQuery(cmd);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        internal bool Delete(long id)
        {
            bool success = false;

            try
            {
                OdbcCommand cmd = new OdbcCommand(_cmdDelete);

                cmd.Parameters.AddWithValue("@Id", id);

                ExecuteNonQuery(cmd);

                success = true;
            }
            catch (Exception ex)
            {
                //Attempt to throw the exception for error handling
                throw ex;
            }

            return success;
        }
        public decimal getItemAmountPrice(long Id)
        {
            decimal totalAmountPaid = 0;

            try
            {
                OdbcCommand cmd = new OdbcCommand(_cmdgetItemAmountPriceByID);
                cmd.Parameters.AddWithValue("@Id", Id);
                DataSet ds = ExecuteQuery(cmd);

                if (ds != null)
                {
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        if (ds.Tables[0].Rows[0].ItemArray[0] != DBNull.Value)
                        {
                            totalAmountPaid = Convert.ToDecimal(ds.Tables[0].Rows[0].ItemArray[0]);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return totalAmountPaid;
        }
        private List<MembershipInvoiceItem> GetListFromDataSet(DataSet ds)
        {
            List<MembershipInvoiceItem> membershipInvoiceItemList = new List<MembershipInvoiceItem>();

            // This Loop reads all Data from DataSet                    
            for (int i = 0; i < ds.Tables["Data"].Rows.Count; i++)
            {
                MembershipInvoiceItem membershipInvoiceItem = new MembershipInvoiceItem();

                membershipInvoiceItem.Id = Convert.ToInt64(ds.Tables["Data"].Rows[i].ItemArray[0]);
                membershipInvoiceItem.InvoiceId = Convert.ToInt64(ds.Tables["Data"].Rows[i].ItemArray[1]);
                membershipInvoiceItem.MembershipId = Convert.ToInt64(ds.Tables["Data"].Rows[i].ItemArray[2]);
                membershipInvoiceItem.Price = Convert.ToDecimal(ds.Tables["Data"].Rows[i].ItemArray[3]);
                membershipInvoiceItem.Description = Convert.ToString(ds.Tables["Data"].Rows[i].ItemArray[4]);
                membershipInvoiceItem.DateJoined = Convert.ToDateTime(ds.Tables["Data"].Rows[i].ItemArray[5]);
                membershipInvoiceItem.DateValidTill = Convert.ToDateTime(ds.Tables["Data"].Rows[i].ItemArray[6]);
                membershipInvoiceItem.StaffInitial = Convert.ToInt64(ds.Tables["Data"].Rows[i].ItemArray[7]);

                membershipInvoiceItemList.Add(membershipInvoiceItem);
            }

            return membershipInvoiceItemList;
        }

        internal List<MembershipInvoiceItem> GetAll()
        {
            try
            {
                //Create instance of sqlcommand
                OdbcCommand cmd = new OdbcCommand(_cmdSelectAll);

                DataSet ds = ExecuteQuery(cmd);

                return GetListFromDataSet(ds);
            }
            catch (Exception ex)
            {
                //Attempt to throw the exception for error handling
                throw ex;
            }
        }

        internal DataSet GetMembershipByLastValidDate(DateTime lastValidDate)
        {
            try
            {
                //Create instance of sqlcommand
                OdbcCommand cmd = new OdbcCommand(_cmdGetMembershipByLastValidDate);
                cmd.Parameters.AddWithValue("@lastValidDate", lastValidDate);
                DataSet ds = ExecuteQuery(cmd);

                return ds;
            }
            catch (Exception ex)
            {
                //Attempt to throw the exception for error handling
                throw ex;
            }
        }

        internal MembershipInvoiceItem GetById(long id)
        {
            try
            {
                OdbcCommand cmd = new OdbcCommand(_cmdSelectById);

                cmd.Parameters.AddWithValue("@Id", id);

                DataSet ds = ExecuteQuery(cmd);

                return GetListFromDataSet(ds)[0];
            }
            catch (Exception ex)
            {
                //Attempt to throw the exception for error handling
                throw ex;
            }
        }

        internal List<MembershipInvoiceItem> GetListByInvoiceId(long invoiceId)
        {
            try
            {
                //Create instance of sqlcommand
                OdbcCommand cmd = new OdbcCommand(_cmdSelectByInvoiceId);

                cmd.Parameters.AddWithValue("@InvoiceId", invoiceId);

                DataSet ds = ExecuteQuery(cmd);

                return GetListFromDataSet(ds);
            }
            catch (Exception ex)
            {
                //Attempt to throw the exception for error handling
                throw ex;
            }
        }

        internal bool DeleteByInvoiceId(long invoiceId)
        {
            bool success = false;

            try
            {
                OdbcCommand cmd = new OdbcCommand(_cmdDeleteByInvoiceId);

                cmd.Parameters.AddWithValue("@InvoiceId", invoiceId);

                ExecuteNonQuery(cmd);

                success = true;
            }
            catch (Exception ex)
            {
                //Attempt to throw the exception for error handling
                throw ex;
            }

            return success;
        }

        public List<MembershipInvoiceItem> GetListByUserId(long UserId)
        {
            try
            {
                //Create instance of sqlcommand
                OdbcCommand cmd = new OdbcCommand(_cmdSelectByUserId);

                cmd.Parameters.AddWithValue("@ClientId", UserId);

                DataSet ds = ExecuteQuery(cmd);

                return GetListFromDataSet(ds);
            }
            catch (Exception ex)
            {
                //Attempt to throw the exception for error handling
                throw ex;
            }
        }
    }
}
