﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace daoProject.Infrastructure
{
    public enum InvoiceType
    {
        General,
        WalkIn,
        MomMobile
    }

    public enum PaymentType
    {
        CreditCard,
        Cash,
        Check,
        Medical,
        Mixed,
        None
    }

    public enum OrderState
    {
        InCart,
        Ordered,
        Processed,
        Refunded,
        Paid,
        Reprocessed
    }

    public enum ClassInvoiceItemState
    {
        NA =0,
        Processed =1,
        RequireReprocessing =2,
        //concurrentlyProcessedForOrderedShippmentAddress =3,
        Ordered =3
    }

    public enum UserType
    {
        Administrator,
        Member,
        SuperAdmin,
        Visitor
    }

    public enum InvoiceItemType
    {
        Purchase,
        Rental,
        Class,
        Membership
    }

    public static class ConstantsGlobal
    {
        public const int ConnectionTimeoutBypass = 60;
    }

    public enum DiscountType
    {
        WHRCPackageDiscounts=1,
        WHRCPercentageDiscounts=2,
        WHRCOtherDiscounts=3
    }

    public enum ReturnStatus
    {
        NotReturned =0,
        ReturnedBroken =1,
        ReturnedDirty =2,
        ReturnedGood =3,
        Rented =4
    }

    public static class EnumHelper
    {
        public static string GetRentalStatus(ReturnStatus returnStatus)
        {
            string strToReturn = "";
            switch (returnStatus)
            {
                case ReturnStatus.NotReturned:
                    strToReturn = "";
                    break;
                case ReturnStatus.ReturnedBroken:
                    strToReturn = "Returned Broken";
                    break;
                case ReturnStatus.ReturnedDirty:
                    strToReturn = "Returned Dirty";
                    break;
                case ReturnStatus.ReturnedGood:
                    strToReturn = "Returned Good";
                    break;
                default:
                    break;
            }

            return strToReturn;
        }
    }

    public enum PackageType
    { 
        Product,
        Class
    }

    public enum CreditCardTransactionStatus
    {
        NA,
        Approved,
        Declined,
        Error,
        HeldForReview
    }
    public enum PaymentGatewayType
    {
        OnSite,
        ByAuthorizeDotNet,
        ByPaypal,
        ByGoogleCheckout,
        ByAmazonCheckout
    }

    public enum UserAction
    {
        Add,
        Update,
        Delete
    }
    public enum UserStatus
    { 
        Active,
        Inactive
    }

    public enum PaymentStatus
    { 
        Paid,
        Unpaid
    }

    public enum PackageActive
    {
        Active,
        Inactive
    }
    public enum IsReceiveEmailNotification
    {
        Yes,
        No
    }

    public enum ActivationStatus
    {
        InActive,
        Active
    }
    public enum ClassProperties
    {

    }
    public enum RentalProperties
    {

    }
    public enum MembershipProperties
    {

    }

    public static class ManagingServiceHelper
    {
        public static string GetJijotyToolName(string serviceClassName)
        {
            string jijotyToolName = "";
            switch (serviceClassName)
            {
                case "ClassWHRC":
                    jijotyToolName = "ManageClass";
                    break;
                case "ClassTypeWHRC":
                    jijotyToolName = "ClassType";
                    break;
                case "Instructor":
                    jijotyToolName = "Instructor";
                    break;
                case "ProductCategoryWhrc":
                    jijotyToolName = "ProductCategory";
                    break;
                case "Location":
                    jijotyToolName = "Location";
                    break;
                case "PackageDiscountWHRC":
                    jijotyToolName = "WHRCPackageDiscounts";
                    break;
                case "ManageSalesTaxRateWHRC":
                    jijotyToolName = "ManageSalesTaxRate";
                    break;
                case "MembershipWHRC":
                    jijotyToolName = "WHRCMemberships";
                    break;
                case "OtherDiscountWHRC":
                    jijotyToolName = "WHRCOtherDiscounts";
                    break;
                case "PercentageDiscountWHRC":
                    jijotyToolName = "WHRCPercentageDiscounts";
                    break;
                case "CreditCardPaymentSetup":
                    jijotyToolName = "CreditCardPaymentSetup";
                    break;
                case "ProductItemWHRC":
                    jijotyToolName = "ManageProductItems";
                    break;
                case "ProductTypeWHRC":
                    jijotyToolName = "ProductType";
                    break;
                case "RentalEquipmentWHRC":
                    jijotyToolName = "MgeRentalEquipment";
                    break;
                case "RentalEquipmentDelayFeeWHRC":
                    jijotyToolName = "RentalEquipmentDelayFee";
                    break;
                case "RentalPeriodWHRC":
                    jijotyToolName = "ManageRentalTerm";
                    break;
                case "Section":
                    jijotyToolName = "ManageSections";
                    break;
                case "StudentFlatRateWHRC":
                    jijotyToolName = "DiscountRateperClassforStudent";
                    break;
                case "VendorWHRC":
                    jijotyToolName = "VendorName";
                    break;
                case "ProductInvoiceItem":
                    jijotyToolName = "ProductInvoiceItem";
                    break;
                case "RentalInvoiceItem":
                    jijotyToolName = "RentalInvoiceItem";
                    break;
                case "ClassInvoiceItem":
                    jijotyToolName = "ClassInvoiceItem";
                    break;
                case "MembershipInvoiceItem":
                    jijotyToolName = "MembershipInvoiceItem";
                    break;
                case "Invoice":
                    jijotyToolName = "Invoice";
                    break;
                default:
                    jijotyToolName = "Error (no match found)!";
                    break;
            }

            return jijotyToolName;
        }
    }
}
