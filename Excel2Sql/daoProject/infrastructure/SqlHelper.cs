﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Xml.Linq;
using System.Data.Odbc;
using daoProject.Infrastructure;

namespace daoProject.Infrastructure
{
    public class SqlHelper
    {
        protected DataSet ExecuteQuery(OdbcCommand cmd)
        {
            // Import Global Sqlconnection in local variable
            using (OdbcConnection conn = DBConnection.GetConnection())
            {
                try
                {
                    // Set OdbcConnection
                    cmd.Connection = conn;

                    // Open the Sqlconnection
                    
                    conn.Open();

                    // This property create instance of Dataset
                    DataSet dsExecute = new DataSet();

                    // This property create instance of OdbcDataAdapter
                    OdbcDataAdapter daExecute = new OdbcDataAdapter();

                    //Sqlcommand as a select command
                    daExecute.SelectCommand = cmd;

                    //Accept the change
                    daExecute.AcceptChangesDuringFill = false;

                    //Fill Data from DataAdapter to DataSet
                    daExecute.Fill(dsExecute, "Data");

                    DataTable dt = dsExecute.Tables[0];

                    // Close the OdbcConnection
                    conn.Close();

                    //Retunr the dataset
                    return dsExecute;
                }
                catch (Exception ex)
                {
                    //Handle the exception
                    throw ex;
                }
            }
        }

        protected DataSet ExecuteQuery(OdbcCommand cmd, System.Data.Odbc.OdbcConnection conn, System.Data.Odbc.OdbcTransaction tx)//Bypass
        {
            try
            {
                // Set OdbcConnection
                cmd.Connection = conn;

                cmd.Transaction = tx;

                // This property create instance of Dataset
                DataSet dsExecute = new DataSet();

                // This property create instance of OdbcDataAdapter
                OdbcDataAdapter daExecute = new OdbcDataAdapter();

                //Sqlcommand as a select command
                daExecute.SelectCommand = cmd;

                //Accept the change
                daExecute.AcceptChangesDuringFill = false;

                //Fill Data from DataAdapter to DataSet
                daExecute.Fill(dsExecute, "Data");

                //Retunr the dataset
                return dsExecute;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        protected object ExecuteScalar(OdbcCommand cmd)
        {
            using (OdbcConnection conn = DBConnection.GetConnection())
            {
                OdbcTransaction tx = null;

                // Set the Connection to the new OdbcConnection.
                cmd.Connection = conn;

                // Open the connection and execute the transaction.
                try
                {
                    conn.Open();
                    // Start a local transaction
                    tx = conn.BeginTransaction();

                    // Assign transaction object for a pending local transaction.
                    //cmd.Connection = connection;
                    cmd.Transaction = tx;

                    // Execute the commands.
                    object obj = cmd.ExecuteScalar();

                    // Commit the transaction.
                    tx.Commit();

                    return obj;
                }
                catch (Exception ex)
                {
                    try
                    {
                        // Attempt to roll back the transaction.
                        tx.Rollback();

                        throw ex;
                    }
                    catch (Exception ex1)
                    {
                        // Do nothing here; transaction is not active.
                        throw ex1;
                    }
                }
                // The connection is automatically closed when the
                // code exits the using block.
            }
        }

        protected long ExecuteNonQueryAndScalar(OdbcCommand cmd)
        {
            using (OdbcConnection conn = DBConnection.GetConnection())
            {
                OdbcTransaction tx = null;
                
                // Set the Connection to the new OdbcConnection.
                cmd.Connection = conn;

                OdbcCommand cmd1 = new OdbcCommand("SELECT @@IDENTITY; ");
                cmd1.Connection = conn;

                // Open the connection and execute the transaction.
                try
                {
                    conn.Open();
                    // Start a local transaction
                    tx = conn.BeginTransaction();

                    // Assign transaction object for a pending local transaction.
                    //cmd.Connection = connection;
                    cmd.Transaction = tx;
                    cmd.ExecuteNonQuery();
                    
                    cmd1.Transaction = tx;
                    long id = Convert.ToInt64(cmd1.ExecuteScalar());

                    // Commit the transaction.
                    tx.Commit();

                    return id;
                }
                catch (Exception ex)
                {
                    try
                    {
                        // Attempt to roll back the transaction.
                        tx.Rollback();

                        throw ex;
                    }
                    catch (Exception ex1)
                    {
                        // Do nothing here; transaction is not active.
                        throw ex1;
                    }
                }
                // The connection is automatically closed when the
                // code exits the using block.
            }
        }

        protected long ExecuteNonQueryAndScalar(OdbcCommand cmd, System.Data.Odbc.OdbcConnection conn, System.Data.Odbc.OdbcTransaction tx)//Bypass
        {
            // Set the Connection to the new OdbcConnection.
            cmd.Connection = conn;

            OdbcCommand cmd1 = new OdbcCommand("SELECT @@IDENTITY; ");
            cmd1.Connection = conn;
            
            // Open the connection and execute the transaction.
            try
            {
                cmd.Transaction = tx;
                cmd.ExecuteNonQuery();

                cmd1.Transaction = tx;
                long id = Convert.ToInt64(cmd1.ExecuteScalar());

                return id;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        protected void ExecuteNonQuery(OdbcCommand cmd)
        {
            using (OdbcConnection conn = DBConnection.GetConnection())
            {
                OdbcTransaction tx = null;

                // Set the Connection to the new OdbcConnection.
                cmd.Connection = conn;

                // Open the connection and execute the transaction.
                try
                {
                    conn.Open();
                    // Start a local transaction
                    tx = conn.BeginTransaction();

                    // Assign transaction object for a pending local transaction.
                    //cmd.Connection = connection;
                    cmd.Transaction = tx;

                    // Execute the commands.
                    cmd.ExecuteNonQuery();

                    // Commit the transaction.
                    tx.Commit();
                }
                catch (Exception ex)
                {
                    try
                    {
                        // Attempt to roll back the transaction.
                        tx.Rollback();

                        throw ex;
                    }
                    catch (Exception ex1)
                    {
                        // Do nothing here; transaction is not active.
                        throw ex1;
                    }
                }
                // The connection is automatically closed when the
                // code exits the using block.
            }
        }

        protected void ExecuteNonQuery(OdbcCommand cmd, System.Data.Odbc.OdbcConnection conn, System.Data.Odbc.OdbcTransaction tx)//Bypass
        {
            // Set the Connection to the new OdbcConnection.
            cmd.Connection = conn;

            // Open the connection and execute the transaction.
            try
            {
                // Assign transaction object for a pending local transaction.
                //cmd.Connection = connection;
                cmd.Transaction = tx;

                // Execute the commands.
                cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
