﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Xml.Linq;
using System.Data.Odbc;

namespace daoProject.Infrastructure
{
    public class DBConnection
    {
        private static string connectionString = string.Empty;

        //public static int connCount = 0;
        public static OdbcConnection GetConnection()
        {
            //new daoProject.Utilities.MailHelper().Send("anis@jijoty.org", "Test Subject", "Test Body");
            //string JijotyConnectionString = "Dsn=ycimssql;Uid=sa;Pwd=123456";
            connectionString = ConfigurationManager.ConnectionStrings["JijotyConnectionString"].ToString();
            OdbcConnection conn = new OdbcConnection(connectionString);

            //using (System.IO.StreamWriter writer = new System.IO.StreamWriter(new System.IO.FileStream(@"C:\JijotyLog.txt", System.IO.FileMode.Append)))
            //{
            //    writer.WriteLine("Conn Count" + ": " + ++connCount);
            //}

            return conn;
        }

        /// <summary>
        /// close the OdbcConnection
        /// Throws no exceptions 
        /// </summary>
        /// <param name="conn">connection that will be closed</param>
        public static void Close(OdbcConnection conn)
        {
            try
            {
                conn.Close();
            }
            catch (Exception e)
            {
                //Do not do anything, we are closing the connection anyway       
                throw e;
            }
        }
    }
}
