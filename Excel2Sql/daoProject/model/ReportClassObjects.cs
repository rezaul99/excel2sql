﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace daoProject.Model
{
    public class ReportClassObjects
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public long DataBaseId { get; set; }
    }
}
