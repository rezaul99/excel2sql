﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;
using daoProject.Infrastructure;
using daoProject.Service;

namespace daoProject.Model
{
    public class ClassTypeWHRC
    {
        public ClassTypeWHRC()
        {
            ClassTypeName = String.Empty;
        }

        public long RecordID { get; set; }
        public string ClassTypeName { get; set; }
        public long UserID { get; set; }
    }
}
