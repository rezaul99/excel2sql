﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;
using daoProject.Infrastructure;
using daoProject.Service;

namespace daoProject.Model
{
    public class RentalPeriodWHRC
    {
        public RentalPeriodWHRC()
        {
        }

        public long RecordID { get; set; }
        public int LengthofRentalinDays { get; set; }
        public string TotalDescriptionForDDL { get; set; }
        public string ModelName { get; set; }
        public decimal Fee { get; set; }
        public decimal Tax { get; set; }
        public decimal Total { get; set; }
        public long UserID { get; set; }
    }
}
