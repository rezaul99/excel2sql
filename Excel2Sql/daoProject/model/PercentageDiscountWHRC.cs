﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;
using daoProject.Infrastructure;
using daoProject.Service;

namespace daoProject.Model
{
    public class PercentageDiscountWHRC
    {
        public PercentageDiscountWHRC()
        {
            DisountType = String.Empty;
        }

        public long RecordID { get; set; }
        public string DisountType { get; set; }
        public decimal Percentage { get; set; }
        public long UserID { get; set; }
    }
}
