﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace daoProject.Model
{
    public class RptClassMedical
    {
        public RptClassMedical()
        {
        }
        public string ClassType { get; set; }
        public string StartDate { get; set; }
        public string Section { get; set; }
        public string Title { get; set; }
        
        public int MaxSize { get; set; }
        public int Enrollment { get; set; }
        public int MediCalSignups { get; set; }
    }
}
