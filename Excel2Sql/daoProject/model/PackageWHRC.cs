﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using daoProject.Infrastructure;

namespace daoProject.Model
{
    public class PackageWHRC
    {
        public PackageWHRC()
        {
            PackageItemsWHRC = new List<PackageItemWHRC>();
            CreateDate = Convert.ToDateTime("1/1/1900");
            UpdateDate = Convert.ToDateTime("1/1/1900");
        }

        public long ID { get; set; }
        public string PackageName { get; set; }        
        public string PackageDescription { get; set; }
        public PackageType PackageType { get; set; }
        public decimal DiscountAmount { get; set; }
        public bool Active { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime UpdateDate { get; set; }

       
        
        public List<PackageItemWHRC> PackageItemsWHRC { get; set; }
    }
}
