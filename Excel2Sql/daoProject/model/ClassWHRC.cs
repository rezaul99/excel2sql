﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;
using daoProject.Infrastructure;
using daoProject.Service;

namespace daoProject.Model
{
    [Serializable]
    public class ClassWHRC
    {
        public ClassWHRC()
        {
        }

        public long RecordID { get; set; }
        public string ClassType { get; set; }
        public string Title { get; set; }
        public long Year { get; set; }
        public string SectionCodePrefix { get; set; }
        public decimal Fee { get; set; }
        public string Description { get; set; }
        public bool PublishThisClass { get; set; }
        public long UserID { get; set; }
        public string ClassDescription { get; set; }
    }
}
