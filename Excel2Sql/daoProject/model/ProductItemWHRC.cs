﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace daoProject.Model
{
    [Serializable]
    public class ProductItemWHRC
    {
        public long Id { get; set; }
        public long RecordId { get; set; }
        public string ProductType { get; set; }
        public string ProductCategory { get; set; }
        public string ItemName { get; set; }
        public Decimal SalePrice { get; set; }
        public bool Taxable { get; set; }
        public string QuantityInStock { get; set; }
        public string ReorderLevel { get; set; }
        public string VendorName { get; set; }
        public string VendorItemNumber { get; set; }
        public Decimal VendorPrice { get; set; }
        public DateTime LastModifiedDate { get; set; }
        public DateTime CreateDate { get; set; }
        public string StaffInitials { get; set; }
        public bool EligibleForShipping { get; set; }
        public string ImgLink { get; set; }
        public string thumbnailUrl { get; set; }


        public string TaxableStringYN
        {
            get
            {
                return Taxable ? "Yes" : "No";
            }
        }
        public int QuantitySold { get; set; }
        public int QuantityInStockInt { get; set; }
        public int ReorderLevelInt { get; set; }
        public bool StatusOfProduct { get; set; }
        public long UserID { get; set; }

        //Helper
        public string ProductNamePrice
        {
            get {
                return ItemName + "  -  $" + SalePrice;
            }
        }
        //
        public string Description { get; set; }
    }
}
