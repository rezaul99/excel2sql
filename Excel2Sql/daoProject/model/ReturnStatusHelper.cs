﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;
using daoProject.Infrastructure;
using daoProject.Service;

namespace daoProject.Model
{
    public class ReturnStatusHelper
    {
        public ReturnStatusHelper()
        {
        }

        public int ReturnStatusID { get; set; }
        public string ReturnStatusDescription { get; set; }
    }
}
