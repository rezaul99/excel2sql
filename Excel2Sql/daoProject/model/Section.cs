﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;
using daoProject.Infrastructure;
using daoProject.Service;

namespace daoProject.Model
{
    [Serializable]
    public class Section
    {
        public Section()
        {
            StartDate = Convert.ToDateTime("1/1/1900");
        }

        public long RecordID { get; set; }
        public string ClassTitle { get; set; }
        public string SectionCode { get; set; }
        public DateTime StartDate { get; set; }
        public long NumberofMeetings { get; set; }
        public string StartTime { get; set; }
        public string EndTime { get; set; }
        public int MaximumSize { get; set; }
        public string Instructor { get; set; }
        public string Locations { get; set; }

        public string StartDateString { get; set; }
        public long Available { get; set; }

        public string DayOfWeek { get; set; }
        public bool PublishThisSection { get; set; }
        public long UserID { get; set; }

        //public string InstructorDescription { get; set; }
    }
}
