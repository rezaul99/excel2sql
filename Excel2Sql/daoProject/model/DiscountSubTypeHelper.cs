﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;
using daoProject.Infrastructure;
using daoProject.Service;

namespace daoProject.Model
{
    public class DiscountSubTypeHelper
    {
        public DiscountSubTypeHelper()
        {
        }

        public long RecordID { get; set; }
        public string Description { get; set; }
        
        public decimal Amount { get; set; }
        public bool IsPercentageMode { get; set; }
    }
}
