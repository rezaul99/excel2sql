﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;
using daoProject.Infrastructure;
using daoProject.Service;

namespace daoProject.Model
{
    public class Invoice
    {
        public Invoice()
        {
            PurchaseDate = Convert.ToDateTime("1/1/1900");
            AddedDate = Convert.ToDateTime("1/1/1900");
            ModifiedDate = Convert.ToDateTime("1/1/1900");
            PaymentDate = Convert.ToDateTime("1/1/1900");
            ClientRequestedDiscountMsg = string.Empty;

            ClassInvoiceItems = new List<ClassInvoiceItem>();
            ProductInvoiceItems = new List<ProductInvoiceItem>();
            RentalInvoiceItems = new List<RentalInvoiceItem>();
            MembershipInvoiceItems = new List<MembershipInvoiceItem>();
            InvoicePackages = new List<InvoicePackage>();
            PaymentNotes = string.Empty;
            CreditCardTransactionStatus = CreditCardTransactionStatus.NA;
            CreditCardTransactionID = string.Empty;
            CreditCardTransactionDetails = string.Empty;
        }

        public long Id { get; set; }
        public long ClientId { get; set; }
        public InvoiceType InvoiceType { get; set; }
        public long ShippingAddressId { get; set; }
        public DateTime PurchaseDate { get; set; }
        public Decimal TotalAmount { get; set; }
        public Decimal Discount { get; set; }
        public Decimal NetPayable { get; set; }
        public PaymentType PaymentMode { get; set; }
        public Decimal PaidAmount { get; set; }
        public DateTime PaymentDate { get; set; }
        public long AddedBy { get; set; }
        public DateTime AddedDate { get; set; }
        public long ModifiedBy { get; set; }
        public DateTime ModifiedDate { get; set; }
        public OrderState OrderStatus { get; set; }
        public string ClientRequestedDiscountMsg { get; set; }
        public bool IsShipmentToOneAddress { get; set; }

        public long DiscountTypeId { get; set; }
        public long DiscountSubTypeId { get; set; }
        public Decimal TotalRefundAmount { get; set; }
        public long StaffInitial { get; set; }
        public string PaymentNotes { get; set; }
        public Decimal PackageDiscount { get; set; }
        public CreditCardTransactionStatus CreditCardTransactionStatus { get; set; }
        public string CreditCardTransactionID { get; set; }
        public string CreditCardTransactionDetails { get; set; }
        public string MedicalNumberOrNationalId { get; set; }
        public string CheckNumber { get; set; }

        public List<ClassInvoiceItem> ClassInvoiceItems { get; set; }
        public List<ProductInvoiceItem> ProductInvoiceItems { get; set; }
        public List<RentalInvoiceItem> RentalInvoiceItems { get; set; }
        public List<MembershipInvoiceItem> MembershipInvoiceItems { get; set; }
        public List<InvoicePackage> InvoicePackages { get; set; }
        
    }
}
