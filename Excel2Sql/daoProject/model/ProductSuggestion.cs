﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace daoProject.Model
{
    class ProductSuggestion
    {
        public long ID { get; set; }
        public long ProductID { get; set; }
        public long SuggestionID { get; set; }
    }
}
