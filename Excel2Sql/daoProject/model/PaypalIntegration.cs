﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using daoProject.Service;

namespace daoProject.Model
{
    public class PaypalIntegration 
    {
        private string url;
        private string securityInfo;

        public string baseURL
        {
            get
            {
                url = HttpContext.Current.Request.Url.Scheme + "://" + HttpContext.Current.Request.Url.Authority + HttpContext.Current.Request.ApplicationPath;
                return url;
            }

        }

        public string returnURL
        {
            get
            {
                url = baseURL + "/OrderConfirm.aspx";
                return url;
            }
           
        }
        public string instantPaymentNotificationURL
        {
            get
            {
                url = baseURL + "/OrderConfirm.aspx";
                return url;
            }

        }
        public string cancelURL
        {
            get
            {
                url = baseURL + "/InvoicePayment.aspx";
                return url;
            }

        }

        public string paypallURL
        {
            get
            {
                url = "https://www.sandbox.paypal.com/cgi-bin/webscr";
                return url;
            }

        }
        public string AmazonPaymentUrl
        {
            get
            {
                url = " https://authorize.payments-sandbox.amazon.com/pba/paypipeline"; //without -sandbox when you will start using real accounts (remember, sandbox accounts are for testing purpose only!).
                return url;
            }

        }
        public string AccessKeyID
        {
            get
            {
                securityInfo = "6261-8562-6839";
                return securityInfo;
            }

        }
        public string SecretKey
        {
            get
            {
                securityInfo = "PaZr+zYuaDmZJF0YMwesLl1Cc6NV1SxdtXbjECCX";
                return securityInfo;
            }

        }
        public string AccountID
        {
            get
            {
                securityInfo = "AKIAJDZIXMGB4HT67PFA";
                return securityInfo;
            }

        }
        public CreditCardPaymentSetup creditCardPaymentSetup
        {
            get
            {
               CreditCardPaymentSetup _creditCardPaymentSetup = null;
               List<CreditCardPaymentSetup> creditCardPaymentSetupList = new CreditCardPaymentSetupServiceImpl().GetAllCreditCardPaymentSetup();
               if (creditCardPaymentSetupList.Count > 0)
               {
                   _creditCardPaymentSetup = creditCardPaymentSetupList[0];
               }
               return _creditCardPaymentSetup;
            }
        }
        public CreditCardPaymentSetup creditCardPaymentSetupForAmazon
        {
            get
            {
                CreditCardPaymentSetup _creditCardPaymentSetup = null;
                PaymentGatewayDetail paymentGatewayDetail = null;
                List<daoProject.Model.PaymentGatewayDetail> paymentGatewayList = new PaymentGatewayServiceImpl().GetAll();
                if (paymentGatewayList.Count > 0)
                {
                    paymentGatewayDetail = paymentGatewayList.FirstOrDefault(x => x.GatewayName == "AmazonCheckout");
                }
                List<CreditCardPaymentSetup> creditCardPaymentSetupList = new CreditCardPaymentSetupServiceImpl().GetAllCreditCardPaymentSetup();
                if (creditCardPaymentSetupList.Count > 0)
                {
                    if (paymentGatewayDetail != null)
                    {
                        _creditCardPaymentSetup = creditCardPaymentSetupList.FirstOrDefault(x => x.GatewayID == paymentGatewayDetail.Id);
                    }
                }
                return _creditCardPaymentSetup;
            }
        }
        public CreditCardPaymentSetup creditCardPaymentSetupForPaypal
        {
            get
            {
                CreditCardPaymentSetup _creditCardPaymentSetup = null;
                PaymentGatewayDetail paymentGatewayDetail = null;
                List<daoProject.Model.PaymentGatewayDetail> paymentGatewayList = new PaymentGatewayServiceImpl().GetAll();
                if (paymentGatewayList.Count > 0)
                {
                    paymentGatewayDetail = paymentGatewayList.FirstOrDefault(x => x.GatewayName == "Paypal");
                }
                List<CreditCardPaymentSetup> creditCardPaymentSetupList = new CreditCardPaymentSetupServiceImpl().GetAllCreditCardPaymentSetup();
                if (creditCardPaymentSetupList.Count > 0)
                {
                    if (paymentGatewayDetail != null)
                    {
                        _creditCardPaymentSetup = creditCardPaymentSetupList.FirstOrDefault(x => x.GatewayID == paymentGatewayDetail.Id);
                    }
                }
                return _creditCardPaymentSetup;
            }
        }
        public CreditCardPaymentSetup creditCardPaymentSetupForGoogleCheckout
        {
            get
            {
                CreditCardPaymentSetup _creditCardPaymentSetup = null;
                PaymentGatewayDetail paymentGatewayDetail = null;
                List<daoProject.Model.PaymentGatewayDetail> paymentGatewayList = new PaymentGatewayServiceImpl().GetAll();
                if (paymentGatewayList.Count > 0)
                {
                    paymentGatewayDetail = paymentGatewayList.FirstOrDefault(x => x.GatewayName == "GoogleCheckout");
                }
                List<CreditCardPaymentSetup> creditCardPaymentSetupList = new CreditCardPaymentSetupServiceImpl().GetAllCreditCardPaymentSetup();
                if (creditCardPaymentSetupList.Count > 0)
                {
                    if (paymentGatewayDetail != null)
                    {
                        _creditCardPaymentSetup = creditCardPaymentSetupList.FirstOrDefault(x => x.GatewayID == paymentGatewayDetail.Id);
                    }
                }
                return _creditCardPaymentSetup;
            }
        }
        public CreditCardPaymentSetup creditCardPaymentSetupForAuthorizeDotNet
        {
            get
            {
                CreditCardPaymentSetup _creditCardPaymentSetup = null;
                PaymentGatewayDetail paymentGatewayDetail = null;
                List<daoProject.Model.PaymentGatewayDetail> paymentGatewayList = new PaymentGatewayServiceImpl().GetAll();
                if (paymentGatewayList.Count > 0)
                {
                    paymentGatewayDetail = paymentGatewayList.FirstOrDefault(x => x.GatewayName == "AuthorizeDotNet");
                }
                List<CreditCardPaymentSetup> creditCardPaymentSetupList = new CreditCardPaymentSetupServiceImpl().GetAllCreditCardPaymentSetup();
                if (creditCardPaymentSetupList.Count > 0)
                {
                    if (paymentGatewayDetail != null)
                    {
                        _creditCardPaymentSetup = creditCardPaymentSetupList.FirstOrDefault(x => x.GatewayID == paymentGatewayDetail.Id);
                    }
                }
                return _creditCardPaymentSetup;
            }
        }
    }

    public class CopyOfPaypalIntegration
    {
        private string url;
        private string securityInfo;

        public string baseURL
        {
            get
            {
                url = HttpContext.Current.Request.Url.Scheme + "://" + HttpContext.Current.Request.Url.Authority + HttpContext.Current.Request.ApplicationPath;
                return url;
            }

        }

        public string returnURL
        {
            get
            {
                url = baseURL + "/OrderConfirm.aspx";
                return url;
            }

        }
        public string instantPaymentNotificationURL
        {
            get
            {
                url = baseURL + "/OrderConfirm.aspx";
                return url;
            }

        }
        public string cancelURL
        {
            get
            {
                url = baseURL + "/InvoicePayment.aspx";
                return url;
            }

        }

        public string paypallURL
        {
            get
            {
                url = "https://www.sandbox.paypal.com/cgi-bin/webscr";
                return url;
            }

        }
        public string AmazonPaymentUrl
        {
            get
            {
                url = " https://authorize.payments-sandbox.amazon.com/pba/paypipeline"; //without -sandbox when you will start using real accounts (remember, sandbox accounts are for testing purpose only!).
                return url;
            }

        }
        public string AccessKeyID
        {
            get
            {
                securityInfo = "6261-8562-6839";
                return securityInfo;
            }

        }
        public string SecretKey
        {
            get
            {
                securityInfo = "PaZr+zYuaDmZJF0YMwesLl1Cc6NV1SxdtXbjECCX";
                return securityInfo;
            }

        }
        public string AccountID
        {
            get
            {
                securityInfo = "AKIAJDZIXMGB4HT67PFA";
                return securityInfo;
            }

        }
        public CreditCardPaymentSetup creditCardPaymentSetup
        {
            get
            {
                CreditCardPaymentSetup _creditCardPaymentSetup = null;
                List<CreditCardPaymentSetup> creditCardPaymentSetupList = new CreditCardPaymentSetupServiceImpl().GetAllCreditCardPaymentSetup();
                if (creditCardPaymentSetupList.Count > 0)
                {
                    _creditCardPaymentSetup = creditCardPaymentSetupList[0];
                }
                return _creditCardPaymentSetup;
            }
        }

    }
}
