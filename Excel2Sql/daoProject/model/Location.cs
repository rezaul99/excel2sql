﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;
using daoProject.Infrastructure;
using daoProject.Service;

namespace daoProject.Model
{
    public class Location
    {
        public Location()
        {
            LocationDetails = String.Empty;
        }

        public long RecordID { get; set; }
        public string LocationDetails { get; set; }
        public long UserID { get; set; }
    }
}
