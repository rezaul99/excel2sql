﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;
using daoProject.Infrastructure;
using daoProject.Service;

namespace daoProject.Model
{
    [Serializable]
    public class LogData
    {
        public LogData()
        {
            ActionName = UserAction.Add;
            ActionDate = Convert.ToDateTime("1/1/1900");
        }

        public long Id { get; set; }
        public long UserID { get; set; }
        public string ObjectName { get; set; }
        public string FieldName { get; set; }
        public string ControlValue { get; set; }
        public DateTime ActionDate { get; set; }
        public UserAction ActionName { get; set; }
        public long RecordId { get; set; }
    }
}
