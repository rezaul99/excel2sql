﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;
using daoProject.Infrastructure;
using daoProject.Service;

namespace daoProject.Model
{
    public class RentalEquipmentDelayFeeWHRC
    {
        public RentalEquipmentDelayFeeWHRC()
        {
        }

        public long RecordID { get; set; }
        public decimal DelayFee { get; set; }
        public long UserID { get; set; }
    }
}
