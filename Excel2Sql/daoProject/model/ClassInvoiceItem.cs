﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;
using daoProject.Infrastructure;

namespace daoProject.Model
{
    public class ClassInvoiceItem
    {
        public ClassInvoiceItem()
        {
            SignupDate = Convert.ToDateTime("1/1/1900");
            CancelDate = Convert.ToDateTime("1/1/1900");
            RefundedDate = Convert.ToDateTime("1/1/1900");
            IsInWaitingList = false;

            IsClassCanceledByClient = false;
            DateClassCanceledByClient = Convert.ToDateTime("1/1/1900");
            ClassInvoiceItemState = ClassInvoiceItemState.NA;
        }

        public long Id { get; set; }
        public long InvoiceId { get; set; }
        public long SectionId { get; set; }
        public DateTime SignupDate { get; set; }
        public int NumberAttending { get; set; }
        public Decimal Price { get; set; }
        public string Description { get; set; }

        public bool IsClassCanceled { get; set; }
        public DateTime CancelDate { get; set; }
        public decimal RefundAmount { get; set; }
        public bool IsRefundProcessed { get; set; }
        public DateTime RefundedDate { get; set; }
        public bool IsEnrollmentAsVisitor { get; set; }
        public bool IsInWaitingList { get; set; }

        public bool IsClassCanceledByClient { get; set; }
        public DateTime DateClassCanceledByClient { get; set; }
        public ClassInvoiceItemState ClassInvoiceItemState { get; set; }
        public long UserID { get; set; }

    }

}
