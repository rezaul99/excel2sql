﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;
using daoProject.Infrastructure;
using daoProject.Service;

namespace daoProject.Model
{
    public class PackageDiscountWHRC
    {
        public PackageDiscountWHRC()
        {
            PackageName = String.Empty;
        }

        public long RecordID { get; set; }
        public string PackageName { get; set; }
        public decimal DiscountAmount { get; set; }
        public long UserID { get; set; }
    }

    public class CopyOfPackageDiscountWHRC
    {
        public CopyOfPackageDiscountWHRC()
        {
            PackageName = String.Empty;
        }

        public long RecordID { get; set; }
        public string PackageName { get; set; }
        public decimal DiscountAmount { get; set; }
        public long UserID { get; set; }
    }
}
