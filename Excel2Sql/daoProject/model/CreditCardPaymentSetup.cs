﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;
using daoProject.Infrastructure;
using daoProject.Service;

namespace daoProject.Model
{
    public class CreditCardPaymentSetup
    {
        public CreditCardPaymentSetup()
        {
            MarchentLoginID = String.Empty;
            MarchentTransactionKey = String.Empty;
            PaymentGatewayType = PaymentGatewayType.OnSite;
            ImplementationMode = "Test";
        }

        public long Id { get; set; }
        public string MarchentLoginID { get; set; }
        public string MarchentTransactionKey { get; set; }
        public PaymentGatewayType PaymentGatewayType { get; set; }
        public string PaymentGatewayTypeStr { get; set; }
        public string ImplementationMode { get; set; }
        public long UserID { get; set; }
        public long GatewayID { get; set; }
        public string MarchentPassword { get; set; }
        public int isActive { get; set; }
    }
}
