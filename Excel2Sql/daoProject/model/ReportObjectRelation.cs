﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace daoProject.Model
{
    public class ReportObjectRelation
    {
        public long Id { get; set; }
        public string Table1Id { get; set; }
        public string ColumnName1 { get; set; }
       public string ColumnPK { get; set; }
        public string Table2Id { get; set; }
        public string ColumnName2 { get; set; }
        public string ColumnFK { get; set; }
    }
}
