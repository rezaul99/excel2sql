﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;
using daoProject.Infrastructure;
using daoProject.Service;

namespace daoProject.Model
{
    public class VendorWHRC
    {
        public VendorWHRC()
        {
            VendorName = String.Empty;
        }

        public long RecordID { get; set; }
        public string VendorName { get; set; }
        public long UserID { get; set; }
    }
}
