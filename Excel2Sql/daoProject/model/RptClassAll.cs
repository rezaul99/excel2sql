﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace daoProject.Model
{
    public class RptClassAll
    {
        public RptClassAll()
        {
        }

        public string ClassType { get; set; }
        public string Title { get; set; }
        public string SectionCode { get; set; }
        public int MaxSize { get; set; }
        public int SpacesAvailable { get; set; }
        public int Enrollment { get; set; }
    }
}
