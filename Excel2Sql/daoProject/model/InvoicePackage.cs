﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace daoProject.Model
{
    public class InvoicePackage
    {
        public InvoicePackage()
        {
        }

        public long Id { get; set; }
        public long InvoiceID { get; set; }
        public long PackageID { get; set; }
    }
}
