﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace daoProject.Model
{
    public class PaymentDetailReportHelper
    {
        public PaymentDetailReportHelper()
        {
        }

        public string PaymentMode { get; set; }
        public string PaymentDate { get; set; }
        public Decimal PaymentAmount { get; set; }
        public string TransactionID { get; set; }
        public string PaymentReceiptNo { get; set; }
        public string PatientReceiptNo { get; set; }
    }
}
