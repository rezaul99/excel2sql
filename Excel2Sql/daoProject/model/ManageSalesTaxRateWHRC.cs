﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;
using daoProject.Infrastructure;
using daoProject.Service;

namespace daoProject.Model
{
    public class ManageSalesTaxRateWHRC
    {
        public ManageSalesTaxRateWHRC()
        {
        }

        public long RecordID { get; set; }
        public decimal SalesTaxRate { get; set; }
        public long UserID { get; set; }
    }
}
