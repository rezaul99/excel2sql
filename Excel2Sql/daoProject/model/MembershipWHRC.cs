﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;
using daoProject.Infrastructure;
using daoProject.Service;

namespace daoProject.Model
{
    public class MembershipWHRC
    {
        public MembershipWHRC()
        {
            NameofMembership = String.Empty;
        }

        public long RecordID { get; set; }
        public string NameofMembership { get; set; }
        public string DurationPeriod { get; set; }
        public decimal MembershipFee { get; set; }
        public long UserID { get; set; }
    }
}
