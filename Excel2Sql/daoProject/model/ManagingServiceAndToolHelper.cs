﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using daoProject.Infrastructure;

namespace daoProject.Model
{
    public class ManagingServiceAndToolHelper
    {
        public ManagingServiceAndToolHelper(string objectName, string displayName)
        {
            ObjectName = objectName;
            ToolName = ManagingServiceHelper.GetJijotyToolName(objectName);
            DisplayName = displayName;
        }

        public string ObjectName { get; set; }
        public string ToolName { get; set; }
        public string DisplayName { get; set; }
    }
}
