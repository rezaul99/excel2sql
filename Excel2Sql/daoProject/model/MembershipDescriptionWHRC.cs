﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace daoProject.Model
{
    [Serializable]
    public class MembershipDescriptionWHRC
    {
        public MembershipDescriptionWHRC()
        {
        }
        public long RecordID { get; set; }
        public string MembershipDescription { get; set; }
        public long UserID { get; set; }
    }
}
