﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace daoProject.Model
{
    public class PaymentGatewayDetail
    {
        private long id;

        public long Id
        {
            get { return id; }
            set { id = value; }
        }
        private string gatewayName;

        public string GatewayName
        {
            get { return gatewayName; }
            set { gatewayName = value; }
        }

        
    }
}
