﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace daoProject.Model
{
    public class RptClientPayment
    {
        public RptClientPayment()
        {
        }
        
        public string LastName { get; set; }
        public string FirstName { get; set; }
        public string Phone { get; set; }
        public decimal AccountBalance { get; set; }
        public string description { get; set; }
    }
}
