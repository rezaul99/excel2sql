﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace daoProject.Model
{
    [Serializable]
    public class ProductTypeWHRC
    {

        public long Id { get; set; }
        public long RecordId { get; set; }
        public string Name { get; set; }
        public long UserID { get; set; }
    }
}
