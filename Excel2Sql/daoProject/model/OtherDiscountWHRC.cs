﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;
using daoProject.Infrastructure;
using daoProject.Service;

namespace daoProject.Model
{
    public class OtherDiscountWHRC
    {
        public OtherDiscountWHRC()
        {
            DisountType = String.Empty;
        }

        public long RecordID { get; set; }
        public string DisountType { get; set; }
        public long UserID { get; set; }
        public string DisountMode { get; set; }
        public decimal DiscountValue { get; set; }
    }
}
