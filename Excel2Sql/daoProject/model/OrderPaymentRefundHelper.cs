﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;
using daoProject.Infrastructure;
using daoProject.Service;

namespace daoProject.Model
{
    public class OrderPaymentRefundHelper
    {
        public OrderPaymentRefundHelper()
        {
        }

        public long InvoiceID { get; set; }
        public string PaymentDetails { get; set; }
        public string RefundDetails { get; set; }
    }
}
