﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace daoProject.Model
{
    public class ClassPackageHelper
    {
        public ClassPackageHelper()
        {
        }

        public long PackageID { get; set; }
        public long ClassID { get; set; }
        public string ClassTitle { get; set; }
        public long SectionID { get; set; }
        public string SectionTitle { get; set; }
        public DateTime SignupDate { get; set; }
        public int NumberOfAttandance { get; set; }
        public decimal Price { get; set; }
        public bool IsInWaitingList { get; set; }
        public int NumberAttending { get; set; }
        public string DayOfWeek { get; set; }
        public string ClassDescription { get; set; }
    }
}
