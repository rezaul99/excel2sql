﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;
using daoProject.Infrastructure;
using daoProject.Service;

namespace daoProject.Model
{
    public class StudentFlatRateWHRC
    {
        public StudentFlatRateWHRC()
        {
        }

        public long RecordID { get; set; }
        public decimal DiscountRatePerClass { get; set; }
        public long UserID { get; set; }
    }
}
