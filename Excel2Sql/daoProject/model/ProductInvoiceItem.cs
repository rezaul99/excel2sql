﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;
using daoProject.Infrastructure;

namespace daoProject.Model
{
    public class ProductInvoiceItem
    {
        public ProductInvoiceItem()
        {
            ReturnDate = Convert.ToDateTime("1/1/1900");
            RefundedDate = Convert.ToDateTime("1/1/1900");
        }
        public long Id { get; set; }
        public long InvoiceId { get; set; }
        public long ProductId { get; set; }
        public int Quantity { get; set; }
        public Decimal UnitPrice { get; set; }
        public Decimal Price { get; set; }
        public Decimal SubTotal { get; set; }
        public Decimal TaxAmount { get; set; }

        public bool IsItemReturned { get; set; }
        public DateTime ReturnDate { get; set; }
        public decimal RefundAmount { get; set; }
        public bool IsRefundProcessed { get; set; }
        public DateTime RefundedDate { get; set; }
        public long ShippingAddressId { get; set; }
        public long UserID { get; set; }
    }
}
