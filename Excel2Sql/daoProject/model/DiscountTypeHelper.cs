﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;
using daoProject.Infrastructure;
using daoProject.Service;

namespace daoProject.Model
{
    public class DiscountTypeHelper
    {
        public DiscountTypeHelper()
        {
        }

        public long DiscountType { get; set; }
        public string DiscountTypeName { get; set; }
    }
}
