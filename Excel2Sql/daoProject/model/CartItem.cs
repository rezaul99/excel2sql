﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;
using daoProject.Infrastructure;
using daoProject.Service;

namespace daoProject.Model
{
    public class CartItem
    {
        public CartItem()
        {
            PurchaseDate = Convert.ToDateTime("1/1/1900");
            ExtensionActionDescription = "NoAction";
            ExtensionAdminActionDescription = "NoAction";
        }

        public long InvoiceItemId { get; set; }
        public InvoiceItemType InvoiceItemType { get; set; }
        public string InvoiceItemTypeDescription { get; set; }
        public string Description { get; set; }
        public Decimal Price { get; set; }
        public string  PriceString { get; set; }
        public DateTime PurchaseDate { get; set; }
        public long Quantity { get; set; }
        public string InvoiceItemIdAndInvoiceItemType { get; set; }
        public string ActionDescription { get; set; }
        public string ReturnDateString { get; set; }
        public string OverdueChargeString { get; set; }
        public string CancelOrReturnByClientStr { get; set; }
        public string PaymentActionDescription { get; set; }
        public string ShippingAddress { get; set; }
        public string ExtensionActionDescription { get; set; }
        public string ExtensionAdminActionDescription { get; set; }

    }
}
