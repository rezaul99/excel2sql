﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;
using daoProject.Infrastructure;
using daoProject.Service;

namespace daoProject.Model
{
    public class ClassType
    {
        public ClassType()
        {
            ClassTypeName = String.Empty;
        }

        public string ClassTypeName { get; set; }
    }
}
