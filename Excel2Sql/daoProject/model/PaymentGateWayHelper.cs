﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace daoProject.Model
{
    public class PaymentGateWayHelper
    {
        public PaymentGateWayHelper()
        {
        }

        public int GatewayID { get; set; }
        public String GatewayName { get; set; }
    }
}
