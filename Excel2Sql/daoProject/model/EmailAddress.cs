﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace daoProject.Model
{
    public class EmailAddress
    {
        public long Id { get; set; }
        public string MailAddress { get; set; }
    }
}
