﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace daoProject.Model
{
    public class RptMemberShip
    {
        public RptMemberShip()
        {
        }
        public string Month { get; set; }
        
        public string LastName { get; set; }
        public string FirstName { get; set; }
        public string LastDateValid { get; set; }
    }
}
