﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;
using daoProject.Infrastructure;

namespace daoProject.Model
{
    public class ClientDeposit
    {
        public long Id { get; set; }
        public long ClientId { get; set; }
        public Decimal Amount { get; set; }
        public string Comment { get; set; }
        public long ModifiedBy { get; set; }
        public DateTime ModifiedDate { get; set; }
    }
}
