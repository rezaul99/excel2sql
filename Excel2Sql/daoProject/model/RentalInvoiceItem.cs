﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;
using daoProject.Infrastructure;

namespace daoProject.Model
{
    public class RentalInvoiceItem
    {
        public RentalInvoiceItem()
        {
            REStartingDate = Convert.ToDateTime("1/1/1900");
            REEndingDate = Convert.ToDateTime("1/1/1900");
            RPStartingDate = Convert.ToDateTime("1/1/1900");
            RPEndingDate = Convert.ToDateTime("1/1/1900");
            ReturnDate = Convert.ToDateTime("1/1/1900");
            RentalItemStatus = ReturnStatus.NotReturned;
            RefundedDate = Convert.ToDateTime("1/1/1900");

            DateExtensionRequestByClient = Convert.ToDateTime("1/1/1900");
            DateExtensionApprovedByAdmin = Convert.ToDateTime("1/1/1900");
        }

        public long Id { get; set; }
        public long InvoiceId { get; set; }
        public long RentalEquipmentID { get; set; }
        public string AgreementNo { get; set; }
        public DateTime REStartingDate { get; set; }
        public string RESerial { get; set; }
        public string REVendorName { get; set; }
        public string ModelName { get; set; }
        public DateTime REEndingDate { get; set; }
        public string REConditionWhenReturned { get; set; }
        public long StuffInitials { get; set; }
        public DateTime RPStartingDate { get; set; }
        public DateTime RPEndingDate { get; set; }
        public string RPDescription { get; set; }
        public Decimal RPPreTaxAmount { get; set; }
        public Decimal RPSalesTax { get; set; }
        public Decimal RPTotal { get; set; }

        public Decimal DepositAmount { get; set; }

        public bool IsItemReturned { get; set; }
        public DateTime ReturnDate { get; set; }
        public bool IsRentAmountRefundable { get; set; }
        public decimal RefundAmount { get; set; }
        public ReturnStatus RentalItemStatus { get; set; }
        public bool IsRefundProcessed { get; set; }
        public DateTime RefundedDate { get; set; }
        public Decimal OverdueCharge { get; set; }
        public Decimal RegularFee { get; set; }
        public long UserID { get; set; }

        public bool IsExtensionRequestByClient { get; set; }
        public DateTime DateExtensionRequestByClient { get; set; }
        public int ExtensionDays { get; set; }
        public decimal ExtensionPreTaxAmount { get; set; }
        public decimal ExtensionSalesTax { get; set; }
        public decimal ExtensionTotalAmount { get; set; }
        public bool IsExtensionApprovedByAdmin { get; set; }
        public DateTime DateExtensionApprovedByAdmin { get; set; }
    }
}
