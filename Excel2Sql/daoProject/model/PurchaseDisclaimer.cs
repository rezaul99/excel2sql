﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace daoProject.Model
{
    public  class PurchaseDisclaimer
    {
        public long Id { get; set; }
        public string PurchaseDisclaimerValue { get; set; }
    }
}
