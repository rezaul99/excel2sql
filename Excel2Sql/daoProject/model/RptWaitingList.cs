﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace daoProject.Model
{
    public class RptWaitingList
    {
        public RptWaitingList()
        {
        }

        public string ClassType { get; set; }
        public string Title { get; set; }
        public string SectionCode { get; set; }
        public int InWaitingList { get; set; }
    }
}
