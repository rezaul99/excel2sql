﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;
using daoProject.Infrastructure;
using daoProject.Service;

namespace daoProject.Model
{
    public class InvoicePaymentDetail
    {
        public InvoicePaymentDetail()
        {
            MedicalNumberOrNationalId = String.Empty;
        }

        public long Id { get; set; }
        public long InvoiceID { get; set; }
        public long UserId { get; set; }
        public PaymentType PaymentMode { get; set; }
        public decimal Amount { get; set; }
        public DateTime PaymentDate { get; set; }
        public CreditCardTransactionStatus TransactionStatus { get; set; }
        public string TransactionID { get; set; }
        public string TransactionDetails { get; set; }
        public string MedicalNumberOrNationalId { get; set; }
        public string CheckNumber { get; set; }
        public string PatientReceiptNumber { get; set; }
        public PaymentStatus PaymentStatus { get; set; }
    }
}
