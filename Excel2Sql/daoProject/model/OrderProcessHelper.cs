﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;
using daoProject.Infrastructure;
using daoProject.Service;

namespace daoProject.Model
{
    public class OrderProcessHelper
    {
        public OrderProcessHelper()
        {
            PurchaseDate = Convert.ToDateTime("1/1/1900");
        }

        public long InvoiceID { get; set; }
        public DateTime PurchaseDate { get; set; }
        public string CustomerName { get; set; }
        public string OrderDescription { get; set; }
        public string OrderItemsDescription { get; set; }
        public Decimal TotalPrice { get; set; }
        public string TotalPriceStr { get; set; }

        public string OrderPlacedDateStr { get; set; }
        public string OrderNumber { get; set; }
        public string Recipient { get; set; }

    }
}
