﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;
using daoProject.Infrastructure;

namespace daoProject.Model
{
    public class MembershipInvoiceItem
    {
        public long Id { get; set; }
        public long InvoiceId { get; set; }
        public long MembershipId { get; set; }
        public Decimal Price { get; set; }
        public string Description { get; set; }
        public DateTime DateJoined { get; set; }
        public DateTime DateValidTill { get; set; }
        public long StaffInitial { get; set; }
        public string MembershipType { get; set; }
        public long UserID { get; set; }
    }
}
