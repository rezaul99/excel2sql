﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace daoProject.Model
{
    public class ReportDetails
    {
        private long _id;
        private string _title;
        private string _description;
        private string _name;
        private string _xmlDataSetName;
        private string _path;

        public long ReportId
        {
            get
            {
                return _id;
            }
            set
            {
                _id = value;
            }

        }
        public string ReportTitle
        {
            get
            {
                return _title;
            }
            set
            {
                _title = value;
            }

        }
        public string Description
        {
            get
            {
                return _description;
            }
            set
            {
                _description = value;
            }

        }
        public string ReportFileName
        {
            get
            {
                return _name;
            }
            set
            {
                _name = value;
            }

        }
        public string ReportFilePath
        {
            get
            {
                return _path;
            }
            set
            {
                _path = value;
            }

        }
        public string ReportTableName
        {
            get
            {
                return _xmlDataSetName;
            }
            set
            {
                _xmlDataSetName = value;
            }

        }
    }
}
