﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;
using daoProject.Infrastructure;
using daoProject.Service;

namespace daoProject.Model
{
    public class ClassEnrollmentWaitingHelper
    {
        public ClassEnrollmentWaitingHelper()
        {
            SignupDate = Convert.ToDateTime("1/1/1900");
        }

        public long SectionID { get; set; }
        public long ClassInvoiceItemID { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public string Address { get; set; }
        public DateTime SignupDate { get; set; }
        public string SignupDateStr { get; set; }
        public bool IsInWaitingList { get; set; }
        public bool IsClassCanceled { get; set; }
        public bool IsClassCanceledByClient { get; set; }
        public string ClassCanceledByClientMsg { get; set; }
        public string SectionChangeDescription { get; set; }

    }
}
