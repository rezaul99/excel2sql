﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;
using daoProject.Infrastructure;
using daoProject.Service;

namespace daoProject.Model
{
    public class RentalEquipmentWHRC
    {
        public RentalEquipmentWHRC()
        {
            StartingDate = Convert.ToDateTime("1/1/1900");
            EndingDate = Convert.ToDateTime("1/1/1900");
            DateReceivedFromVendor = Convert.ToDateTime("1/1/1900");
            DateReturnedToVendr = Convert.ToDateTime("1/1/1900");
            Note = string.Empty;
        }

        public long RecordID { get; set; }
        public DateTime StartingDate { get; set; }
        public string Serial { get; set; }
        public string VendorName { get; set; }
        public string ModelName { get; set; }
        public string Status { get; set; }
        public DateTime EndingDate { get; set; }
        public string StuffInitials { get; set; }
        public decimal DepositAmount { get; set; }
        public DateTime DateReceivedFromVendor { get; set; }
        public DateTime DateReturnedToVendr { get; set; }
        public string Note { get; set; }

        //+Helper Properties
        public string ConditionWhenReturned { get; set; }
        public string Client { get; set; }
        public string DateReturnedToVendor { get; set; }
        //+Helper Properties
        public long UserID { get; set; }
    }
}
