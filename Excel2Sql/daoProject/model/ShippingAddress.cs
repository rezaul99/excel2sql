﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;
using daoProject.Infrastructure;

namespace daoProject.Model
{
    public class ShippingAddress
    {
        public long Id { get; set; }
        public long ClientId { get; set; }
        public string FullName { get; set; }
        public string AddressLine1 { get; set; }
        public string AddressLine2 { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Zip { get; set; }
        public long CountryId { get; set; }
        public string PhoneNo { get; set; }
        public string DetailDescription { get; set; }
        public int DefaultShippingAddess { get; set; }
    }
}
