﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace daoProject.Model
{
    public class ReportHeaderManager
    {
        public long Id { get; set; }
        public string ReportHeaderValue { get; set; }
    }
}
