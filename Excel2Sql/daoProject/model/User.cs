﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;
using daoProject.Infrastructure;

namespace daoProject.Model
{
    [Serializable]
    public class User
    {
        public User()
        {
            FirstName = "";
            MiddleName = "";
            LastName = "";
            Email = "";
            Password = "";
            UserType = UserType.Member;
            UserRecordId = 0;
            Address1 = "";
            Ad1City = "";
            Ad1State = "";
            Ad1Zip = "";
            Address2 = "";
            Ad2City = "";
            Ad2State = "";
            Ad2Zip = "";
            Insurance = "";
            Hospital = "";
            HospitalPractice = "";
            Notes = "";
            AreaCode = "";
            DateOfBirth = Convert.ToDateTime("1/1/1900");
            DueDate = Convert.ToDateTime("1/1/1900");
            phone = "";
            phoneType = "";
            PhoneNotes = "";
            Active = 1;
            AreaCode2 = "";
            phone2 = "";            
            phoneType2 = "";
            PhoneNotes2 = "";
            UserStatus = UserStatus.Active;
            MedicalRecordNo = "";
            OBProviderName = "";
            ReferringPhysician = "";
            ReceiveEmailNotification = IsReceiveEmailNotification.No;            
        }
        public long Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public UserType UserType;
        public long UserRecordId;
        public string MiddleName { get; set; }
        public string Address1 { get; set; }
        public string Ad1City { get; set; }
        public string Ad1State { get; set; }
        public string Ad1Zip { get; set; }
        public string Address2 { get; set; }
        public string Ad2City { get; set; }
        public string Ad2State { get; set; }
        public string Ad2Zip { get; set; }
        public DateTime DateOfBirth{ get; set; }
        public DateTime DueDate { get; set; }
        public string Insurance { get; set; }
        public string Hospital { get; set; }
        public string HospitalPractice { get; set; }
        public string Notes { get; set; }
        public string AreaCode { get; set; }
        public string phone { get; set; }
        public string phoneType { get; set; }
        public string PhoneNotes { get; set; }
        public int Active { get; set; }
        public string AreaCode2 { get; set; }
        public string phone2 { get; set; }
        public string phoneType2 { get; set; }
        public string PhoneNotes2 { get; set; }
        public UserStatus UserStatus;
        public string MedicalRecordNo { get; set; }
        public string OBProviderName { get; set; }
        public string ReferringPhysician { get; set; }
        public IsReceiveEmailNotification ReceiveEmailNotification { get; set; }
    }
}
