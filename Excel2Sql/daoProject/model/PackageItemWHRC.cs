﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace daoProject.Model
{
    [Serializable]
    public class PackageItemWHRC
    {
        public long ID { get; set; }
        public long PackageId { get; set; }
        public long RecordId { get; set; }
        public int Quantity { get; set; }
    }
}
