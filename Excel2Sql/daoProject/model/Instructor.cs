﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;
using daoProject.Infrastructure;
using daoProject.Service;

namespace daoProject.Model
{
    public class Instructor
    {
        public Instructor()
        {
            InstructorName = String.Empty;
        }

        public long RecordID { get; set; }
        public string InstructorName { get; set; }
        public string Position { get; set; }
        public string Designation { get; set; }
        public string DetailsViewLink { get; set; }
        public string ImageURL { get; set; }
        public string ThumbnailURL { get; set; }
        public long UserID { get; set; }
    }
}
