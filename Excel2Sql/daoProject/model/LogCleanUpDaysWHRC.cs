﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;
using daoProject.Infrastructure;
using daoProject.Service;


namespace daoProject.Model
{
    [Serializable]
    public class LogCleanUpDaysWHRC
    {
        public LogCleanUpDaysWHRC()
        {
        }
        public long RecordID { get; set; }
        public int LogCleanUpDays { get; set; }
        public long UserID { get; set; }
    }
}
