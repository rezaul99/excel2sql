﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace daoProject.Model
{
    public class ReportAttributes
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public long TableId { get; set; }
        //public int IsSortable { get; set; }
    }
}
