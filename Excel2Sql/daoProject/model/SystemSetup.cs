﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;
using daoProject.Infrastructure;

namespace daoProject.Model
{
    public class SystemSetup
    {
        public long Id { get; set; }
        public Decimal SalesTaxRate { get; set; }
        public Decimal MembershipFee { get; set; }
        public Decimal EquipmentDeposit { get; set; }
        public string HospitalDiscountName { get; set; }
        public Decimal HospitalDiscountAmount { get; set; }
    }
}
