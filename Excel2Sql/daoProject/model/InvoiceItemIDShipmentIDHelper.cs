﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;
using daoProject.Infrastructure;
using daoProject.Service;

namespace daoProject.Model
{
    public class InvoiceItemIDShipmentIDHelper
    {
        public InvoiceItemIDShipmentIDHelper()
        {
        }

        public long InvoiceItemID { get; set; }
        public long ShipmentAddressID { get; set; }
            }
}
