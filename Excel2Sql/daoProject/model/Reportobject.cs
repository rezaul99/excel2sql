﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using daoProject.Infrastructure;
using ReportLibrary.Infrastructure;

namespace daoProject.Model
{
    public class Reportobject
    {
        private Dictionary<string, string> _ClassObjects;
        private Dictionary<string, Dictionary<string, string>> _Attributes;

        private Dictionary<string, string> _Product;
        private Dictionary<string, string> _Class;
        private Dictionary<string, string> _Section;
        private Dictionary<string, string> _Rental;
        private Dictionary<string, string> _RentalOrder;
        private Dictionary<string, string> _MemberShip;
        private Dictionary<string, string> _Packages;
        private Dictionary<string, string> _Orders;
        private Dictionary<string, string> _Users;

        private Dictionary<string, string> _location;
        private Dictionary<string, string> _instructor;
        private Dictionary<string, string> _vendorWHRC;
        private Dictionary<string, string> _paymentDetails;
        private Dictionary<string, string> _shippingAddress;
        private Dictionary<string, string> _discountDetails;
        private Dictionary<string, string> _studentFlatRateWHRC;
        private Dictionary<string, string> _percentageDiscountWHRC;
        private Dictionary<string, string> _otherDiscountWHRC;
        private Dictionary<string, string> _manageSalesTaxRateWHRC;

        private Dictionary<string, string> _ProductOrder;
        private Dictionary<string, string> _ClassOrder;
        private Dictionary<string, string> _SectionOrder;
        private Dictionary<string, string> _MemberShipOrder;
        private Dictionary<string, string> _PackageOrder;

        private Dictionary<string, FieldDataType> _FieldFormat;

        public Dictionary<string, string> JijotyClassObjects
        {
            get
            {
                _ClassObjects = new Dictionary<string, string>();

                //jijoty web service
                _ClassObjects["ManageProductItems"] = "Product Details";
                _ClassObjects["ManageClass"] = "Class Details";
                _ClassObjects["ManageSections"] = "Section Details";
                _ClassObjects["Location"] = "Location Details";
                _ClassObjects["Instructor"] = "Instructor Details";
                _ClassObjects["VendorName"] = "Vendor Details";
                _ClassObjects["MgeRentalEquipment"] = "Rental Equipment Details";
                _ClassObjects["ManageRentalTerm"] = "Rental Period Details";
                _ClassObjects["WHRCMemberships"] = "MemberShip Details";
                _ClassObjects["DiscountRateperClassforStudent"] = "Student Flat Rate Details";
                _ClassObjects["WHRCPercentageDiscounts"] = "Percentage Discount Details";
                _ClassObjects["WHRCOtherDiscounts"] = "Other Discount Details";
                _ClassObjects["ManageSalesTaxRate"] = "Sales Tax Rate Details";

                return _ClassObjects;
            }
        }

        public Dictionary<string, string> WhrcDBClassObjects
        {
            get
            {
                _ClassObjects = new Dictionary<string, string>();
                //whrc db
                _ClassObjects["Whrc_ProductInvoiceItem"] = "Product Order Details";
               // _ClassObjects["Whrc_ClassInvoiceItem"] = "Class Order Details";
                _ClassObjects["Whrc_ClassInvoiceItem"] = "Section Order Details";
                _ClassObjects["Whrc_RentalInvoiceItem"] = "Rental Order Details";
                _ClassObjects["Whrc_MembershipInvoiceItem"] = "MemberShip Order Details";
                _ClassObjects["Whrc_Package"] = "Whrc All Packages";
                _ClassObjects["Whrc_PackageItem"] = "Whrc Packages Order Details";
                _ClassObjects["Whrc_Invoice"] = "Orders Details";
                _ClassObjects["WHRC_InvoicePayment"] = "Payment Details";
                _ClassObjects["Whrc_ShippingAddress"] = "Shipping Address Details";
                _ClassObjects["Whrc_Users"] = "User Details";
                return _ClassObjects;
            }
        }

        public Dictionary<string, Dictionary<string, string>> Attributes
        {
            get
            {
                _Attributes = new Dictionary<string, Dictionary<string, string>>();
                //jijoty web service
                _Attributes["ManageProductItems"] = Product;
                _Attributes["ManageClass"] = Class;
                _Attributes["ManageSections"] = Section;
                _Attributes["MgeRentalEquipment"] = RentalEquipment;
                _Attributes["ManageRentalTerm"] = RentalTerm;
                _Attributes["WHRCMemberships"] = MemberShip;
                _Attributes["DiscountRateperClassforStudent"] = StudentFlatRateWHRC;
                _Attributes["WHRCPercentageDiscounts"] = PercentageDiscountWHRC;
                _Attributes["WHRCOtherDiscounts"] = OtherDiscountWHRC;
                _Attributes["ManageSalesTaxRate"] = ManageSalesTaxRateWHRC;
                _Attributes["Location"] = Location;
                _Attributes["Instructor"] = Instructor;
                _Attributes["VendorName"] = VendorWHRC;

                //whrc db
                _Attributes["Whrc_ProductInvoiceItem"] = ProductOrder;
               // _Attributes["Whrc_ClassInvoiceItem"] = ClassOrder;
                _Attributes["Whrc_ClassInvoiceItem"] = SectionOrder;
                _Attributes["Whrc_RentalInvoiceItem"] = RentalOrder;
                _Attributes["Whrc_MembershipInvoiceItem"] = MemberShipOrder;
                _Attributes["Whrc_Package"] = Packages;
                _Attributes["Whrc_PackageItem"] = PackageOrder;
                _Attributes["Whrc_Invoice"] = Orders;
                _Attributes["WHRC_InvoicePayment"] = PaymentDetails;
                _Attributes["Whrc_ShippingAddress"] = ShippingAddress;
                _Attributes["Whrc_Users"] = Users;

                return _Attributes;
            }
        }

        public Dictionary<string, string> Product
        {
            get
            {
                _Product = new Dictionary<string, string>();
                _Product["ManageProductItems_ProductCategory"] = "Category";
                _Product["ManageProductItems_ItemName"] = "Name";
                _Product["ManageProductItems_SalePrice"] = "Price";
                _Product["ManageProductItems_QuantityInStock"] = "Total Quantity in Stock";
                _Product["ManageProductItems_QuantitySold"] = "Total Quantity Sold";
                return _Product;
            }
        }

        public Dictionary<string, string> Class
        {
            get
            {
                _Class = new Dictionary<string, string>();
                _Class["ManageClass_ClassType"] = "Class Type";
                _Class["ManageClass_Title"] = "Title";
                _Class["ManageClass_Year"] = "Published Year";
                _Class["ManageClass_Fee"] = "Fee";
                _Class["ManageClass_Description"] = "Description";
                _Class["ManageClass_PublishThisClass"] = "Publish Status";
                _Class["ManageClass_ClassDescription"] = "Class Description";
                return _Class;
            }
        }

        public Dictionary<string, string> Section
        {
            get
            {
                _Section = new Dictionary<string, string>();
                _Section["ManageSections_ClassTitle"] = "Class Title";
                _Section["ManageSections_StartDate"] = "Start Date";
                _Section["ManageSections_NumberofMeetings"] = "Number of Meetings";
                _Section["ManageSections_StartTimeTOEndTime"] = "Time period";
                _Section["ManageSections_MaximumSize"] = "Maximum Size";
                _Section["ManageSections_Available"] = "Available";
                _Section["ManageSections_TotalEnrolled"] = "Total Enrolled in this section";
                _Section["ManageSections_TotalWaiting"] = "Total Waiting List";
                _Section["ManageSections_Instructor"] = "Instructor Details";
                _Section["ManageSections_Locations"] = "Location Details";
                _Section["ManageSections_DayOfWeek"] = "Day Of Week";
                _Section["ManageSections_PublishThisSection"] = "Publish Status";
                return _Section;
            }
        }

        public Dictionary<string, string> RentalEquipment
        {
            get
            {
                _Rental = new Dictionary<string, string>();
                _Rental["MgeRentalEquipment_Serial"] = "Serial";
                _Rental["MgeRentalEquipment_VendorName"] = "Vendor Name";
                _Rental["MgeRentalEquipment_ModelName"] = "Model Name";
                _Rental["MgeRentalEquipment_Status"] = "Status";
                _Rental["MgeRentalEquipment_DateReceivedfromVendor"] = "Date Received from Vendor";
                _Rental["MgeRentalEquipment_DateReturnedtoVendor"] = "Date Returned to Vendor";
                _Rental["MgeRentalEquipment_DepositAmount"] = "Deposit Amount";
                _Rental["MgeRentalEquipment_Notes"] = "Notes";
                return _Rental;
            }
        }

        public Dictionary<string, string> RentalTerm
        {
            get
            {
                _Rental = new Dictionary<string, string>();
                _Rental["ManageRentalTerm_ModelName"] = "Model Name";
                _Rental["ManageRentalTerm_RentalFees"] = "Fee";
                _Rental["ManageRentalTerm_LengthofRentalinDays"] = "Length of rental in days";
                return _Rental;
            }
        }

        public Dictionary<string, string> RentalOrder
        {
             get
            {
                _RentalOrder = new Dictionary<string, string>();
                //_RentalOrder["ClientDetails"] = "Client Details";
                _RentalOrder["Whrc_RentalInvoiceItem_REVendorName"] = "Vendor Name";
                _RentalOrder["Whrc_RentalInvoiceItem_ModelName"] = "Model Name";
                //_RentalOrder["Fee"] = "Fee";
                //_RentalOrder["LengthofRentalinDays"] = "Length of rental in days";
                _RentalOrder["Whrc_RentalInvoiceItem_REStartingDate"] = "Rental eqipment start time";
                _RentalOrder["Whrc_RentalInvoiceItem_REEndingDate"] = "Rental eqipment end time";
                _RentalOrder["Whrc_RentalInvoiceItem_RPStartingDate"] = "Rental start period";
                _RentalOrder["Whrc_RentalInvoiceItem_RPEndingDate"] = "Rental end period";
                _RentalOrder["Whrc_RentalInvoiceItem_RPDescription"] = "Description";

                _RentalOrder["Whrc_RentalInvoiceItem_RPPreTaxAmount"] = "Tax Amount";
                _RentalOrder["Whrc_RentalInvoiceItem_RPSalesTax"] = "Sales Tax";
                _RentalOrder["Whrc_RentalInvoiceItem_RPTotal"] = "Total Price";

                _RentalOrder["Whrc_RentalInvoiceItem_DepositAmount"] = "Deposit Amount";
              //  _RentalOrder["IsItemReturned"] = "Item Returned";
                _RentalOrder["Whrc_RentalInvoiceItem_ReturnDate"] = "Return Date";
              //  _RentalOrder["RentalItemStatus"] = "Renta lItem Status";
                _RentalOrder["Whrc_RentalInvoiceItem_OverdueCharge"] = "Overdue Charge";
              //  _RentalOrder["IsExtensionRequestByClient"] = "Extension Request By Client";
               // _RentalOrder["DateExtensionRequestByClient"] = "Date Extension Request By Client";
                _RentalOrder["Whrc_RentalInvoiceItem_ExtensionDays"] = "Extension Days";
                _RentalOrder["Whrc_RentalInvoiceItem_ExtensionPreTaxAmount"] = "Extension PreTax Amount";
                _RentalOrder["Whrc_RentalInvoiceItem_ExtensionSalesTax"] = "Extension Sales Tax";
                _RentalOrder["Whrc_RentalInvoiceItem_ExtensionTotalAmount"] = "Extension Total Amount";
              //  _RentalOrder["Whrc_RentalInvoiceItem_IsExtensionApprovedByAdmin"] = "ExtensionApprovedByAdmin";
             //   _RentalOrder["Whrc_RentalInvoiceItem_DateExtensionApprovedByAdmin"] = "Date Extension Approved By Admin";
                _RentalOrder["Whrc_RentalInvoiceItem_RegularFee"] = "Regular Fee";
                 
                return _RentalOrder;
            }
        }

        public Dictionary<string, string> MemberShip
        {
            get
            {
                _MemberShip = new Dictionary<string, string>();
                _MemberShip["WHRCMemberships_NameofMembership"] = "Name of Membership";
                _MemberShip["WHRCMemberships_DurationPeriod"] = "Duration Period";
                _MemberShip["WHRCMemberships_MembershipFee"] = "Membership Fee";
              
               
                return _MemberShip;
            }
        }

        public Dictionary<string, string> Packages
        {
            get
            {
                _Packages = new Dictionary<string, string>();
                _Packages["Whrc_Package_PackageType"] = "PackageType";
                _Packages["Whrc_Package_PackageName"] = "Package Name";
                _Packages["Whrc_Package_PackageDescription"] = "Description";
                _Packages["Whrc_Package_DiscountAmount"] = "Discount";
                _Packages["Whrc_Package_Active"] = "Active Status";
                _Packages["Whrc_Package_CreateDate"] = "Create Date";
                _Packages["Whrc_Package_UpdateDate"] = "Update Date";
                _Packages["Whrc_Package_Quantity"] = "Quantity";
                return _Packages;
            }
        }

        public Dictionary<string, string> Orders
        {
            get
            {
                 _Orders = new Dictionary<string, string>();
                 //_Orders["ClientDetails"] = "Client Details";
                 //_Orders["InvoiceItemType"] = "Item";
                 //_Orders["InvoiceItemTypeDescription"] = "Description";
                 //_Orders["Description"] = "Description";
                 //_Orders["Quantity"] = "Quantity";

                 _Orders["Whrc_Invoice_PurchaseDate"] = "Purchase Date";
                 _Orders["Whrc_Invoice_PaymentDate"] = "Payment Date";

                 //_Orders["Price"] = "Price";
                 _Orders["Whrc_Invoice_Discount"] = "Discount";
                // _Orders["OverdueChargeString"] = "Overdue Charge";
                 _Orders["Whrc_Invoice_TotalRefundAmount"] = "Total Refund Amount";
                 _Orders["Whrc_Invoice_TotalAmount"] = "Total Amount";
                 _Orders["Whrc_Invoice_PaidAmount"] = "Paid Amount";
                 _Orders["Whrc_Invoice_NetPayable"] = "Net Payable";
                 _Orders["Whrc_Invoice_PaymentMode"] = "Payment Mode";

                 //_Orders["OrderStatus"] = "Order Status";
                 //_Orders["PaymentStatus"] = "Payment Status";

                //_Orders["ActionDescription"] = "Action Description";
                //_Orders["ReturnDateString"] = "Return Date";
                //_Orders["CancelOrReturnByClientStr"] = "Cancel Or Return By Client Status";
                //_Orders["PaymentActionDescription"] = "Payment Description";
                //_Orders["ShippingAddress"] = "Shipping Address";
                //_Orders["ExtensionActionDescription"] = "Extension Description";
                //_Orders["ExtensionAdminActionDescription"] = "Extension by Admin Description";

                 _Orders["Whrc_Invoice_MedicalNumberOrNationalId"] = "Medi-Cal Number Or National Id";
                 _Orders["Whrc_Invoice_CheckNumber"] = "Check Number";
              //  _Orders["PatientReceiptNumber"] = "Patient Receipt Number";
              
                return _Orders;
            }
        }

        public Dictionary<string, string> Users
        {
            get
            {
                _Users = new Dictionary<string, string>();
                _Users["Whrc_Users_FirstName"] = "First Name";
                _Users["Whrc_Users_LastName"] = "Last Name";
                _Users["Whrc_Users_Email"] = "Email";
                _Users["Whrc_Users_DateOfBirth"] = "Date Of Birth";
                _Users["Whrc_Users_UserType"] = "User Type";
                _Users["Whrc_Users_Address1"] = "Address";

                _Users["Whrc_Users_Ad1City"] = "City";
                _Users["Whrc_Users_Ad1State"] = "State";
                _Users["Whrc_Users_Ad1Zip"] = "Zip";

                _Users["Whrc_Users_Insurance"] = "Insurance";
                _Users["Whrc_Users_Hospital"] = "Hospital";
                _Users["Whrc_Users_HospitalPractice"] = "Hospital Practice";

                _Users["Whrc_Users_AreaCode"] = "Area Code";
                _Users["Whrc_Users_phone"] = "phone";
                _Users["Whrc_Users_phoneType"] = "phoneType";
                _Users["Whrc_Users_Active"] = "Active Status";
               
                return _Users;
            }
        }

        public Dictionary<string, string> Location
        {
            get
            {
                _location = new Dictionary<string, string>();
                _location["Location_LocationDetails"] = "Location Details";
                return _location;
            }
        }

        public Dictionary<string, string> Instructor
        {
            get
            {
                _instructor = new Dictionary<string, string>();
                _instructor["Instructor_InstructorName"] = "Instructor Name";
                return _instructor;
            }
        }

        public Dictionary<string, string> VendorWHRC
        {
            get
            {
                _vendorWHRC = new Dictionary<string, string>();
                _vendorWHRC["VendorName_VendorName"] = "Vendor Name";
                return _vendorWHRC;
            }
        }

        public Dictionary<string, string> PaymentDetails
        {
            get
            {
                _paymentDetails = new Dictionary<string, string>();
                _paymentDetails["WHRC_InvoicePayment_PaymentMode"] = "PaymentMode";
                _paymentDetails["WHRC_InvoicePayment_Amount"] = "Amount";
                _paymentDetails["WHRC_InvoicePayment_PaymentDate"] = "Payment Date";

                _paymentDetails["WHRC_InvoicePayment_MedicalNumberOrNationalId"] = "Medical Number Or National Id";
                _paymentDetails["WHRC_InvoicePayment_PatientReceiptNumber"] = "Patient Receipt Number";
                _paymentDetails["WHRC_InvoicePayment_CheckNumber"] = "Check Number";

                //_paymentDetails["TotalRefundAmount"] = "Total Refund Amount";
                //_paymentDetails["TotalAmount"] = "Total Amount";
                //_paymentDetails["PaidAmount"] = "Paid Amount";
                //_paymentDetails["NetPayable"] = "Net Payable";
                //_paymentDetails["PaymentMode"] = "Payment Mode";

                //_paymentDetails["OrderStatus"] = "Order Status";
                //_paymentDetails["PaymentStatus"] = "Payment Status";

                //_paymentDetails["ActionDescription"] = "Action Description";
                //_paymentDetails["ReturnDateString"] = "Return Date";
                //_paymentDetails["CancelOrReturnByClientStr"] = "Cancel Or Return By Client Status";
                //_paymentDetails["PaymentActionDescription"] = "Payment Description";

                //_paymentDetails["MedicalNumberOrNationalId"] = "Medi-Cal Number Or National Id";
                //_paymentDetails["CheckNumber"] = "Check Number";
                //_paymentDetails["PatientReceiptNumber"] = "Patient Receipt Number";
                return _paymentDetails;
            }
        }

        public Dictionary<string, string> ShippingAddress
        {
            get
            {
                _shippingAddress = new Dictionary<string, string>();
                _shippingAddress["Whrc_ShippingAddress_FullName"] = "FullName";
                _shippingAddress["Whrc_ShippingAddress_AddressLine1"] = "AddressLine1";
                _shippingAddress["Whrc_ShippingAddress_AddressLine2"] = "AddressLine2";
                _shippingAddress["Whrc_ShippingAddress_City"] = "City";
                _shippingAddress["Whrc_ShippingAddress_State"] = "State";
                _shippingAddress["Whrc_ShippingAddress_Zip"] = "Zip";
                _shippingAddress["Whrc_ShippingAddress_PhoneNo"] = "PhoneNo";
                return _shippingAddress;
            }
        }

        public Dictionary<string, string> DiscountDetails
        {
            get
            {
                _discountDetails = new Dictionary<string, string>();
                _discountDetails["DiscountTypeName"] = "Discount Type Name";
                _discountDetails["Description"] = "Descriptione";
                _discountDetails["Amount"] = "Amount";
                _discountDetails["IsPercentageMode"] = "Percentage Mode Status";
                return _discountDetails;
            }
        }

        public Dictionary<string, string> StudentFlatRateWHRC
        {
            get
            {
                _studentFlatRateWHRC = new Dictionary<string, string>();
                _studentFlatRateWHRC["DiscountRateperClassforStudent_DiscountRatePerClass"] = "Flat Rate";
                return _studentFlatRateWHRC;
            }
        }

        public Dictionary<string, string> PercentageDiscountWHRC
        {
            get
            {
                _percentageDiscountWHRC = new Dictionary<string, string>();
                _percentageDiscountWHRC["WHRCPercentageDiscounts_DisountType"] = "DisountType";
                _percentageDiscountWHRC["WHRCPercentageDiscounts_Percentage"] = "Percentage";
                return _percentageDiscountWHRC;
            }
        }

        public Dictionary<string, string> OtherDiscountWHRC
        {
            get
            {
                _otherDiscountWHRC = new Dictionary<string, string>();
                _otherDiscountWHRC["WHRCOtherDiscounts_DisountType"] = "Disount Type";
                _otherDiscountWHRC["WHRCOtherDiscounts_DisountMode"] = "Disount Mode";
                _otherDiscountWHRC["WHRCOtherDiscounts_DiscountValue"] = "Discount Value";
                return _otherDiscountWHRC;
            }
        }

        public Dictionary<string, string> ManageSalesTaxRateWHRC
        {
            get
            {
                _manageSalesTaxRateWHRC = new Dictionary<string, string>();
                _manageSalesTaxRateWHRC["ManageSalesTaxRate_SalesTaxRate"] = "Sales Tax Rate";
                return _manageSalesTaxRateWHRC;
            }
        }

        public Dictionary<string, string> ProductOrder
        {
            get
            {
                _ProductOrder = new Dictionary<string, string>();
                _ProductOrder["Whrc_ProductInvoiceItem_Quantity"] = "Quantity";
                _ProductOrder["Whrc_ProductInvoiceItem_UnitPrice"] = "UnitPrice";
                _ProductOrder["Whrc_ProductInvoiceItem_Price"] = "Price";
                _ProductOrder["Whrc_ProductInvoiceItem_TaxAmount"] = "TaxAmount";
                _ProductOrder["Whrc_ProductInvoiceItem_ReturnDate"] = "ReturnDate";
                _ProductOrder["Whrc_ProductInvoiceItem_RefundAmount"] = "RefundAmount";
                _ProductOrder["Whrc_ProductInvoiceItem_RefundedDate"] = "RefundedDate";
                _ProductOrder["Whrc_ProductInvoiceItem_ShippingAddressId"] = "ShippingAddressId";
                return _ProductOrder;
            }
        }

        public Dictionary<string, string> ClassOrder
        {
            get
            {
                _ClassOrder = new Dictionary<string, string>();
                _ClassOrder["SignupDate"] = "Signup Date";
                _ClassOrder["NumberAttending"] = "Number of Attending";
                _ClassOrder["Price"] = "Price";
              //  _ClassOrder["Description"] = "Description";
                _ClassOrder["RefundAmount"] = "Refund Amount";
                _ClassOrder["DateClassCanceledByClient"] = "Date Class Canceled By Client";
                return _ClassOrder;
            }
        }

        public Dictionary<string, string> SectionOrder
        {
            get
            {
                _SectionOrder = new Dictionary<string, string>();
                _SectionOrder["Whrc_ClassInvoiceItem_SignupDate"] = "Signup Date";
                _SectionOrder["Whrc_ClassInvoiceItem_NumberAttending"] = "Number of Attending";
                _SectionOrder["Whrc_ClassInvoiceItem_Price"] = "Price";
               // _SectionOrder["Description"] = "Description";
                _SectionOrder["Whrc_ClassInvoiceItem_RefundAmount"] = "Refund Amount";
                _SectionOrder["Whrc_ClassInvoiceItem_DateClassCanceledByClient"] = "Date Class Canceled By Client";
                return _SectionOrder;
            }
        }

        public Dictionary<string, string> MemberShipOrder
        {
            get
            {
                _MemberShipOrder = new Dictionary<string, string>();
                _MemberShipOrder["Whrc_RentalInvoiceItem_Price"] = "Price";
                _MemberShipOrder["Whrc_RentalInvoiceItem_Description"] = "Description";
                _MemberShipOrder["Whrc_RentalInvoiceItem_DateJoined"] = "Date Joined";
                _MemberShipOrder["Whrc_RentalInvoiceItem_DateValidTill"] = "Date Validation";
                return _MemberShipOrder;
            }
        }

        public Dictionary<string, string> PackageOrder
        {
            get
            {
                _PackageOrder = new Dictionary<string, string>();
                _PackageOrder["Whrc_PackageItem_Quantity"] = "Quantity";
                return _PackageOrder;
            }
        }

        public Dictionary<string, FieldDataType> FieldFormat
        {
            get
            {
                _FieldFormat = new Dictionary<string, FieldDataType>();

                _FieldFormat["ProductCategory"] = FieldDataType.stringFormat;
                _FieldFormat["ItemName"] = FieldDataType.stringFormat;
                _FieldFormat["SalePrice"] = FieldDataType.decimalFormat;
                _FieldFormat["QuantityInStock"] = FieldDataType.intFormat;
                _FieldFormat["QuantitySold"] = FieldDataType.intFormat;

                _FieldFormat["ClassType"] = FieldDataType.stringFormat;
                _FieldFormat["Title"] = FieldDataType.stringFormat;
                _FieldFormat["Year"] = FieldDataType.longFormat;
                _FieldFormat["Fee"] = FieldDataType.decimalFormat;
                _FieldFormat["Description"] = FieldDataType.stringFormat;
                _FieldFormat["PublishThisClass"] = FieldDataType.intFormat;
                _FieldFormat["ClassDescription"] = FieldDataType.stringFormat;

                _FieldFormat["ClassTitle"] = FieldDataType.stringFormat;
                _FieldFormat["StartDate"] = FieldDataType.dateFormat;
                _FieldFormat["NumberofMeetings"] = FieldDataType.intFormat;
                _FieldFormat["StartTime"] = FieldDataType.timeFormat;
                _FieldFormat["MaximumSize"] = FieldDataType.intFormat;
                _FieldFormat["Available"] = FieldDataType.intFormat;
                _FieldFormat["TotalEnrolled"] = FieldDataType.intFormat;
                _FieldFormat["TotalWaiting"] = FieldDataType.intFormat;
                _FieldFormat["Instructor"] = FieldDataType.stringFormat;
                _FieldFormat["Locations"] = FieldDataType.stringFormat;
                _FieldFormat["DayOfWeek"] = FieldDataType.stringFormat;
                _FieldFormat["PublishThisSection"] = FieldDataType.stringFormat;

                _FieldFormat["REVendorName"] = FieldDataType.stringFormat;
                _FieldFormat["ModelName"] = FieldDataType.stringFormat;
                _FieldFormat["Fee"] = FieldDataType.decimalFormat;
                _FieldFormat["LengthofRentalinDays"] = FieldDataType.intFormat;
                _FieldFormat["RPDescription"] = FieldDataType.stringFormat;
                _FieldFormat["RPPreTaxAmount"] = FieldDataType.decimalFormat;
                _FieldFormat["RPSalesTax"] = FieldDataType.decimalFormat;
                _FieldFormat["RPTotal"] = FieldDataType.decimalFormat;
                _FieldFormat["DepositAmount"] = FieldDataType.decimalFormat;
                _FieldFormat["RentalItemStatus"] = FieldDataType.stringFormat;

                _FieldFormat["ClientDetails"] = FieldDataType.stringFormat;
                _FieldFormat["REVendorName"] = FieldDataType.stringFormat;
                _FieldFormat["ModelName"] = FieldDataType.stringFormat;
                _FieldFormat["Fee"] = FieldDataType.decimalFormat;
                _FieldFormat["LengthofRentalinDays"] = FieldDataType.intFormat;
                _FieldFormat["REStartingDate"] = FieldDataType.dateFormat;
                _FieldFormat["REEndingDate"] = FieldDataType.dateFormat;
                _FieldFormat["RPStartingDate"] = FieldDataType.dateFormat;
                _FieldFormat["RPEndingDate"] = FieldDataType.dateFormat;
                _FieldFormat["RPDescription"] = FieldDataType.stringFormat;
                _FieldFormat["RPPreTaxAmount"] = FieldDataType.decimalFormat;
                _FieldFormat["RPSalesTax"] = FieldDataType.decimalFormat;
                _FieldFormat["RPTotal"] = FieldDataType.decimalFormat;
                _FieldFormat["DepositAmount"] =  FieldDataType.decimalFormat;
                _FieldFormat["IsItemReturned"] = FieldDataType.stringFormat;
                _FieldFormat["ReturnDate"] = FieldDataType.stringFormat;
                _FieldFormat["RentalItemStatus"] = FieldDataType.stringFormat;
                _FieldFormat["OverdueCharge"] = FieldDataType.decimalFormat;
                _FieldFormat["IsExtensionRequestByClient"] = FieldDataType.stringFormat;
                _FieldFormat["DateExtensionRequestByClient"] = FieldDataType.stringFormat;
                _FieldFormat["ExtensionDays"] = FieldDataType.intFormat;
                _FieldFormat["ExtensionPreTaxAmount"] = FieldDataType.decimalFormat;
                _FieldFormat["ExtensionSalesTax"] = FieldDataType.decimalFormat;
                _FieldFormat["ExtensionTotalAmount"] = FieldDataType.decimalFormat;
                _FieldFormat["IsExtensionApprovedByAdmin"] = FieldDataType.stringFormat;
                _FieldFormat["DateExtensionApprovedByAdmin"] = FieldDataType.stringFormat;

                _FieldFormat["NameofMembership"] = FieldDataType.stringFormat;
                _FieldFormat["DurationPeriod"] = FieldDataType.stringFormat;
                _FieldFormat["MembershipFee"] = FieldDataType.decimalFormat;

                _FieldFormat["PackageType"] = FieldDataType.stringFormat;
                _FieldFormat["PackageName"] = FieldDataType.stringFormat;
                _FieldFormat["PackageDescription"] = FieldDataType.stringFormat;
                _FieldFormat["DiscountAmount"] = FieldDataType.decimalFormat;
                _FieldFormat["Active"] = FieldDataType.stringFormat;
                _FieldFormat["CreateDate"] = FieldDataType.dateFormat;
                _FieldFormat["UpdateDate"] = FieldDataType.dateFormat;
                _FieldFormat["Quantity"] = FieldDataType.stringFormat;

                //_Orders["Price"] = "Price";
                _FieldFormat["Discount"] = FieldDataType.stringFormat;
                // _Orders["OverdueChargeString"] = "Overdue Charge";
                _FieldFormat["TotalRefundAmount"] = FieldDataType.decimalFormat;
                _FieldFormat["TotalAmount"] = FieldDataType.decimalFormat;
                _FieldFormat["PaidAmount"] = FieldDataType.decimalFormat;
                _FieldFormat["NetPayable"] = FieldDataType.decimalFormat;
                _FieldFormat["PaymentMode"] = FieldDataType.stringFormat;

                _FieldFormat["MedicalNumberOrNationalId"] = FieldDataType.stringFormat;
                _FieldFormat["CheckNumber"] = FieldDataType.stringFormat;
                _FieldFormat["PurchaseDate"] = FieldDataType.dateFormat;
                _FieldFormat["PaymentDate"] = FieldDataType.dateFormat;

                _FieldFormat["FirstName"] = FieldDataType.stringFormat;
                _FieldFormat["LastName"] = FieldDataType.stringFormat;
                _FieldFormat["Email"] = FieldDataType.stringFormat;
                _FieldFormat["DateOfBirth"] = FieldDataType.dateFormat;
                _FieldFormat["UserType"] = FieldDataType.stringFormat;
                _FieldFormat["Address1"] = FieldDataType.stringFormat;

                _FieldFormat["Ad1City"] = FieldDataType.stringFormat;
                _FieldFormat["Ad1State"] = FieldDataType.stringFormat;
                _FieldFormat["Ad1Zip"] = FieldDataType.stringFormat;

                _FieldFormat["Insurance"] = FieldDataType.stringFormat;
                _FieldFormat["Hospital"] = FieldDataType.stringFormat;
                _FieldFormat["HospitalPractice"] = FieldDataType.stringFormat;

                _FieldFormat["AreaCode"] = FieldDataType.stringFormat;
                _FieldFormat["phone"] = FieldDataType.stringFormat;
                _FieldFormat["phoneType"] = FieldDataType.stringFormat;
                _FieldFormat["Active"] = FieldDataType.stringFormat;
                _FieldFormat["LocationDetails"] = FieldDataType.stringFormat;
                _FieldFormat["InstructorName"] = FieldDataType.stringFormat;
                _FieldFormat["VendorName"] = FieldDataType.stringFormat;
                _FieldFormat["PaymentMode"] = FieldDataType.stringFormat;
                _FieldFormat["Amount"] = FieldDataType.decimalFormat;
                _FieldFormat["PaymentDate"] = FieldDataType.dateFormat;

                _FieldFormat["MedicalNumberOrNationalId"] = FieldDataType.stringFormat;
                _FieldFormat["PatientReceiptNumber"] = FieldDataType.stringFormat;
                _FieldFormat["CheckNumber"] = FieldDataType.stringFormat;
                _FieldFormat["FullName"] = FieldDataType.stringFormat;
                _FieldFormat["AddressLine1"] =  FieldDataType.stringFormat;
                _FieldFormat["AddressLine2"] = FieldDataType.stringFormat;
                _FieldFormat["City"] = FieldDataType.stringFormat;
                _FieldFormat["State"] = FieldDataType.stringFormat;
                _FieldFormat["Zip"] = FieldDataType.stringFormat;
                _FieldFormat["PhoneNo"] = FieldDataType.stringFormat;
                _FieldFormat["DiscountTypeName"] = FieldDataType.stringFormat;
                _FieldFormat["Description"] = FieldDataType.stringFormat;
                _FieldFormat["Amount"] = FieldDataType.decimalFormat;
                _FieldFormat["IsPercentageMode"] = FieldDataType.stringFormat;
                _FieldFormat["DiscountRatePerClass"] = FieldDataType.decimalFormat;
               // _FieldFormat["DisountType"] = FieldDataType.stringFormat;
                _FieldFormat["Percentage"] = FieldDataType.stringFormat;
                _FieldFormat["DisountType"] =  FieldDataType.stringFormat;
                _FieldFormat["DisountMode"] =  FieldDataType.stringFormat;
                _FieldFormat["DiscountValue"] = FieldDataType.decimalFormat;
                _FieldFormat["SalesTaxRate"] = FieldDataType.decimalFormat;
                _FieldFormat["Quantity"] = FieldDataType.intFormat;
                _FieldFormat["UnitPrice"] = FieldDataType.decimalFormat;
                _FieldFormat["Price"] = FieldDataType.decimalFormat;
                _FieldFormat["TaxAmount"] = FieldDataType.decimalFormat;
                _FieldFormat["ReturnDate"] = FieldDataType.dateFormat;
                _FieldFormat["RefundAmount"] = FieldDataType.decimalFormat;
                _FieldFormat["RefundedDate"] = FieldDataType.dateFormat;
                _FieldFormat["ShippingAddressId"] = FieldDataType.longFormat;
                _FieldFormat["SignupDate"] = FieldDataType.dateFormat;
                _FieldFormat["NumberAttending"] = FieldDataType.intFormat;
                _FieldFormat["Price"] = FieldDataType.decimalFormat;
                // _SectionOrder["Description"] = "Description";
                _FieldFormat["RefundAmount"] = FieldDataType.decimalFormat;
                _FieldFormat["DateClassCanceledByClient"] = FieldDataType.stringFormat;
                _FieldFormat["Price"] = FieldDataType.decimalFormat;
                _FieldFormat["Description"] = FieldDataType.stringFormat;
                _FieldFormat["DateJoined"] = FieldDataType.dateFormat;
                _FieldFormat["DateValidTill"] = FieldDataType.dateFormat;
                _FieldFormat["Quantity"] = FieldDataType.longFormat;
                return _FieldFormat;
            }
        }

        public Dictionary<string, string> GetAllAttributesByClassObjectName(List<string> tableNameList)
        {
            Dictionary<string, string> propertiesTable = new Dictionary<string, string>();
            foreach (string name in tableNameList.Distinct())
            {
                List<string> ValueList = Attributes[name].Values.ToList();
                List<string> KeyList = Attributes[name].Keys.ToList();
                if ((KeyList.Count == ValueList.Count) && (KeyList.Count > 0) && (ValueList.Count > 0))
                {
                    for (int i = 0; i < KeyList.Count; i++)
                    {
                        propertiesTable.Add(KeyList[i], ValueList[i]);
                    }
                }
            }
            return propertiesTable;
        }
    }
}
