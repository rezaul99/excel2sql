﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;
using daoProject.Infrastructure;
using daoProject.Service;

namespace daoProject.Model
{
    public class InstructorWHRC
    {
        public InstructorWHRC()
        {
            InstructorName = String.Empty;
        }

        public long RecordID { get; set; }
        public string InstructorName { get; set; }
    }
}
