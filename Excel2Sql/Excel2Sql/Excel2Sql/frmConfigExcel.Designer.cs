namespace Excel2Sql
{
    partial class frmConfigExcel
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnNext = new System.Windows.Forms.Button();
            this.txtboxPathClient = new System.Windows.Forms.TextBox();
            this.btnBrows = new System.Windows.Forms.Button();
            this.opnfildlExcel = new System.Windows.Forms.OpenFileDialog();
            this.btnExit = new System.Windows.Forms.Button();
            this.txtboxPathOrder = new System.Windows.Forms.TextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.txtboxPathCredit = new System.Windows.Forms.TextBox();
            this.txtboxPathMembership = new System.Windows.Forms.TextBox();
            this.txtboxPathClassSection = new System.Windows.Forms.TextBox();
            this.button2 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.txtboxPathClassSignup = new System.Windows.Forms.TextBox();
            this.txtboxPathPurchase = new System.Windows.Forms.TextBox();
            this.button5 = new System.Windows.Forms.Button();
            this.button6 = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.txtboxPathRental = new System.Windows.Forms.TextBox();
            this.button7 = new System.Windows.Forms.Button();
            this.label8 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // btnNext
            // 
            this.btnNext.Location = new System.Drawing.Point(12, 294);
            this.btnNext.Name = "btnNext";
            this.btnNext.Size = new System.Drawing.Size(75, 23);
            this.btnNext.TabIndex = 4;
            this.btnNext.Text = "Ok";
            this.btnNext.UseVisualStyleBackColor = true;
            this.btnNext.Click += new System.EventHandler(this.btnNext_Click);
            // 
            // txtboxPathClient
            // 
            this.txtboxPathClient.Location = new System.Drawing.Point(75, 23);
            this.txtboxPathClient.Name = "txtboxPathClient";
            this.txtboxPathClient.Size = new System.Drawing.Size(161, 20);
            this.txtboxPathClient.TabIndex = 15;
            // 
            // btnBrows
            // 
            this.btnBrows.Location = new System.Drawing.Point(242, 23);
            this.btnBrows.Name = "btnBrows";
            this.btnBrows.Size = new System.Drawing.Size(38, 20);
            this.btnBrows.TabIndex = 0;
            this.btnBrows.Text = ". . .";
            this.btnBrows.UseVisualStyleBackColor = true;
            this.btnBrows.Click += new System.EventHandler(this.btnBrowsClient_Click);
            // 
            // opnfildlExcel
            // 
            this.opnfildlExcel.FileOk += new System.ComponentModel.CancelEventHandler(this.opnfildlExcel_FileOk);
            // 
            // btnExit
            // 
            this.btnExit.Location = new System.Drawing.Point(180, 294);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(75, 23);
            this.btnExit.TabIndex = 5;
            this.btnExit.Text = "Exit";
            this.btnExit.UseVisualStyleBackColor = true;
            this.btnExit.Click += new System.EventHandler(this.btnExit_Click);
            // 
            // txtboxPathOrder
            // 
            this.txtboxPathOrder.Location = new System.Drawing.Point(75, 49);
            this.txtboxPathOrder.Name = "txtboxPathOrder";
            this.txtboxPathOrder.Size = new System.Drawing.Size(161, 20);
            this.txtboxPathOrder.TabIndex = 16;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(242, 48);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(38, 20);
            this.button1.TabIndex = 17;
            this.button1.Text = ". . .";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.btnBrowsOrder_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(10, 30);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(32, 13);
            this.label1.TabIndex = 18;
            this.label1.Text = "client";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(9, 55);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(30, 13);
            this.label2.TabIndex = 19;
            this.label2.Text = "debit";
            // 
            // txtboxPathCredit
            // 
            this.txtboxPathCredit.Location = new System.Drawing.Point(75, 75);
            this.txtboxPathCredit.Name = "txtboxPathCredit";
            this.txtboxPathCredit.Size = new System.Drawing.Size(161, 20);
            this.txtboxPathCredit.TabIndex = 20;
            // 
            // txtboxPathMembership
            // 
            this.txtboxPathMembership.Location = new System.Drawing.Point(75, 101);
            this.txtboxPathMembership.Name = "txtboxPathMembership";
            this.txtboxPathMembership.Size = new System.Drawing.Size(161, 20);
            this.txtboxPathMembership.TabIndex = 21;
            // 
            // txtboxPathClassSection
            // 
            this.txtboxPathClassSection.Location = new System.Drawing.Point(75, 127);
            this.txtboxPathClassSection.Name = "txtboxPathClassSection";
            this.txtboxPathClassSection.Size = new System.Drawing.Size(161, 20);
            this.txtboxPathClassSection.TabIndex = 22;
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(242, 75);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(38, 20);
            this.button2.TabIndex = 23;
            this.button2.Text = ". . .";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.btnBrowsCredit_Click);
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(242, 100);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(38, 20);
            this.button3.TabIndex = 24;
            this.button3.Text = ". . .";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.btnBrowsMember_Click);
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(242, 127);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(38, 20);
            this.button4.TabIndex = 25;
            this.button4.Text = ". . .";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.btnBrowsClassSection_Click);
            // 
            // txtboxPathClassSignup
            // 
            this.txtboxPathClassSignup.Location = new System.Drawing.Point(75, 153);
            this.txtboxPathClassSignup.Name = "txtboxPathClassSignup";
            this.txtboxPathClassSignup.Size = new System.Drawing.Size(161, 20);
            this.txtboxPathClassSignup.TabIndex = 26;
            // 
            // txtboxPathPurchase
            // 
            this.txtboxPathPurchase.Location = new System.Drawing.Point(75, 179);
            this.txtboxPathPurchase.Name = "txtboxPathPurchase";
            this.txtboxPathPurchase.Size = new System.Drawing.Size(161, 20);
            this.txtboxPathPurchase.TabIndex = 27;
            // 
            // button5
            // 
            this.button5.Location = new System.Drawing.Point(242, 152);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(38, 20);
            this.button5.TabIndex = 28;
            this.button5.Text = ". . .";
            this.button5.UseVisualStyleBackColor = true;
            this.button5.Click += new System.EventHandler(this.btnBrowsClassSignUp_Click);
            // 
            // button6
            // 
            this.button6.Location = new System.Drawing.Point(242, 178);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(38, 20);
            this.button6.TabIndex = 29;
            this.button6.Text = ". . .";
            this.button6.UseVisualStyleBackColor = true;
            this.button6.Click += new System.EventHandler(this.btnBrowsPurchase_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(9, 82);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(33, 13);
            this.label3.TabIndex = 30;
            this.label3.Text = "credit";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(12, 108);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(63, 13);
            this.label4.TabIndex = 31;
            this.label4.Text = "membership";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(12, 131);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(67, 13);
            this.label5.TabIndex = 32;
            this.label5.Text = "classSection";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(12, 160);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(64, 13);
            this.label6.TabIndex = 33;
            this.label6.Text = "classSignup";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(12, 185);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(51, 13);
            this.label7.TabIndex = 34;
            this.label7.Text = "purchase";
            // 
            // txtboxPathRental
            // 
            this.txtboxPathRental.Location = new System.Drawing.Point(75, 205);
            this.txtboxPathRental.Name = "txtboxPathRental";
            this.txtboxPathRental.Size = new System.Drawing.Size(161, 20);
            this.txtboxPathRental.TabIndex = 35;
            // 
            // button7
            // 
            this.button7.Location = new System.Drawing.Point(242, 204);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(38, 20);
            this.button7.TabIndex = 36;
            this.button7.Text = ". . .";
            this.button7.UseVisualStyleBackColor = true;
            this.button7.Click += new System.EventHandler(this.btnBrowsRental_Click);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(12, 212);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(33, 13);
            this.label8.TabIndex = 37;
            this.label8.Text = "rental";
            // 
            // frmConfigExcel
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(309, 329);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.button7);
            this.Controls.Add(this.txtboxPathRental);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.button6);
            this.Controls.Add(this.button5);
            this.Controls.Add(this.txtboxPathPurchase);
            this.Controls.Add(this.txtboxPathClassSignup);
            this.Controls.Add(this.button4);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.txtboxPathClassSection);
            this.Controls.Add(this.txtboxPathMembership);
            this.Controls.Add(this.txtboxPathCredit);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.txtboxPathOrder);
            this.Controls.Add(this.btnExit);
            this.Controls.Add(this.btnBrows);
            this.Controls.Add(this.txtboxPathClient);
            this.Controls.Add(this.btnNext);
            this.Name = "frmConfigExcel";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Config Excel";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnNext;
        private System.Windows.Forms.TextBox txtboxPathClient;
        private System.Windows.Forms.Button btnBrows;
        private System.Windows.Forms.OpenFileDialog opnfildlExcel;
        private System.Windows.Forms.Button btnExit;
        private System.Windows.Forms.TextBox txtboxPathOrder;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtboxPathCredit;
        private System.Windows.Forms.TextBox txtboxPathMembership;
        private System.Windows.Forms.TextBox txtboxPathClassSection;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.TextBox txtboxPathClassSignup;
        private System.Windows.Forms.TextBox txtboxPathPurchase;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txtboxPathRental;
        private System.Windows.Forms.Button button7;
        private System.Windows.Forms.Label label8;
    }
}

