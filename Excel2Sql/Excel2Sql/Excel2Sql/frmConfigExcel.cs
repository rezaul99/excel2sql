using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using daoProject.Service;

namespace Excel2Sql
{
    public partial class frmConfigExcel : Form
    {
        private string SheetNameClient;
        private string SheetNameDebit;
        private string SheetNameCredit;
        private string SheetNameMembership;
        private string SheetNameClassSection;
        private string SheetNameClassSignUp;

        public frmConfigExcel()
        {
            InitializeComponent();
        }       

        private void opnfildlExcel_FileOk(object sender, CancelEventArgs e)
        {
            String str = opnfildlExcel.SafeFileName.Replace(".xlsx", "");
            if (str == "tblClient")
            {
                txtboxPathClient.Text = opnfildlExcel.FileName;
                SheetNameClient = opnfildlExcel.SafeFileName.Replace(".xlsx", "");
            }
            else if (str == "tblDebit")
            {
                txtboxPathOrder.Text = opnfildlExcel.FileName;
                SheetNameDebit = opnfildlExcel.SafeFileName.Replace(".xlsx", "");
            }
            else if (str == "tblCredit")
            {
                txtboxPathCredit.Text = opnfildlExcel.FileName;
                SheetNameCredit = opnfildlExcel.SafeFileName.Replace(".xlsx", "");
            }
            else if (str == "tblMembership")
            {
                txtboxPathMembership.Text = opnfildlExcel.FileName;
                SheetNameMembership = opnfildlExcel.SafeFileName.Replace(".xlsx", "");
            }
            else
            {
                txtboxPathClient.Text = opnfildlExcel.FileName;
                SheetNameClient = opnfildlExcel.SafeFileName.Replace(".xlsx", "");
            }
            //SheetName = opnfildlExcel.SafeFileName.Replace(".csv", "");
        }

        private void btnNext_Click(object sender, EventArgs e)
        {
            ExcelToSqlServiceImpl eToSql = new ExcelToSqlServiceImpl();
            try
            {
                eToSql.InsertToDb(txtboxPathClient.Text, SheetNameClient);
                
                //eToSql.InsertToDb(txtboxPathClient.Text, SheetNameClient, txtboxPathOrder.Text, SheetNameDebit, txtboxPathCredit.Text, SheetNameCredit, txtboxPathMembership.Text, SheetNameMembership);
              //  DataTable DT = eToSql.ExcelToSqlConversion(txtboxPath.Text, SheetName);                
            }
            catch
            {                
            }

            //frmConfigSql frmSql = new frmConfigSql(txtboxPath.Text, SheetName, txtboxSourceCell.Text, txtboxDestinationCell.Text);
            //frmSql.ShowDialog();

        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnBrowsClient_Click(object sender, EventArgs e)
        {
            opnfildlExcel.ShowDialog();
        }

        private void btnBrowsOrder_Click(object sender, EventArgs e)
        {
            opnfildlExcel.ShowDialog();
        }

        private void btnBrowsCredit_Click(object sender, EventArgs e)
        {
            opnfildlExcel.ShowDialog();
        }

        private void btnBrowsMember_Click(object sender, EventArgs e)
        {
            opnfildlExcel.ShowDialog();
        }

        private void btnBrowsClassSection_Click(object sender, EventArgs e)
        {
            opnfildlExcel.ShowDialog();
        }

        private void btnBrowsClassSignUp_Click(object sender, EventArgs e)
        {
            opnfildlExcel.ShowDialog();
        }

        private void btnBrowsPurchase_Click(object sender, EventArgs e)
        {
            opnfildlExcel.ShowDialog();
        }

        private void btnBrowsRental_Click(object sender, EventArgs e)
        {
            opnfildlExcel.ShowDialog();
        }
    }
}